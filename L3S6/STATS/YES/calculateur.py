import math

# INPUTS #
r = float(input("\nr(x,y)  ? " ))
scy = float(input("s²c(y) VARIANCE ?  "))
scx = float(input("s²c(x) VARIANCE ?  "))
xbarre = float(input("x̄ MOYENNE ?  " ))
ybarre = float(input("ỹ MOYENNE ?  " ))
n = float(input("n ?  "))


# OPERATIONS
h = r*((math.sqrt(scy))/(math.sqrt(scx)))
a = ybarre - (h*xbarre)
s2x = scx*((n-1)/n)

# OUTPUT
print('\n' + '*'*20)
print(f"{'':>3}s²(x) = {s2x:.6f}")
print(f"{'':>3}â = {a:^10.6f}")
print(f"{'':>3}ĥ = {h:^10.6f}")
print('*'*20 + '\n')
aaaa = input('Appuyer sur entrée pour continuer')
print("\n\n")

# INPUT & OUTPUT
reg = input('Calculer un résidu de régression ? OoYy/Nn  ')
reg = reg.lower()
if reg == 'o' or reg == 'y' or reg == 'oui' or reg == 'yes':
    valeurx = float(input("Valeur de x pour le résidu ? "))
    valeurtab = float(input("Valeur du prélèvement dans l'échantillon :  "))
    residuregression = valeurtab - (a+h*valeurx)
    print(f"{'':>3}résidu = {residuregression:.6f}")
    aaaa = input('\nAppuyer sur entrée pour continuer')

# INPUT
y = float(input("Valeur y(?) ? " ))
c = float(input(f"Valeur critique table Student à {n-2:.0f} ddl ? ATTENTION SEUIL  " ))


# OPERATIONS
s2B = ((1-(r**2))*scy)/((n-2)*scx)
s2L = s2B*(s2x+((y-xbarre)**2))
ybiz = a + y*h
IClow = ybiz-(c*math.sqrt(s2L))
IChigh = ybiz+(c*math.sqrt(s2L))

# OUTPUT
print('\n' + '*'*40)
print(f"{'':>3}sB² = {s2B}")
print(f"{'':>3}sL² = {s2L:.6f}")
print(f"\n{'':>3}ỹ({y}) = {ybiz}")
print(f"{'':>3}IC : {IClow:.6f} - y({y:.0f}) - {IChigh:.6f}")
print('*'*40 + '\n')
aaaa = input('Appuyer sur entrée pour continuer')
