# Les récepteurs nucléaires

## Domaine C : site de fixation de l'ADN.

* Région qui comportent une région non structurées et qui vont se structurer en présence de l'ADN.
* 2 doigts de zinc : l'un sert à la fixation à l'ADN, l'autre à la formation du dimère

### Reconnaissance :

* Reconnait 2 demi sites et espacement entre les demi sites
* Spécificité de reconnaissance? Au delà de cette spécificité on peut aussi parler de cinétique
* Séquences palindromes / répétitions directes / anti palindromes

* Complexe homodimère sur palindrome

## Domaine E 

* Structure canonique qui est spécifique (les récepeurs nucléaires sont les seuls à adopter cette conformation), structure qui es remodelée avec le ligand

* Hélice H12

* Position de l'hélice H12 en présence d'un antagoniste empêche la fixation du co activateur ; H12 : position Agoniste et Antagoniste

* Hélice H12 prend la place du peptide co activateur en présence d'un antagoniste

* 2 classes : basés sur deux types de ponts salins (hélice H1 et H8) et quand il y avait ce premier pont salin il y avait aussi une deuxième classe qui avait cces deux
    * classe 1 : homodimères
    * classe 2 : hétérodimères
    * 4 AA
    
## Les différents états de récepteurs nucléaires

* Actif : lorsque le ligand est lié et il y a une intéraction avec le co activateur.
* Très grande variété de ligands, des molécules controlées par un ligand, et régulé par un certains nombres de régulations
* cibles privélégiés ; on essaie de définir des ligands des récépteurs nucléaires pour agir sur une maladie
* A partir d'une première structure, à partir du premier alignement, va permettre de prédire ce qui se passe dans la poche de fixation. 

## Récépteur de la vitamine D (un exemple d'ingénierie des protéines)

* Ne cristallisait pas
* grande boucle (peut etre qu'il empeche les contacts au niveau du cristal), on a donc enlever cette boucle, (protéine chimérique), rapidement cristallisé, et dizaine de structures, et un certain nombre de médicaments y correspondent, on peut enlever des parties flexibles qui peuvent géner le cristallisation. Cela montre que un multialignement va aussi permettre à définir, ce qu'il faut que je clone, qu'est ce que je peux enlever, pour pouvoir cristalliser la protéine, et comment prédire que cette boucle ne sert à rien?
