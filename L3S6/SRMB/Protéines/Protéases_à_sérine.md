# Reconnaissance macromoléculaire
## Protéases à sérine
### Mécanisme catalitique des protéases à sérine
* Triade catalytique Hisp Asp Ser
    * **2 réactions donc deux états de transition**?, il faut que la protéine fournisse des composés pour l'état de transition. Pas de reconnaissance spécifique des chaînes latérales, coupe souvent après arginine, donc la chaîne primaire a peu d'importance. Sérine (groupement OH agit comme nucléophile), NH de l'imidazole de l'histidine agit comme éléctrophile (donneur de protons, et accepteur dans un deuxième temps). La deuxième étape fait intervenir une molécule d'eau.
* Le résidu situé avant le site à couper est appelé **la poche de spécificité**
* deuxième étape : une molécule d'eau intervient : Nomenclature P1 et P'1 (côté amino terminal du peptide qui est coupé) -> juste après la liaison peptidique, unité S1 de la protéase
* protonation du Nd1 plutôt que du Ne2
* Sérine/cyst/thréonine => formation d'un intermédiaire covalent, mais pas dans le cas des metallo protéases
* Trou de l'oxyanion : la chymotrypsine contient un trou d'oxyanion pour stabiliser l' intermédiaire tétraédrique anion formé au cours de la protéolyse et protège le substrat de l' oxygène chargé négativement

### Les 2 familles : 2 repliements 
* Même environnement chimique du site actif 
#### Protéases à sérine type Trypsine
* En rouge : porte les AA importants 
* 240 AA
* Superfamille = Superfonction
* AA dans la poche qui participent à la stabilisation de l'état de transition
* Asp102, His57, Ser195
#### Protéases à sérine de type subtilisine
* 275 AA
* Pas d'homologie de séquences, AA qui ne sont pas de le même ordre en comparaison avec les protéases à sérine type trypsine, feuillet =/ tonneaux anti parallèles
* Site actif : carboXy terminal, car boucle se forme donc forme une crevasse.
* Connexion gauche apporte l'histidine du site actif, la connexion n'est pas droite (sinon elle serait à l'extérieur du site actif)
* Asp32, His64, Ser221
## Récépeurs nucléaires
* Domaine E : domaine qui fixe le ligand 
* LBD : Ligand Biding Domain