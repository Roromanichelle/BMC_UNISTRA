# Mise en place de la maladie, résistance constitutive et résistance induite non spécifque

## Stress 
* Situation qui altère sa croissance soit son développement soit sa reproduction ou tout à la fois
* stress abiotique : situations contraignantes d'origine environnemental )
* stress biotique : stress engendré par un autre être vivant ou pression parasitaire
    * Si une plante est soumise en stress biotique alors elle est en situation de maladie donc si sa croissance est altérée.
    * une plante "malade" est une plante sensible : pas capable de mettre en place un mécanisme de défense

## Agents pathogènes (phytophatogènes)
* Spectre d'hôte : on entend l'ensemble des organismes hôtes qui peuvent être infecté par un agent pathogène donné.
    * Peut être variable chez les agents phytopathogènes : certains virus peuvent infecter des centaines de plante : c'est le cas de la famille de la mosaique du tabac qui infecte les solanacés.
    * Certains peuvent être réduits à une seule plante : infecte la vigne **lequel?**
* Caractérisé par la pathogénicité (pouvoir pathogène)
    * caractères définis génétiquement
    * Virulence = agent pathogène
    * Avirulent = agent non pathogène
* Étudié dans le contexte de la phytopathologie : causes conséquences et préventions des attaques par les agents pathogène.
    * S'intéresse des agents pathogènes de type microorganisme
    * On ne s'intéresse pas aux herbivores aux insectes ou aux plantes parasites
    * la phytopathologie = équivalent de la médecine humaine chez les plantes
* Les plantes possèdent un système immunitaire : ensemble des mécanismes qui leur permettent de se défendre. Cette immunité est innée =/ des animaux qui un système inné et adaptatif : propre à un individu, une plante ne sera pas capable d'acquérir au cours de sa vie une nouvelle résistance. Cette immunité innée est génétiquement donnée.
    * La capacité des plantes à acquérir un mécanisme des plantes se fait uniquement aux descendances suivantes.
        * pression parasitaire et co évolution des plantes avec les agents pathogènes.
        * Plante résistante : peut se défendre vis à vis de l'agent pathogène
* Pathosystème :
    * On parle d'une plante donné et d'un agent pathogène.
        * Si on change la souche de l'agent pathogène : la réponse et la sensibilité chez la plante peut être différente 
* Les agents phytopathogènes sont regroupés en 5 classes de MO :
    * Les plus gros ce sont les nématodes
    * Il existe uniquement une espèce de nématode pathogène, la plupart vivent dans le sol et sont ceux qui embêtent les plantes maladie **télurique**
    * Champignon (eumycètes) et oomycètes deux grands taxons différents on a donné à cette double catégorie, le terme de maladie cryptogamique, sont dûs à des organismes parasitaires filamenteux. considérables car 90 % des maladies que l'on observe chez les plantes. 
    * Bactéries : pseudomonas aussi des bactéries pseudomonas qui sont bénéfiques pour les plantes
    * Virus : virus de la mosaïque du tabac (entraine un stress biotique) même mécanismes qui vont être mis en place

* Symptômes des maladie chez les plantes
    * Dans le cas d'un champignon : ergot du seigle 
        * Ces excroissance au ni veau de l'épis sont des sclérotes qui si on utilise les grains pour faire de la farine : peut causer des maladies chez l'homme, très important au moyen âge : cause l'ergotisme (encore présent en Afrique)
        * Poudre blanche sur les feuilles oïdium chez les plantes ; peut prendre un autre aspect
        * Botrytis cinera : en fin de saison en automne on peut avoir btrytis qui vont être présent sur les grains de raisin et peut être utilisée.
    * Nématode à galles 
        * excroissance des racines de la laitue, le nématode va s'installer au niveau de ses racnes, tous les nutriments de la plante vont être détournés au détriment de la plante
    * chancre bactérien chez les citronniers : meme si les fruits restent mangeables : entraine des pertes
        * pythium nécrotrophe
    * Virus du court noué 
        * gauffrement des feuilles et ralentissement de la croissance. 
* La plupart des plantes sont en bonne santé, elles ont un système immunitaire efficace

## Contexte historique
* Depuis que l'homme cultive les plantes il a observé que les plantes pouvaient être attaquées par les agents pathogènes.
    * Souvent attribués à des causes climatiques
    * Causes religieuses
        * Divinités pour protéger les plantes
            * Robigus
        * Ce mode de lutte contre les maladies a persister car les églises faisaient des célébrations à travers les champs pour protéger les cultures
        * L'ère des MO est apparu, et on a commencé à observer la cause
            * Les nématodes ont été observés au 18 ème siècle, d'abord au niveau des graines puis des racines
            * Champignons au 19 ème siècle : découverte des champignons
            * Fin du 19 ème siècle : mildiou le pomme de terre oomycète : a duré plusieurs années et a causé de grandes famines. 
                * A fait avancé la science
                * La plante était malade car la pourriture poussait sur la plante : agent causal pour que la maladie se développe
                * Antoine de Diarry (professeur de botanique à l'université de Strasbourg, père de la phytopathologie) a observé des spores de champignons qui peuvent germer et des hyphes peuvent se développer entre les cellules. Se montre plus en tête mais fin du 19 ème siècle, louis Pasteur éméte la théorie microbienne et on découvre en même temps que les bactéries sont également capables de développer les maladies chez les plantes  (arbres fruitiers)
                * PLus tardivement on émet l'hypothèse des virus, particules/molécules que l'on injecte
                * 20 ème siècle : accélération car on connait les agents responsables des maladies et on va essayer de lutter contre ces agents.

## Mise en plase de la maladie (ETS)
* Trois critéres pour le développement de la maladie
    * Plante sensible : affaiblie par exemple, trop jeune ou trop vieille, facteurs génétiques car certaines plantes offrent des conditions favorables, adaptés au développement d'agent pathogène = facteurs génétiques
    * Le pathogène doit être virulent : souvent rarement seul : une abondance de spore / de bactérie 
    * Environnement qui doit être favorable aux conditions de vie de l'agent pathogène
        * Aspect négligé, rarement pris dans le contexte environnemental
        * Période de pluie / température / vents typhons tornades qui apportent humidité et endommagent la plante donc pics d'infection au niveau des cultures
    * Homme responsable 
        * Par la dispersion des agents pathogènes
        * Favorise les maladies en monocultures car si on a une attaque d'un agents sur une plante, se généralise aux centaines d'hectar
        * D'autres pratiques : utilisation de pesticides engrais affaiblissent les plantes
        * Irrigation : si mal gérée peut favoriser les maladies

* Le pathogène soit s'y fixer : pour cela de nombreux pathogènes produisent des polyssacharides, ou encore des biofilms chez les bactéries, qui va permettre d'adhérer à la surface végétale. Les bactéries p.e agrobacterium tumefasians, possèdent des pilus, structures qui permettent à la bactérie de s'accrocher à la cellule végétale, pilus : sert de tunnel pour insérer des **: à ne pas confondre avec les flagelles qui ne servent qu'à se déplacer. Le fait d'empêcher la fixation permet d'empêcher l'infection. Pulvériser la plante avec des molécules qui empêchent l'adhésion. Les défenses passives vont être franchies. --> doit ensuite franchier les défenses actives

* Étape de pénétration : différentes stratégies
    * Ouvertures naturelles (stomates) ; lenceille
    * Blessures
    * mécanique : grâce à différents structures : appressorium tube germinatif qui se développe sur les feuilles par des mécanismes de pression de turgescence va perser la paroi et la membrane plasmique. (oomycètes et champignon)
        * Nématode : utilise le style : structure mobile au niveau de la bouche du nématode. Ce stylet va servir au nématode à se nourrir en creusant dans le tissus végétal. Il va aussi pouvoir injecter des effecteurs des facteurs de virulence.
    * chimique : l'agent pathogène va produire des enzymes qui vont digérer la paroi, la cuticule, qui vont affaiblir la cellule végétale
    * Les virus vont être opportunistes : les blessures ou apporter pas des vecteurs = nématodes / insectes
    
* Une fois que l'agent pathogène est entré dans l'hôte on distingue 3 catégories d'infection
    * biotrophe : 
        * complexe mais la plus spécialisé : l'agent pathogène ne va pas se faire détécter rapidement la plante, il veut l'exploiter mais durablement : nécéssite des intéractions complexes entre unne plante jeune ou en bonne santé : but maintenir les tissus vivants de la plante don stratégie peu agressive. contacts intercellulaires : échanges entre l'hôte végétal et l'agent microbien --> issus d'un co évolution donc à spectre d'hôte réduit
        * souvent parasite obligatoire : les agents pathogènes ont perdus certaines capacités, ils ne peuvent pas se reproduire sans hôte(virus et nématodes).
        * On retrouve des bactéries des champignons et des oomycètes
    * nécrotrophe : Tuer/ digérer ou faire macérer très rapidement les tissus végétaux pour s'en nourir le plus rapidement possible. Pas d'intéractions fines ; ces nécrotrophes sont très souvent opportunistes, plantes souvent sénescentes ou des plantes très jeunes qui n'ont pas encore des mécanismes de défenses. Nécrotrophe on retrouvera des bactéries des champignons et des oomycètes
    * hémibiotrophes : l'agent pathogène établit une intéraction biotrophe : exploite la plante au maximum, lorsque la plante va bientôt mourrir : profit de se nourir de la plante en achevant la plante avec des enzymes. 

* Modèle végétale : arabisopsis thaliana : petites plantes / nombre de gènes ? ; cycle de vie court,
    * Autre avantage : on peut la transformer : en surexprimant 
    * génétique inverse : on a un gène d'intérêt qu'on le connait : on le surexprime dans des tissus ou il ne s'exprime pas ou à l'échelle de l'organisme entier : ou on inactive le gène --> facteurs qui viennent interrompre la transcription. on peut alors en déduire la fonction des gènes.
    * Beaucoup des connaissances ont été obtenues à partir A.thaliana

* Les agents pathogènes sont capables d'infecter les plantes mais la majorité des plantes est résistante à la majorité des MO 
* Cette résistance existe en réponse ou pas de l'agent pathogène. Dans tous les cas la résistance s'exprime

##  Les défenses constitutives (passives) :
* Barrière physique : 
    * renforcée par la lignine ; les polysacharrides
    * Paroi dotée d'une cutilcule plus ou moins hydrophobe : cette hydrophobicité va suffir pour empêcher lafixation des agents pathogènes
    * La forme des ouvertures naturelles, certaines espèces ont modifié la structure de leurs stomates (lesstomates repliement de l'épiderme p.e : le stomate est caché), d'autres espèces vont en plus mettre à l'entréedes poils (donc camouflé et protégé par des poils), épines : barrière physique plutôt pour les gros prédacteurs 
* Barrière chimiques :
    * composés produits en permanence par la plante qui ont des composés
        * Inhibiteur de protéases : pour les pathogènes
        * phytoanticipines : métabolites spécialisés (métabolites secondaires) : propriétés antimicrobiennes
            * Composés phénoliques
            * glycosinolates : sont phytoanticipines --> chez les brassicacées (cresson moutarde chou) (amer)
            * Ce sont des métabolites spécialisés produits de façon constitutives mais se comportent comme desphytoalexines : métabolites spécialisé produits en réponses par une attaque pathogène
            * Ces glycosinolates sont stockés au niveau des vacuoles des cellules végétales
            * Enzyme : la *myrosinases* , les glycosinolates sont hydrolysés par les myrosinases --> produisent
            * Elles ne sont hydrolisés que lorsqu'elles sont en contact avec des pathogènes donc se comportentcomme des phytoalexines mais sont des phytoanticipines car toujours produits de façon constitutives.
* Cette résistance passive : 90 % de la résistance
    
## Défenses actives : non spécifique PRI  
* Dans tous les cas de résistances actives il y a une étape de détection de l'agent pathogène pour que les mécanismes des plantes se mettent en place quelque soit la résistance induite. 
* Reconnaissance induit une cascade de signalisation qui va mettre en place une réponse immunitaire

### La PTI: résistance induite non spécifique 
* Reconnaissent des élliciteurs généraux, qui vont être détectés par la plante pour mettre en place la résistance.
    * Tous les composés capables d'induire des réponses de défenses
    * Ici contre un large spectre de pathogènes
    * Élliciteurs exogènes : produits par les pathogènes : PAMP ou MAMP (pathogen or microb)
        * Ces élliciteurs peuvent être produits par des microorganismes qui ne sont pas pathogènes mais bénéfiques pour la plante dans ce cas : microb
        * Pattern : il faut combrendre motif
        * Ce P est soit le patern triggered immunity : mortif qui entraine la résistance ou PAMP qui entraine l'immunité.
    * Il existe des elliciteurs endogène :
        * DAMP domage associated : molécules produites par dérivés de la paroi végétale, petits peptides qui sont formés que lors d'une attaque pathogène par la plante elle même et la plante va détécter ces DAMP pour activer ces mécanismes de défense

#### Caractéristiques des éliciteurs généraux :
* Commun à des microorganismes pathogènes ou non, ces élliciteurs généraux assurent des fonctions vitales chez les  microorganismes du métabolismes primaires des bactéries ou des champignons.
* Catégories moléculaires trés variées.
* Il vont présenter dans tous les cas un motif structural très conservés, que les récépteurs de les plantes vont être capables de reconnaître.
* Exemple :
    * Flagellines : composé principal des flagelles, ici le motif structural minimal de la flagelline 22 AA est appelé? ****
    * flg 22 et elf18 : facteur d'élongation assure la traduction des ARNm en protéines: le motif minimal elf18
    * Ces PAMP sont très utilisés car il suffit de faire des solutions et possibilité de mimer des réactions pathogènes en laboratoire.
    * Peptidoglycanes : dans la paroi des bactéries
    * CHITINE 
    * B - GLUCANE
* Tous présentent des motifs reconnus par les récépteurs des plantes

#### Les PRR
* Motifs conservés reconnus par une classe de protéines : par les PRR --> récépteurs qui reconnaissent les motifs.
* Tous localisés au niveau de la MP avec un domaine extra C et un domaine intra C. domaine extra : reconnait l'elliciteur et intra C induit la cascade de signalisation.

* RLK : intéractions prot prot
    * On observe des PRR capables de reconnaître des eliciteurs généraux bactériens
* RLP : doméine extracellulaire pour reconnaitre le PAMP à l'extérieur de la cellule, mol intracellulaire qui prend le relais pour induire la cascade.
* Les récepteurs PRR correspondant sont reconnus
    * FLS2 : très largement représent"s (monocotylédones et dicotylédones)
    * EFR : brasicacées
* Contournement (ETS)
    * Les pathogènes pour contrer cette PTI 
    * ETS : plante sensible (la maladie se développe)

#### Caractéristiques des effecteurs 
* Effecteurs spécifiques d'un groupe donné, ou d'un agent pathogène.
* Petits motifs 4-5 A aminés repérés au sein de certains récépteurs "core effectors" que l'on peut cibler. 
* Ces effecteurs sont soumis à une évolution rapide 
    * PAM soumis à des pressions de conservation ici soumis à une pression d'évolution rapide
* La majorité des effecteurs vont être injectés dans la cellule végétale, suivant l'agent pathogène il y a différents systèmes.
    * Bactérie phytopathogènes : système de sécrétion (de type 3) fonctionne comme une seringue moléculaire. Injection des récépteurs qui vont se positionner à la periphérie de la cellule
        * Système de type 4 (T pilus chez agrobacterium)
        * Chez des agents pathogènes bactériens le simple fait de supprimer ce systeme de sécrétion peut les rendre non pathogènes.
    * (Agents pathogènes biotrophes : champignons/ oomycète) Eustorium (après germination du spore), excroissance qui s'intercalle entre la paroi végétale et la membrane plasmique, il forme également une matrice extra eustoriale entre la membrane plasmique et l'eustorium, cette matrice va etre un lieu d'échanges intenses : les effecteurs vont pouvoir pénétrer à l'intérieur de la cellule

    * (Nématodes) Stylet : lui permet aussi de se nourrir --> injection grâce aux glandes de sécrétion