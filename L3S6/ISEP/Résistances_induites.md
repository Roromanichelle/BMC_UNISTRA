# Deuxième niveau de résistances induite et résistance non hôte

## ETS : mise en place de la maladie 
### Caractéristiques des effecteurs 
* Effecteurs spécifiques d'un groupe donné, ou d'un agent pathogène.
* Petits motifs 4-5 A aminés repérés au sein de certains récépteurs "core effectors" que l'on peut cibler. 
* Ces effecteurs sont soumis à une évolution rapide 
    * PAM soumis à des pressions de conservation ici soumis à une pression d'évolution rapide
* La majorité des effecteurs vont être injectés dans la cellule végétale, suivant l'agent pathogène il y a différents systèmes.
    * Bactérie phytopathogènes : système de sécrétion (de type 3) fonctionne comme une seringue moléculaire. Injection des récépteurs qui vont se positionner à la periphérie de la cellule
        * Système de type 4 (T pilus chez agrobacterium)
        * Chez des agents pathogènes bactériens le simple fait de supprimer ce systeme de sécrétion peut les rendre non pathogènes.
    * (Agents pathogènes biotrophes : champignons/ oomycète) Eustorium (après germination du spore), excroissance qui s'intercalle entre la paroi végétale et la membrane plasmique, il forme également une matrice extra eustoriale entre la membrane plasmique et l'eustorium, cette matrice va etre un lieu d'échanges intenses : les effecteurs vont pouvoir pénétrer à l'intérieur de la cellule

    * (Nématodes) Stylet : lui permet aussi de se nourrir --> injection grâce aux glandes de sécrétion

### Stratégies mises en place par les pathogènes
* Répréssion de la transcription des gènes de défense
* Mécanismes de silencing de la plante hôte
    * Certains agents pathogènes peuvent entrainer chez la plante un mécanisme de VIGS (virus induce gene silencing) mécanisme de défense qui cible les ARNm viraux pour qu'ils soient dégrader. Pour contrer ce VIGS les virus développent des molécules qui contrent cette machinerie de VIGS chez la plante. 
    * Lors d'infections bactériennes on peut avoir des miRNA associé aux mécanismes de défense, les bactéries sont capables de produire des BSR, bacterial suppressor of RNA silencing

* On peut observer des effecteurs qui vont avoir des activités post traductionnelles
    * Domaines E3 ligase : vont avoir la capacité à cibler des protéines pour qu'elles soient poly ubiquitinées puis envoyées au protéasome pour qu'elles soient dégradées
    * protéases  : dégrade d'autres protéines
    * Inhibiteur de protése ou de liaiasons à la chitine : effecteurs apoplastique --> agissent au niveau de l'apoplaste à l'extérieur de la cellules.

* Cascade de signalisation
    * Empeche les phosphorylations (portiénes kinases)
    * 3 grandes hormones dans la signalisation : La bactérie est capable de synthétiser la coronatine qui est capable de bloquer le récepteur de l'acide jasmonique --> bloque la cascade de signalisation de l'acide jasmonique

**Ces effecteurs vont être reconnus par une autre catégorie immunitaire par la plante**

## ETI : deuxième niveau de résistance induite
* Les molécules détéctées sont les effecteurs.
* Les protéines R
* Cet ETI commence par une étape de reconnaissance.
* Les effecteurs sont injectées dans la cellule végétale donc on retrouve les protéines R principalement au niveau intracellulaire.
### Caractéristiques
* Agent pathogène nécrotrophe
* Pression évolutive importante pour pouvoir générer d'une descendance à l'autre une nouvelle résistance
* Protéine R : reconnaissance spécifique d'un agent pathogène donné par une plante donnée
* Motifs structuraux : motifs conservés
* Localisation de nombreuses séquences homologues physiquement proches au niveau du génome (cluster de résistance) qui regroupent beaucoup de gènes impliqués dans les mécanismes de résistance chez les plantes.
* Dans ces gènes on observe une classe majoritaire : NBS (nucleotid binding site) et LRR (leucine-ric repeat) impliqué dans les intéractions prot prot).
* Ces protéines sont toujours associés à TIR (interleukine receptor) ou CC (coiled cole)
* NLS : domaine de localisation au noyau, domaine WRKY (famille de facteurs de transcription)
--> Protéine Pto = protéine kinase
* Quelques effecteurs apoplastiques : donc quelques protéines R dirigées vers l'extérieur de la protéine

### Sur quoi repose l'ETI ?
* Cette ETI reposait sur cette notion de reconnaissance gène à gène ; idée : si la plante hote est resistante elle est capable de reconnaitre l'effecteur chez l'agent pathogène, et on est en situation incompatible (résistance de type ETI). 
* Dans d'autres situations : pas de protéines R ou protéines R incapables de reconnaître l'effecteur --> on est en situation de compatibilité. ETI basé sur une reconnaissance très spécifique. Il y a tout de même des limites (d'où vient l'ambiguité de produits d'avirulence). Ces effecteurs peuvent être des facteurs de virulence mais ces même effecteurs peuvent être avirulent dans le cas d'ETI. Donc si facteur de virulence ou d'avirulence c'est le génotype de la plante qui le défini.
* On a vu qu'une meme protéine R est capable de reconnaitre des effecteurs différents, ou un meme effecteurpeut être reconnue par des protéines R différentes.
* Intéractions directe :
    *  protéine R capable d'intéragir directement avec l'effecteur : ce n'est pas la majorité des cas car on a réussi à mettre très peu en évidence. Interactions directes mettraient la plante en situation de faiblesse : il faudrait que la plante ait suffisament de protéines R pour cibler tous les effecteurs de l'agent pathogène. I
* Intéractions indirectes : 
    * Modèle de garde
        * Protéines de garde modifiée qui serait reconnu par la protéines R. La protéine de garde pourrait être modifiée pour une multitude d'effecteurs.
    * Modèle du leurre :
        * Protéines pas directement modifée mais la protéine Leurre va s'unir à un effecteur et ce serait le complexe qui sera reconnue par le portéine R.

## Résistance non-hôte
**Assure le fait que la majorité des plantes sont résistantes à la majorité des parasite.**
* La plante est résistante car elle n'est pas adaptée/apropriée à l'infection par l'agent pathogène. Constitue la plus large résistance et la plus durable : ne peut pas être contournée rapidement ou facilement par l'agent pathogène. 
* Pouvoir pathogène sur certaines plantes mais d'autres plantes vont être capables d'y résister.
* Concept simple, mais résistance mal connue.
* Combinaison de la résistance constitutive (mécanismes préformés qui existent en présence ou non de l'agent pathogène) et induites.
* NHR 
    * de type 1 : pas de phénotype visuel
    * de type 2 : la plante va choisir de provoquer la mort de ses propres cellules au niveau du site d'infection. Petite HR très rapide et localisée limitée à 1 ou 2 cellules.
* Déterminé par les caractéristiques de la plante
### Pré-invasives :
* Protéines R pourrait être impliqués dans cette détéction
* N'entre pas dans les tissus
### Post-invasives
* inhibition de la croissance de bactéries.
    * Glucosinolates : hydrolysé -->  libère des molécules avec propriétés antibiotiques
    * Blanc : blé
    * Noir : fève
    * On observe le pourcentage des spores, capables de germés sur la fève etc (pourcentage de germination)
    * Spore à proximité des stomates chez le blé alors que chez la fève très faible pourcentage lié au hasard.
    * Si on considère ces spores à proximité des stomates : tous la capacité de germer chez le blé ou la fève
    * On voit que tous les spore à proximité des stomate sont capables de développer des hyphes
    * Les hyphes vont former des haustoria chez le blé mais pas chez la fève.
    --> deux paramètres bloqués : spore de s'approcher des stomates et incapacité à former des haustoria

## Co-évolution
### Modèle en zigzag
* Basé sur les intéractions plantes bactéries.
* Amplitude de la réponse de défense en fonction du temps
* La pression de séléction fait émerger de nouvelles résistances dans la descendance suivante.
* Au niveau basale on a l'ETS (plante incapable de résister)
* PTI : niveau de résistance non spécifique, pour contrer PTI : ETS etc.
* Limite de ce modéle : on a pas réussi à conceptualiser la résistance non-hôte.
* Ce modèle en zig zag, ce schéma évolutif --> cette aquisition ou cette perte de résistance se fait d'une génération à l'autre, à un temps T : soit à ETS soit ETI mais pas de zigzag au cours de la vie de la plante, on est soit dans une situation soit dans l'autre. 
### Au niveau du génome
* Permet une diversification des séquences génétiques.
* Les gènes de pathogénicité sont soit très isolé et subir des remaniement ou soit en cluster et favorise les échanges intergéniques pour favoriser la diversité
* Les gènes de protéines R : beaucoup de séquences homologues qui peuvent être recombinées entre elles. 
