# Mise en place des Réponses de défense

## Signalisation 

* se met en place quelques minutes après l'infection
* Productions de mol signales
* motif du métabolime cellulaire
* activation de voies de biosynthèse 
* Ce seront les même voies qui seront activées dans un contexte d'ETI ou de PTI.
* En fonction de la nature du pathogène la plante va adapter la réponse de la même signalisation et modulation du niveau de réponse défense. Très couteux énérgétiquement, donc cette réponse doit être bien ciblée --> et appropriée en terme quantitatif.

### Les messagers de l'immunité

#### Flux ioniques

* Flux ioniques : particulièrement des flux calciques --> premier messager de l'immunité.
* Protéine fluorescente qui va dépendre de la concentration de Ca2+ 
* Elliciter les plantes : mimer l'attaque d'un pathogène; montée de la concentration en Ca2+ très rapide après stimulation. Influx H+ pour garder l'équilibre homéostasique de la cellule.
* Le stock d'ions calcium sont surtout au niveau apoplastique puis en moindre quantité dans le vacule et le RE
* Les CDPK (kinases calcium dépendante), cascade de phosphorylation et CAM (protéines calcium dépendantes)
* Ces protéines vont activer la voie des MAPKinases (famille multigénique de protéines kinases (ex : 20 chez arabidopsis) dans les intéractions biotiques les Mapkinases 4 et MPK3 et MPK6 qui vont être activées. Facteurs de transcription qui vont activer certains gènes pour mettre en place la réponse donnée. 
* Phosphatase : toujours pour équilibrer l'homéostasie (également calcium dépendante)
* Réponses immunitaires : quelques heures à plusieurs jours

#### Burst oxydatif

* ROS dont on parle ici : O2- et H2O2 : ces espèces sont produites au niveau de l'apoplastes par des enzymes pariétales activée par des flux calciques. Au niveau cytosolique, la production de ROS est ???
* En terme de cinétique ce burst oxydatif est aussi très rapide quelques minutes après la détéction de pathogène. On observer différentes cinétiques : on peut avoir un pic qui est rapide et transitoire (non spécifique), dans le cas de l'ETI on a une production biphasique (carcatéristique de l'ETI).

#### Production de NO

* Joue un rôle de régulateur notamment dans les situations de stress.
* Présente également une production biphasique dans une intéraction spécifique et un seul pic dans une intéraction aspécifique
* Cette production de NO va entretenir elle même le flux calcique. 
* Chez les animaux : NOS
* Chez les plantes on observe une activité enzymatique qui produit du NO mais on a pas identifié l'enzyme NOS. (complexe de plusieurs sous unités??) On pense que la majorité du pool de NO est produit par la réduction des nitrites en NO par la nitrase réductase.

### Reprogrammation transcriptionnelle

* Puce à ADN : presque tous les gènes exprimes sur les lames et on peut tester le transcriptome entier sur ces plantes.
* Séquencer l'ensemble du transcriptome
* Modifications transcriptionnelles importantes
* Facteurs de transcription :
    * Certains facteurs WRKY vont inhiber des résistances d'autres vont les stimumer
* Activation transcriptionnelle de miRNA 
* Convergence des voies de signalisation 'intra' PTI
    * On l'observe aussi entre PTI et ETI, on va avoir activation de mêmes gènes lors de la cascade de signalisation.

### Modifications de la balance hormonale

* 3 hormones majeures associées à la signalisation du stress biotique
* Il y a des interconnexion entre ces 3 voies
* mise EN plase de résitance contre les agents biotrophes ou hémibiotrophes
* VOie de l'acide jasmonique associée à A.salicylique --> 
* Jasmonique et Ethylène --> résistance aux nécrotrophes
* cofacteur NPR 1 (Récépteur de l'A salycilique)
    * Ensemble entraine le transfère de ce complexe vers le noyant, dans ce noyant il va y avoir activateur des WRKY 70 va lui même activer la transcription de PR1 (pathogenesis related proteines)
    * Après perception de l'A jasmonique : activation de familles de transcription (MYC 2 ou ORA 59) qui vont eux meme  activer la transcription de PR.
     * Ethylène : la famille de facteurs de transcription activée : ERF1 qui activent la trancription dde gêne de type PDF. 
    * ABA : régulation du stress abiotique : considérée comme une enzyme clé qui équilibre les réctions de la plante    lorsuq'elle est soumise à des stress biotiques et abiotiques : plutot un effet négatif sur les pathogène des types biotropes et positifs vis à vis des agents nécrotrophes.
    * Enzymes impliquées dans la croissance des plantes
    * D'un point de vue stress biotique on a plutot mis en évidence que GA un effet activateur contre les agents biotrophes et inhibiteur pour les nécrotrophes
    * Auxin : également responsable de la croissance cellulaire, l'interconnexion entre les auxines et les A salyciliques,  l'accumulation d'A salyciillque va inhiber la signalisation de l'auxin : rejoint cette balance entre comment utiliser ces réerves énergétiques pour la plante, chère pour la plante de se défendre contre les agents pathogènes et doit mettre en parenthèse l'action de croissance et de reproduction. Quand la plante doit se défendre contre un agent pathogène biotrophe : la voie de l'auxine est inhiber.



## Réponses

* Réponses très localisée au site de l'infection ; au tissus adjacents, mais également toute la plante va être mise en alerte et prévenue de cette attaque, très générale donc à l'échelle de la plante. Ces réponses se mettent en place quelques heures post infection et vont pouvoir perdurer plusieurs jours après l'infection.
* Les renforcements de la paroi végétale 
    * Signalisation ROS : active NADPH etc qui entrainent le pontage de protéines pariétales, ces protéines vont être oxydées et cela les rend plus résistantes à des mécanismes de défenses : riche ne glycine et proline.
    * Renforcement de la paroi cellulosique. 
    * Composés de type sucre : polyssacharides : la callose et composés phénoliques qui épaississent et rendent plus imperméable la paroi déjà existante.
    * Structures qui se mettent en place lors de l'infection par des agents pathogènes filamenteux. Apresorium : perce la paroi. Si la plante est résistante : la plante va mettre en place au niveau de ce site un dépot de callose. 
    * Exemple entre arabidopsis et un champignon.
        * Éteint le gene par RNA interference, on a coloré le champignon au bleu de comassie et bleu d'aniline qui a une affinité pour la callose. 
        * Dans la lignée transgénique : la plante n'a pas été capable de mettre en place cette papille, donc le champignon va infecter la plante d'avantage.
* Les ROS : espèces réactives de l'oxygène, ont également des propriétés cytotoxiques : entrainent des dommages importantes sur les macromolécules biologiques : cassures de l'ADN arn, oxydation de protéines donc affectent la structure, affectent la membrane également.
    * Ces ROS sont produits à partir de différents composés  : enzymes / réactions chimiques
    * Ces ROS ont donc un rôle de signalisation et de défense.
* Production de protéines PR
    * On a identifié 17 classes fonctionnelles au sein de plusieurs espèces
    * Protéines intra ou extra cellulaire et induites / produites en réponse à certains pathogènes (ou elliciteurs:qui induisent une réponse pathogène par les plantes).
    * PR protéines vont souvent avoir comme fonction de dégrader la paroi des champignong chitinase 
    * Deux grandes catégories :
            * Defensin ; thionin
            * Les défensines existent aussi chez les animaux ; 
            * thionine : propriétés cytotoxiques --> ce sont des protéines propres aux végétaux
            * D'autres vont affecter la croissance des bactéries 
            * Oxydo réduction souvent : oxalate de calcium (p.e dans les feuilles de rhubarbe)
* Production de composés anti-microbiens  les phytoalexines (ou phytoanticipines) métabolites secondire(spécialisés) ; les phytoalexines : métabolites spécialisés ou secondaires ne vont être produits qu'en réponse à une attaque pathogène. On peut souligner l'importance de la voie des phénylpropanoïdes. Accumulation de la flavonoïde qui va avoir un effet inhibiteur sur la croissance, sur la vie des champignons. 
    * Myrosinase : 4 - methoxyglucobrassicin seulement activé par la myrosinase.
    * Toutes ces réponses se mettent en place aussi bien dans le contexte une PTI qu'une ETI.

### Réponse immunitaire spécifique de l'ETI 

#### Réaction d'hyper sensibilité
* HR : réponse d'hyper résistance. Cet HR va entrainer la mort cellulaire de la cellule végétale infectée. Sacrifie des cellules, pour confiner l'agent pathogène au site d'infection : cette mort cellulaire a été considéré comme une hyper sensibilité. La plante va choisir de sacrifier ses cellules mais pour contenir l'infection. Mécanismes très régulé  et induit les cellules adjacente également. 
* La mort cellulaire de type HR n'est pas une nécrose (subit pas un tissus) : type blessure, subit, mais pas quelque chose d'actif don différente de la mort cellulaire programmée. Encore différent de l'apoptose (on ne parle en général pas d'apoptose chez les plantes)
    * On observe une condensation de la chromatine
    * Activité protéase et autophagique
    * Réorganisation du cytosquelette
    * Accumulation d'espèces réactives de l'oxygène au niveau des mitochondires et chloroplastes
    * perte d'intégrité de la mb plasmique
* SAR : se met en place à la suite d'une HR
* Elle n'est pas déclenchée dans le cas d'attaque nécrotrophe (sinon la plante lui offre des tissus morts à digérer) donc vraiment ciblé contre les agents biotrophes.
* Coût énergétique très important donc hautement régulé
* Bleu de trypan : les cellules mortes restent colorées
* Iodure de propidium : peut être utilisé pour mettre en évidence les cellules mortes à épifluorescence
* DAB : capable de détecter le peroxyde d'hydrogène. 

#### Résistance systémique acquise

* Activité systémique : à l'échelle de la plante entière
* Activation de mécanismes de défense (40 min)
* Molécule signale qui va permettre la mise en place de la SAR
    * Plusieurs molécules signales ; composés lipidiques ; 
    * Confère une résistance de large spectre à des agents de type biotrophes.
* SAR : associé à la signalisation de l'A. salycilique

#### Résistante systémique induite

* mis en place par des MO bénéfiques ou par des éliciteurs généraux
* on a mis en évidence que cette ISR ga passer par la signalisation de l'acide jasmonique etc, donc dirigé contre des nécrotrophes.
* Grande différence avec la SAR : la plante va mettre en place des mécanismes de défense dans laSAR alors que dans l'ISR la plante va être beaucoup plus potentialisée ; la plante va se préparer pour répondre plus fortement et plus rapidement mais ne met pas en place de suite ses mécanismes de défense.