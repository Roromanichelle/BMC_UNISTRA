# Aplications aux méthodes culturales

* intégrée : biologique mais si risque de perte de la culture alors l'agriculteur s'autorise à utiliser des pesticides.

## Approche chimique : les pesticides

* Épandage en avion : normalement interdit en France mais on en voit encore.
* Stratégies moléculaires des pesticides : trés spécialisées
* Résistances des MO faces aux pesticides
* L'homme contribue à l'échelle mondiale à la propagation des pathogènes
* Xylella fastidiosa (Pierce)
    * Détécté pour la première fois en Europe
    * Forme des biofilms, colonies de bactérie
    * obstrue le transport de la sève brute : arbre meurt car il ne peut plus s'alimenter
    * Les premiers symptomes observés : resemble à un arbre qui a un peu soif (stress hydrique)
    * Arbres très agés, oliviers centenaires dans le sud de l'Italie
    * Beaucoup de règles à cause de cette bactérie qui ont été mis en place à cause de ces oliviers

## Meilleures pratiques culturales

* Mettre en place des approches biologiques : uniquement des produits ou organismes naturels : une stratégie qui fait sa place : biofumigation : consiste à traiter les sols (couche supérieures) pour les désinfecter. Les glucosinolates.
* Les agriculteurs qui utilisent les brassicacés
* Massérations à partir de ces plantes, on mélange la couche superficielle des champs, désinfecte la couche superficielle
* solarisation : couvrir le sol avec des baches qui vont capter l'énergie solaire, permet d'augmenter la température des sols jusqu'à 40 °C, on combine souvent la biofumigation  et la solarisation
* Agents de luttes biologiques : 
    * Coccinelles par exemple mangent les pucerons
    * On peut utiliser des parasites de l'agent pathogène
    * Pulvérisation de Bactériophages (virus spécifiques des bactéries)
    * Bactérie qui ont des propriétés anti microbiennes vis à vis d'autres bactéries
    * Mise en place de l'ISR : les rhyzobactéries qui favorisent la nutrition azotée des plantes.

## Approche biologique
## Stimulation des défenses naturelles (SDN)

* Utilisation de stimulateur des défenses naturelles
    * On peut utiliser des éliciteurs (pas utilisés à l'échelle des cultures)
    * Par stimulation mécanique : toucher le plantes : permet de les éliciter
    * Algues qui ont des propriétés stimulatrices sur les plantes.
    * Cellule possède un bon niveau d'immunité si attaqué par un agent pathogène, si avant on la stimule (on la prime/potentialise) alors elle va se préparer, elle va produire des éléments qui vont lui permettre de répondre plus rapidement et intensément. On connait ces éléments qui permettent à la plante de se préparer (des dormants, préformés pour etre mis en action), modification de la chromatine, on a observé qu'au niveau des séquences de certains gènes de défense peuvent être modifiées pour que ces promoteurs soient rapidement accessibles par les facteurs de transcription. Stockage d'ATP, potentiallisation de séquences promotrices, de voies métaboliques. 
    * Dure en moyenne 5-6 jours.

## Approches génétiques : amélioration des plantes (plant breeding)

* Améliorer la reconnaissance du parasite

* Deux stratégies : 
    * Intégration de gènes de résistance
        * Introgresser des plantes sauvages sur des plantes cultivées
        * Au bout de 15 ans 
        * Avec les connaissances que l'on a on essaie de mieux cibler les gènes de résistance
    * Plantes transgéniques
        * Papaye transgénique avec ces gènes de résistances : 95% des papayes hawaiennes sont transgéniques.
        * EFR : spécifique des brassicacées
        * Intégré dans le génome de la tomate : permet de conférer à la tomate et au tabac, permet de conférer une résistance à large spectre.

