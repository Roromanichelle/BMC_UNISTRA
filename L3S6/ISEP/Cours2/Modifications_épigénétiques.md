# Épigénétique
* Inscrite dans la "chromatine" (pas dans la séquence d'ADN)

Ca n'a pas d'importance.

Les équipes de recherche qui travaillent sur l'épigénétique sont, à strasbourg, à l'IGBMC et à l'IBMP (génèse et mode d'action des petits ARN).

L'épigénétique, c'est la régulation des gènes par des processus biochimiques (et non génétiques). Cette régulation des gènes se fait par des enzymes qui modifient les histones de sorte que l'ADN soit ou non accessible, càd transcrit ou non.

Cela conduit à des phénotypes différents. On voit ici deux inflorescences dépendantes d'un même gène dont l'épigénétique est différente. Autre exemple de fleurs qui ont des couleurs différentes selon qu'on les expose à une température plus ou moins élevée.

modifs épigénétiques en lien direct avec l'environnemet de l'organisme. Les plantes sont forcées de subir les changements.

toujours pareil : stress extérieurs induisent changement phénotypique (facteurs biotique ou ab.)

capacité d'un orgs à exprimer différents phéno selon condition environement, qu'elles soient biotiques ou abiotiques, le tout pour un génome donné.

J'ai oublié de vous dire, j'ai mis le diaporama sur moodle.

Les modifications épigénétiques SONT TRANSMISSIBLES À LA DESCENDANCE (truc de ouf)

Enfin.. il y en a qui sont transmissibles et d'autres qui ne le sont pas. Tout dépend si le stress est temporaire ou pas.

Modifications de l'DNA et des histones sont les principales modifications. Elles conduisent, encore une fois, à du silencing transcriptionnel, càd à un accès ou non à l'ADN du fait de sa compaction (ou non...).

Rappel : E de nombreux motifs répétés dans les régions péricentromérique. 

Rappel : l'ADN est chargé négativement, à cause des phosphates.

Quand on parle de méthylation de l'ADN, on parle de la méthylation des cytosines en position 5. C'est qqch de très général, on le trouve aussi chez les animaux, même chez les humains qui en sont, au passage. Mais ce n'est pas chez les nématodes, ni chez les levures. On pense que ça concerne tous les organismes pluricell à reproduction sexuée.

Une dernière remarque : comme vous le savez, il y a aussi des mitoch et des chloroplastes.

Ce qui nous intéresse aujdh, c'est l'ajout d'un méthyl en position 5 des cytosines.

Ca va être possible grâce à l'action de méthyl transférase, qui porte bien son nom.

Oups... (problème technique)

Gardons en tête que ces modifs sont réversibles. Méthylation de novo, c'est une modification nouvelle: les cytosines doivent être méthylées, ca c'est de novo.

Si la cytosine est oui, non. retenez juste que ces méthyl transférases sont dépendantes de petits ARN.

Autre possibilité : méthylation de maintenance. E une réplication, il faut que la méthylation qui était initialement présente se retrouve sur les brins néoformés (on aurait tendance à croire que c'est ça, le de novo, mais bon...).
Pour de novo, on n'avait pas de méthylation au départ, c'est pour ça qu'on l'appelle de novo.

Lorsque la C est suivie d'une G, on a afaire à une méthyl trasnférase (MET1)

Chromométhylase 3 si CNG ou voie asymétrique (cf dia pour piger).

Ce qu'il faut retenir de tout ça, c'est qu'on n'a pas toujours les mêmes enzymes qui agissent à chaque fois.

Pour lire ce genre de graph : grand bras du Chms à gauche du centromère (boule entre les zones péricentromériques qui contiennent les régions répétées).

Si on regarde mtn la densité des gènes, on les voit principalement sur les bras du chms et absent de la zone centromérique. A l'inverse, les éléments répétitifs sont faiblement représentés sur les bras et pas etc.

On constate que méthylation se fait surtout niveau zone centro et péricentromérique.

Mutant MET1 c'est (probleme technique)

Ca c'est à l'échelle du chms. Peut être fait de la même façon au niveau du gène. 

La méthylation de l'ADN sert A REPRIMER l'expression de l'ADN. Dia de la répression contient tout ce qui a à apprendre, et rappel que tout est réversible.

Achtung : on dit UN pétale (le tout formant le corolle).

Aparté : 11:12 - Romane me fait un vent.

Aujourd'aujourd'hui...

Inérêt de la variabilité de Linaria vulgaris : on pense que la plante a été dans des conditions ou la polinisation n'a pas été optimale (polinisation entomophile) il n'y avait que peu d'insects présents. Or les éperons contiennent du nectar. On a donc optimisation de l'attirance des insects qui se frottent au passage aux gamètes etc.

Remarque : cycloidea s'exprime dans plusieurs autres plantes, genre tournesol. Chez lui rien à voir avec méthylation de l'ADN mais du a transposons.

Méthylation de cycloidea : Rhodiola peut pousser à très haute altitude : impact sur la taille de la plante, très fonction de l'altitude.

Rappel : La patience est mère de la joie.

Plus de 38% des ADN qui vont être méthylés.
* Lien entre la morphologie de la plante et le taux de méthylations du génome de la plante.

Autre marque épigénétique imporante : modif des histones (H).

Rappelons H2A/B, H3, H4 dont les extrémités Nter sont sortantes. Retenons que les hélices alpha ne sont pas accessibles aux enzymes. Mais les séquences Nter sont libres et modifiables, car accessibles quant à elles. Ca peut parfois être le cas des extrémités Cter, mais plus rare.

Il y a énormément de modifs sur la séquence des lysines et serines de Nter de H3.

Ca modifie la structure de la chmt : soit une ouverture, soit une fermeture de la chmt, de sorte que la trascription ne se fasse pas.

Modifs sont nombreuses chacune impliquant la fermeture ou l'ouverture de l'ADN : 
    méthylation  -  1 ou 2 ou 3 groupement sur l'azote, grâce à histone méthyl transférase. la substitution des H ne change rien à la charge. En revanche, méthylation augmente l'hydrophobicité.

    acétylation  -  peut se faire aussi sur la lysine. Engendre un relachement de la chromatine, car lysine devient neutre alors que DNA est négatif. Implique transcription. Là encore, 

    phosphorylation - en général, se fait sur les S et T. donne une charge négative -> répulsion donc ouverture de la chmt.

    ubiquitination - ajout d'un peptide (76aa) à un aa : K ou S. répression de l'activation de la transcription. Prendre en compte l'encombrement stérique. On a remarqué que si elle se fait sur H2A elle active, 

    symoylation, idem que précédent. En dehors de H2A.


On remarquera que les modifs se font essentiellement sur les K et en Nter.

HMT fait une modif, puis Complex protéiques qui lisent ces modifs (readers) et permettent activation ou inhibition de la transcription, sachant que des désacétylases ou déméthylases peuvent faire le chemin inverse. Donc DYNAMIQUE.

E des marques antinomiques : peut pas avoir des signaux antagonistes en même temps (soit activation, soit inhibition), même si il existe des exceptions.

H3K27me3 -> histone H3 triméthylaiton sur la lysine en position 27.

Les histones H2 et H3 eux-mêmes peuvent être remplacés.


concernant le controle des transposons - ce sont des petits segments d'ADN capable de se... cf diapo (askip c'est aléatoire) grâce aux transposases - il faut comprendre qu'un grain de maïs est soudé au trognon et que l'albumen rouge ou jaune exprime un gène donné. Ac code pour transposase qui code pour de dissociateur (cf diapo). Si DS interromp le gène rouge sur le schéma, il n'y aura pas expression de la couleur rouge et le grain sera jaune. Mais (2e ligne) le Ds peut se trouver dans le gène (grain jaune initial) et alors, quand il est exprimé, il "fou de camp" et permet l'expression du gène rouge.

Il existe cependant des cas intermédiaires ou le grain est tacheté.

E deux classes de transposons, class I et II.

En II, il y a les transposases qui coupent etc et le transposon se retrouve à une autre position qu'au départ, on appelle ce mode le mode "cut-paste".

Class I : le transposon est copié et mis dans un autre endroit alors que l'ancien est toujours en place, c'est ce qu'on appelle les "rétrotransposons" - très courant chez les végétaux. Dans ce cas, la quantité d'ADN du génome est donc augmentée. 85% de transposons dans le génome du maïs vs environ 45% chez les humains.

Les transposons se trouvent près des gènes, mais surtout au niveau des zones centromériques et péricentromériques - on parle même d'îlots. Kakutani et al. montre que si l'on inhibe le controle des transposons on constate qu'au fur et à mesure de générations, la taille de la plante diminue considérablement.

Chromosome 3 d'A. Thaliana; Beaucoup de méthylations au niveau du centrosome et peu sur les bras des chrosomes (répression des transposons localisés dans les zones centromériques). Ce n'est pas la seule --> Autre modification épigénétique qui y participe (présence de 2 méthyls sur H3K9m2). Quelque fois les transposons sont déméthylés ce qui peut impliquer une mobilité. Lorsqu'on a un élément transposable qui s'exprime, il est reconnu comme anormal, il va etre clivé. Ces petits ARN vont ensuite être un signal (pour signaler que QQCh ne devait pas etre transcris), puis protéines vont méthyler ce transposon. Méthylation de novo (la séquence n'est plus méthylé, il faut méthyler la séquence pour que ça ne s'exprime pas) c'est exactement la voir RDDM (ce sont des petits ARN qui recrutent les protéines DRM qui permettent la méthylation de novo des ADN).

Idéalement les transposons doivent être méthylés --> permet la stabilité du génome 
On a l'environnement qui agit sur la plante (on a un stress hydrique) par des métaux ou des agents pathogènes, qui entrainent une déméthylation de l'ADN et donc une activation des transposons. La cellule est capable  de méthyler ce transposon et retour à l'état initial. Conséquences : Le transposon va dans une zone où il n'y a pas de gènes transcris : pas de conséquences ; si gène transcris (peut être létal pour la plante), sinon répercursion relative (plein de cas possibles)
Exemple : 
Protéines tronquées interne
Activateur (gène très peu induit)


Ex1 - plantes qui poussent sur les sols pollués en aluminium -> solution de la plante : sécrétion de citrate et de malate, peittes molécules qui permettent de compleer l'aluminium, de sorte qu'il ne soit pas absorbé par la plante. Pour que ces petites molécules soient sécrétées, il existe des transporteurs. Ces plantes capables de pousser dans de tels conditions ont ces transporteurs fortement exprimés. Exemple de transporteur : MATE.

Au départ, il y a une plante sensible à l'Al, elle subit donc ce stress, d'où la mobilité de certains transposons situés à proximité du gène MATE : ils permettraient une expression plus forte du malate (insertion du trosponson dans un activateur (voir cas enhancer)), d'où l'efflux plus important des anions organiques tq apex racinaires sont protégés de la consommation involontaire de l'Al par la plante (ici le riz). 
Il s'agit d'une modification qui permet ces adaptations.

Un stress thermique peut conduire à l'activation de transposons (famille des scrophulariacées), un gène (nivea) qui code pour la voie de biosynthèse de pigments, on a mis en évidence que lorsque la température est élevée : présence d'un transposon qui est réprimée car méthylé donc réprime le gène nivea donc très faiblement coloré. Si diminution de la température il n'y a plus le transposon et donc n'empeche plus l'expression du gène nivea.


Stress a impliqué l'arrivée d'un rétrotransposon qui empêche la synthès du pigment

-----

Ex2 les oranges, genre citrus, famille rutacées. Il existe la variété Navalina qui n'exprime pas un gène donné, dit Ruby. Insertion du rétrotranposon en amont du gène qui va activer le gène. Tarocco : haute altitude donc température froide la nuit = stress thermique qui a amené le rétrotransposon à proximité du gène ruby.

Lorsqu'on a une plante soumise à un stress : les transposons peuvent être mobiles et modifications génétiues --> la plante produit des graines ; Si ces transposons sont de nouveau activé, ARN messager reconnu comme anormal, permettent de méthyler ces transposons, on suppose que ces petits ARN sont transporter vers les cellules à proximité. 


















