# Semaine 1 Végétaux

* Mise en évidence des réseaux de microtubules dans la cellule végétale
* Utiliser des gènes rapporteurs pour mettre en évidence des 
* Fusions GFP approche de localisation de protéine
* Plantules d'Arabidopsis transformées

Particularités :

* digestion de la paroi ; chloroplaste -> cellules cultivées à l'obscurité
* Cellules acentrosomales
* Approches expérimentales communes mais à adapter aux différents modèles

## Approches de génomique fonctionnelle

* Analyse de promoteur
* GUS

## Immunomarquage
* Bleu : marquage dapis
* b) Prophase : anneau propréphasique
* d)

* MANOSIDASE : Protéine résidente de l'appareil de golgi

## Régulation de l'expression d'un gène d'intérêt
* Promoteur à expression constante
* Promoteur dépend de la séquence réulatrice
* transformation --> plutôt dans les cellules cancéreuses sur le modèle animale (Transfection plutôt)
### transformation transitoire
* Mise en oeuvre rapide pour avoir le résultat

### Transgenèse végétale
* Pas d'intégration ciblée : analyser les plantes pour savoir si le gène est intégré au bon endroit
* Intégration non ciblée en train de changer dans le modèle végétale (CRIPR)
* Marqueurs de selection en modèle végétale : antibiotique ou gène de résistance à un herbicide
* Technique de transfection : polyéthylène glycol (protoplaste = cellule végétale sans sa paroi)
* Transfection de cellules : microinjection (assez rare)
* Biolistique : pulvériser les cellules de billes enrobées d'ADN.
* Digestion de la paroi par des enzumes pectocellulolytiques. sert aussi à faire des protoplastes 
* maïs : monocotylédone
* On utilise Agrobacterium tumefaciens capacité transgénique de la bactérie utilisé pour transférer un gène
* oPINES : CONJUgués AN sucres qui sont source de carbone et d'azote, les agrobactéries ont des gènes impliquées dans le catabolisme des opines --> les agrobactéries s'en servent comme nutriment.
* Plasmide trop grand : on le divise en deux plamsmides. Ti désarmé n'a plus de Tdna mais a les gènes Vir ; les gènes vir sur le système assistant désarmé, système à deux plasmides (binaires) il faut que l'ORI soit compatible pour le maintient d'une réplicaiton dans les agrobactéries. Il faut que ce vecteru de clonage puisse se maintenir dans E.coli. 

## Méthodes de transformation avec A.tumefaciens
### Méthode des disques foliaires
* Transgénèse :
ON prend des disques foliaires on les met en présence des , la cellules transformées se met à se diviser à former des cal,  on regénére les tiges feuilles pour avoir une plantule. On fait de l'autofécondation et on travaille sur les générations suivantes , toutes les cellules n'ont pas été transformées, CAL cellules transformées non différenciées puis on ajoute des hormones, balance hormonale qui fait qu'on va plutot aller vers une tige feuille etc..
* Les plantules d'arabidopsis sont plus peties, on plonge dans une suspension d'agrobactérie, on transforme au niveau du gamétophyte femelle, on laisse monter en graines et on les met en milieu selectif (on pourrait utiliser ici un herbicide).
* La plante résistante reste bien verte, on ne travaille pas sur la première plante qu'on a généré, on travaille sur les descendants. On travail sur une plante homozygote, on ne peut pas travailler uniquement sur homozygote dès la première insertion.
AVec des insertions multiples on peut utiliser des modèles de DNA silencing, lorqu'on a des insertions multicopies.

### Construction génique type
* cDNA complémentaire de l'ADN messager avec la séquence codante du gène d'intéret mis sous la dépendance d'un autre promoteur
* on doit donc avoir un promoteur 
* caMv : ADN DOUBLE BRIN ADN 35 S peut grâce à la reverse transcription regénérer l'ADN ; promoteur fort, très actif, il est exprimé dans toute la plante exprimée de manière forte (il est fort constitutif)
* Il existe des promoteurs inductifs ; hormones --> permettent de rendre le promoteur actif ; durant tout le processus de transgénése on peut empêcher l'expression d'un gène d'intérêt. Temps : à T0 on voit le changement d'un phénotype
* spécifique et régulé : promoteur dépendant d'un moment donné du cycle cellulaire
* Terminateur 

### Etude de l'expression d'un gêne d'intéret
* L'activité d'un promoteur ; la transcription permet de traduire, permet d'étudier la force ou sa spécificité

### Gène rapporteur 
* ne doit pas être naturellement présent dans les cellules de plantes (non endogène)
* codant pour une protéine facilement détéctable

### Substrats de la B-glucoronidase
* Fluorimétrie : on broye tout, on utilise MUG, en présence d'enzyme donne une molécule qui donne de la fluorescence, permet de remonter à la présence d'enzyme, si enzyme on remonte à l'activité transcriptionnelle. 
* On aura aussi X-Gluc, dimérise pour donner un précipité bleu, dans quelle cellule il y a de l'enzyme. Histone : pendant la phase S

## Localisation des protéines au niveau subcellulaire
* Mettre en évidence l'appareil de golgi
* Immunomarquage sur cellules fixés (mortes)
* GFP sur cellules vivantes

### Immuno marquage
* paraformaldéhydes ; pontages d'acides aminés ; fixation protéique ; c'est un bon fixateur
* Suite à l'étape de fixation , digestion partielle de la paroi cellulosique ; enzyme
* En même temps on met du triton (détérgent) pour perméabiliser la membrane (à faire également sur des cellules animales)
* Serum normal : anticorps ne reconnaissent rien de spécifique, anticorps sera déplacé ; serum normal vient de l'animal qui sert aux anticorps secondaires couplé au fluorochrome : augmente l'amplification du signal, facilité d'acheter l'anticorops secondaire avec une grande quantité de fluorochrome différents. /= anticorps primaires ; 
* La spécificité
    * Les anticorps polyclonaux : mélange d'AC reconnaissant plusieurs parties de la protéine d'intéret qu'on avait purifié et injecté
    * On peut également aoir des anticorps monoclonaux ; fusion d'un hybridome (cellule qui prolifère), chaque hybridome secrète une classe d'iGG reconnaissant un épitope. avantage : sans devoir sacrifier des animaux. On peut maintenir indéfiniement la production des anticorps.
* Obtention de l'antigène
    * Système hétérologue
* Les peptides de synthèse 
    * produire des anticorps contre une protéine d'intéret
* Le choix des anticorps secondaires
    * doit reconnaitre les anticorps primaires (doit être produit dans un hôte différent)
    * Le secondaire permet d'amplifier le signal car plusieurs secondaires vont se fixer sur le primaire, plusieurs fluorochrome sur l'anticorps secondaire. Alexa 488 excitable à cette longueur d'onde pour émettre dans le vert.

### GFP
## Les chromobodies
* Parties d'anticorps en fusion GFP
* ONt des anticorps qui ne possèdent que des chaînes lourdes
* Caractéristiques de ces anticorps qui sont exprimées dans des espèces très particulières
* On peut l'exprimer en système bactérien hétérologue et pas un fluorochrome chimique, molécule inorganique
* Marquage de la lamine
* On peut aussi les exprimer directement en cellules vivantes, on peut les exprimer en cellules vivantes.

