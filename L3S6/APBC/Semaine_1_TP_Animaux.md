# Dans quels cas utiliser quelles méthodes ?

## 2) FRAP

* Photobleaching : photoblanchiement, ou photodécoloration
* On a une cellule en culture qui exprime une fluorescence donné, on applique un flash très intenses, ne donne plus de signal, devient noir au microscope (perte de fluorescence). Au bout d'un moment, cette tâche noire va regagner sa fluorescence car les molécules du viosinage vont également bouger (étudier la migration de transport d'une protéine dans le noyau)
* Photoactivation : à l'endroit ou on élumine, au début on avait pas de fluorescence et dans le spot on obtient de la fluorescence, cette fluorescence va dissiper.
* Photoconversion : perd la fluorescence verte mais gagne la fluorescence rouge.

## 3) FLIP

* L'illumination qui est constante, la région d'intérêt se trouve à l'extérieur de la zone photoblanchie. Décroissance exponentielle de la fluoresence; mesurer leurs déplacements/taux de migration.

## 4) FRAP

## 5) FLIM

* La fluorescence ne dépend pas uniquement de la longueur d'excitation mais dépend aussi de l'environnement car la fluorescence peut varier en fonction de la concentration ionique, du pH des intéraction etc.
* Ce microscope utilise un laser pulsé, qui envoie des excitations lasers répétées.
* Cette illumination va exciter les fluorochromes dans la cellule et vont émettre une lumière très rapidement mais d'autres vont tarder car les photons ne se comportent pas toutes de la même manière. La plupart répondent instantanéement, d'autres vont arriver dans le détécteur avec un décalage. COmportement de la population photonique après impulse.
* Décrit le comportement de fluorochrome
* On peut calculer la demie vie de fluorochrome (temps moyen que le fluorochrome passe à l'état excité à partir de la pulsation du laser)
* Dépend des intéractions moléculaires, molécule voisine peut également absorber et il y aura un décalage dans le temps.
* c) Le même fluorochrome émet la fluorescence à des temps différents, et?
c) 5 ns : donc fluoresce plus longtemps donc demie vie plus longue
* Cette technique permet de mesurer le FRET, permet de savoir s'il y a eu intéraction avec d'autres molécules
* Peut se faire sur des cellules vivantes contrairement à la technique FRET qui mesure également des intéractions.

## 6) TIRFM

* On illumine sur le côté, la lumière va être réfléchie et va passer dans l'interface
* Il faut que l'indice de réfraction des deux milieux soient différents : n2 > n1
* Eau : 1,3

* Si on illumine verticalement, il y aura une petite réfraction, on peut modifier l'angle de réfraction, le faisceau va passer dans l'interface entre les deux et si on passe au dessus de cet angle critique, la lumière pénétre seulement partiellement, on peut éclairer des éléments qui sont à proximité de la membrane par exemple, éléments du cytosquelette proche de la membrane (du support).

* L'image est dépourvue de bruit, beaucoup moins de "bruits de fond" car
* On gagne en résolution axiale (200nm en microscopie optique) 30 nm en Z
* microscopie confocale : on regarde une tranche optique également (600 à 700 nm), deux fois plus épais!
* Phénomène de tranfic membranaire : exocytose, migration cellulaire, remodellage du cytosquelette.

## 7) Microscopie à deux photon (microscope multiphotonique)

* On arrive à pénétrer dans le tissus épais ; but : observer des échantillons épais
* Que ce soit animal vivant ?
* On peut observer directement plusieurs mm en dessous de la surface, avec confocale seulement 300 um.
* Un seul laser, et après émet photon vert
* Pour utiliser deux photons on envoie ces deux photons de manière consécutive avec un intervalle de 5 fs 
* Pour pouvoir exciter le même fluorochrome on va utiliser la longeur d'onde double. ELles vont s'additionner uniquement si le photon arrive sur le même volume, sur le même point.
* Si je veux exciter ce même fluorochrome : il faut taper au même endroit il faut additionner l'effet des deux. Longueur d'ondes deux fois plus grandes : pénètrent mieux dans le tissus --> permet d'entrer dans l'échantillon plus facilement jusqu'à 1200 nm
* On peut utiliser plusieurs photons mais il faut tripler ou quadrupler la longueur d'onde, alors les lasers doivent taper au même endroit 3 ou 4 fois.
* Résolution plus importante. Cette technique permet d'oberserver même les animaux edormis, par exemple : circulation sanguine, on peut observer le contrôle, l'animal et ensuite l'animal traité.

## 8)

On illumine juste un feuillet de l'échantillon
L'illumination se fait sur le côté, on peut même illuminer des deux côtés, la puissance de laser n'arrive pas toujours à traverser la totalité de l'échantillon.
* Peu de phototoxicité, si on veut observer un mécanisme qui dure des jours, avec des illuminations répétées.
* Quasiment pas de photobleaching, (confocal time lapse, quasiment tous ceux qui illuminent ) il faut toujours recalculer la fluorescence comme si on l'avait pas blanchit, ici pas besoin de calculer la fluorescence réelle.
* Résolution très mauvaise (1-10um)
* Microscope très bien pour étudier les migrations cellulaires
* On utilie souvent chez l'embryon de posson zèbre, chez la drosophile, chez C elegans
* On peut utiliser ces techniques sur l'animal vivant (transgénique car nécéssite d'exprimer une molécules fluorescente)
* Échantillon translucide ?
    * L'échantillon est opaque car indice de réfraction n'est pas le même que l'air. Comment rendre l'échantillon transparent, si pas de changement d'indice de réfraction il devient translucide, si on le raproche de l'indice de réfraction de l'eau, soit de l'air etc
    * 1er : utiliser des solvants, mais toxiques, mais ne marche pas avec la fluorescence
    * Pb : pouvoir de pénétration des anticorps pour un immunomarquage (il faut prendre un organe qui exprime de façon endogène une protéine fluorescente recombinante)
    * Dans l'urée
    * Clarity : dissous les lipides, si on veut juste observer le positionement des cellules : OK
    * Tout cela marche dans des échantillons morts.
    * Exemple : Études concernant des réseaux neuronaux beaucoup plus rapides


## 9) STED

**S'utilise aussi en cellules vivantes?OUI  et photoblanchiement?on éteint la fluorescence??**

* Très haute résolution Dans les trois dimensions,
* On utilise deux lasers simultanéement, les deux illuminent ensemble l'échantillon. Les deux lasers illuminent en un point l'image (faisceau d'excitation), on utilise un deuxième laser en forme de laser (faisceau STED) atténue la fluorescence en périphérie, on désexcite les photons qui sont émis. 
* S'utilise en mode confocale si on pa de faisceau sted.
* Possibilité de mesurer des spots de 60 nm d'épaisseur.
* bien pour voir par exemple

## 10) CLEM

* On corrèle les images avec des images de microscopie éléctronique et des images de MO.
* On veut obtenir une information ultrastructurale détaillée avec une résolution nanométrique
* Quelques années : impossible. 
* Aggarose va prendre mais l'animal est toujours vivant dedans. Sur le verre il y a un quadrillage et on peut observer en miscroscope à fluorescence, il y a ensuite des indicateursqui permettent de se situer sur le quadrillage, pour identifer quelles cellules on regarde etc. On prend l'animal on fait une biopsie pour récupérer et fixer par le froid, cette fixation physique, on va inclure un bloc??, après avoir taillé le bloc on peut observer l'échantillon.
* Très bonne résolution

## 1) Cryomicroscopie éléctronique

* Cryo = congeler
* La criofixation doit être rapide, azote sous pression, pour éviter la production de cristaux d'eau, (la vitrification),la congélation se fait par un impacte contre un bloc de cuivre. Et face à ce bloc on a un système de pouli qui contient un support ou une tête qui contient l'échantillon ???. Cet impacte physique et thermique instantanné va figer l'échantiller.  Pas d'altération chimique par un fixateur, permet de mettre en évidence des mécanismes rapides comme l'exocytose par exemple.
* L'observation des coupes sont rangées ensuite et bien conservée.
* Cryofixation et fixation chimique ? quel est le plus juste??
* Après fixation, on utilise une technique appelée Freeze substitution qui remplace les molécules d'eau par l'acétone.
* But : choisir un solvant qui est liquide encore à l'état congelé, comme l'acétone.
* On réchauffe l'échantillon

* Cette technique s'utilise souvent dans la technique CLEM
* On a un échantillon qui exprime la fluo
* On fixe les cellules
* On remplace les molécules d'eau par l'acétone
---> voir cours (lien pour la méthode CLEM)

## 2) Cryotomographie éléctronique

* Tomographie : utilise aussi la cryofixation et permet de voir en 3D les images en microscope éléctronique
* Cryotomographie : on incline sous différents angles, l'image qui est formé par l'arrivé des éléctrons est toujours différentes parce que l'orientation des éléctrons change. L'image que l'on obtient est interpreté par un logiciel et génère une image en 3D.