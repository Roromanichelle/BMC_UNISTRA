# Cours 5-6

## Purification de Protéines

### 
* BUT : APPLIQUER DES MÉTHODES QUI SOIENT NON DÉNATURANTES ET PERMETTENT DE MAINTENIR LA STRUCTURE DE LA PROTÉINE
1) ENZYME : Premier test : sur l'échantillon brut, puis les autres -> il faut identifier quelle fraction comporte la protéine.
2) PROTÉINE : Protéine non recombinante : on peut effectué un western blot s'il y a la disponibilité d'anticorps ; ce ne sera pas aussi précis qu'un test enzymatique
3) PROTÉINE RECOMBINANTE DE FUSION : en mettant en fusion la protéine avec une étiquette, elle peut être plus ou moins grande, on peut la cloner avec un TAG qui va permettre de la suivre.

* Etiquette 6 histidine : n'améliore pas la solubilisation mais permet une purification par chromatographie d'affinité beaucoup plus simple.
* Tampon : permet de ne pas dénaturer la protéine.
Comment extraire de sa source?
* SI foie de boeuf ? 
    * homogénéisateur / casser les cellules par lyse/ lyse mécanique en utilisant un tampon isotonique qui aura la même osmolarité qui est le cytoplasme cellulaire, on va maintenir la même concentration saline que le cytoplasme + homogénéisateur, le piston est en tefflon, espace entre la paroi du tube et le piston très réduite, donc par va et vient on va obliger les cellules à passer dans un espace très réduit : casse les cellules mais on va garder les organelles internes des cellules eucaryotes intactes (utiliser au préalable un tampon isotonique). 
* Bactéries
* Levures
    * utiliser tampon physiologique pour les grandes quantités

### Stabilisation des protéines
* On évite des faire des bulles dans les solutions, car bulles = contactes avec l'oxygène très grandes, toutes les protéines en contact avec l'oxygène vont être dénaturées.
* Agents réducteurs des ponts disulfures : on doit au préalable savoir si la protéine fonctionnelle n'a pas de pont-disulfure. **__**
* PMSF : inhibe la transmission nerveuse (toxique)
* GLycérol : si on veut un complexe protéique
* détergents non ionique améliore la solubilisation

### Corps d'inclusion
* Protéine tombe avec les débris cellulaires, elle n'est pas soluble ; soit mauvaise manip, on change de tampon etc. Si on arrive aps à améliorer la solubilité cela signifique qu'on est en présence de corps d'inclusion. On peut diminuer laduction. Baisser l'induction : on exprime moins la protéine. L'induction se fait dans la phase de croissance logarythmique. 
* Induire une culture plus dense : on induit pendant un temps d'induction plus courts --> on produit plus de bactéries mais on accumule autant de protéines mais sur un temps plus court..
* Agent chaotropique : destructure les protéines : dénaturation, remplace les liaisons hydrogènes et permet de solubiliser l'échantillon. Mais va de paire avec une dénaturation de la protéine (même chose pour les détergents, pH basique etc) Le défis est alors de renaturer la protéine après l'avoir traité de cette façon.
* Après solubilisation : enlever l'agent chaotropique de façon graduelle pour essayer de maintenir la protéine toujours soluble. On le fait par dialyse, on ne fait généralement pas de l'ultra filtration, souvent des boyaux de membranes semi perméables, des petites cassettes et on fait dialyser dans un tampon.
**Les sels ne permettent pas d'éviter les corps d'inclusion?**

### Méthodes de séparation et de purification

#### Effets des sels sur la solubilité des protéines
* Quand on a des protéines dans un milieu aqueux avec très peu des sels : les protéines s'aggregent entre elles (intéractions éléctrostatiques). 
* Les protéines vont se solubiliser car les cations et anions vont interagir avec les protéines (même intérations éléctrostatiques mais ici avec le sel). Cependant si on ajoute plus de sel on va avoir une précipitation des protéines mais par intéraction hydrophobe, le sel a besoin d'être hyhdraté, l'eau qui s'organise autour des protéines va être soustraite et les protéines vont s'aggreger par leurs domaines hydrophobes, l'eau est insuffisante pour hydrater à la fois les protéines et le sel. Ces protéines aggreger ne sont pas dénaturées, mais les fait précipiter. On va pouvoir l'utiliser pour concentrer les protéines.
* Principe de base de la chromatographie par intérations hydrophobes.
* SUlfate d'ammonium, quand on en a pas : solubilité de la protéine basse, on ajoute du sel : on augmente la solubilité jusqu'à un point optimal, et quand on ajoute du sel --> salting out
* chaotropique : solubilisation : défait les intéractions entre les protéines
* calcium fait plutot solubiliser que de faire précipiter **__**
* Si les protéines ont beaucoup de domaines hydrophobes 

#### La chromatographie
* SEC : size exclusion chromat
* Phase stationnaire avec billes de silice peut supporter des pressions très élevées mais ces pressions sont très dénaturantes : plutot utilisées pour purifier des acides nucléiques ou des peptides de courtes tailles, on a en générale pas besoin de le garder avec structure.
* La phase mobile est le solvant que l'ont fait passer à travers cette phase stationnaire et qui entrainera les protéines. 
* On parle d'élution quand on ajoute (effectuer un changement de tampon et on peut ainsi faire détacher les protéines de la résine)
* Chromatogramme 

##### Chromatograpie d'adsorption
###### Chromatographie par échange d'ions

* Basique : lysine, arginine, glutamine
* Acide : glutamate/aspartate
* Intensité de la charge sur une zone --> permet de bien intéragir sur la résine.
* Première phase : équilibration de la résine : on va lier un cation / résine cationique = échangeur d'anions
* On utilise pas de tampons qui changent de pH.
* Les nacelles ne changent pas le pH de la solution.
* En augmentant la quantité de nacelles on augement la concentration en Na+ et Cl-. Échange ionique! Pas par compétition. 
*  Augmente la force ionique de la solution.
* 3 pics :
    * Faiblement chargées + ou _
    * Moyennement + ou -
    * Fortement chargées + ou -
* On va aussi retenir les A.nucléiques (très fortement chargés négativement) et seront élués tard dans la chromatographie. On effectue en premier lieu un lavage qui va retenir toutes les protéines chargées positivement (basiques), on commence ensuite l'éllution.
* Tous les A.nucléiques de longues tailles vont être élués à des concentrations bien supérieures à celles qu'on a utilisées pour les protéines (on peut donc séparer prot et A.nucléiques)
* DEAE/ Q cépharose: chargés positivement donc échangeur d'anions
* **PO3++** 
* Faire des tests avant la chromatographie  
    * Tampon optimal (en gris prot) ; résine au fond, on prend des bactéries, on fait la lyse des bactéries, on test et quand on voit qu'il n'y a plus de protéines dans le surnagent, on sait qu'il s'agit du bon pH pour l'absorption, si on augmente trop le pH : difficulté à éluer la protéine. 
    * Déterminer la concentration en sel pour la liaison et l'élution de la protéines, toujours du sel dans le tampon, si 'on en met trop : la protéine ne va pas se lier car déjà trop de contre ions dans la solution pour qu'elle soit neutralisée.
    * A partir de 0,3 M : suffisant pour éluer la protéine.
    * Il faut déterminer la capacité de la résine, cb d'ions la résine peut lier. (cb de protéine on peut lier??)
* On peut avoir un volume très grand, et une quantité de volumes de 50 ml, tous les 100 ml d'extraits bruts car la résine a une capcité de liaison élevée, si la quantité de prot dans 100 ml assez basse pour que la résine puisse toutes les liées. Si la résine pour ces 100 ml arrive à saturation (tous les groupes liés) si on augmente le volume, la protéine passera au travers des 50 ml, donc on a saturé la résine on a atteint la capacité maximale, le volume d'échantillon n'est pas limitant dans la capacité de la résine. 
* Élution par palier (peut être utilisé comme première technique de purificatoin sur un extrait brut). 
    * beaucoup plus rapide 
    * Perte de la finesse dans l'élution (on se sépre plus aussi bien les protéines de charges différentes entre elles.
    * (On monte d'un coup la concentration)
* Mélange de 3 prot, selon le pH et le type d'échangeur on va avoir une bonne séparation
* 3 pics mais pas résolus entre eux (trop de chevauchement) comment améliorer la résolution de ces pics ?
    * Changer la pente du gradient (plus de volume de tampon à une certaine force ionique pour que la protéine soit totalement éluée avant que l'autre protéine soit éluée).


###### Chromatographie par intéraction hydrophobe
* les ions vont être hydratés avant les prot --> solting out
* On peut aussi utilier cet effet (expose les domaines hydrophobes) pour que la protéine puisse interagir avec un ligand hydrophobe fixé sur une résine. (Après sulfate d'ammonium, Technique utilisée lorsque la protéine se trouve dans le surnageant, on peut faire passer cette solution contenant la protéine ).
* Sulfate/ammonium etc : favorise l'exposition des groupes hydrophobes, (NH4)2SO4, Sulfate d'ammonium : dénature très peu les protéines donc très utilisé.
* Si chaine aliphatique est longue, plus il y aura d'intéractions hydrophobes
* Il faut que la concentration en sel soit suffisament importante pour ne pas que la protéine précipite une fois éluée.

###### Chromatographie d'affinité
* Adsorbant de spectre large : p.e les protéines qui interagissent avec l'ADN
* On peut aussi avoir des ligands spécifiques
* GST : fusion, pour pouvoir efectuer une purification d'activité de protéines qui ne possèdent pas de ligands.
* MBP? Dans les deux cas on obtient des protéines de fusion par clonage, on améliore la solubilité et permet de faire une purification
* ENtre l'étiquette et la protéine de fusion toujours une protéase spécifique d'une séquence pour effectuer le clivage entre la protéine d'intéret et l'étiquette.
* 6 histidines : la plus petite
* GSP : 25 kDalton (relativement petite), 
    * on peut aussi essayer de faire tomber les interctants de la protéine d'intéret 'pool down' ; complémentarité avec le glutathion (tripeptide)
    * Nécéssite peu d'étapes de purification
    * Élution par competitionavec du glutathion libre
    * EN stabilisant la structure : évite que les structures s'aggregent entre elles donc améliore la solubilité
* Gel d'éléctrophorèse
    * soit résine saturée
    * soit protéines de même taille
* Le volumede de l'échantillon n'est pas limiant, no peut faire passer de l'échantillon sur la colonne jusqu'à ce qu'il y ait saturation.

* Faire repasser une deuxième fois l'éluat
* On fait des lavages assez longs car l'intéraction est spécifique (une protéine d'intéret avec une étiquette ou une classe de protéines) quantité de protéines assez limitée. Grande quantité de protéines contaminantes qui ne vont pas être retenues, donc pic de lavage très grand. L'élution : on utilise en général un ligand libre 


###### IMAC
* Affinité entre les histidines et le nickel (ou le cobalt) de la résine
* EDTA ET EGTA vont chilatés, si on a trop d'EDTA ou EGTA on va chilater le nickel (et donc il n'y en aura plus).
* Baisse de pH permet l'élution (si on baisse le pH, favorise l'élution) On ne fait jamais l'élultion par baisse de pH, on augmente la concentration d'imidazole dans le tampon.

###### Chromatographie par exclusion moléculaire
* Plus d'intéraction entre la protéine et la phase solide : résine doit petre inerte vis à vis de la protéine.
* Toutes les protéines plus grandes passeront dans l'espace extre granulaire : toutes les molécules qui ont une taille supérieure aux diamètres des ports : vont être eclus.
* Toutes les petites molécules peuvent emprunter tous les chemins possibles : elles emprunteront le chemin plus ong et sorteront en dernier. Leur volume d"élution est égale au volume total de la colonne.
* Vont être éluées de la plus grande à la plus petite.
* Se base aussi sur l'encombrement de la molécules en question
* Les pores sont fractionnées avec le poids de la molécules. 
    * limite supérieur du domaine de fractionnement : complexe multimoléculaire de 600 kDa de diamètre (considéré comme globulaire)
* Toutes protéines qui se trouvent entre ces deux valeurs vont être éluées dans l'ordre décroisssant du poids molécularie
* Les colonnes de chromatographie sont étroites et très longues, volume en résine important : 330 ml
* Pour une bonne séparation : le volume de l'échantillon est limitant et ne doit pas dépasser les 2 % du volume de la phase solide (ici 6 ml). Au delà de 6 ml : on perd en résolution
* COmmme le maillage des billes permet la séparation. Si peu de solution : toutes les protéines vont se séparer en meme temps dans la colonne. Si no a un peu d'échantillon qui est entré il y a d'autres molécules qui ne sont pas rentrés on va alors avoir des piques très larges et chevauchant. On aura une meilleure résolution. 
* Le tampon ne change pas  : c'est juste un maillage qui va séperer les protéines en fonction de leur taille et de leur encombreùent.
* On ne peut pas séparer un échantillon trop complexe 
* Technique à ne pas utiliser en début de purification
* Optimal comme dernière étape de purification
* Échantillon au départ : petit volume et assez concentré, à la sorti de la colonne : échantillon dilué
* Dans ce cas : volume mort ; : correspond aux billes qu'il n'entrent pas dans la phase solide.
* Limite inférieure du domaine de fractionnement : diamètre d'une protéine de 10 kDalton
* 4 et 5 : éluées en même temps 
* V0 : molécules qui se sont aggrégées entre elles et 

* Déterminer le poids moléculaire d'une protéine (pas encore caractérisé)
    * On connait le poids moléculaire d'un seul monopeptide mais on ne connait pas encore comment elle est exprimée
    en condition native (après la purificatinon)
    * Ce qui nous intéresse : c'est le volume interne ou vide 
    * KAV : on considère le volume totale moins le volume mort. Mais ces deux volumes sont meurés par des
    * Volume total = vole mort + volume interne

* Relation linéaire : pour un certain 
    * Si on calibre la colone avec des protéines de poids moléculaires connus
    * On peut déterminer le poids moléculaire d'une protéine.
    * Dessiner la droite dans le domaine de fractionnement, car toutes les protéines wqui ont un volume d'élution égale au volume mort aurons 
    kav = (vm - vm) = 0
    * Toutes les protéines qui sont inférierues à 10 auront un carré de 1 : 
    * également possible avec le volume d'élution. 
    * Si SDS - Page = 32 kDa poids moléculaire dénaturant
    * 8EC = 128 kDa poids moléculaire natif, combien de sous unitéà à 32 kDa donneront un poids moléculaire de 128
    * En comparant les résultats des conditions natives et dénaturantes on peut comprendre l'agregation de la protéine.

    ###### Stratégie de purification
    * Dasn les autres cas (autre que affinité), toutes les protéines peuvent être hydophobes à une certaine force ionique, protéine d'intéret au mileu d'autres protéines qui ont les meme intéractions. 
    * Capacité importante au début
    * Vitesse : car bcp de proréases dans l'échantillon
    * Résolution : important en fin de purification et en phase intéremédiare.