#  Cours 1-4

* Les enzymes de restriction BamH1(ggatc/c) ET bGLII (AGATC/T) sont compatibles :elles n'ont pas la même séquence de reconnaissance gènere des extrémités sortantes qui peuvent se réappariées : donc ce ne sont ni des isos(?) et des néoschizomère(coupe à des endroits différents)
* A partir d'où n'a t-on pas encore isolé d'ADN polymérase thermostable?
* Quelle est la vitesse d'élongation de l'ADN polymérase la plus rapide (en secondes/kb?) 1 sec/kb
* SDS : dénature les protéines
* éthanol : augemente la fiation sur la résine de silice (pour fixer l'ADN)
* Sulfate d'ammonium et polyéthylène glycol peut être utiliser comme précipitant non dénaturant.
* Bradford : doser toutes les protéines indépendamment des autres macromolécules, aux UV la concentration va être parasyter par les acides nucléiques, on évite d'utiliser cette méthode.
* Donc utiliser le Bradford à 595 nm en début de purification.

## Critères pour le choix de la polymérase
* Stabilité thermique
* Taux d'extension : vitesse à laquelle elle ajoute de nucléotides par seconde vitesse moyenne de 1kb/min à 72° les plus récentes jusqu'à 3 ou 4 fois plus vites
* Erreurs ? nombre de nucléotides incorrectes par milliers de paires de bases : les polymérase modernes sont capables de corriger les erreur "'s
* La processivité : la probabilité que la matrice se détache?

## Comparatif des ADN polymérases
* Taq la moins stable mais supporte un PCR à 95 ; celle qui fait beaucoup d'erreurs ; on l'utilise pour vérifier que qu'un gène a bien été inséré dans un vecteru ; taux d'extension élevé mais Deep Vent plus rapide
* KOD polymérise plus de nucléotides avant de se détacher ; activité exonucléase --> permet de corriger les erreurs
* TRansférase terminale : elle rajoute à la fin du produit de PCR ; ajoute des nucléotides en plus sans amorce

## Banques d'ADNc
* Avant l'ère du clonage : à partir de 30 kg de levure : 200 mg de polymérase
* Souvent des erreurs ; il faut vérifier avant d'utiliser les clones

## Le séquençage de génomes
* très nombreux

## Le clonage
PEG sert d'agent d'encombrement : favorise la réaction (ce n'est pas un catalyseur) ; purifier le produit de ligation puis transformer des bactéries, puis étaler ces bactéries sur un milieu de avec un antibiotique pour trier les bactéries qui ont intégrés le plasmide.
* clonage gibson : dérivé de clonage par ligation
* gateway : très efficace mais aussi couteuse par rapport aux autres

### clonage par restriction : le plus simple ; consiste à mettre un gène 
pb : l'insert ne contient pas les sites de restrictions ; le moyen le plus facile de les ajouter : la PCR, il faut donc utiliser des primers; Spacer : quelques nucléotides avant la séquence **Pourquoi?**
* Choix der ER pour le clonage :
    * Deux enzymes : on perd des sites de restrictions ; il faudra utiliser des sites encore plus en amont, utiliser cettes méthode quand il y a **???**
* La mise au point des amorces de PCR 
    * Les amorces font entre 18 à 30 bases
    * La température de fusion : température à la quelle on va chauffer pour la mise en place des amorces
    * contenu en GC : 
* L'amorce Forward
    * Le site de restriction doit être absent du gène et présent dans la cassette de clonage
    * Les extensions en 5' c'est pour ça qu'il y a des espaceurs , l'enzyme de restriction va se fixer a besoin de place pour se poser, si on ne met pas d'ADN de ce côté, l'enzyme de restriction ne va pas réussir à se poser sur l'ADN, la séquence importe peu. Le nombre de nucléotide nécéssaire est différent pour chaque enzyme de restriciton ;beaucoup prennent une moyenne de 6 --> coupe efficacement dans 99% des cas.
* L'amorce réverse : = site de restriction / 
    * Particularité : Codon stop 

### Le clonage TA
* pCR 2.1 : vendu déjà linéarisé ; le gène se met directement dans le vecteur pas d'enzymes de restrictions etc.. ; voir carte schématique ; permet de mettre rapidement un produit de PCR dans un vecteur pour pouvoir l'amplifier/stocker.

### Le clonage sans ligation 
* longues extrémités sortantes un peu plus d'une 20 de bases; on ajoute un dNTPs (un seul!) pour ne pas qu'elle ne "bouffe" tout ???
* Évite l'utilisation d'une enzyme de restriction, mais on va devoir générer un produit de PCR avec des amorces assez longues.

### Le clonage gibson
* Des séquences qui supperposent (d'une 20 t de paires de base); on ajoute une polymérase ; plus efficace que la méthode prècedente où on laisser les cassures
* 3 enzymes à utiliser

### Le clonage IVA (In vivo Assembly)
* Pas d'étapes de digestion / ligation
* on peut tout faire en une suele étape de PCR
* On met au point une paire d'amorce qui va amplifier le gène, avec en plus des séquences qui se retrouvent dans le vecteur, et on prépare une deuxième paire d'amorce, et on réamplifie le vecteur (on fait 2 PCR) en meme temps dans le même tube --> 2 plasmides -> les 4 amorces (doivent avoir de TM équivalents). 
* Grâce aux propriètés de recombinaison de la bactérie : la bactérie se charge de fabriquer le vecteur cible. 
* Dpn1 Cette enzyme a la particularité de digérer uniquement des vecteurs méthylés donc produit de départ méthylés mais le vecteur ne l'est pas; les seules plasmides qui permet aux bactéries de resister sont ceux dont l'insertion a été réussies.
* --> On fait deux réactions de PCR soit séparément soit ensemble aec les deux vecteurs de départ. Soit on obtient séparement soit on mélange les deux produits, on transforme la bactérie et la bactérie se charge de produire le vecteur. 3 possibilités : insertion: déletion/ et mutagenèse avec un vecteur de départ qui contient déjà l'insert.


### Le clonage Gateway par recombinaison
* Utilise des sites spécifiques ?
* AVantage : on a un vecteur de départ avec le gène , on veut le mettre dans un autre gène de destination ;  on ajoute l'enzyme ; soit enzyme ??? ;  permet d'intervetir les inserts entre les deux vecteurs, beaucoup plus chères que les enzymes de restriction.

### Les souches d'E. Coli pour le clonage
* La plus utilisée DH5 alpha : pas de résistance de base pour un antibiotique
* Top10 : comporte déjà une résistance à un antibiotique
L'avantages de ces souches : absorbe facilement l'ADN

## L'extraction d'ADN plasmidique 
* L'extraction se fait par la **Mim???** ; lyse alcaline; très simple;  désavantage : on ne sait plus comment ça marche
// Voir cours
* ADN génomique s'est réapparié n'importe comment et forme un précipité blanc ; il faut mélanger par inversion, une dizaine de fois, il ne faut pas le casser car risque de mélange de l'ADN génomique et d'ADN plasmidique dans le soluté.

## Test d'alpha complémentation
* La cassette de colnage est inséré dans la séquence codante lacZalpha, pourquoi négatif, car quand on va cassé on va interrompre la séquence codante de lacZalpha, pour cela on met à profit les propriétés l'opéron lactose. 
### La beta galactosidase 
* protéine qui fonctionne sous la forme d'un homotétramère.
### Le répresseur Lac 
### L'alpha complémentation
* Celle qui nous intéresse : les blanches : celle qui n'ont pas le plamside (à vérifier); qui contiennent le gène car lacZ alpha est interrompu ; Les bleues: un lacZ alpha non intérrompu donc fonctionnel.
* On a quand même toujours du vecteur non digéré qui contamine, donc beaucoup de colonies qui contiennent le plsamide sans insert. Le faire migrer sur un gel d'aggarose et découper, mais même avec cette technique, jamais du 100%.

## Choisir le système d'expression
* La bactérie E.coli ; le plus commun, le plus pratique et le moins chère
* Levure ; système eucaryote ; unicellulaire assez facile
* Cellules d'insectes infectées par un baculovirus : système eucaryote plus complexe que la levure 
* Cellules de mammifères
### Comment choisir
* Protéine soluble quand on l'exprime dans E.coli?
    * Dans tous les cas essayer avec E.coli (prends 2 semaines) ; les autres sytèmes plus compliqués
* Est-ce que la protéine a beosoin de PTM?
* Quelle est l'utilisation des codons de la protéine?
    * Dans coli on peut ajouter d'autres plasmides
    * Une autre solution : faire synthétiser complètement la protéine en synthétisant tous les codants ; on demande à une entreprise de produire tout l' ADN. si l'ARN forme des tiges boucles trop stable : peut être éviter en designant l'ensemble de la séquence d'ADN.
## Escherichia coli
* Aucune transformation post-traductionnelle
* Comment intégrer le plasmide ? --> toutes ces méthodes existes 
    * par changement de température
        * D'abord dans un petit volume puis dans un grand volume + solution de cholorure de calcium pour fragiliser 
        * On congèle à -70°
        * Transformation chimique : on laisse incuber30 min sur la glace, on incube à 30 ° pendant 5 à 30 minute et on le remet dans la glace
    * par éléctroporation
        * Décharge éléctrique qui fragilise la membrane, mais avec le sel danger qu'un arc éléctrique se forme et casse les cellules, évite de faire un purification du produit de ligation
        * 1 min 180 W
    * par onde de choc
        * Explosion, qui facilite la transformaiton
### L'efficacité de transformation
### Les promoteurs
* T7lac permet de réduire l'expression basele, si on utilise T7 la bactérie E.coli ne devrait pas être capable de produire la protéine, la polymérase ne doit pas reconnaitre le produit, on introduit le T7lac pour ne pas qu'il y ait production en permanence, on veut que la bactérie se multiplie et quand il y en a beaucoup lui donner le signal.
La bactérie reconnait un peu le promoteur T7 on peut mettre le galactose en amont
Lac I code pour le répresseur, en temps normal empêche T7 d'être reconnu par la polymérase, et on a mis le gène de la T7 sous dépendance du répresseur LAC, la polymérase ne pourra pas se fixer et produire le T3. IPTG bloque le répresseur ce qui permet la synthèse de la T7 : on bloque le système de double répression grâce à l'IPTG.

## L'expression
### Lire la carte d'un vecteur
* thrombin coupe le peptide entre l'arginine et la glycine ; T7 terminator primer ; T7 promoteur primer ;
* On va quand me avoir des acides aminés avant et après. Si on ne veut pas faire une protéine de fusion avec une étiquette, il faut toujours bien choisir le vecteur qu'on veut utiliser en fonction de la protéine qu'on veut produire.

* Souche pour l'expression couramment utiliser : BL21 ( pas les même que pour le clonage)
* Opérateur lac garantit qu'on va avoir de l'ADN polymérase qui va être fabriqué.
* Variant pLysS permet d'exprimer le lysosyme T7 qui va inhiber la T7 ADN polymérase permet d'inhiber la production de protéine, avantage quand il y a un élément toxique pour la bactérie.

### Production dans E.coli
* Petit volume au début
* phase stationnaire : lorsqu'il n'y a pus assez de substrat
* moment pour produire la protéine d'intérêt (fin de phase exponentielle) densité optique de 0,5/0,8
* Se fait dans des 

### Paramètres de cultude 
* fiole conique

### La levure
* Utiliser promoteur Galactose

### En cellules d'insectes
* Médié par un virus de l'insecte
* Lytique : à la fin du cycle du virus, la chenille est entièrement dissoute, autofagia california, ce virus infecte la chenille déroute son système. Polyhédrine christaux de polyhédrine 
* Bacmid : contient tout le génome du virus
* On récupére tout le génome du virus, on extrait avec le kit des cellules bactériennes, ça va induire dans les cellules d'insecte la formation de nouvelles particules virales, puis utiliser pour infecter de nouvelles cellules d'infectes. 

### En cellules de mammifères
* Cellules embryonnaires hummaines de reins, soit cellules ovariennes d'Hamster, si on fait de la transfetion transitoire (sans l'intégrer au génome), mais il faudra refaire une transfection 

### Expression de protéines --in vitro--
* Produit de très petites quantités, mais permet d'utiliser une matrice, on peut exprimer des protéines toxiques, on peut incorporer des acides aminés non naturelles, quantité très faible  et très chère.

## Extraire la protéine d'intérêt

### Lyse chimique
* Pas la plus utilisée car nécéssite de commander des produits de lyse systématiquement alors que mécaniquement permet d'utiliser des machines
* Détéergents non ionique ou zwitterionique qui modifient la solubilité des protéines libérées, fragilise la cellule en cassant la paroi ou la membrane cellulaire
* Les bactérie gram + avec une seule membrane : facile
* gram_ plus compliqué
* Levure : membrane plus épaisse donc produit de lyse plus agressif
* Lyse chimique combinée à une lyse enzymatique qui vont dégrader complètement l'ADN, utilisable pour toutes cellules. 
* Le lysozyme : il existe des versions modifiées pour en améliorer l'efficacité ou la stabilité dans les conditions ou on fait la lyse de cellules.
* La DNase1 : permet de couper (l'ADN simple brin??)
* La benzonase : nucléase qui coupe toutes les formes d'ADN et d'ARN (meilleure élimination des A. nucléiques)

### Lyse enzymatique
//
### Lyse mécanique
* Utiliser des extra sons, la sonde est plongée dans la solution, les ultrasons sont envoyés dans la solution qui créent des microbulles dans le mélange qui vont éclater et en éclatant les cellules qui sont à côtés vont se casser. casse la paroi cellulaire et fragmante les A.nucléique. Se fait dans un caisson isolé ou un casque de chantier pour éviter d'abîmer les tympans.
* Par pression : appuyer avec un piston très fort puis sous pression cassure des cellules
* Par billes de verre : on met des billes dans la solution de cellules, on ferme, l'appareil va secouer très fort et va également se refroidir très fort, les billes en se cognant entre elles vont s'écraser entre elles. On récupére ensuite les billes et on a notre lysat cellulaire.
* Les acides nucléiques sont responsables de la viscosité après la lyse des cellules.

## Purification de protéines
* Les protéines sont fragiles car structure tri dimensionnelle maintenues uniquement par des intéractions faibles non covalente (pont disulfure?? what)
* Les protéines vont être soumises à des conditions qui peuvent altérer leurs structures (changement de solution/dilution) protéines exposées aux protéases, à l'oxydation, aux surfaces et à des variations de températures.
* Pour s'assurer que la protéine est dans de bonnes conditions : voir cours

### PH
* Chaque protéine a un PH d'activité optimum
* pourquoi une solution tampon? si on met de HCL dans l'eau on va avoir une dissolution complète de l'HCL qui va modifier le pH. Si on met une solution tampon, acide faible va raremetn se dissocier dans l'eau et la base conjuguée va rarement arracher un ion H+ pour faire l'acide en réagissant ensemble les deux espèces ne changent pas la concentration. Si on ajoute un HCL, l'acide va être capté par la base conjugué.  ---> Le pH NE VARIE PAS.
* Si on continue d'ajouter de l'acide on va continuer à titrer la base jusqu'àce qu'il n'y en ait plus et le PH va commencer à varier.

### Le choix du tampon
* Voir cours
* Les effets non spécifiques
* tris : très utilisé car il ne coute pas chère tamponne entre 7 et 9 ( pka 8 ), désavantage : son pka est sensible à la températude et il interfère avec le test de Bradford (pour quantifier les protéines), il faudra utiliser une autre méthode que ce tampon

### Protéolyse et inhibiteur de protéase
* éviter la dégradation des protéines une fois que les cellules sont lysées.
* on a toujours de la protéolyses partielles
* Portéolyse est la plus forte dans les premières étapes de purification et on les élimine au fur et à mesure
* On élimine aussi le nombre de cibles potentielle pour la protéase --> il faut s'assurer qu'il n'y en ait plus à la fin
* On met beaucoup d'antiprotéase au début et plus trop à la fin
* il existe deux catégories 
    * endopéptidases : site de coupure spécifique (étiquette de solubilité thrombine)
    * exopeptidase
* Inhibiteur de protéases 
    * Aprotéinie contre les protéases à sérine
    * petites pillules qui contiennent un mélange de protéases
    * EDTA : piège les métaux lourds, si la protéase a besoin de ce métal, la protéase est inhibée.

### Les agents réducteurs pour contrôler l'oxydation
* B-ME
* DTT
* TCEPE-HCL
* Ont tous la même action mais diffèrent par leurs stabilités et leur prix
* Le B-Me est moins chère, sent mauvais et pas très stable
* DTT plus stable sent moins mauvais et plus chère
* TCEP-HCL : en fin de purification volumes pas trop élevés on peut  le TCEP qui est plus chère

### Les détergents
* Surfactant : agents qui réduisent la tension de surface
* SDS : dénature les protéines plutot que pour éviter l'aggrégation ; c'est un détergent anionique; on utilise ces propriétés pour pouvoir faire l'éléctrophorèse.

### Stockage des protéines à 4°
* Voir cours

### Stockage de longue durée
* congeler rapidement
* Ne dénature la plupart du temps pas la protéine, faire des volumes très petit de l'odre de quelques microlitres
* Ajouter du glycérol pour stabiliser
* On peut ajouter une protéine qui permet d'éviter qu'une protéine se fixe sur les parois du tube

## La concentration et dessalage des protéines

### Concentration et dessalage par dialyse

#### Principe
* Séparer les molécules en fonction de leurs tailles par diffusion
* Principe :
    * Membrane : hachuré ; de l'autre côté solution avec une autre composition on veut que les boules jaunes se retrouvent de l'autre côté, une fois qu'il y a autant de triangle vert d'u côté ou de l'autre (**???**)

#### Facteurs qui affectent la dialyse

#### Dialyse en pratique
* Volumes
* Petit volume contre un grand volume
* la dialyse se fait dans un boudin
* on la déroule (membrane sèche), on a des pinces, on pince la membrane d'un côté et on remet ça dans le volume à dialyser, gros becher pour qu'il y ait un volume de 1 
* La dialyse ne sert pas à concentrer, uniquement à l'échange de tampon

#### La diafiltration
* On veut éliminer le sel (bleu)
* Méthode pour concentrer et déssaler : 
    * On dilue avec du tampon sans sel, remettre de la solution et centrifuger une fois qu'on a la bonne concentration en sel, on continue à réduire le volume et la concentraiton en protéine augmente. 
    * Il faut éviter que les protéines s'écrasent pendant la centrifuge, la membrane est tangente, solution pour éviter cela.
    * Méthode pour concentrer et déssaler : 

### Par cristallisation
* cristal = protéine pure
* Comment obtenir un cristal à partir d'une protéine en solution?
    * diffusion de vapeur : méthode la pus courante
    * SUr le diagramme : on a au départ la protéine on la mélange à une goutte de solution de précipitaion, la partie violette : c'est la partie du diagramme où la protéine est soluble, avec le sens interdit : précipité, zone verte : zone haute : christaux se forment, zone basse : christaux vont continuer à croitre, on met le mélange dans une enceinte fermé au dessus de la solution de précipitation, avec le temps le volumen de cette goutte va diminuer, la goutte en réduisant de volume, on revient à la concentration initiale en précipitant et en protéine, concentration va diminuer jusqu'à avoir un cristal de protéine.
* Structure d'un cristal ?
    * protéine ordonée
    * taille maximale : 1/2 centimètre
    * Le plus souvent : christaux de l'ordre du milimètre voire plus petit
    * Source de rayons X importants donc pas besoin de grand christaux

## Concentration de protéines
### Méthode par spectroscopie d'absorbtion
* Loi de Beer Lambert

### Les méthodes colorimétriques
* Le test de Bradford
    * Utilise le Bleu de Coomassie
    * Déplacemement chromatique de 465 à 595 nm
    * Méthode fiable pour des concentrations assez faible, la gamme de linéarité réduite
    * tampon tris : peu perturber le test
* Le test de Lowry 
    * utilise le réactif de Biuret
    * deux étapes : on fait réagir le premier réactif puis amplifier le signal avec un second réactif
    * Méthode séquence dépendante
    * Depuis sa mise au point on l'a modifié pour réduire sa sensibilité à certains composés
* Le test BCA
    * La première réaction utilise toujours l'ion cuivre
    * La réponse est plus linéaire 
 Ces méthodes ne s'utilisent pas toutes pour les même concentration

### Éléctrophorèse

#### Méthodes de coloration
Gel de polyacrilamide : meilleure résolution
