* Utilities: en ligne de commande, i-search/i pody/ dvript shell-> Il y a une commande pour installer les outils du ncbi, à l'issus du processus nouveau logiciel -> dire où sont les outils. 
* e-fetch avec paramètres : on est plus obligé d'utiliser curl -s
* Web service REST (où tout est décrit dans l'adresse) : web service = service fournit par un site web, les sites webs sont devenus programmables, avec l'évolution du web, les gens se sont dit : notre site web pourrait fournir d'autres services qu'une page web : exemples : calculs de BLAST etc. Pour ces services il y a des protocoles de communication. Ce protocole est devenu le service REST.
* E fetch : a le droit d'explorer la base de données suivant les critères, (facilités) à distance.
* On peut installer en local une base de donnée (= site mirroir) on va directement à la source grâce à cela et n'empeche pas les gens qui ont des demandes plus modestes d'y accéder. 
Curl et w -get (équivalents mais w -get a son propre paramétrage).
* Permet de générer une serie d'adresse et permet de télécharger 2000 adresses (automotisation).
* Automatisation : comment on peut aller 
* * Web service : en théorie on peut à partir d'outils de base, on pourrait avoir une plateforme de bioinformatique à partir d'un ordinateur banal, on pourrait à partir d'une simple commande faire un blast etc. Des sites fournissent beaucoup de possibilités (algorithmes) qui calculent et on génére des données
* Possibilités de générer des données en live, le site web est presqu'une coquille vide, site qui propose de donner un avis ou de recherches un avis, avec photo et localisation, permet de mettre en relation un site avec la localisation, avec google maps, qui permet de référencer, permet d'assembler tout cela dans un site. 
* Ce ne sont pas des outils en ligne de commande, ce sont des addresses que l'on utilise. 
* Get : tout est écrit (on utilise GET lorsqu'on clique sur un lien) et POST(ici on a un formulaire et on veut envoyer un maximum de données) On peut aussi soumettre des données sous forme de fichiers. Cette manière d'intéragir, on peut égalemet la faire avec get. Cette manière d'utiliser curl permet d'envoyer un volume de données beaucoup plus importants, pour des web services qui n'ont pas besoin de seulemetn quelques données données (si on souhaite faire un BLAST par exemple). (URL API)
* Flux RSS. Si on fait une application ou faire un programme
* xml --> on a 95% de l'information, et est associée à de la mise en forme : les données doivent être balisées. Format suisse_prot / pdb = format PDB, chaque ligne démarre par un mot et correspond à l'information qu'on va avoir. 
* CSV : format généraliste = format qui signifie comma separated view, permet de décrire des données au format table, les lignes sont séparées en colonnes et on a des délimiteurs, (il faut choisir un caractère de séparation qui n'est pas dans les données). RTF autre format des données
* Sites qui fédérent toutes ces initiatives
* FTP = file transfer protocol ; client (logiciel qui demande des services à un serveur FTP) 
* Torrents (lié au piratage) mais c'est une technologie pour optimiser et paralyser les protocoles de grands volumes de données. 
* fichiers binaires et textuels (tout est binaire en anglais, données textuel= encodage = code informatique qui est destiné à être lu comme du texte). 
* scipt shell =ensemble d'instruction dans le langage de la ligne de commande
* terminal = interface graphique pour la ligne de commande. Un shell = famille de lignes de commande //Bash, le nom du script porte le nom du langage.
* Dans un script shell le langage lit et il y a un temps de conversion (pas de compilation). Avant le script était beuacoup plus long, mais aujourd'hui équivalent avec un fichier binaire.
* Format FASTA : uniquement la séquence


## Cours 2

* GFF format (general feature format) 
* Le format d'élément général, general feature format (gene-finding format, generic feature format, GFF) est un format de fichier utilisé pour décrire les gènes et d'autres éléments de séquences d'ADN, d'ARN et de protéines. L'extension de fichier associée à de tels fichiers est .GFF et le type de contenu qui leur est associé est text/x-gff3 . 
* Viena package (dans le cas des brackets : un programme doit pouvoir repérer qui est apparié avec qui (il faut retrouver des paires qui sont appariées))
* Egalement format table, format bpseq, redondance de la formation, car on repertorie deux fois 
* Awk outil qui sait bien reconnaître les champs, est capable de faire les même choses que egrep. Outil très rapide, écriture avec très peu de caractères de ce qui pourrait être écrit avec beaucoup de commandes en python  (ou autres script)
* format json : décrire des objets informatique
* xml = langage à balises (parent du html)
* grep/sed et awk : pas d'arborescence car ligne par ligne : ./jq
*  Format arbre : pas suffisant, on peut avoir des données qui peuvent avoir des liens sans héritages. => graphe
   *  Permet de mettre des liens unilatéraux (orientés ou pas)
* RDF(ressource description framework) -> encode la sémantique et permet de définir une ontologie = vocabulaire controlé
* Corpus de codes (biostar) avec quelques commandes : permet d'avoir un grand nombre de données.

