&# Cours 4_5_6
## Historique des BD
* 1980 --> Approche UML : permet de faire appelle à des diagrammes qui permettent de modéliser (il s'agit d'une formalisation, pas d'un programme ou d'un langage!)
* Au fur et à mesure du temps, organisation des fichiers beaucoup plus complexes --> introduction du modèle relationnel en 1980. Ajdh : combinaison des modèles (ex : oracle)

## Différents modèles
* Le modèle hiérarchique : modélise une entreprise (chaque département est génré par un manajeur qui gère un projet qui est composé de plusieurs taches), pour acceder au projet il faut forcément passer par la compagnie ; relations de 1 à plusieurs --> dans une compagnie : plusieurs types de départements. Si un département ne peut avoir qu'un seul manager (relation 1:1).
* Le modèle réseau est dérivé du modèle hiérarchique ; on va d'un seul sens pour accéder à la racine, on peut naviger parfois dans les deux sens. On a des relations de 1 à plusieurs mais aussi de plusieurs à plusieurs. 
* Le modèle relationnel = bases de données SQL (trés structurées / rigides) : deux tables illustrent le diagramme ; concept : repose sur plusieurs concepts --> la notion de relation (matérialisé par une flèche) les clés primaires , notion de dépendance p.E --> catégories classe et salaires (dépendances fonctionnelles), à partir de ces modèles on peut modéliser des systèmes très complexes. Opération entre ces deux tables peuvent être faites. (Théorie des ensembles) --> à voir plus tard
* Dans ces 10 dernières années : bases NoSQL (moins rigides) --> moteur de recherches google ; systèmes qui permettent d'indexer ; ex = Lucène. 
* Bases de données objets --> ce sont développés car les développeurs essayer de rassembler dans une entité toutes les propriétés ; ne pas découper les tables en petit morceau ; reconstituer l'objet ; inutile de normaliser à outrance. 

## Bases de données existantes

## Modélisation

* En 3 étapes 
    * Étape 1 = Modèle conceptuel des données : Diagrammes (supports que l'on peut utiliser pour présenter une projet de façon simple à une équipe p.e).
    * Étape 2 : à partir de ce MCD. Libreoffice base on doit établir des relations (UML)
        * Conception avec Merise : on décrit les relations avec les entités identifiées ; on introduit des clés primaires et des clés étrangères, implémenté en utilisant le langage SQL
        * Conception avec UML
            * Historique de l'UML : mutliples initiatives et fusionné en 1 seul nom pour arriver à UML3 
            * Modélisation UML : notation graphique (on mannipule des entités) ex : enseignant ; barèmes etc on a des classes si qqn intéragit peut être défini comme un acteur et on peut modéliser un système. & partir de cela on peut construire des diagrammes : ex les diagrammes de classe
            --> comparaison Merise =/ UML 
        * On peut connecter des ordinateurs avec des segments de fibres. 
        * (voir en détail la hiérarchisation sur diagramme UML)
    * Étape 3

### MCD : normalisation

* Compte 6 niveaux
* Dépendance fonctionnelle : dépendance fonctionnelle de catégories et de classes
    * --> nous permettra d'identifier les anomalies dans notre table
    * La relation
    * On peut avoir une clé primaire naturelle --> attribut qui appartient au domaine (numéro de sécu appartient à la personne)
    * Ou clé primaire artificielle --> généré automatiquement par le systeme (ex : num d'étudiant)
* Première étape : Première forme normale 1NF
    * Au niveau d'une table on ne peut pas voir plusieurs valeurs dans la même cellule
    * On crée autant de colonnes que l'on a de prénoms : cependant on ne va pas rencontrer un 4 ème prénom p.e
    * Autre solution : voir diapo

* Deuxième étape : Deuxième forme normale 2NF
    * Elle doit être en première forme normale au préalable
    * Relation de ce type : un enseignement qui a un nom un code
    * Dépendance fonctionnelle pour le nom et le numéro
    * Clé primaire : dont dépend volume et nom (dépendance fonctionnelle) 


### Construire un MCD

### Associations 

* Associations d'aggrégation : dépendance : si on supprime une salle on sera obligé de tout supprimer
* Arité : cardinalité de complément
* Héritage
    * En phase de conception on essaie de trouver des choses communes entre les entités. 
    * Enseignant hérite toutes les propriétés de personnel
    * Salaire() = fonctions (méthodes)
    * Héritage simple (on peut hériter de plusieurs choses)
    * Héritage complet : on ne peut pas ajouter d'autres entités qui héritent 
    * Héritage disjoint : le système ne peut pas être les deux à la fois, on a l'un ou l'autre

### Représentations 

* "Table" = stéréotype : prend un élément du langage et lui donne un autre sens
* Au niveau d'un MCD on ne soit pas obligatoirement avoir de clés primaires et étrangères; on aura plus besoin des relations

#### Traduction du MCD en MLD

* MLD ; création de clés artificiels p.E open office base. On fait apparaître une clé primaire, qui est soulignée

#### Transformation des associations

* 1 à plusieurs
    * Avion peut apartenir à une seule compagnie et l'avion qui peut posséder plusieurs compagnies

* Plusieurs à plusieurs  
    * QUand on a une realtion de plusieurs à plusieurs on introduit une nouvelle table pour décrire les relations, les clés étrangères matérialisent le lien que l'on avait au niveau du MCD.

* 1 à 1
    * Création d'une clé primaire que l'on trouve parmi les attributs et dans étudiants on fait migrer la clé primaire du stage, la modélisation a une implication au niveau des possibilités de recherche au niveau des applications futures. 
    * Si on peut naviguer dans les deux sens : si on a un stage on ne peut pas savoir quel étudiant est assigné à ce stage, alors on va dans la table étudiant et l'on cherche la clé étrangère correspondante.
    * on peut aussi créer une clé étrangère qui correspond à la clé primaire dans la table stage.

* Transformer une association reflexive
    * Exemple de relations de 1 à plusieurs
    * Chef de pilote : responsable de plusieurs pilotes
    * "#" = clé étrangère

### Transformation de l'héritage

* 3 possibilités
    * Décomposition par distinction
        * On garde les entités qu'on a dans le diagramme (PNC : personnel naviguant commercial ou PNT technique), on alie nom de relation, on défini les attributs comme précédemment mais différence : clé primaire de la superclasse et clé étrangère pour aller rechercher l'information sur le personnel
        * La même clé primaire est partégée par les deux tables
    * Push down
        * on ramène la superclasse vers le bas si on avait eu 10 sous classes on aurait eu 10 tables, las attributs changent en fonction du type d'entité
    * Push up (on regarde l'entité personnel et on remonte toutes les propriétés et les diverses sous classes, on perd en mémoire

### Transformation de l'aggrégation (à ne pas revoir)

## Projection

* Chaque colonne correspond à un attribut
* Obtenir une autre table avec les attributs V,W,Y,Z
* Si on a des articles, projections par colonnes ou par lignes, on sélectionne tous les produits supérieurs à 10
