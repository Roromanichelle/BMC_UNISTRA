# 
* Mécanismes moteurs : qui nécéssécitent de l'énergie
* Microtubules ; actine
* Filaments intermédiaires --> se déplacent mais utilisent les microtubules ou les filaments d'actine.
* Moteurs : dynéine / kinésine ; La dynéine se déplace sur les microtubules (vers l'extremité - des microtubules)
* myosine : c'est un moteur
* Exemple de structures mobiles : neurotransmetteurs (fonction motrice) ; (fonction neurale de neurotransmission) ; fonction régulatrice : tout doit être régulé --> multitude de possibilités fonctionnelles.

## Mouvements de cargos au sein du cytoplasme
* mannosidase qui a sa séquence avec la GFP, puis la protéine Sar1 sont la protéine codante a été fusionné avec la RFP. Quand le rouge et le vert se supperpose : donne une coloration jaune. Cela suggère que le protéines sont localisées dans les même structures. 
* On a du mettre en oeuvre de la biologie moléculaire et pour les observer une méthode de microscopie (ici microscopie à fluorescence). Pour visualiser le cargo in vivo il faudra faire des cellules transformées, trouver le bon promoteur etc. 
* Le DAPI traverse la membrane plasmique et marque la chromatine, dans une cellule vivante il peut être utilisé pour visualiser la chromatine. !Seulement pour les cellules animales!
* Dans une cellule de plante --> ne traverse la membrane donc faire une perméabilisation avec du trypton --> cellules mortes.
* Le DAPI s'observe avec une méthode de microscopie à fluorescence.
* Marquer les ARN rédioactivement. La radioactivité va être perçue par un capteur. Radioactivité utilisée pour suivre un mouvement à longue distance. (suivre le mouvmement de nutriments).
* Microscopie photonique : contraste de phase : le noyau
* Microscopie éléctronique : nécéssite cellules mortes
* Pour voir la chlorophylle : via fluorescence transmets une fluorescence rouge, lumière blanche --> verte ; une fluorescence simple permet de voir bouger la chlorophylle, de localiser les chloroplastes.
* Élodées --> très fine (transparentes) faciles à utiliser
* Épiderme : pas de structure particulière

## 
* Induire la dépolymérisation de l'un ou de l'autre
* Drogues connues qui dépolymérisent un cytosquelette plutot qu'un autre
* La colchicine : une drogue qui active le cystosquelette microtubulaire
* Immunomarquage : oblige de tuer les cellules
* Oryzaline : chez les cellules végétales ; quantité?
* Colchicine (dans matériel animal) mais à haute dose ; quantité?
* Tubudazole : herbicide aussi 
* Pour les filaments d'actine : ce qui dépolymérise l'actine ce sont les cytochalazine (bloque la polymérisation)
* La latrunculine : molécule très active qui dépolymérise le cytosquelette
* Sur la vidéo : dès 180 s on s'apperçoit que les molécules ne se déplacent plus. --> ici la latrunculine bloque les mouvements, elle a un effet immédiat, donc les molécules se déplacent grâce à la latrunculine.

## 
* Les cellules sont très souvent polarisées et n'ont pas les même 
* kinésines non conventionnelles qui vont remplacer une activité dynéine qui n'est pas présente. Les cellules de plante ne possèdent pas de kinésine, certaines kinésines vont permettre un déplacement non conventionnelle.
* l'extrémité - c'est alpha tubuline, l'extrémité + c'est beta tubuline.
* En schéma global; : la tête motrice est formée par une structure globullaire : derrière cette tête une zone flexible et deux chaînes légères qui vont se localiser à l'autre extrémité qui est l'extrémité C terminale.  (partie motrice en N terminale)
* L'arrivée de l'ATP génère le mouvement du bras de levier.
* Kinesin 14 : Moteur en C terminal 
* Chaque fois que l'on a un moteur dans les kinésines de 1 à 14 c'était une kinésine conventionnelle
* Rien qu'en connaissant la séquence on peut émettre l'hypothèse souvent confirmé d'un déplacement dans un sens bien précis.
* Famille des kinésines 13 --> domaine moteur central
* Système moteur a perdu son activité motrice mais a perdu sa capacité à se lier à une kinésine, MAP : propriétés de structure, rôle stabilisateur.
* Plantes supérieures --> ont perdu la production de flagelles
* Plasmodium (paludisme) --> posséde une dinéine donc ce n'est pas kinésine 14 qui fera le transport

## Kinésines
* beaucoup de séquences consensuelles qui s'associent aux kisésines qui peuvent nous donner une indication sur la fonction de ces protéines.
* Alexa vert couplée à des anticorps associé à une immunoglubuline secondaire qui va reconnaitre l'immunoglubuline primaire (porteuse de l'épitope reconnaissant la beta tubuline).  488 (utilisé pour voir du vert)
* Anti KCBP ()
* Microtubules du fragmoplaste (mitose), télophase (dernière étape de la difusion, on voit deux noyaux)
--> Cellulevégétale qui construit sa nouvelle membrane plasmique. ccl --> membrane qui sépare ces deux cellules
* + au centre et le - vers les noyaux ; pas de centrosomes chez les plantes ; le + toujours au centre et le - au niveau de l'équateur où il y a la membrane qui se forme (partie noire) --> mouvements vers le noyau

## Dynéine
* complexe mutiprotéique avec des chaines lourdes, des chaines légères et intermédiaires.
En bleu ciel = zone d'intéraction avec les microtubules. La chaine lourde possède cette séquence. Partie verte --> chaine lourde. Quand on regarde cette zone marquée en bleu vert et rouge. La molécule qui émane ici à l'extrémité, ce domaine de liaison de microtubules. Dynéine n'est pas directement en action avec les microtubules, mais complexe qui entre en intéraction avec les microtubules, qui va faire l'association avec la dynéine = complexe dynactine. En se focalisant sur la dynactine : structure en association avec de nombreuses protéines. Complexes formées d'une association ARP1 ARP11. Ce complexe forme une structure qui permet l'association avec le microtubule. 
* =/ types qui se situent soit dans des cils ou des flagelles. Axonem : le cil et le flagelle constitué de 9 doublets de microtubules et 9 doublets centraux. bBras internes ou externes de Axonème : dans la même boite de dinéine car les parties motrices très conservées. Structure complète va diverger car certaines s'associent à 3 têtes d'intéraction et d'autres à deux têtes. Transports intraflagellaires. Dynéines cytoplasmiques (celles dont on parle indépendamment de ? ))
* Dynéine : mouvements qui permettent de véhiculer des cargos qui vont traverser l'enveloppe nucléaire (dans des vésicules donc ), mouvements qui font se déplacer le noyau, facteurs de transcription, ARN messagers, des virus peuvent utiliser des dynéines pour se déplacer, donc dans beaucoup de cas particuliers pour des déplacements. Joue un rôle dans le déplacementd de la mitose (alignement des microtubules fusoriaux donc fabrique un axe), contôle et régulation du point de contrôle nucléotique, **protéines associés aux kinetochores au moment de la mise en oeuvre de l'anaphase**

## Myosines
* Tous les cargos se déplacent vers l'extremité + 
* On a repéré 17 familles qui diffèrent par leurs parties codales.
* Dans un type cellulaire on peut donc avoir pusieurs gènes différents de myosines. En fonctin de la partie caudale, ce ne seront pas les même cargots qui seront transportées, dont intéractions très différentes. On trouve quand même des exceptions: la myosine 6 et la myosine9b, peuvent aller dans l'autre sens (vers le -)
* La tête de myosine est représentée par une grosse boule blanche, la partie intermédiaire est associé à une structure variable qui peut etre longue et la partie caudale représentée en bleue. Les autres actines interviennent dans d'autres mécanismes (Ex : transport de vésicules, les myosines de type 5 p.E, la myosine 6 dans l'endocytose)

# Fonctions liées à la mise en place d'une polarité
* On peut grâce à une polarité de croissance, qui génére une croissance apicale peu permettre la reproduction en favorisant le déplacement des gamètes, il existe des mécanises cellulaires semblables (chez Arabidopsis ou pin). Petites vésicules qui apportent de la membrane à l'extremité du tube --> permet au tube de grandir et de véhiculer le gamète jusqu'au bout. Ces vésicules viennent de l'appareil de golgi, les mouvements vésiculaires sont effectués par les microtubules et les filaments d'actine. 
* On s'intéresse à un déplacement de mitochondries, ils ont regardés (à gauche des microtubules) un déplacement en 50s, on a une localisation d'une mitochondrie qui suit le rail occupé par le microtubule (on peut émettre l'hypothèse que le déplacement est microtubule dépendant). A droite : filaments d'actines (en 13s) mouvement sacadé (représenté sur la dernière image), distance parcourue plus élevée dans un temps plus court --> vitesse de migration plus rapide avec l'actine. Pour vérifier que les mitochondries peuvent se déplacer sur les cytosquelettes on a analysé les moteurs. ---> immunomarquage (on veut identifier la présence d'un moteur) avec des grains d'or coloidal. Les mitochondries possèent donc deux types de moteurs. 
* Moyen de blocage du cytosquelette : dépolymériser les microtubules. herbicide : orizaline --> dépolimérise le cytosquelette microtubulaire, les mouvements qui subsitent sont associés à un mouvement actine dépendant vu qu'il n'y a plus de myosines, les mouvements lents ne sont donc plus présents --> tout ce qui plus lents que 0,6 um par secondes ont disparus --> donc microtubulaire dépendant. Un certain nombre de mitochondries acquièrent une vitesse plus rapide. **On favorise l'augmentation de la vitesse des mitochondries en supprimant le cytosquelette microtubulaires, donc le cytosquelette microtubulaire ralentie les déplacements???**
* Les chercheurs ont plusieurs inhibiteurs possibles:
    * cytosquelette d'actine
        * cytokalasine
        * latrinculine B
* Vacuole marqu"e avec une sonde : en présence de l'inhibiteur le sysntème vacuolaire ne se déplace plus --> donc on associe des déplacements globaux, (type vacuoles, tonoplaste, appareil de golgi) et on va ssocier cela à un type de cytosquelette. Vers la partie amont du tube pollinique : microtobule impliqué dans callose , tube tellemetn long cytoplsme ne peut pas toujours occuper tout l'espace, microtubule créer des espacements qui vont se resserer. La myosine est un moteur qui peut petre inhiber par la BDM. ces inhibiteurs ont montrés que des déplacements vers la pointe du tube ont été bloqués. Les deux cytosquelettes interviennent et les deux plus ou moins impliqué en amont ou en aval du tube pollinique. 
A quoi servent les mouvements intracellulaires du tube --> déplacer le gamète --> deux gamètes males doivent être transportés. 
* Quand on regarde un filament d'actine on cherche la polarité, l'extremité pointue = - l'extrémité bardée est l'extrémité +, les mouvements les pllus standards associés à l'actine vont déplacer le cargot par l'extremité + (voir plus haut). Comment sont polarisés les filaments d'actine dans le tube pollinique? In vivo avec le mm marqueur : deux images d'une zone cellulaire proche de la périphérie (pret de la paroi), La pointe est situé vers le haut. L'extremité + est localisé vers le haut. Cela signifie qu'on a généré un systeme cellualire bien séparé, avec un tuyau de filament d'actines qui sont deux polarités différents, avec le même moteur mais en sens inverse. Quand on regarde les mouvements de vésicules --> le milieu se déplacent vers la périphérie et revient dans le milieu du cytoplasme dans le sens inverse. Permet d'apporter des enzymes, des membranes pour agrandir la mb plasmique et pour pouvoir apporter les cellules gamètes avec les microtubules. 

## Les mouvements tissulaires au cours du développement
* Invagination de tissus qui progressivement va donner une série de tissus qui se multiplient au sein de la sphère puis génére une polarité au sein de l'organisme. Plaque neurale au départ qui contietn des cadhérine, associé à l'actine. Invagination apparait, elle se referme et structure un tube neural, --> cadhérine E --> cadhérine N.
* On a un épiderme (similaire à un épithélium intestinal) 
* Actine : commence par être une actine globulaire qui va ensuite être structurée en polymère. Va permettre de faire de micro contractions qui vont resserer un "rectangle cellulaire". Microtubules ont modifié le sens d'orientation cellulaire, coté latéral devient plus long que le coté frontal, forme des cellules bouteilles. 
* Modifications cellulaires associées à la neurulation : microtubules et actine

## Déplacement de filaments intermédiaires dans le neurone
* mEos2 : il est photoconvertible, une fois que la structure verte est visible et montre tous les neurofilaments, avec un rayon laser on excite un partie cytoplasmique, les molécules se modifient et ne vont plus être sensibles. Après avoir reçu un rayonnement laser --> vont émettre une autre fluorescence. Dans une zone particulière : on voit que le vert s'est transformé en rouge, la périphérie est verte. On essaie de suivre les neurofilaments au cours du temps, on constate à 630 min, fragment de neurofilaments se mélangent, ne sont pas juste positionnés les uns à cotés des autres mais vont s'associers les uns aux autres --> deviennent partiellement verts ou rouges. Mécanismes moteurs qui vont être mis en jeu (cassures recolables), les déplacements se font le long des microtubules qui sont présents dans ces axones,  cassures qui se font grace à des enzymes pour casser chacun des polymères en fragments, ces fragments mobiles vont être accrochés ensemble pour augmenter la longueur des neurofilaments --> apparition de neurofilaments verts et rouges. Ceci est microtubulaire dépendant
* Mouvements de pigments : les mélanosomes qui vont se déplacer dans les malanocytes -> On regarde le cytosquelette : on constate que des cellules qui sont des mélanocytes, on regarde l'organisation du cytosquelette et on constate qu'il y a un centrosome duquel émanent un résau de microtubules. On sait que cette structure va véhiculer des mélanosomes qui peuvent avoir deux positions stratégiques pour la cellule: soit vers le centre --> - ; soit dispersé dans l'ensemble du volume cytoplasmique --> + . Il est facile lorsque l'on joue avec des hormones. MCH va avoir tendance à générer un mouvement. On voit des microtubules, on voit un effet avec une action hormonale. Si microtubulaire dépendant on imagine des moteurs dynéines et kinésine. Si dynéine : accumulation des mélonosomes, sinon dispersion.
* AMP c --> acitve la mélanophilline : inhibe une activité dynéine dépendante, l' AMP c agit sur la mélanophilline qui active la kinésine. On peut comprendre comment les hormones MSH et MCH. On est capable de comparer ce qui se passe dans un témoin. Si pas de mélanophilline : AMP c ne trouve pas sa cible, on constate une aggrégation supplémentaire et une absence de dispersion, produit un peu de dispersion mais ne dépsasse pas le stade initial d'un témoin. Ce n'est donc pas juste le long des microtubules que se fait ce mouvement : il y a autre chose qui intervietn. La première chose analysée : regarder s'il ya des microtubules, les deux cytosquelettes sont présents donc on peut émettre l'hypothèse qu'il y ait aussi un déplacement actine dépendant. Ils ont utilisé la latrunculine A qui inhibe l'actine. On remarque un cytosquelette microtubulaire préservé et sur ces microtubules on a pas des dispersions et des déplacements équivalents aux témoins. Dans le sauvage on a une dispersion beaucoup plus importante. Les mécanismes avec ces expériences ont montré que chacun doivent se mettre en oeuvre pour que la dispersion soit totale et que la concentration soit complète, la dispersion est plutot dépendante de l'actine que des microtubules. 

* Mélanosome associés à plusieurs types de moteurs. Selon le moteur associé on a ou aggrégation ou dispersion. L'AMP c active la mélanophilin puis une fois activée bloque la dynéine donc on supprime l'aggrégation et la kynésine va agir et va générer plutot de la dispersion. La mélanophiline peut s'associer à la myosine de type 5 et va permettre des mouvements actine dépendant. L'activité mélonophiline est associé à la myosine à partir du moment ou c'est déphosphorylé.

* On a 2 cytosquelettes, on a 3 moteurs, mais on ades mécanismes moteurs très variables car associés à diffrents types de structures intermédiaires (différents types de cargots), avec intermédiaire entre cargot et cytosquelette. Permet de sélectionner le cargot et le rail. Un seul cargot peut s'associer à des moteurs multiples et donc s'associer avec plusieurs types de cytosquelettes.

## Les MAPs

* Dépolymérisation/polymérisation ; les intéractions faites au niveau du kinétochore sont les seules qui se stabilisent. 
* Les mécanismes de recherche et capture : le chromosome a tendance à se déplcer en direction de l'extremité - du microtubule, mais pour pouvoir faire cete liaison de l'autre coté il faut éviter qu'il y ait les deux chromatines au niveau du mm pole, la cellule régule cela en corrigeant l'erreur, lorsqu'un microtubule arrive de l'autre côté. Une fois que l'interaction est faite, comment faire grandir ou rétrécir la liaison kinétochorienne. Le microtubule pousse sur le kinétochore et le kinétochore s'éloigne des poles. Pour que ça marche il faut que des moteurs se déplacent le long des microtubules. Une dynéine qui va vers l'extremité - va pouvoir se déplacer, une kinésine qui va vers l'extremité +. Au cours de la division il n'y a pas un seul moteur : plusieurs types de kinésines et dynéines interagissent : 5 types de kinésines différentes. Une fois de plus --> régulaiton fine, un seul type de moteur, mais plusieurs moteurs moléculaires. 

* Mécanismes anaphasiques : on ne va pas marquer l'int"gralité des microtubules car ne nous permet pas de savoir comment se fait le déplacement des sous unité au sein du polymère : il va falloir marquer les microtubules de manière succinte. On a mis en oeuvre un système qui exprime très peu de tubuline fluorescente et image d'un fuseau : quasi impossible de suivre le mouvement de tous les spots : on a généré l'observation dans le temps d'une petite tranched e cellules dans lesquels on observe le déplcement des spots fluorescents. Permet sur une tranche d'avoir quelques spots visibles. Les chromosomes ne bougent pas, un chimographe --> on peut suivre le mouvement de certains spots, ici permet de suivre chaque spot associé à un microtubule et des lignes obliques apparaissent --> en métaphase il ya une dynamique des sous unité microtubulaires au sein d'une fibre. Le spot se déplace du kinétochore vers les poles --> de nouveau dimères associent au chinétochore pour repousser les dimères qui étaient présents au départ, met en évidence une polymérisation équatoriale. Différences mesurées en utilisant un systèmede photoconvertion a mis en évidence que la dépolymérisation se fait au niveau chinétochorien à l'inverse de ce qui se passe en métaphase. 
* Différents types cellulaires font fonctionner leurs mitoses différeements, parfois : amplification la vitesse du mouvement. 