# TD 1 TMC

## Exercice 1
* secretagogue : toutes substances qui induit des sécrétions régulées (le calcium est un secretagogue)
* dépolariser la membrane : joue sur la concentration en ion : 
    * On ajoute du potassium en forte concentration dans le milieu extérieur (potassium : est un neurosécréteur)
    * nicotine est un secratogue des cellules chromaffines, récépteur acétyl choline 
    * filopode : extensions membranaires
* Cellules A chromaffines : medulo surénale
* Cellules B : PC c'est quoi un chromossitum ? cancer des cellules chromaffines (cellules )
Cellues transfectées par une construction fluorescente verte : glycoprotéine membranaire, on peut voir toute la membrane,
Avec microscope TIRF on voit le plan basal. On voit lasurface surlaquelle la cellule est fixé au support. Ba?? secrétagogue, même fonction que le Ca mais plus intense, exacerbé.
En dessous : cellules chromaffines qui ont reçus le Vecule (contrôle) : le tampon dans lequel le barium était dissous mais sans le barium. On ne voit que la partie basale --> augementation de la surface? possible mais on ne peut pas visualiser. 
* Chromaffine : culture primaire, plus proche de l'In Vivo
* PC12 : lignée
* Pouvoir voir si on produit le même effet chez une cellule cancéreuse ; ils vont transfécter les cellules ---> beaucoup plus facile sur une lignée que sur une culture primaire. Puisque la lignée se comporte comme la primaire, tout résultat de la lignée pourrait être extrapolés sur une cellule primaire.
* NPY : neuropeptide Y --> Marqueur des granules (marqueur rouge), après sécrétion on attend une perte de signal. Ici augmentation puis décroissance de l'intensité du signale. Cette bosse correspond à l'arrivée des granules : une légère augmentation.
* Un point fait un flasch : moment de la fusion. La majorité des événements d'exocytose se fait juste après la stimulation. (entre 100 et 120). Ensuite décroissance très lente : pic au moment de la fusion d'une vésicule.

### Figure 3
DKD: Lignée PC12 qui n'exprime plue Munc18 (protéine auxiliaire des vésicules snare). Même avec le KO il y aune augmentation de l'emprunte : l'emprunte se modifie avant la fusion --> remaniement de morphologies cellulaires 'cytosquelette) pour augmenter l'emprunte et l'exocytose arrive après. Les deux évenements n'ont pas l'air lié. La cellule va sécreter : on va apporter la mb supplémentaire à la MP, d'origine vésiculaire. Elle suit cette sécrétion puisqu'on a apporté une membrane supplémentaire et suite à ça : augmentation de surface. Augmentation de Foot print ne dépend pas de l'exocytose régulée. 

### Figure 4 ??
* cytopalasine : inhibiteur d'actine et blebbistatin : inhibiteur de la myosine
* Complexe actomyosine : guide les granules d'exocytose, polymérisation de l'actine est donc nécéssaire. Les cellules transfectées par MPY, autour de cet amas granulaire, on voit le contour en vert. Après la stimulation pendant 400 secondes. Augmentation de surface et augmentation de granules, cette surface qui augment va être remplie de granules? 
* Trajecteur sort de l'empreinte initiale car l'empreinte augmente, donc attire des granules car de nouveaux sites d'exocytose se sont formés. 

## Exercice 2
### Figure 1
* On regarde les grosse protéines : l'effet de la myosine sauvage ou mutante sur la sécrétion des grosses protéines. On a la construction sauvage non fluorescente de la myosine et de la myosine inactive et ce qui est visible : ce sont les protéines fluorescentes. Les deux sont fluorescentes : seule chose que l'on peut visualiser dans la cellule. On regarde en TIRF avec une stimulation du KCl. On donne un secretagogue et on visualise les granules en utilisant ces grosse protéines qui marquent la matrice glandulaire. 
    * C : juste la protéine native dans la cellule. La vésicule s'est approchée et a probablement fusionné. Soit la vésicule repart soit a relargué une partie de ses protéines, soit une partie du cargo s'est dissiminé, soit perte du cargo. 
    * On surexprime la protéine sauvage
    * On surexprime la protéine native
* On surexprime la protéine sauvage
* On prend la myosine qui ne fonctionne pas, on surexprime une protéine inactive : renforce la compétition, masque l'activité de la protéine endogène : s'apparente à une inhibitiON


### Figure 2 :
* Même expérience mais à la place : peptides
* Rôle de la myosine --> ne joue aucun rôle 

### Figure 4 :
* Montre l'implication de la myosine dans l'ouverture du pore ; (seul article valable)

### Figure 5 :
* m- RFP : colocalisation d'une protéine endogène et exogène --> moyen
* Ils auraient du regarder une protéine de la matrice endogène.
* Faire un immunomarquage pour la chromogranine
* Myosine partout donc ne colocalise pas toujours
* Impression que la myosine disparait du granule??
* On ne sait pas cb de temps le pore est ouvert. 