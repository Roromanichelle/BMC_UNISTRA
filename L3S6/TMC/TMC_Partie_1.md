# Trafic membranaire lors de la neurosécrétion

* Exocytose régulée : exocytose qui n'est pas constitutive, qui est régulée par le calcium.
* Comment ces vésicules sont acheminées du réticulum vers le golgi. Quels sont les gènes concernés par ce mécanisme
* James Rothman : molécules de Snare, il travaillait exclusivement in vitro sur des levures
* Südhof : a étudié les calciums nécéssaire à la fusion de la membrane

## Étapes de la neurosécrétion 
### Cycle des vésicules

* Une fois que les vésicules ont été fabriquées par vont être acheminé vers la memmbrane de terminaison axonale, ils vont êtra amorcé (priming) ils vont etre stimulés par (qui peut être une stimulation hormomnal ou éléctrique, le calcium va ensuite réguler cette fusion) Après la fusion, apporte une membrane supplémentaire à la membrane cellulaire. La cellule augemente sa surface, la cellule fait une exocytose compensatrice, dans les neurones, se fait en dehors de la zone active. Cette sécrétion est directionnelle dans les cellules neuronales, dans le sang également, ailleurs peut également faire un peu partour dans la membrane plasmique.
* Contenant : protéines synthétisées dnas le réticulum endoplasmique, acheminé de manière antérograde vers la membrane plasmique.
* A ce stade les vésicules sont reconnaissables par la cellules car elle porte un manteau appelé (COPII) (caotomère)?
* Ensuite dirigé vers l'appareil de golgi.
* Autre structure décrite : ERGIC (endomembrane entre le reticulum et l'appareil de golgi, ces membranes reticulum et golgi très maniable, peuvent être connecté de manière dynamique). (Cette structure porte d'autres noms : TCG dans les cellules végétales).
* Une fois que les vésicules quittent le RE vont porter un autre manteau le COPI, certaines vont être acheminées de manière rétrograde vers COPI, la plupart vont sortir de l'appareil de golgi et vont porter la clathrin. TOus ces transports sont permis grâce aux microtubules. 

#### ER --> Golgi
* En arrivant sur le golgi (enlève le manteau?)
* Fusion : les protéines SNARE (à revoir) t-snare : target snare qui sont ici sur le golgi ou sur la membrane plasmique
* Certaines protéines ne sont pas assez matures, retour vers le RE
* Les protéines qui sont synthétisées quittent vers ERES (exit site)
##### Comment se forme le manteau?
* Nécéssite une protéine monomérique GTpase, lie le GDP ou le GTP, SEC12 permet d'activer Sar1. Le manteau COPII est recruté, et dans ce manteau il y a des protéines : Sec23/24 (marqueur).
* Une fois cette partie incorporée il y aura l'activation d'autres Sec associées à cette structure et va permettre la polymérisation du manteau, va générer des courbures membranaires négatices et grace à 23 27 il y aura l'individualition de ces molécules. 
* Pour pouvoir fusionner avec le golgi il faut libérer les protéines Vsnare, le manteau va se désassembler,et la fusion va être activé par d'autres protéines Gtpase. Et les protéines sont libérées dans la lumoère de l'appareil de Golgi côté cis.
* Résumé : Sec12 active Sar1, lie le GTP, permet le recrutement de manteau coop2 contenant sec23 et 24 et dans un second temps sec 13 31 ce qui permettra la polymérisation du manteau. La cision va être générer par  ?
* Sec 16 controversé dans la littérature
* COP I : différences entre les espèces; il existe bien un manteau cop I de l'ergic vers le gogli présent dans le transport antérograde et rétrograde.
##### Comment cela a été démontré?

* Publié il y a 20 ans
* Les chercheurs utilisent des cellules épithéliales de rein de singe, qui ont été transfectées par une construction qui code pour une protéine chimérique, glycoprotéine thermosensible qui ont tagé. Constitue l'intérieur des vésicules si elle est fabriquée. (OUTIL). THermosensible: permet de suivre son chemin dans la cellule, on peut la faire diriger dans des structures différentes, si on fixe la température reste fixer dans telle ou telle compartiment. 
* Commarquage avec la calnexin : donc oui présence dans le RE et à 40° elle se trouve dans le RE. Pour faire diriger les vésicules vers le golgi on passe par le marqueur ERGIC, si on refroidit les cellules à 15° on arrive à faire diriger les protéines dans cette structure. Si on rechauffe à 20° par la suite, les granules? vont s'accumuler dans le golgi. 
* Mouvement de la protéine thermosensible pendant 6 min, on refroidit, et les vésicules vont bourgeonner et quitter le réticulum, en même temps réalisation de marquage avec Beta cop qui est un marqueur de manteau 1 et sec 13 qui est un marqueur de manteau 2. Au bout de 6 min commencent à apparaître des vésicules qui sortent et s'individualisent. Couleur verte GFP, cop1 : rouge et cop2 : bleu. 
* Au début de l'expérience: beaucoup portent le manteau copII, à deux minutes : toujours COPII majoritaire et COPI minoritaire, et après 2,5 minute : la tendance s'inverse. Ce graphe décrit qu'il y a un changement de proportions de marquage donc ils pensent que c'est changement de manteau. 
* Ils suivent cette structure de manière antérograde : Pendant les 2,5 min? La protéine ne bouge pas.
* Si on refroidit? on dépolymérise tous les microtubules, si on veut figer la dynamiques de transport, on met les cellules sur glace, après on peu lentement réchauffer et filmer, le déplacement de ces protéines GFP, si on superpose les images, les molécules se déplacent sur les microtubules, donc les vésicules se déplacent sur les microtubules.

##### possibilité
* On choisit une protéine dans le manteau, soit dans la membrane de la vésicule ou une protéine cargo de la matrice, on choisit ensuite quelle protéine on va suivre. On peut aujourd'hui transfecter ces protéines fluorescentes. C'est important de bien choisir la cible.
* Molécules sur le manteau? Peu de mouvement car on marque un seul des manteaux de ce processus. Les molécules vont être ailleurs alors que les vésicules sont ailleures, alors que le cargo est toujours présent.

#### Naissance et formation des granules à partir du Golgi
##### Granulogénèse (quand les vésicules bourgeonnent sur le golgi)
Processus régulé par le calcium
* Les protéines sont prêtes, modifiées de manière pst traducitonnelle, sont compactées dans une partie du golgi, le bourgeonnement va commencer à l'aide des protéine granulogénése. Sur la partie trans il y aura empactation des vésicules, elle sont à ce stade relativement grosses : 100 à 200 nm de diamètre, mais contiennent encore des molécules immatures. Une fois les vésicules bourgeonnées on fait maturer le contenu des vésicules. Elles vont être targété ou retargété dans le golgi et vont subir une acidification et va perdre au moment de bourgeonnement les clathrines. 
* Première étape : les potéines se compactent grâce aux granines, 
    * Granine : il s'agit d'une famille dans laquelle on trouve :
        * des Chromogranines : **chgr A**; Chgr B
        * des Sérétogranines : SgrI ; SgtII
    * Ce sont des grosse protéines qui permettent de recruter le produit à sécreter, ils vont compacter de manière assez solide et une fois retrouvée ensembles, ces molécules intéragissent avec des Lipid Raft. Des lipides comme le choléstérol avec une petite tête polaire, permettent de générer une courbure de manière physique et donc faire apparaitre une vésicule.
    * A.Golgi bcp plus de cholesterol.
    * En transfectant une lignée cellulaire COS (cellules non sécrétrices), mais si on transfecte avec Immunogold la Cga, on va pouvoir observer des granules
    * Lamp 1 : marqueur de lysosyme (non)
    * Colocalise avec la protéine NPY (cargo, marqueur des vésicules)
    * Surexprime de manière exogène la CgA, force la cellule à fabriquer des granules. --> CgA permet la néoformation des granules dans la cellule.
* Par cellules on trouve 30000 granules.
* La souris est qui est dépourvu de gène de CgA ne va pas pouvoir faire des granules fonctionnels. Pb d'osmolarité ; s'il manque la protéine granulogène, il manque qqch de plus hypertrophique donc (osmolarité) moins hypotonique? Souci au niveau des mitochondries. Et il y a en plus plus de vésicules présynaptique, les vésicules présynaptique ne contiennent pas.

#### --> membrane plasmique
##### Systèmes de sécrétion et modèles cellulaires
* Pour étudier ces modèles de sécrétion, on utilise des modèles.
* On utilise des modèles neuronales. Mais si on étudie la sécrétion des hormones on utilise des cellules hématopoiétiques.
* Glande surrénale : composée de deux partie : la cortico surrénale et la partie médullo surrénale. 
* Cohabitation de petites vésicules (microvesicules), elles n'ont pas de coeur dense (40-50nm).
* Calcium : 
    * entre dans la cellule de manière exogène
    * Ou intracellulaire dans le RE
    * Dans les mitochondries
    * Dans les granules
* LDCV : on y trouve une grande quantité de calcium ( concentration dans le cytoplasme : 1 à 2 mM), personne ne sait si le calcium est nécéssaire pour les vésicules. Charge les granules et permet la compactation des granules. Pour permettre la libération de ces granules, certains articles montrent qu'il faut éliminer ce calcium dans les granules. Fusion RE : pourrait reprendre le calcium pour pouvoir ensuite dégranuler les granules.
* Dans une granule de sécrétion on y trouve 
    * voir cours (souligné en bleu : à connaître)
    * Adrénaline, noradrénaline, dopamine
    * Chromogranines A et B, sérétogranine II
    * Enképhalines/proenképhalines, neuropeptide Y, substance P
    * Ions Calcium, magnésium

* Ces granules n'approchent pas la membrane plasmique simplement. 
* Il y a un réseau d'actine qui empêche le rapprochement. Il faut que le réseau d'actine se dépolymérise. C'est ce qui va se produire lors de l'excitation. Cette jonction neuroglandulaire où l'acétylcholine est deversée pour que les cellules chromaffines libèrent les granules. Arrimage, Amorçage, fusion grâce au calcium. Le pool de réserve de granules dérrière la barrière d'acite, ils se trouvent à 200 nm de distance de la membrane (donc encore visible par un mcroscope TIRF). Une fois dépolymériser les granules s'approchent et les granules entrent dans UPP??, ensuite lors de l'arrimage, le Slowly releasable pool, le readily releasable pool.
* Pour relarger ce qui est déjà amorcé, il suffit de relarger une petite stimulation. Pour relarger les granules qui sont dans le SRP il faut une stimulation plus importante 10 Hz pendant 10 ms par ex.

## Comment a t-on démontré que l'actine et la myosine étaient impliqués

* Comment les granules s'attachent à l'actine? MyRIP va lier l'acine et les granules.
* La dépolymérisation n'est pas à 100 %, le reste va être utiliser comme rail de guidage pour les granules. Complexe actomyosine, soit RAb27(intérmédiare) associé au granule intéragit avec MyRIP qui intéragit avec l'actine. 
* Expérience d'immunoprécipitation, ils ont montré également la localisation par immunomarquage.
* SI surexprime le MyRIP ou Rap27 : bouge beaucoup moins, on lie avec beaucoup plus de force, on lie à l'actine de manière plus importante, elles sont liées mais ne peuvent pas être dirigées. Il s'agit d'un équilibre fin pour diriger les granules à leur place pour permettre l'exocytose. Chaque fusion granulaire donne un pique. 

--> L'actine est importante, pour le guidage et aussi importante pour générer ces sites d'exocytose. Ces sites doivent être prêt comme un signal pour les granules. On ne sait pas encore comment le granule sait qu'il va pouvoir fusionner à cet endroit. C'est tout un microenvironnement au niveau de la membrane qui fait que la fusion va se faire à un endroit plutot qu'à un autre.

* Renversement d'un dogme : auparavant, les chercheurs pensaient que d'abord le granule va venir et fusionner et apporter une membrane supplémentaire, cela va apporter une empreinte cellulaire et pas le contraire. On observe l'empreinte cellulaire, on remarque qu'elle augmente après stimulation. Ils ont marqués le contenu des granules également, mais l'empreinte finale on voit la fusion, pas dans l'empreinte initiale. La fusion arrive parfois dans la nouvelle empreinte. 

Le site d'exocytose était déjà préexistant qui attire le granule et après le site ne change pas, remodelage du cytosquelette qui se fait en amont de la fusion (alors qu'avant fusion et remodelage du cytosquelette et augmention de surface). 

QUESTIONS

   **Différence Granules et vésicules (Granule lors de la sécrétion et on parle de vésicule dans la phase précoce de la voie sécrétoire)?** 
    **Compréhension des images, marqueur de lysosyme** ; 
    **Vecteur vide transfecté = témoin?**  
    **Les cellules chromaffines ne font pas de neurosécrétion?** 
    **Cellules neurosécrétrices = cellule nerveuse qui a la possibilité de sécréter des hormones** **Cellules épithéliales de reins de singe : il ne s'agit pas ici de neurosécrétion?** --> **Phases précoces de la voie sécrétoire = ER --> Golgi générale pour tous les types de sécrétions et phase tardive spécifique des cellules neurosécrétrices?** **UPP?** 
    **Sécrétion neuroendocrine?** --> **Seules les neurones sécrètent des neurostransmetteurs/Seules les cellules endocrines produisent des hormones (neuroendocrine car les même procédés?)**
    **Toujours le manteau de clathrine dans pool de réserve**?

* L'accostage nécéssite l'action de plusieurs protéines, la barrière d'actine fait mur contre ces vésicules, d'autres protéines GTpases impliquées dans ce mécanisme, complexes protéiques se mettent en place  (assemblage de plusieurs protéines qui gouvernent ce processus)
* Arrimage : dure relativement longtemps (jusqu'à 10sec) permet le rapprochement des prot snare, (qui determinent le site d'exocytose), 
* L'ammorcage rend le granule compétent à la fusion, le but de ce mécanisme est de rapprocher les deux membranes --> les membranes vis )à vis de leurs contstructions, la tete polaire est chargée négativement, barrière éléctrostatique qui empêche l'approchement des deux membranes, protéines qui permettent d'affranchir cette barrière énergétique.
* SNARE = Soluble N-ethylmaleimide-sensitive factor attachment protein receptor
* VAMP : vesicle associated membran protein/ même protéie que la synaptobrévine (même séquence/même géne) ; monde endocrinien dit VAMP2.
* SNAP 25 n'est pas transmembranaire mais associée à la membrane cellulaire. Les deux membranes se rapprochent grace à ça, en forme d'hélice s'enroulent ensemble et font le rapprochement.
* Toxine botuline (botox) BoNT target la snap25 ET bONT/C LA SYNTAXIN, si la toxine bloque le SNARE il n'y aura pas de mécanisme de sécrétion. Le Botox a été délivré par la médecine on inhibe la sécrétion de l'acétylcholine qui va agir sur les récépteurs colinergiques des muscles (qui donnent l'indication au muscle de se contracter).
* Toxine tetanique (TeNT) inhibe également la sécrétion/neurotransmission
* KO snap 25 : léthal
    * mesure la capacitence : technique éléctrophysiologique qui permet de mesurer la surface cellulaire (notamment l'augementation de la surface). La membrane de la vésicule va s'insérer dans la membrane plasmique. temporairement on a une augmentation de surface. La cellule élimine cet excès par endocytose (qui suit tous mécanismes d'exocytose)
* MUnc 18 = protéine auxiliaire
* SYntaxine 1 KO : viable (pb d'apprentissage) d'autres syntaxines (en tout 27)
* Combien de complexes snare plur une fusion efficace?
    * Expérience in Vitro ou en tubes
    * SUV : small unilamellar vesicle (vésicules synthétisées) on peut les synthétiser car on connait la composition lipidique d'une membrane, utilisaiton de la stoechiométrie et ont inséré la VAMP (protéine V snare vésiculaire), ils ont fait d'abord avec une VAMP par vésicule, deux, etc.. et ont testé à partir de cb de v sare ces vésicules vont fonctionner. Flux continue de vésicule à travers ce tube puis mise en contact avec la membrane, s'il y a suffisament de snare, on peut suivre au microscope (TIRF), quand elle fusionne elle va s'étaler, D : arrimage; F : amorcage, fusion : commence à s'étaler, l'intensité du signal diminue mais la surface augements, ils en ont conclus qu'il fallait minimum 5 vésicules de snare pour réussir une fusion vésiculaire.
    
    * A partir de deux : on a deux complexes snare qiu suffisent.
    * Dans le neurone : pas seulement les vésicules snare ; il y a des protéines auxiliaires qui facilitent le fonctionnement de ces vésicules

## Rôle des protéines auxiliaires et du Ca 2+

* 1. réserve (actine) et commence à étre recruté au niveau des sites d'excytose Munc 18 : s'associe avec la syntaxine : essentiel au niveau du repose ; on pense que MUNC 18 empêche la fusion et stabilise le complexe avec la syntaxin.
* Munc 18 = soluble
* Au moment de l'amorçage le Munc18 relache son intéraction avec la syntaxin de manière très temporaire, suffisament pour permettre que le complexe snare s'assemble
* Munc 13 entre en action, protéines RIMs, qui viennent s'associer à tous l'ensemble et qui destabilisent le Munc 18. Une fois le complexe snare assemblé, le Munc 18 s'associe de nouveau avec la syntaxin, une autre protéine : complexin s'emboite dans ce complexe : stabilise le complexe et permet un resserage plus important. Le calcium va à ce moment etre capté par la synaptotagmin qui est capable de lié le calcium, ceci va activer la synaptotagmin qui va interagir avec la membrane plasmique. Cette étape la synaptotagmin va permettre la formation du pore de fusion.

* Protéines auxiliaires au snare : COmplexin, munC18 , SYNTAXIN, Rims, Snap 25
* Rôle des Rims : recruter les canaux calciques qui sont dans la membrane plasmique : les canaux vont pouvoir se déplacer. Une fois les canaux calciques recrutés, le calcium va pouvoir s'associer avec la synaptotagmin, et qui va permettre le resserrage. Au moment de la fusion (Rab 23 dans les neurones et Rab 27 dans les cellules endocrines) le Munc 13 peut aussi s'associer dans les cellules de mammifère se relier à la membrane plasmique la phosphatidil inositol 4-5 bis phosphate (lipide clé de ces fusions) (PIP).
* Dans les neurones pas uniquement des vésicules pré synaptiques, si la vésicule sécrète des peptides comme l'endorphine qui sont dans des granules de sécrétion : taille des granules beaucoup plus importantes. 

## Au moment de la fusion : comment se forme le Pore

* Au moment de larrimage : ils pensent que la syntaxin et dans la vésicules VATpase (permet de pomper des protons à l'intériur de la vésicule pour acidifier). 3 syntaxines : on forme un trou délimité par un pore protéique cela va générer un port de fusion. 
* tige  déformation complexe dans les deux foyers ; hémifusion qui pourrait se faire soit par extension symétrique axiale, plus il y a une extension plus on s'approche à une rupture et évolue vers un pore de fusion. POur pouvoir permettre cette extension latérale il y a des protéines qui aident et dirigent ce mécanisme, mais la présence des lipides est primordiale.

## Les différents mode de fusion

* 1. Un tout petit pore va se former, la membrane de la vésicule ne va pas s'aplatir, ceci génére une taille de pore très étroit (moin de 5 nm de diamètre), seulement des petits stimulus 2 HERZ , Kiss and run; permet seulement de relarger des toutes petites vésicules, (trou bcp trop petit pour faire passer des protéines surtout des ions et des catacholamines) 
* 2. Expands until flattened theory : fusion complète qui peut générer des pores de 500 nm, tout est relargé dans ce cas : chrg srg, tout ce qui était dans le granul est relargé
* 3. Entre les deux cavicapture : ce n'est pas une fusion complète mais permet de relarger des petits peptides
* Theory dynamic du pore : dit que tout existe; mécanisme très dynamique. Taille du pore peut varier entre 1 et 490 nm.
* Actine : va exercer une pression ; responsable de l'extension du pore ; Les deux mécanismes ; élargissement et resserage sont en compétition d'où la dynamique du pore.

## Dynamique des lipides lors de l'exocytose et la fusion membranaire

* 1. Formation des radeaux lipidiques (raft) enrichis en cholestérol
* Nanondomaines lipidiques et lors de la fusion vont se rassembler en nanodomaines et ceci est régulé par des protéines, entre les nanodomaines environnant il y a une tension, pour qu'on les regroupe va générer la courbure des membranes pour permettre la fusion. 

### Métabolisme des lipides

* Phosphoinositides = phosphaidyl inositol phosphorylé par une kinase / 1% des lipides
* Phosphatidyl inositol 4-5 bisphosphate (PIP2) le plus majoritaire 0,05% des lipides, c'est très important pour la signalisation de trafic membranaire. PIP2 : surtout dans le feuillet interne
* Rôle de PIP 2 dans les cellules : peut recruter certaines protéines gradulaires : la synaptotagmine 
**PIP2 est consommé mais par quoi?** 
**PLusieurs PIP2 nécéssaire pour l'arrimage??**

### Comment visualiser les lipides

* Sondes fluorescentes, sondes biologiques qui ont un domaine de reconnaissance lipidique. On peut transfecter des cellules en utilisant un plasmide ceci va coder pour un petit peptide pour interagir avec les lipides. on peut donc tager le lipide dans la cellule vivante. 
* Plextrin homology domain, substrat de protein kinase, mais est capable de reconnaitre le PIP2, il suffit de prendre ce domaines d'intéraction (100 AA), et une séquence fluorescente dérrière. 
* La plextrin et plein d'autres protéines sont capables d'interagir avec la PIP2. 
* On utilise toujours le PH domaine. 
* Sécrétagoge : molécule qui génére une sécrétion régulée.
* Sonde se décroche de la membrane : signifiequ'elle n'a plus d'intéractions de target, le pip2 n'existe plus : a été temporairement consommé.
* Cette expérience montre le comportement différent des deux sondes meme si les deux reconnaissent pip2 : la dynamique de pip2 est plus réactive si on utilise cette sonde (GFP tubby domain). 
* DAG  : néoformé, peut être visualisé par le recrutement , on voit clairement que la dynamique de formation du lipide n'est pas la même que dans la première expérience. Domaine C1 : une cinquantaine d'AA qui reconnait spécifiquement le DAG. 
* Ce sont des outils puissants / intéressants/ mais il faut effectuer plusiseurs contrôles et garder tete qu'il peut y avoir plusieurs artefactes. 
* Lipides : allourdis par le poid qu'il porte.
* Certes outils puissants mais toujours être critiques par rapport aux résultats

### Acide phosphatidique 

* Généré à partir de phosphatidilcholine enrichi dans le feuillet interne du membrane, la phospholipase D est l'enzyme qui génére l'A. phosphatidique à partir de la phosphatidilcholine
* A phosphatidique a une petite tête polaire conique et génère une courbure
* La sonde PA reconnait le PA néoformé
* Il existe des anticorps, après la stimulation les granules sont doqués et en contact avec la membrane, immunogold : aussi intensifiés par l'argent. 

* Comment détécter l'endocytose ?
    * Avec une technique éléctrochimique : ampérométrie 
    * La cellule suit au stimulus va fusionner les granules, éléctrode proche de la cellules, oxyder l'éléctrode et cette oxydation va être repéré par l'éléctrode, on mesure la dynamique de fusion d'une seule granule, au moment de l'ouverture du pore : petite bosse apparait et quand tout est relargé on obtient une grande activité éléctrique. Un seul pic montre l'ouverture et ça nous donne pour chaque élément la demie vie de descente, la demie vie ; l'aire sous la courbe est proportionnel à la quantité de chotacholamine reversé.
    * A. phosphatidique dans cette expérience on montre l'importance de la PLD1 , on stimule l'exocytose dans cette expérience.
    * Beaucoup moins de fréquence (nombre d'événements inférieurs)/ aggrandissement de la surface cellulaire, montre que l'A phosphatidique néoformé grace à la ?? nécéssaire à l'exocytose. La PA : nécéssaire à l'intéraction avec la syntaxine.
    * A phosphatidique entre en intéraction avec la syntaxin 
    * Si on utilise la sonde P1

* Comment les phosphoisonitides arrivent à la membrane plasmique?
    * lipides synthétisés dans le réticulum endoplasmique sytosy diphosphate diacyglycérol à partir de PIS : phosphatidylinositol synthase transforme en phosphatidylinositol --> molécule précurseur des phosphatidylinsitol phosphorylé ???
    * mRFP -ER : rouge / PIS : vert ; on voit que des molécules ne sont pas exprimés au même en droit (vert pas jaune)
        * silencing : ces structures vertes disparaissent, stucture membranaire mobile du réticulum et qui amène la protéine, ces vésicules ont tendance à aller vers la membrane et à revenir. Ils ont décrit l'existence de ce nouveau compartiment mobile qui contient cette enzyme, ce sont ces vésicules qui apportent les phosphotydoinsitide à la membrane plasmique, aussi bien le précurseur --> renverse le dogme des années 80 selon laquelle il y a des protéines transporteurs qui transportent les phospholipides vers la membrane. Donc TRANSPORT VÉSICULAIRE.
        (il se peut que les deux mécanismes existent en parallèle)
        * Embrassade avec la membrane et différents organelles, différents lipides, avec le golgi, peroxysomoe (quasiment toutes les organelles), quand on parle de contact on ne parle pas de fusion : se rapproche à une distance de 10 à 30 nm, à ce moment le reticulum est dépourvu des ribosomes (car en même temps synthétisé dans le réticulum endoplasmique).
        * Microscope TIRF 

## Fusion membranaire
    * cytosquelette de l'actine : dépolymérisé
    * Manteau d'actine qui polymérisé autour de granule
    * Ce manteau d'actine, va être utilisé comme un resserrage de la vésicule donc exerce une pression sur cette organelle pour vidanger.

### Comment détecter/ visualiser en temps réel?

* Ampérométrie : oxydation des catacolamine, chaque pic correspond à un seul élément de fusion, on peut donc mesurer ce mécanisme sur les cellules vivantes et analyser la dynamique
* Capacitance : mesure l'apport membranaire qui se passe lors d'une insertion et augementation de la surface cellulaire
* Electrophysiologie : détéction éléctrique des neurotransmetteurs : donne des informations in vivo/in stitu
* Microscope TIRF 
    * Dans ce granule on a effecturé le VMAT2 'vesicular mono amyl transferase' : enzyme qui permet de générer des catacholamines. surexprission : se localise au niveau des vésicules ; au début de l'expérience --> colocalisé et au cours du temps, au début tout les deux intense, le vert reste identique par contre le rouge beaucoup moins intense : le rouge se diffuse donc on perd l'intensité, on perd le siganal et le siganl vert, reste localisé dans la membrane. Une fois fusionné devient vert, signal diminue car sort de la région d'intéret que l'on observe --> il faut faire un tracking du signal, si le signal reste il faut dire de se déplacer : comportement endocytose compensatrice.
    * **NPY**
    * Comportement des vésicules très hétérogène au sein de la même cellule avec la même stimulation, ce n'est pas parce qu'il y a un stimulus que toutes les vésicules vont fusionner, chaque vésicule a un microenvironnement, p.e concentration locale des molécules qui régules le mécanisme etc etc. Plein de vésicules n'arrivent pas forcément au même moment et pas toujours la fusion. On connait les molécules qui interviennent dans leur guidage (cytosquelette, protéines, lipides), hypothèse : le microenvironnement. pas d'explication uniquement constatation. Quand on analyse la dynamique : sur 3 cultures différents ; chacune 20 cellules ; 100 
* Fluorin : vert --> même spectre d'émission, mais modifée. fluorin ecliptic n'est pas du tout fluorescente dans les pH acides, seulement à pH neutre qu'il devient fluorescent. Si on transfecte la VAMP : protéine transmembranaire,et côté luminal on ajoute la fluorin ecliptic. pH acide à l'intérieur de la vésicule --> donc on ne voit pas la fluorin, si elle fusionne --> le pH s'équilibre, devient neutre et la VAMP s'allume, quand il s'allume on sait que la vésicule a fusionné. Il y a après l'endocytose compensatrice. Une fois que la vésicule se referme, les pompe à protons pour acidifié, en quelques minutes réacidifié et la vésicule va s'éteindre, on peut donc mesurer la durée de l'exocytose.
    * Surexpression de la myosin II (sa chaine régulatrice légère), la cinétique est plus lente (l'ouverture est plus longue), si surexprime une dominante négative (mutant pas très actif), entre en compétition avec la forme endogène. Inhibition de l'effet endogène et cinétique plus rapide. La myosin régule donc ce mécanisme d'exocytose (ouverture et durée d'ouverture du pore)

## Endocytose compensatrice 

* Excès membranaire : endocytose relativement lent qui recquière la clathrin (ou cavéole) quand il y a des exocytose massives on va pas les reprendre un par un, on reprend une grande partie de la mebranire, (Bulk endocytosis) ne demande pas de la clathrine, génère une courbure membranaire, et cela ne peut pas être recyclé par la clathrine. Soit recylclé et resynthétisé par le golgi ou pourrait être recyclé directement. 
* Amorça prtoéine adaptateur P2.
* la cission va se faire par la dynamine.
* DBH : dopamin beta hydroxydase : génère les catacolamines dans les premières étapes. On prends un anticorps anti DBH et on génère des granules, au moment de la fusion (qui dure quelques secondes), à travers des pores d'ouverture si relativement gros, dans ce cas les grosses molécules peuvent atteindre les. Donc quand les cellules sont en train de fusionner, on laisse intéragir et on fixe, on a pas de marquage sur la surface et sur la totalité de la surface il y a des pointillés = fusion des vésicules. L'exosytose peut être marqué de cette manière, on fait ensuire un lavage, on enlève l'anticorps et après l'exocytose beaucoup des . Les quantifications montrent que l'on perd la colocalisation de la clathrine et de l'enzyme car une fois fusionnée la clathrine va dissocier de l'échantillon, spots à l'intérieur : ccl : anticorps transporté avec les vésicules. 
* A une minute : anticorps pénétre déjà
* La membrane fusionne et c'est cette membrane ci qui repare ; différent chez les neurones : fusionne et membrane reprise ailleurs, (même si ce sont les meme molécules qui entrent dans le mécanisme de l'exocytose). 

### Implication des lipides 

* Flipase va faire flipper les lipides de l'extérieur vers le feuillet interne, le floppase fait l'inverse. Dès qu'il y a une activation (migration/ stress osmotique), la symétrie membranaire peut subir des changements. Scramblase : sont capables de transférer dans les deux sens. La scramblase permet la mise en plase de cette assymétrie. Le marquage de l'enzyme 
* PLSCR KO : pas d'endocytose ; défaut d'endocytose si on supprime ce gène. Pour que l'endocytose compensatrice puisse fonctionner on a besoin de cette assymétrie membranaire. 
* Valable dans des cellules chromaffines et dans des neurones également 
* Synaotophysin : marqueur des molécules pré synaptiques

### Vitesse dans des neurones

* Il existe des endocytoses très rapides ; cryofixation et cryomicroscopie éléctronique
* Structure vésiculaire pré-synaptique
* PSD : post synaptic density 
* Zone active de la synapse. L'exocytose au bout de 15 mmsec, non avons déjà un élément de fusion. Si la stimulation est faible, difficile, que une dizaine de fusion..
* Pas de clathrine dans cette exocytose ultra rapide compensatrice, il y a la dinamine pour la scission et l'égogement de cette vésicule.
* Grandit à cause de la dynasore car scission (égorgement) bloqué.

#### Mécanisme?
* Intersectine ITSN1 impliquée : l'exocytose se fait en présence des snare qui déterminent le site d'exocytose, mais pour permettre une exocytose rapide et qui répond à des stimulations répétées, Ces stimiluations nécéssitent une neurotransmission rapide. Autre mécanisme en parallèle ; endocytose : les snare sont repris, se fait par l'intersectine, qui va intéragir avec la synaptobrévine (vsnare) ap180 adaptateur : gouverne le mécanisme que tout soit repris, les snare pas  encore utilisé : restent. Recyclage local, et par un autre mécanisme???. Turn over pour enlever les snare déjà utilisés. PB d'apprentissage car transmission rapide ne peut pas se faire pour les ITSN1 KO.

## Pathologie
* Tumeurs : multiplication excessive de cellules mais pas cancérigène
* 