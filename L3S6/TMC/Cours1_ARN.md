Granules : pas de complexes lipidiques mais Prot + ARN.
Certains complexes vont être déplacés. Localisation particulière médié par les ARN dans la C. Important dans le dvpt et neurones.
Modèles : fibroblastes mais aussi embryons de drosophiles et neurones. 
Comment les complexes vont être transporter et arriver. Pourquoi ils localisent? TRadution d'ARN localisés seulement, outils pour détécter les mécanismes cellulaires. Réviser les techniques analytiques prot + ARN. Sur gel (northern blot pour ARN et western blot pour prot) RT -PCR ou Q-PCR (reverse transcription, puis on amplifie par PCR une fois qu'on a l'ADN) Hybridation in situe, immunomarquage : anticorps spécifiques contre la protéine d'intérêt avec étiquette GFP. Si on veut regarder la dynamique : on va observer en temps réel, fusion GFP mais il faut être sur qu'elle se comporte comme la protéine d'intérêt, on doit retrouver le même phénotype. On peut injecter ARN fluorescent/ Boucle MCP fusionné avec la GFP. Intéractions ARN-Prot. Matériel : microscopie à épi fluorescence, techniques de biochimie, techniques statistiques. 
* Dans le cytoplasme : on a les ARNmessager (avec queue poly A) Comment cette information est transférée du noyau vers le cytoplasme? Transport de l'ARNmessager. 
* Les expériences qui ont permis de montrer que l'ADN était le support de l'information génétique : Avery , souris, qui ont été infecté avec différentes souches de pneumocoques, souchez virulentes et non virulentes. Plusieurs expériences : il a ajouté différents composants des souches. Si on prend les protéines, avec la souche non virulente : le phénotype ne change pas. Les acides nucléiques : rendent des souches  non virulentes virulentes, on ajoute des RNAses ou des DNase. Si on enlève les ARN on a toujours la mort des souris. Si avec DNase on a plus la mort des souris : l'ADN est le support de l'information génétique. Structure de l'ADN : rayon X : ont permis d'observer la structure. Rosalin Francklin obtient la première photographie  d'ADN. Whatson et crick : montre la double hélice et en 54, ils ont pu montrer les différentes bases : ATCG, une dizaine d'années après => prix nobel. 
* Cytoplasmes? : Zamenick a montré que le cytoplasme était le lieu de synthèse des protéines. Il a pu en 1953 faire des purifications d'extraits cellulaires, grâce à des centrifugation on va pouvoir isoler les différents compartiments. IL a pris des foies de rats qui permettaient la synthès protéique à partir d'une source d'A.aminés.  Il ajoute des A.A radioactifs, qui vont s'intégrer dans les protéines synthétisés. Puis centrifugation (les protéines vont être plus lourdes généralement), elles vont être dans le culot et on va pouvoir détecter une source radioactive. Il a étudié la synthése des protéines, mais il a choisi de travailler sur ce sujet d'un point de vue biochimique. 22 AA différents. Leur intégration ou incorporation est portée par le code génétique. 
* On a d'autres acteurs, ils ont montré que le lieu de synthèse des protéines est indépendant du noyau cellulaire, ils ont pu montrer. Algue unicellulaire : un cytoplasme et un noyau, on peut faires des énucléactions, et ils ont montré que la synthése protéique était toujours possible => donc synthése protéique se fait indépendamment du noyau cellulaire, donc pas de participation directe de l'ADN => molécule intérmédiaire : l'ARN.
* ARN messager : comment va être formé cet ARN messager. 
* Transcription (ARN polymérase)=/ types, on prend une transcription classique, ARN pol 2 (ARN non mature) en grif foncé les exons, puis on va avoir la maturation des ARN, ajout d'une coiffe en 5', l'ajout d'une queue poly'A en 3', polyssage, avoir le dépot du complexe EJC, on peut avoir les cliages etc. Il y a d'autres ARN : ARNt , ARN r micro ARN. Ces ARN vont être exporté à travers les ports nucléaires : on va avoir plusieurs protéines qui vont permettre l'export: évite la division des ARN et du cytoplasmes. Les protéines de l'EJC sont importants pour cet export, des exportins également. XPOT (important pour l'export des ARNt dans le cytoplasme) Activtion des AA : s'associe avec l'ARNt synthétase et permet le couplage de l'ARNt avec son AA correspondant, et nécéssite l'utilisation de 2 phosphate 'énergie) Ces AA activés vont pouvoir etre utilisés. Ribosomes : les AA sont associés les uns aux autres/  
* ARN exportés à travers les ports nucléaires, plusieurs prot vont permettre l'export. permet d'aviter la diffusion des ARN, permet le contrôle des ARN qui sont exportés dans le cytoplasme. Les prot de l'EJC ou les exportines ou XPOT son importants pour cet export.
* Les protéines le l'ejc sont importantes pour cet export. 
* ARN SOUVENT SIMPLES BRINS QUI SE REPPLIENT sur eux même pour former des structures primaires et secondaires. AN qui s'associent les unes aux autres pour former des boucles distinctes. 
* On peut regarder le monde des ARN
* Transcription = production d'ARN, pour produire ces ARN, ARN polymérase ADN dépendante chez les eucaryotes : 4 ARN polymérase, ARN pol1 = synthèse des ARN ribosomaux mais pas sous unité 5S.

## Marquage des ARN : in vivo
* Permet le marquage de ARN, systèe à deux composants : l'ARN et une protéine (1998),Bobsinger avec Edouard bertrand, qui ont utilisé les données d'un bactériophage connu. ARN de 19 nucléotides = boucle MS2, cet ARN a une structure en tige boucle, il peut s'associer à la protéines MCP ou protéine MS2 coat protéine. on ajoute la GFP
* On met plusieurs boucles MS2 pour pouvoir détécter la région fluorescente.

## Export actif vers le cytoplasme
* ARN pol2 = coiffe en 5'
* Contrôle actif, vérifier que ces ARN sont produits correctement

### Maturation
* queue poly A 3', polyssage, EJC complexe, clivage, coiffe en 5'
* méthionie en 5'
* Condon stop permet l'arret de la traduction
### Initiaition = mise en place de l4ARN pol2
### Elongation 
* ajout de la coiffe en 5 ' selon 3 étapes qui recquièrent 3 ENzymes (à savoir) :
    * RTpase = enlever des phosphates (hydrolysation du phoshpate Gamma de l'ARN naissant) premier nucléotide de l'ARN soit AUC ou G, p représente le phosphate et pi le phosphate inorganique
    
    * Gtranférase : permet de maturer en 5' l'ARN messager
Mtransférase : ajout en 5'd'un groupement méthyl
ARN pol 2 rencontre le signal de polyadénylation et permet la terminaison de la transcription qui se caractérise par l'ajout de la queue poly qui fait intervernir la poly A polymérase.
Pourquoi aux extrémités? car la dégradation des ARN passe par les extremités en général, évite la dégradation rapide des ARN. Sur la diapo suivante au niveau chimique si on 
### Epissage
* site donneur permet le premier clivage, à l'extrémit de l'intron on a un site accepteur et site de branchement permet la formation du laceau, le clivage au niveau du site donneur puis formation du laceau et donc rapporchement de l'exon 1 et l'exon 2, l'étape 1 et 2 s'enchaîne pour que l'ARN messager puisse rapprocher l'exon 1 de l'exon 2. U1 reconnait le site donneur, et U2 le site accepteur, puis mise en place de la première étape, élimiation de l'intron, recyclage des protéines et assemblage de l'exon 1 et 2. Etape 1 : reconnaissance du site donneur par U1, puis 2