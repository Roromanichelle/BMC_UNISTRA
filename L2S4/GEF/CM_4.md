Dihybridisme : gènes liés - calcul des distances
On fait un croisement test entre un hétérozygote et un homozygote récessif pour réaliser une cartographie.

AB   --> cis ou couplage
ab 

Double évenement de recombinaison --> gamètes parentales mais elles ont subi deux événements de recombinaison. Les individus formés ont un phénotype parentale mais ont subit une double recombinaisno
Conséquence : aucune sur le plan phénotypique mais si on veut calculer une fréquence de recombinaison. ILs sont comptés comme des parentaux alors qu'ils ont le statut de double recombiné, donc quand on mesure le nombre de recombiné dans un test cross on mesure ceux qui ont un événement de recombinaison, voire un nombre impair mais on oublie ceux qui ont une double voir pair. Nos calculs se basent uniquement sur les évenements simples de recombinaison.

**CCL** : on sous estime la fréquence de recombinaison et également la distance. La distance sera donc plus courte que ce qu'elle n'est en réalité parce qu'on ne tient pas compte de ces évenements.

_Comment peut on y remédier et tenir compte des évenements de recombinaison double?_

Approches mathématiques?

Pour une fréquence en dessous de 10% les deux courbes sont confondus, on peut assimiler le pourcentage de recombinaison et la distance entre les gènes car distance très proches. Plus la fréquence de recombinaison est importante plus on a de chance de se tromper.

Phénomène biologique qui peut se mettre en place qui va lorsqu'un crossing over se met en place va empêcher 
Le phénomène d'interférence qui si il a lieu va empêcher un deuxième crossing over de se produire.
Une infrastructure à un endroit ; ne peut pas être présent à un autre endroit. (complexe synaptonémal)
Dépend des chromosomes ; des espèces ; des régions chromosomiques

--> Conduit à une sous estimation de la distance proposée par Morgan

On va faire un carte dans laquelle on ne va pas se limiter à deux gènes --> on prend trois gènes. 
Test 3 points : méthode de cartographie permettant de positionner 3 gènes et permettant de tenir compte de phénomènes de recombinaisons multiples et même d'interférence. Méthode de cartographie très utilisée pour suivre différents gènes.

Premier exemple: 
On a pas équiprobabilité ; les gènes sont dépendants --> ils vont être liés ou au moins --> deux d'entre eux

Les deux haplotypes parentaux sont majoritaires ; puis AbC et aBc sont minoritaires --> crossing over multible 

S'il n'y a pas d'interférence la fréquence théorique double recombinant correspond aux produits des distances minimum entre 2 deux gènes. Or on en a un peu moins : il s'agit des phénomènes d'interférence
Autant de double recombiné observé que dans notre calcul : l'interférence est égale à 0.

30 : non prise en compte des doubles et des interférences
COmment on peut tenir compte des doubles 

**Test 3 points**: méthode de cartographie qui permet de positionner 3 gènes les uns part rapport aux autres et de mettre en évidence les phénomènes de double recombinaison et d'interférence.

V____ct___cv --> 19,5
<___________>
    18,5

Avec les 18,5 on ne prend pas en compte les phénomènes de double recombinaison.

Principe de calcul qui permet de retouver cette valeur de 19,6 par raisonnement

**Calcul de la distance entre v et cv**

V+cv+ : 134
V-cv- : 134
v+cv- :597
v-cv+ : 583

Fq(rb): 0,185 ---> en regardant les classes phénotypiques on peut en tenir compte
Au niveau des effectifs on constate deux classe majoritaires ; deux classes minoritaires et deux classes intermediaires. Les deux dernières classes sont les recombinés mais phénomène de recombinaison double!!!
 donc : (134+134+3+5) / (597+583)

On peut avoir une première approche où en observant les effectifs on va pouvoir déduire en analysant les phénotypes et les effectifs. On peut trouver l'ordre des gènes et retrouver le phénotype de la femelle.

Les deux classes majoritaires : définissent les hapltoypes parentaux mais ne définissent pas l'ordre des gènes. On connait déjà un génotype partiel :
v-cv+ct+
v+cv-ct-
Double recombinant : vont nous aider pour retrouver l'ordre des gènes

* 3 possibilités d'ordre :
    * v cv ct
    * cv v ct
    * v ct cv
Si on applique un phénomène de double recombinaison quest ce qui me conduit aux classes minoritaires?

# Carte génétique dr

Groupe de liaison : ensemble de gènes que l'on va trouver lié si on réalise une cartographie et notamment une cartographie de type 3 points.

La carte génétique était un moyen (longtemps avant le séquençage et avant l'établissement de caryotype) permettait d'établir le nombre de chromosome. 

La séquence complète du génome de la drosophile a été déterminé. On connait la séquence de chaque chromosomes, on connait la distance physique entre ces gènes. Distance physique : en nombre de paires de base. Mesure de la distance génétique exprimé en cM correspondant à une fréquence de mise en place de crossing over.

Voir diapo : carte génétique de la levure et de la drosophile. Un génome de levure est beaucoup plus recombinogène. Recombinaison plus importante : on arrive plus facilement à étudier ces phénomènes. CCL : l'activité recombinogène n'est pas la même d'un organisme à l'autre et le cM n'a pas la même valeur partout. 1cM=1% de recombinaison ; mais ce pourcentage de recombinaison dépendra de l'espèce considérée

H.Sapiens : pas ou peu de recombinaison au niveau du centromère.