resultat = "" 
for c in "Bonsoir" : 
    resultat = resultat + c 
print(resultat)

n = 10 
while n>=11 : 
    n = n + 2 
print(n) 

def func(val): 
    if val<0.0: 
        return 0 
    return val 

a = func(-1.5) 
print(a)

def fibo(n): 
    a=1 
    b=1  
    lst=[]
    i=0 
    while i < n : 
        lst.append(a)
        b=a 
        a=b+a
        i+=1 
        
    return lst 
print(fibo(3)) 

def inconnu(d,n) :
    for i in range(-n,n) :
       print(d*abs(i))

print(inconnu(10,5))

print(inconnu([1,2,3],3))