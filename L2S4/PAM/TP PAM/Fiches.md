# Diploblastiques ; métazoaires

## Embranchement des spongiaires

* Classe des calcisponges
    * Sous classe des Homocoeles
        * Leucosolenia
    * Sous classe des Hétérocoeles
        * Leucandra
* Classe des Hexactinellide
    * Ordre des Hexastérophores
        * Euplectella
* Classe des Desmosponges
    * Ordre des Monoaxonides
        * Tethya
    * Ordre des Monocératides
        * Euspongia

# Diploblastiques ; eumétazoaires
## Embranchement des cnidaires

* Classe des Hydraires
    * Ordre des Hydraires
        * Sous ordre des Limnoméduses
            * Hydra
        * Sous ordre des Anthoméduses
            * Hydractinia, clava
        * Sous ordre des Leptoméduses
            * Obelia, Kirchenpaueria (pousse sur les algues/feuilles)
    * Ordre des siphonophore
        * Vellela
* Classe des scyphozoaires
    * Pelagia, Aurelia aurita
* Classe des Anthozoaires
    * Sous classe des Octorallidaires
        * Veretillum
        * Alcyonium
        * Coralium
        * Eunicella
    * Sous classe des Hexacorallaires
        * Anemonia

# Triploblastiques ; Eumétazoaires ; protostomiens ; symétrie bilatérale

## Coelomates :

### Embranchement des Plathelminthes

* Classe des turbellariés
    * Ordre des triclades
        * dendrocoelum lacteum
        * polycelis nigra
>
* Classe des cestodes
    * Ordre des cyclophyllidiens
        * Taenia
        * Echinoccocus
>
* Classe des trématodes
    * Ordre des digéniens
        * Fasciola Epatica

## Pseudocoelomates :

### Embranchement des Nematodes

* Ascaris
* Caenorhaptidis elegans

## Acoelomates :

### Embranchement des Annelides (segmentés)

* Classe des polychètes
    * Ordre des errantes
        * Nereis
        * Aphrodite
    * Ordre des sédentaires
        * Sabella (tubicole) 
        * Serpula (tubicole)
        * Arenicola (liminicole)
>
* Classe des oligochètes
    * lombricus terrestris
    * megascolex
>
* Classe des achètes
    * Hirudo medicinalis

# Eumétazoaires, triploblastiques, symétrie bilatérale, cœlomates, protostomiens

## Embranchement des mollusques

* Classe des polyplachophores
    * Chiton
>
* Classe des gastéropodes 
    * Sous classe des prosobranches (streptoneure et cavité paléale à l'avant)
         * Ordre des archéogastéropodes (organes pairs)
            * Platella
            * Haliotis
            * Trochus
        * Ordre des mésogastéropodes (organes impairs)
            * Crepidula
            * Littorina
            * Trivia
        * Ordre des néogastéropodes
            * Buccinum
            * Nassarius
            * Murex
    * Sous classe des Opisthobranches (euthyneures et cavité paléale à l'arrière)
        * Ordre des tectibranches (coquille réduite )
            * Aplysia
        * Ordre des nudibranche (coquille absente )
            * Doris
            * Aeolis
    * Sous classe des pulmonées (cavité paléale sur le côté)
        * Ordre des stylommatophores :deux paires de tentacules, yeux portés au sommet des tentacules post.
            * Helix aspersa
            * Helix pomatia
            * Cepaea
        * Ordre des basommatophores: deux paires de tentacules, yeux portés à la base des tentacules
            * Planorbis
            * Limnaea
>
* Classe des lamellibranches (bivalves
    * Lamellibranches fixés
        * Mytilus
        * Anomia
        * Ostra

    * Lamellibranches nageurs
        * Orticules symétriques : Pecten
        * Orticules asymétriques : Chlamys

    * Lamellibranches fouisseurs
        * Cardium (1,5 cm à 3)
        * Venus (4,5 à 5,5cm)
        * Tapes (paloudes ; 3 à 5cm)
        * Ensis (couteux)
        * Mya

    * Lamellibranches perceurs
        * Phloas

* Classe des scaphopodes
    * Dentalium

>
* Classe des céphalopodes
    * Sous classe des tétrabranchiaux (4 branchies)
        * Nautilus
    * Sous classe des dibranchiaux
        * Ordre des décapodes (10 tentacules)
            * Sepia
            * Loligo
        * Ordre des octopodes (8 tentacules)
            * Eledone
            * Octopus 
            * Argonauta

# Arthroposes

* Classe des Crustacés

    * Sous classe des brachiopodes (présence de péréiopodes ; milieu saumate)
        * Ordre des notostracés (carapace)
            * Apus
        * Ordre des diplostracés (pas de carapace)
            * Daphnia

    * Sous classe des Branchioures (les branchies se situent dans la queue) --> ectoparasite ; hematophage
        * Argulus

    * Sous classe de cirripèdes : hermaphrodite ; mode de vie fixé ;
        * Ordre des thoraciques
            * Balanus
            * Lepas
        * Ordre des rhizocéphales
            * Sacculina Carcini (parasite du crabe)

    * Sous classe de malacostracés (19 segments
    * Groupe des hoplocarides
        * Ordre des stomatopodes
            * Squilla
    * Groupe des péricarides
        * Ordre des isopodes
            * Lygia
            * Oniscus
            * Aselus
            * Anilocra

        * Ordre des amphipodes
            * Gammarus
            * Talitrus
            * Caprella

        *