- [Biologie animale des invertébrés](#biologie-animale-des-invert%C3%A9br%C3%A9s)
    - [I. Les spongiaires](#i-les-spongiaires)
        - [a) Les caractères dérivés et les grands modèles de spongiaires](#a-les-caract%C3%A8res-d%C3%A9riv%C3%A9s-et-les-grands-mod%C3%A8les-de-spongiaires)
        - [b) Fonction d'alimentation](#b-fonction-dalimentation)
        - [c) La reproduction](#c-la-reproduction)
            - [i.Reproduction asexuée](#ireproduction-asexu%C3%A9e)
            - [ii.Reproduction sexuée](#iireproduction-sexu%C3%A9e)
        - [d) Les différents types d'éponge](#d-les-diff%C3%A9rents-types-d%C3%A9ponge)
            - [i.Desmosponges](#idesmosponges)
            - [ii.Hexactinellides](#iihexactinellides)
            - [iii.Les éponges calcaires](#iiiles-%C3%A9ponges-calcaires)
        - [2. Les Eumetazoaires](#2-les-eumetazoaires)
            - [a) La mise en place de l'épithélium](#a-la-mise-en-place-de-l%C3%A9pith%C3%A9lium)
    - [III Les Protostomiens](#iii-les-protostomiens)
        - [A) Les Spiraliens](#a-les-spiraliens)
        - [B) Des Gnatifères aux Syndermates](#b-des-gnatif%C3%A8res-aux-syndermates)
            - [i. Les Rotifères](#i-les-rotif%C3%A8res)
            - [Acanthocéphales](#acanthoc%C3%A9phales)
        - [C) Des Rouphozoaires aux Plathelmynthes](#c-des-rouphozoaires-aux-plathelmynthes)
            - [Les Turbellariés](#les-turbellari%C3%A9s)
                - [1. Le tube digestif](#1-le-tube-digestif)
                - [2. Locomotion et système nerveux](#2-locomotion-et-syst%C3%A8me-nerveux)
                - [3. Les organes excréteurs](#3-les-organes-excr%C3%A9teurs)
                - [4. La Reproduction](#4-la-reproduction)
                    - [a) Reproduction asexuée](#a-reproduction-asexu%C3%A9e)
                    - [b) Reproduction sexuée](#b-reproduction-sexu%C3%A9e)
            - [Les cestodes et les trématodes](#les-cestodes-et-les-tr%C3%A9matodes)
        - [D. Les Lophotrochozoaires](#d-les-lophotrochozoaires)
        - [E. Les Némertes](#e-les-n%C3%A9mertes)
    - [IV. Les annelides](#iv-les-annelides)
        - [A. La métamérie](#a-la-m%C3%A9tam%C3%A9rie)
            - [1. Organisation des métamères autour des sacs coelomiques](#1-organisation-des-m%C3%A9tam%C3%A8res-autour-des-sacs-coelomiques)

# Biologie animale des invertébrés

Invertébrés : groupe paraphylétique car tout sauf les vertebrés quis ont une partie des deuterostomiens ; parler des invertébrés c'est oublié un ancêtre commun (deuterostomiens)

## I. Les spongiaires

Les spongiaires sont des organismes exclusivement aquatiques et pour la grande majorité d'entre eux marins. Les spongiaires sont des organismes qui sont entièrement spécialisés sur la filtration de l'eau. Un spongiaire c'est augmenter les surfaces entre l'animal et le milieux pour pouvoir exploiter la matière organique présent dans le milieu. On peut faire un parallélisme entre ces spongiaires et des végétaux chlorophylliens dont l'objectif est d'augmenter la surface pour récupérer de la lumière.

### a) Les caractères dérivés et les grands modèles de spongiaires

**Ostium** : l'eau va rentrer dans l'éponge (milieu extérieur), elle va rentrer dans un espace délimité par les parois de l'éponge, et va resortir par **l'oscule**.

Épaisseur de la paroi du corps
Coupe dans cette paroi (on va retrouver les même structures), on trouve les deux premiers caractères dériviés : face externe : deux couches de cellules --> pinacoderme et choanoderme --> ces termes s'utilisent uniquement pour les spongiaires. Ces deux couches ne sont pas des épithéliums vrais.

**Pinachocytes** : ce sont des cellules de revetement (cellules de protection)
**Choanocytes** : cellules munies d'un flagelle (opistochonte) --> ce falgelle est un flagelle propulseur. Ces choanocytes vont être les moteurs qui vont faire passer l'eau du côté pinacodermique au côté choanodermique grâce à leur flagelle
Les cellules qui forment les pores de l'éponge sont **les Porocytes**
La mesoglée : gelée (matrice extra cellulaire de collagène) 
Lophocytes : cellules responsables de la mise en place de cette matrice
Sclérocytes : type de cellules qui vont sécreter des spicules (structures rigides qui vont consolider ; structurer la mésoglée et lui donner une tenue ; lui éviter de s'écraser de s'avachir). Ces spicules peuvent être de composition chimique différentes : ils peuvent être en matière organique et en matière minérale
On a des spicules **calcaire** et spicules **siliceux**
Les archéocytes ou amoebocytes sont des cellules qui sont indifférenciées qui vont pouvoir donner tous les types cellulaires différents.
La surface du choanoderme est proportionnelle à la quantité de MO que va pouvoir récupérer l'animal

Surface du choanoderme : permet un apport en matière organique 
Si on a une augmentation de la taille la surface de filtration devient trop petite par rapport à la taille est trop faible. L'apport et la consommation devient.
Ce type **Ascon** ne dépasse pas quelques milimètres.

**Sycon** : on va avoir des vides : on augmente la surface du choanoderme. Avec ce mode on reconnait l'oscule ; l'ostium

**Leucon** : dans toute la paroi on va creuser des cavités ; des miliers et des miliers de cavités avec des petites chambres qui ne contiennent que du choanocyte, oostium vers l'oscule

Espèces particulières : 10 000 chambres vibratiles par mm³

### b) Fonction d'alimentation

Les choanocytes créent un courant d'eau, l'eau va passer. L'ostium a un certain diamètre ; ce qui va passer : des unicellulaires planconiques, des bactéries, des virus, des débrits de MO, mais également des débrits de matière minéral. Tout ce qui ne va pas passer ce qui est plus gros que 50 um
Modalités de la digestion très simples :  par phagocytose
La première modalité que l'on retrouve est une digestion intracellulaire. Les cellules qui vont réaliser la phagocytoses sont les archéocytes.

Les métabolites sortent des archéocytes, diffusions : diffusion au voisinage et pas de mécanismes spécifiques de la distribution. IL y a une distribution qui se fait de façon passive par simple diffusion. La question ne sera pas de savoir s'il y a ou s'il n'y a pas de distribution. De la même façon on ne parlera jamais.
Les déchets vont être rejetés hors de la cellule et diffuser, tout se fait de la cellule vers le milieu extérieu. Distribution, respiration, excrétion --> présent mais rien à dire.

### c) La reproduction 

Peut se voir de deux façons distinctes, la reproduction clonale ou asexuée, il s'agit ici de parler de deux termes opposés mais complémentaire, de fragmentation et regénération.

#### i.Reproduction asexuée

Fragment d'éponge: à partir de pas grand chose on génére un individu adulte mais de plus grande taille. Notion de fragmentaiton qui existe chez de nombreuses éponges d'eau douce. A la fin de 
on va voir une éponge qui se fragmente qui relache des paquets de cellule : ce n'est pas de la déliquescence --> ce sont des paquets qui vont se reformer sur eux même et qui vont former ce qu'on appelle des gemules 
**gemules** : forme de passage de la mauvaise saison, les gemules vont se fixer sur les sédiments à la bonne saison (celles qui ont survécu)
--> reproduciton clonale

#### ii.Reproduction sexuée

Production de gamètes mâles et de gamètes femelles : oeufs fécondés et les oeufs fécondés vont être libérés dans le milieu.
L'individu relache des gamètes et va pénétrer l'ostiole qui va être phagocyté par le choanocyte puis dédifférenciation du choanocyte en archéocyte va migrer dans le tissus et va libérer les spoermatozoides à proximité des ovocytes à côté des spermatozoïdes. Communication des cellules les unes avec les autres. Reconnaissance --> ne doit pas faire l'objet d'une digestion.

### d) Les différents types d'éponge

#### i.Desmosponges

Premier groupe d'éponge : les demosponges (plus grand groupe d'éponge = 8000 espèces), elles sont toutes de type Leucon. Ces demosponges ont des spicules qui sont toujours siliceux ; elle sécréte une protéine particuliére : spongine
espèce la plus classique : spongine officinalis
Les plus grands types leucon peuvent aller jusqu'à un mètre. 

#### ii.Hexactinellides

Spicules à 6 pointes

#### iii.Les éponges calcaires

Sycon elegans

### 2. Les Eumetazoaires

* C'est le taxon frère des spongiaires. 
* Ces métazoaires vrais sont dotés d'un épithelium au sens strict du terme.
Cet epithelium est enraciné dans une lame basale (pas présent au niveau du choanoderme et pinachoderme)
* La digestion ne sera plus intracellulaire mais elle se fera dans un espace que l'on va appeler un sac digestif. Parfois de ce sac on fera un tube ; un sac a 1 orifice ; un tube en a 2.
* Cette oposition entre une digestion intra et extra cellulaire ; la digestion extracellulaire deviendra la norme.
* L'épithelium va se différencier en deux tissus différents ; l'ectoderme et l'endoderme.
* Chez ces eumétazoaires on va voir une différenciation d'un type nouveau des cellules musculaires ;  Des cellules nerveuses ; l'un ne va pas sans l'autre. On a pas de cellules musculaires sans les cellules de contrôle.

#### a) La mise en place de l'épithélium
voir cours papier



## III Les Protostomiens

Coelome : Classification phylogénétique qui ne sont pas faux en terme de description mais en terme d'évolution : faux! 

Les Coelomates ; les pseudocoelomates et les acoelomates.

Spiraliens/ecdysozoaires

### A) Les Spiraliens

* Repose sur un développement de l'oeuf qui va se faire en spiral. On va avoir des subdivisions et les plans de divisions à partir de 8 cellules (3ème division) vont se déplacer en oblique par rapport à un axe qui va aller du pôle végétatif vers le pôle animal. --> caractère dérivé propre des spiraliens.
* On va obtenir des cellules différentes : 
    * 1a --> 4 micromères animmaux
    * 1dA--> 1D : macromères duu pole végétatif
    * Développement spiraliens: concerne tous les organismes présents dans le taxon 

____ COURS PAPIER !!!

### B) Des Gnatifères aux Syndermates

* Gnati : mâchoire ; le pharynx est armé de mâchoire avec des pièces masticatrices. On va regarder les syndermates --> cuticule de kératine produite par les cellules de l'épiderme mais cette cuticule est intracellulaire (caractère dérivé propre des syndermates). Dispartition de la membrane entre deux cellules adjacetntes et formation d'un syncitium épidermique. CCL : cuticule de kératine intracellulaire mais continue grâce au caractère syncitial de l'épiderme.

#### i. Les Rotifères

* Animaux de petite taille qui vivent en eau douce (quelques centaines de micron max), la région antérieure de ces animaux porte une couronne ciliée qui donne son nom au groupe. Cette  courone : les cils battent mais battent en faisant une onde --> organe rotateur (d'où le nom de rotifères).

* Muscle de deux types : des muscles dans un plan : et des muscles perpendiculaires à des muscles circulaires dans une multitude de plans (muscles rétracteurs = muscles longitudinaux). En se contractant les muscles ramènent l'appareil rotateur vers le bas. 
    * Ces animaux ont la capacité de faire du mouvement (par les cils/les muscles)

* Anatomie interne du rotifère
    * Tube digestif placé sur un axe antéro postérieur avec à l'avant la bouche et à l'arrière l'anus.
    * Couronne ciliée autour de la bouche
    * Pharynx après la bouche 
    * Oesophage : relie la cavité bucal à l'estomac
    * Milieu extérieur controlé : on secrète des enzymes grâce aux glandes salivaires et gastriques.
    * Chez les bilatériens : processus de céphalisation (concentration de centre nerveux au niveau de la tête) parce qu'on est au voisinage de la bouche. --> Cerveau : ne définit rien de particulier. Le cerveau n'est qu'un centre de concentration de neurone. Quand il est unique et à l'avant on le nomme cerveau. 
    * Pseudocoelome : cavité creusée entre l'endoderme et le mésoderme --> il s'agit de liquide extracellulaire (liquide interstitielle).
        * Du point de vu physique : incompressibles mais déformables
        * Quest ce qu'un liquide d'un point de vue biologique?
            * Permet de diffuser par rapport à une matrice qui est plus figée
            * Le liquide est brassable ; on peut le prasser beaucoup plus facilement qu'une structure qui contient des matrices de fibres.
            * Déformables mais incompressibles
            * Dans un liquide des substances diffusent mieux que dans un liquide qui contiendraient une matrice.
        * Ce pseudocoelum est une cavité de distribution liquide dans lequel les métabolites vont diffuser facilement.
        * En fonction des compartiments que l'on considère on ne s'intéressera pas aux même propriétés (mais c'est à tort).
    * Les protonéphridies : ce sont les organes excréteurs de ces protifères. C'est la première fois que l'on voit un organe excréteur qui porte un nom particulier. 
    * Cloaque : désigne un carrefour entre les voies urinaires ; génitales ; et digestives. Orifice génital ; orifice digestif ; orifice urinaire = anus

#### Acanthocéphales

* Caractère dérivé du groupe : le fait d'avoir un probosicis en fait un syndermate --> homoplasie car c'est également une convergence évolutive 

* Anatomie :
    * Pas d'appareil digestif
    * Qu'un appareil reproducteur 
    * CCL : ce sont des parasites 
    * Probscyste : permet de s'accrocher à l'organe intestinal de l'hôte.
    * (Gonades sur le schéma)

* Ces Acanthocéphales sont considérés comme des Rotifères devenurs parasites. 

Com : Perte de fonctin = Perte de gènes??? similitude au niveau du génome (pseudogènes ou transposons semblables?)

### C) Des Rouphozoaires aux Plathelmynthes

* L'ingestion des proies se fait par aspiration
* Le tube digestif n'est qu'un simple sac (contradiction avec les bilitariens! axe antéro postérieur avec un anus à l'arriére et une bouche à l'avant)
    * Il s'agit d'une perte lié à ce mode d'alimentation particulier
    * Anatomie reste une anatomie antéro postérieur
    * Les proies vont être aspirée et on va avoir la digestion
* Ce tube digestif présente des subdivisions

#### Les Turbellariés

* Animaux libres, le plus souvent marins
* Ce corps est recouvert par un épithelium cilié 
* On va trouver 3 autres types cellulaires 
    * Des cellules à Rhabdites : cellules qui rejètent vers l'extérieur de baguettes de protéines sulfurées et de phosphate de calcium. Certains en font un organe de défense ; pour d'autres déchets.
    * Des cellules tactiles ciliées : cellules qui apportent des informations sur l'environnement
    * Les cellules glandulaires : cellules qui vont secreter du mucus (un liquide qui va contenir des protéines (glycoprotéine)) Ces mucus vont avoir deux conséquences différentes : lubrifiant et mucus collant (adhésif).

##### 1. Le tube digestif 

* Pas d'anus ; que la bouche 
* Quand le tube digestif va augementer de volume : pas de surface pour l'assimiler --> on va trouver des plis ; des replis ---> augemente la surface d'échange
* Derrière ce tube digestif : pas de cavité --> mais ne veut pas dire qu'il ne va pas y avoir de diffusion.
* Ces animaux sont des carnivores
* L'animal va s'entourer autour de la proie 

##### 2. Locomotion et système nerveux

* Si présence d'une musculature : il y a forcément un contrôle nerveux 
* Système qui va se développer tout au long de l'animal. Ces cordons vont former des ganglions (ganglions cérébroides ou cerveau) et des organes sensoriels : oeil 
Traiter les informations : info --> il y a de la lumière et il n'y en a pas
et l'information qui nous entoure
Parfois les deux systèmes peuvent être séparés. Ici on parle d'oeil parce qu'on a la formation d'une image (information extérieure). On a les deux aspects : photosensibilité et information visuelle.

* Complexité des muscles, qui permet à l'animal de moduler sa forme dans tous les sens. Il n'y a pas de coelome --> mais on a une matrice. Les métabolites ne diffusent pas aussi bien que dans un liquide mais une gelée est déformable et incompressible. Les muscles vont agir de la même façon que si on avait du liquide. Ceci va donc permettre à l'animal d'avoir des déplacements dirigés par les muscles

##### 3. Les organes excréteurs

--> Protonéphridie (comme chez les rotifères) tout le long de l'animal. Ces protonéphridies correspondent à des tubes qui se ramifient. Au bout du tube on a une cellule que l'on appelle une celle flamme.
Cellule flamme : cellule qui repose sur le tube par des prolongements cytoplamiques qui sont espacés les uns des autres. Au centre de la lumière du tube : la cellule a des flagelles. Nous sommes chez les opistochontes donc flagelle propulseur. Du liquide va être prelevé de l'extérieur, et placé dans ce système excréteur. Pour se débarrasser des déchets, on a deux grandes stratégies : soit on cherche les déchets un par un, et on les récupère, soit on ne cherche rien, on met tout dehors. Ccl : deux étapes dans l'excrétion:
* Une étape de filtration : les prolongements cytoplasmiques des cellules flammes sont espacés par des fentes de filtration --> les liquides et les solutés vont passer. Ce qu'on va mettre dans le tube: composition interne de la cellule --> une urine primaire. Cette urine primaire a les même caractéristiques que tous les liquides dans lesquels baignent les tissus. Derrière cette première étape de filtration on a une deuxième étape.
* Le traitement de l'urine va permettre de passer d'une urine primaire à une urine élaborée. (Urine qui a été traitée) --> On récupère ce qui n'a pas à être jeter (A.aminé, A.Gras, Glucose). Dépend du milieu de vie de l'animal. Si l'animal est plus concentré que le milieu ou moins concentré que le milieu en sel. Un animal d'eau douce est plus concentré en ion que son  milieu. Un animal qui vit en milieu marin.

En Résumé : 
* On filtre --> urine primaire
* On traite cette urine primaire
* Régulation ionique et hydrique qui dépendent du milieu de vie

Protonéphridie = organe excréteur
Ici on met tout dehors, tout déchet est mis dehors et on récupère ce que l'on veut.

##### 4. La Reproduction

###### a) Reproduction asexuée
Les turbellariés ont une grande capacité de regénération : 1/300 de l'organe suffit pour en former 1 entier.
Reproduction clonale : l'organisme se divise par sisciparité ou par bourgeonnement 

###### b) Reproduction sexuée

Hermaphrodite protandres: (d'abord male puis ensuite femelle)il y a des organes mâles et des organes femelles.  Les turbellariés sont hermahrodites. Ce n'est pas parce que l'animal est hermaphrodite qu'il va y avoir une autofécondation.
Le système mâle est mature en premier. --> fécondation croisée (les deux systèmes ne sont pas matures en même temps!)

#### Les cestodes et les trématodes 

Ce sont des parasites qui présentent des morphologies orientées sur cette vie parasitaire
Fasciola Hepatica --> grosse ventouse buccale qui permet à l'animal de se fixer. Chez les cestodes on va beaucoup plus loin : le corps est rubané, constitué de segments (proglotis) qui vont se former à l'arrière de la partie antérieure (à l'avant de l'animal (tête = scolex--> crochets et ventouses pour se fixer au tube digestif de l'autre)). Cette partie est appelée le scoles --> l'animal va se fixer grâce à ce scolex. Il s'agit ici d'un tube digestif à l'envers. Les microvillosités se trouvent à l'extérieur. Distribution centrifuge et non centripète. On retrouve les caractéristiques d'un endothélium. Microtriches = homologues des microvillosités de entérocytes/
Autofécondation : repliement segments que l'on appelle des proglottis vont se détacher à l'arrière (5 à 10 segments par jour qui contiennent 5000 oeufs).

### D. Les Lophotrochozoaires

Trochozoaire : vient de larve trocophore qui est une larve caractéristique de ce groupe et qui possède une forme de toupie sur laquelle on va retrouver le tube digestif. On va trouver des couronnes ciliées (ciliature apicale) en dessous --> une autre couronne de ciliature Prototroche  puis Métatroche et une télotroche.
On a également un massif mésodermique et une future chaîne nerveuse ventrale.

Larve trocophore = caractère dérivé propre

Les mollusques sont ce qu'on devrait appeler des Trochozoaires ; toutefois cette larve trocophore possède des modifications particulières qui vont faire qui vont en faire une larve velligère.

### E. Les Némertes

Ce sont des vers longs, pas de segmentation --> on les a mis en relation avec les plathelminthes. Ces animaux sont considérés comme des acoelomates. Leur caractère particulier --> **Proboscis** (marque de fabrique des Némerte (également des Arcantocéphales)) --> convergence évolutive. Proboscis entouré de muscle

**Rhyncocoele**: cavité remplie de liquide et creusée entres les muscles dans autour du proboscis --> rétracteurs et des muscles circulaires. On a une cavité de liquide qiu est entre les muscles. C'est donc une cavité creusée dans du mésoderme. C'est la définition des coelomes.
Etre coelomate c'est avoir des cavités creusées dans le mésoderme. Ici on ne peut pas les considérer comme des coelomates parce que ce n'est pas un caractère générale. Cette cavité n'a qu'un intérêt --> avoir du liquide sur lequel les muscles peuvent agir. Permet de transmettre une force, apposée par les muscles sur un compartiment dont on va pouvoir modifier la forme. C'est ce que l'on appelle un **hydrosquelette** ou un squelette hydrostatique. 

Les Némertes possédent un **tube digestif orienté**. Linéus Longissimus (jusqu'à 30m). On trouve dans ce groupe ce que l'on considére être les plus longs animaux du monde vivant.
Associé à l'alimentation il y a une distribution. On va voir pour la première fois un sstème circulatoire qui n'est rien d'autre qu'une distribution orientée. 

Cellules myoépithéliales --> vaisseaux simples (muscle longitudinaux et muscles circulaires) --> les muscles longitudinaux ici sont à l'extérieur. On a une cavité creusée dans un environnement mésodermique remplie de liquide coelomique. 
Ce système circulatoire est un système clos (il est bouclé sur lui même). Type du système circulatoire (clos, ouvert, pseudo-ouvert ne donne aucune information sur l'évolution.)
Le liquide avance dans ce système grâce à une onde de contraction. Péristaltisme : pas un endroit particulier responsable de fiare avancer ce liquide --> c'est une propriété du système. 

La locomotion se fait par leur musculatuer et par leur épiderme cilié pui par la mise en place d'un **péristaltisme**

## IV. Les annelides

(une annelide)

### A. La métamérie

Figure 40 : on voit comment on passe de la larve au vers. Prostomium et Peristomium --> ne sont pas des métamères.
Un métamère est un segment mais tous les segments ne sont pas des métamères
Les deux mécanismes n'ont rien à voir l'un avec l'autre. 

Dans une organisation métamérisé on va avoir une succession de segments identiques les uns aux autres.

#### 1. Organisation des métamères autour des sacs coelomiques

Cette segementation affecte l'ectoderme et le mésoderme. Dans chaque segment il va y avoir une paire de vésicules coelomiques (une paire de sacs coelomiques 1 à droite et 1 à gauche). Ces sacs coelomiques. Deux caissons (deux demies cylindres) sont remplis de liquide coelomique. Le métamère s'organise autour d'une paire de vésicule coelomiques. Ces caissons de coelome possèdent des contacts entre eux. Ces caissons s'affrontent. Contact du même segement au dessus et en dessous du tube digestif : correspond au **mésentère** (deux caissons coelomiques du même segment!). Caissons coelomiques qui heurtent un autre segment : c'est ce que l'on appelle le **dissépiment** (acolement de deux parois de caissons coelomiques de deux segments qui se suivent).
Ces caissons coelomiques peuvent être au contact d'autre chose que des contacts coelomiques (soit vers l'intérieur ; soit vert l'extérieur) La splachnopleure ou le feuille viscérale.
La somatompleure ou feuillet pariétal ; splanchnopleure ou feuillet vicéral.

De nombreux systèmes vont être affectés par la métamérie ; d'autres qui ne sont pas affectés par la métamérie.

Le tube digestif n'est pas métamérisé. Le système circulatoire non plus.

Segmentation au niveau des organes excréteurs

Pas de protonéphridie chez les annelides --> ce sont les organes excréteurs de la larve et ne se retrouvent pas chez l'adulte. 
Ces métanéphridies apparaissent avec ces caissons coelomiques.Elles n'ont pas la même origine. Elles ont une origine mésodermique. Ce système s'ouvre dans une cavité --> néphrostome où on a un pavillon avec des cils autour qui vont aspirer du liquide --> on obtient une urine primaire puis on excréte l'urine élaborée par le néphridiopore..

Organes intersegmentaires

Le système nerveux est à la fois affecté ; à la fois non. Développement des ganglions nerveux plus important dans chaque segment mais système nerveux continu.




