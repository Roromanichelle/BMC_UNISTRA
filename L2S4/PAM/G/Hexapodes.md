# F) Des Hexapodes aux

## 2) Le développement des hexapode : aptérygotes
### a) Les Hexapodes amétaboles
Le juvénile ne va pas changer de morphologie, croissance au cours des mues.
Mues imaginale : mue qui fait la transition entre l'imago (=maturité sexuelle) et l'organisme juvénile.
### b) Les insectes hétérométaboles ; ptégygotes
Les insectes sont des pterygotes, présence d'ailes. Il y a une mue particulière qui donne les ailes.
Larve, nymphe, adulte. La première mue = mue nymphale (donne les ailes), puis mue imaginale. 
-Paurométaboles : les individus ont le même mode de vie et vivent au même endroit,
-Hemimétaboles : la larve ne vit pas dans le même milieu que l'adulte, ex : les ogonatoctères.
### c) Les insectes Holométaboles

Permet de réduire la compétition entre les individus et façon pour l'espèce de mieux gérer la globalité du milieu écologique.

## 3) Pièces buccales des insectes
A gauche : criquet 
Pièces buccales : au nombre de 4
Labre = pièce unique frontale ; 2 Mandibules ; 2 Maxilles ; 2 labium
Lypopharynx : 
Sur ces pièces buccales, on va pouvoir trouver des palpes (=structures sensorielles), situées sur le maxille ou sur le labium. 
On voit que mandibule et maxille ont un rôle différent.
Mandibule : deux parties --> molaire : broie / incisive : coupe

Une diversité se contstruit sur l'unité --> on arrive à une multiplicité des morphologies.

## 4) Autre fonction

Nutrtion :
Intestin subdivisé : Première partie = intestin moyen ou mmésentheron ; qui s'oppose à l'intestin postérieur = iléon ; rectum. CHez les arachnides et chez les myriapodes ; Tube de Malpighi = tubes fermés à une extremité. Sécretion , l'urine va être fabriqué dans le tube par sécretion. Idée de base : on prend tout et on récupère ce que l'on veut récupéré ; ici pas de filtration ; on va avoir une secretion --> les produits vont être sécrétés un par un, on va également sécreter des ions de façon à ce que l'eau suive. Cependant cette eau est perdue puisqu'on la secrete dans le tube pour que le contenu du tube avance. Une fois dans le tube digestif : les deux se mélangent et l'eau n'est plus utilise à la mobilité dans l'intestin, le rectum va alors réabsorber de l'eau. Chez nous, dans l'intestin grêle, le contenu du TB est très liquide, et l'intestin est le lieu de la réabsorption hydrique, et nous empêche de perdre de grandes quantités d'eau. = **Adaptation à la vie aérienne**

Respiration :
Stigmates = point d'entrée au niveau d'un organe très ramifié. Ce système respiratoire assure lui même la distribution des gazs partout ou il va aller. DIstribution centripète. Grand trons : voies de communication entre les segmes --> ce ne sont pas les lieux d'échange. Ces troncs servent à amener de l'oxygène dans les segments où il n'y a pas de stigmate. (ex: encéphale). Mécanisme de ventilation très fort : l'air peut entrer au niveau du thorax et sortent de l'abdomen. Déplacement par diffusion dans le tronc trachéen jusqu'à lencéphale, ou au niveau des stigmates.
Stigamete eet déshydratation? De nombreux insectes possèdent des stratégies d'ouverture et de fermeture des stigmates.

## 5) Le vol

IL existe différentes modalités pour actionner les ailes. Insertion directe et indirecte
Indirecte: ailes insérées entre tergite et pleurite ; le mouvement des tergites va être transmis aux ailes. et permetd'entretenir le monvement vibratoire. On peut obtenir des fréquences d'ailes que l'on ne pourrait atteindre par des simples mouvement contration/décontractions. 1000 Hz : 1000 battements à la seconde.

Le vol? l'animal est porté par le milieu?
Au départ : la locomotion est un appui sur le substrat.
Je me déplace : je fais un appui dessus. On peut faire cela sur du sol. Se déplacer dans le milieu aérien : bave pour réduire la force de frottement, on peut également porter l'animal sur des apendices. ARticulation : on fait une économie monstrueuse puisque c'est le squelette qui porte. Ce n'est donc plus une force de frottement mais un déplacement de poids.Le vol c'est comment faire pour que lo corps ne soit plus porté par lui même, mais porté par autre chose. Tout milieu dans lequel on est exerce sur le corps. QUand une aile avance : surpression au dessous et dépression au dessus, qui s'additionne et qui augmente la force de portence. 
Le vol est-il une économie d'énergie? Oui et non
Ce qui est important lorsqu'on se déplace : la vitesse (distance et temps) ; cout du vol : par rapport au temps? par rapport à la distance. Un insecte qui vole est au maximum de son métabolisme par unité de temps.
Son déplacement est beaucoup plus grand par distance parcouru, est très largement diminué ; pour une même quantité d'énergie on peut exploiter une plus grande surface. 

# G Des Nématoozoaires aux Nématodes

Chez les Nématozoaires : nous avons deux caractéristiques générales : la chitine est en très forte régression. Dans la cuticule la chitine est globalement remplacée par du collagène. Il n'y aura dans ce groupe que des muscles longitudinaux.

## 1) Présentation génrale des nématodes

Ces nématodes sont des vers ronds, le corps n'est pas segmenté, et sous la cuticule il y a un épiderme, qui est syncitiale, qui sécrete la cuticule de collagène.s En dessous de cette cuticule, on va retrouver les muscles longitudinaux que l'on va retrouver en 4 bandes. Dans le plan verticale, on trouve deux nerfs et dans le plan horizontal : deux canaux excréteus. Les deux nerfs se rejoignent au niveua du cerveau et forment un anneau. Autour du TD on a une cavité : un hemocoele. 4 muscles longitudinaux. Locomotion? Comment envisager qu'un système fonction, si tous les muscles sont dans le même sens.
Ici il y a à la fois une mue et une croissance intermue qui sont possibles.


### a) La nutrition des nématodes

Ici cette notion est très claire, si un groupe a un rôle fondamental à jouer = groupe des nématodes. 
On peut soit avoir une cavité buccale, soit un stylet, soit des dents, on va avoir de tout, des détritivores, des herbivores, des parasites. Dans cette partie on va illustrer les détritivores.
Une pomme pourrie =90 000 nématodes moyennes du nombre des nématodes d'une pomme en décomposition.
En regardant sous un mètre carré de plage : on trouve 4 mio 500 mille nématodes. Dans ce groupe on a environ 20 mille espèces.

