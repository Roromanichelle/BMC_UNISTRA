- [PAM 1](#pam-1)
    - [Introduction](#introduction)
        - [_a) Biologie?_](#a-biologie)
        - [_b) Autonomie du vivant ?_](#b-autonomie-du-vivant)
            - [i) Limite à l'autonomie](#i-limite-%C3%A0-lautonomie)
            - [ii) Notion d'adaption](#ii-notion-dadaption)
            - [iii) Bénéfice de l'un implique perte de l'autre](#iii-b%C3%A9n%C3%A9fice-de-lun-implique-perte-de-lautre)
    - [La place des animaux dans le monde vivant](#la-place-des-animaux-dans-le-monde-vivant)
        - [_a) Les animaux sont des eucaryotes_](#a-les-animaux-sont-des-eucaryotes)
        - [_b) Les eucaryotes, des subdivisions complexes_](#b-les-eucaryotes-des-subdivisions-complexes)
        - [_c) Les métazoaires ou animaux_](#c-les-m%C3%A9tazoaires-ou-animaux)
    - [Plan d'organisation et anatomie fonctionnelle des métazoaires](#plan-dorganisation-et-anatomie-fonctionnelle-des-m%C3%A9tazoaires)
        - [_a) Les critères de science_](#a-les-crit%C3%A8res-de-science)
        - [_b) Les classifications phylogénétiques : émergence_](#b-les-classifications-phylog%C3%A9n%C3%A9tiques-%C3%A9mergence)
    - [La classification phylogénétiques : notion de base](#la-classification-phylog%C3%A9n%C3%A9tiques-notion-de-base)
        - [a)](#a)
        - [b)](#b)
    - [L'organisme animal : un système physico chimique en intéraction avec son environnement](#lorganisme-animal-un-syst%C3%A8me-physico-chimique-en-int%C3%A9raction-avec-son-environnement)
        - [_a) L'alimentation_](#a-lalimentation)
        - [_b) La digestion_](#b-la-digestion)
        - [_c) La distribution_](#c-la-distribution)
        - [_d) La respiration_](#d-la-respiration)
        - [_e) L'excrétion ou la régulation du milieu intérieur_](#e-lexcr%C3%A9tion-ou-la-r%C3%A9gulation-du-milieu-int%C3%A9rieur)
        - [_f) Les systèmes de coordination_](#f-les-syst%C3%A8mes-de-coordination)
        - [_g) Les fonctions tégumentaires_](#g-les-fonctions-t%C3%A9gumentaires)
        - [_h) La fonction de locomotion_](#h-la-fonction-de-locomotion)
    - [III Les Protostomiens](#iii-les-protostomiens)
        - [A) Les Spiraliens](#a-les-spiraliens)
        - [B) Des Gnatifères aux Syndermates](#b-des-gnatif%C3%A8res-aux-syndermates)
            - [i. Les Rotifères](#i-les-rotif%C3%A8res)
            - [Acanthocéphales](#acanthoc%C3%A9phales)
        - [C) Des Rouphozoaires aux Plathelmynthes](#c-des-rouphozoaires-aux-plathelmynthes)
            - [Les Turbellariés](#les-turbellari%C3%A9s)
                - [1. Le tube digestif](#1-le-tube-digestif)
                - [2. Locomotion et système nerveux](#2-locomotion-et-syst%C3%A8me-nerveux)
                - [3. Les organes excréteurs](#3-les-organes-excr%C3%A9teurs)
                - [4. La Reproduction](#4-la-reproduction)
                    - [a) Reproduction sexuée](#a-reproduction-sexu%C3%A9e)
                    - [b) Reproduction asexuée](#b-reproduction-asexu%C3%A9e)
            - [Les cestodes et les trématodes](#les-cestodes-et-les-tr%C3%A9matodes)
        - [D. Les Lophotrochozoaires](#d-les-lophotrochozoaires)
        - [E. Les Némertes](#e-les-n%C3%A9mertes)
    - [IV. Les annelides](#iv-les-annelides)
        - [A. La métamérie](#a-la-m%C3%A9tam%C3%A9rie)
            - [1. Organisation des métamères autour des sacs coelomiques](#1-organisation-des-m%C3%A9tam%C3%A8res-autour-des-sacs-coelomiques)

# PAM 1

## Introduction

### _a) Biologie?_
 * bios : le vivant
 * logos : le discours
La discussion autour du vivant, l'argumentation --> notion de raisonnement et de démonstration
vivant? la capacité de se reproduire, avoir un métabolisme, pouvoir évoluer, intéraction avec l'environnement.
Un organisme se développe, se maintient et se reproduit

### _b) Autonomie du vivant ?_

 * Le tout se fait grâce à un plan, un plan de maintient qui est codé par son patrimoine génétique
 * La suite d'opérations dépend de l'expression de ce que l'organisme contient. Il y a là une notion d'autonomie
 * Dans le cas du vivant, c'est son information interne qui va condtionner ce qu'il va devenir (développement, maintien, reproduction, ...). 
 

#### i) Limite à l'autonomie

 * Vivre c'est exploiter son patrimoine génétique, pour développer, maintenir et reproduire un organisme.
 * Développer, maintenir, reproduire un organisme c'est former de l'ordre à partir du désordre. Ceci est thermodynamiquement défavorable: il faut donc prélever de l'Energie dans l'environnement. Or cette énergie est limitée: il faut donc se battre pour elle. La vie est un combat pour l'Energie, à toutes les échelles, entre individus, entre espèces, entre plans d'organisations.
 * Extraire l'Energie et la matière de l'environnement pour former, maintenir et reproduire un organisme: c'est le métabolisme.
 * Notion à la base de l'écologie (relation des organismes avec leur milieu) ; la vie est un combat pour l'énergie dans lequel les organismes vont être en compétition les uns avec les autres car les ressources sont limitées, on reçoit un certain nombre de lumen par m2, il y a des ressources physiquement limitées --> on ne peut pas avoir de croissance biologique illimitée 


#### ii) Notion d'adaption

* Adaptation au sens trivial : on s'adapte à son environnement
* Adaptation au sens biologique durable : modifications des caractéres qui vont être transmis à la descendance --> évolution
* Caractères adaptatifs de l'espèce (Séléction de solution adaptative)
* L'Evolution est une conséquence de ce qu'est le vivant


#### iii) Bénéfice de l'un implique perte de l'autre

* Course à l'armement
* Comment les organismes développent des solutions pour se développer en compétition avec les autres et pour exploiter les ressource énergétiques?
* Anatomie commparée : elle est à comprendre comme une solution d'adaptation aux milieux, qui n'est à comprendre que comme un moteur de l'évolution donc de cette relation complexe entre l'ensemble des organismes vivants. 
    * L'autonomie qui repose sur le patrimoine génétique
    * La relation à l'énergie


## La place des animaux dans le monde vivant

Les animaux sont dit hétérotrophes pour le carbone, ils vont prélever leur énergie à partir de la matière carbonnée 

### _a) Les animaux sont des eucaryotes_

 * ADN contenu dans un noyau délimité par une enveloppe nucléaire
 * La division cellulaire de ces Eucaryotes est une mitose qui font intervenir les centromères et un fuseau mitotique formé de microtubules cytoplasmiques
 * Les végétaux ont perdu leur centrosome (particularité)
 * Ces microtubules sont des polymères de tubuline --> constituant majeur du cytosquelette. Ces microtubules vont être à la base de la formation de flagelles
 * Cytosquelette --> 3 échelles d'action: dedans, en surface et déplacement de la cellule

    
### _b) Les eucaryotes, des subdivisions complexes_

On a deux subdivisions principales: les animaux et les végétaux.

Tubuline? La structure n'est pas la même chez tous, pas la même séquence en AA.

Deux séparations:
 * Bichontes : deux flagelles
 * Unichonte : un flagelle unique

Les opisthochontes: possèdent la particularité d'avoir un flagelle propulseur
 * Séquence de fonctionnement qui est inversé
 * Toute la descendance en est marquée : on a deux grands groupes: 
    * Les champignons
    * Les métazoaires
 * Capacité de produire de la chitine

Eucaryotes --> unichontes --> opisthochontes --> métazoaire

### _c) Les métazoaires ou animaux_

**Caractéristiques:**
 * Cellules différenciées organisées en tissus
 * Matrice extra cellulaire entre ces cellules (caractère très spécifique des métazoaires) --> collagène ; Organisme avec collagène --> suffisant pour définir les métazoaires
 * A la fin de la méiose nous avons des gamètes et pas des spores
 * Caractère qui n'est pas vrai pour tous les métazoaires --> présence de desmosomes, de jonctions serrées entre les cellules ; attention faux chez les spongiaires
 * D'autres molécules sont spécifiques des métazoaires: intégrines et fibronectine

## Plan d'organisation et anatomie fonctionnelle des métazoaires

### _a) Les critères de science_


>" _L'ensemble des connaissances, d'études d'une valeur universelle caractérisées par un objet ou domaine et une méthode déterminées, fondées sur des realtions objectives vérifiables._ "
>

>_" La science a pour objet de décrire des réalités par une recherche constante._"
>

**Commentaire :**
Des réalités: ce que nous sommes capables de mesurer, de percevoir par nos sens, d'appréhender. Ce mot n'a rien à voir avec la vérité. En science nous connaissons des réalités, et ces réalités nous permettent de tirer des situations reproductives et vérifiables.
La science a pour objet de décrire la réalité et non la vérité
Si on peut contredire quelque chose qui a été dit en science on est dans la recherche des réalités et non de la vérité. Nous serons amenés à reprendre ce que nous avons vu dans le passé. La notion de science est une notion continue : On ne peut pas ignorer ce qui a existait, même si on le contredit.

>_" Elle a pour objectif ultime de décrire, d'analyser de comprendre et d'expliquer et non pas d'agir._"
>

**Commentaire :**
Relation de causalité entre les performances de lecture et la pointure des pieds --> ils sont eux même relier à un autre facteur, causalité ou corrélation? 
Lien avec le développement psychomoteur.
Etude de documents: un document permet de tirer une réalité, il faut étudier un document pour que l'on parle dessus, il faut ingurgiter les données. 1ère étape: la description d'un document est un élément clé
Méthode: l'un voit le document, l'autre le ne voit pas, le premier décrit le document l'autre doit le comprendre et doit décrire les informations que contient le document originale.

Comment mettre les documents en relation les uns avec les autre? Comportement qui les aurait lié?


 * Observation : demande rigueur
 * Analyse : demande expérience
 * Synthèse : comment je les confronte avec ce que je sais et ce que les autre savent 



Le fait de ne pas comprendre n'est pas de la non science, la plupart du temps c'est un avoeu d'honneteté, ne pas essayer de comprendre est de la non science.
La science a pour mission de livrer tout ce qu'elle sait.
La science doit être sociétale, elle n'est pas individuelle. Le scientifique donne des explication rationnelle mais ne doit pas être décisionnaire. Il ne doit pas prendre de décision politique dans son intéret mais participe à la politique en donnnant son expérience 

Si on ne fait pas ce travail d'observation et d'analyse il ne sert à rien de donner une explication 

Exemple du réchauffement climatique: 
Evaluation d'une augmentation de la température depuis une centaine d'année, on ne peut pas nier ce point. Des évaluations de température on en a eu tout au cours de l'histoire géologique. T°C de 5à 6° de plus de 5 à 10 000 ans et pas sur un siècle. Au moyen âge les températures étaient beaucoup plus élevée.
Il y a des rythmes qui sont des variations de température.
AUtre réalité: l'espèce humaine utilise des réserves fossiles qui rejètent dans l'atmosphère qui est bénéficiaire par rapport à l'activité sur la planète
Le C02 augment l'effet de serre. L'Homme participe donc au réchauffement climatique. On en conclut que l'Homme est seul responsable de l'élévation de température? On ne peut pas, incertitude sur les modèles. Quelles décisions politiques?
Ce n'est plus le rôle du scientifique.
La science n'a pas pour vocation d'agir. On a des infos et ces infos sont honnetes cela ne veut pas dire qu'elle est réel.

>"_Elle a le souci permanent de produire des critéres de validation publique._"

**Commentaire :**
Lorsqu'on a un résultat on s'attend à ce que ces résultats soient expliqués.
La science est refutable, on peut critiquer la science, mais uniquement avec des arguments de science, des arguments descriptifs et compréhensibles.

>"_Rien n'est plus dangereux qu'une idée quand on a qu'une idée_" - Philosophe Alain (20 ème siècle)

>_" Quand j'ai une idée je la détruits c'est ma seule façon de la vérifier_" ; essayer de démonter ce que l'on raconte.

Platon - réfléchir c'est dire non aux apparences

### _b) Les classifications phylogénétiques : émergence_


 * L'hitoire de la science est une histoire évolutive
>
 * L'homme est doué du langage (réalité). L'Homme par le langage nomme ce qui l'entoure : c'est le début de la science, à partir du moment où l'on nomme on fait une relation logique entre un objet et un mot. Ce que je nomme par un mot le différencie d'un autre mot. On trie les mots, on les classe. La première classification apparait avec le langage. 
>
 * Premier exemple de classification du monde vivant, premier siècle, Dioscoride a classé les plantes en 5 catégories: les plantes aromatiques ; comestibles ; médicinales ; vénéneuses et vineuses (celles qu'on peut vinifier) --> **Classification utilitaire** mais classification **artificielle** car elle est imposée par l'Homme. Pendant longtemps on s'est demandé: artificiel =/ naturel. Existe t-il des classifications naturelles ?
>
 * 18 ème siècle : Georges Cuvier, naturaliste. Il a proposé une classificaiton du monde animale en 4 embranchements, 4 groupes :
    * Les Radiata (plan de symétrie radiale (oursins, étoiles de mer))
    * Les Annelida (annélides)
    * Les Arthropoda
    * Le Vertebrata
    --> classification artificielle : nous choisissons le critére pour créer le groupe
>
 * Existe t-il des classifications naturelles? Oui mais qui s'impose à nous pas par choix mais parce que le monde vivant nous oblige à ce choix
    * Modifications qui s'impose à la descendance et qui sont des héritages transmis par le support de l'information génétique.
    * Le monde vivant s'est diversifié et cette diversification traduit l'adaptation et l'évolution. On peut retrouver une histoire du monde vivant et en fonction des branches on peut donner un nom à ces branches et on va obtenir une classification qui n'est pas mienne mais celui que le monde vivant raconte. Histoire que l'on va essayer de lire. Mais sera t-elle vraie? NON Il n'est pas possible de refaire 3,5 mia d'année en étant un spectateur et en le regardant de façon acceléré. On va reconstituer l'histoire à partir de morceaux qu'elle nous a laissé. (ex: trâce d'une histoire passée dans notre génome)

Dans ces classifications on va retracer l'histoire du monde vivant, on va essayer de retracer qui est plus proche de qui. IL ne faut pas confondre généalogie et phylogénie.

Nageoire actinoprérygien: nageoire rayonnante
Nageoire charnue : (os, disposé de façon longitudinale et pas à côté les uns des autres)

Qui est le plus proche parent de qui? individu qui se resemblent ne sont pas systématiquement proches

## La classification phylogénétiques : notion de base

### a)

 * La classification phylogénétique est également appelée de la cladistique: c'est une méthode d'analyse des caractères qui vise à mettre en évidence la séquence évolutive de leur transformation (qui vise à mettre en évidence leur évolution dans le temps.
>
Évenement qui différencie un avant et un après: notion de classification va reposer sur une polarisation des évenements. La phenetique est un degré de ressemblance.
>
 * Wlly eling: Notion de caractères dérivés et groupe monophyhlétique
On va polariser les choses
Notion de caractères dérivés: caractère qui apparait
Changement: il y a un avant et un après , apomorphe (apo=nouveau) signifie caractère nouveau. Plésiomorphe signifie avant c'était comme ça.
>
Caractère dérivé vient d'un ancêtre : on ne peut jamais trouver d'ancêtre. ex: mammifères: glandes mammaires et les poils
On va trouver un premier animal qui va posséder des poils. Conclusion: Un individe qui existe et qui est inconnu.
Autapomorphie apo=nouveau ; aut=autonome --> un caractère nouveau qui est propre à un groupe et qui permet de définir ce groupe. 
>
synapomorphie : syn : que nous partageons 
symplésiomorphie:  syn : que nous patageons dans le passé ; avant la création du groupe par rapport à l'autapomorphie
>

**_Définitions_**:
 * **Groupe monophylétique** : Goupe qui contient l'ancêtre et tous ses descendants
 * **Groupe paraphylétique** : On a un ancêtre et pas tout ses descendants --> reptiles : les pseuropsydées (ancêtre: tous ses descendants sauf les anciens)
 * Quand on a un groupe monophylétique: c'est un **clade**
 * Taxon = groupe ; un clade est un taxon mais un taxon n'est pas forcément un clade.
 * **Groupe polyphylétique** : groupes où il y a une construction intellectuelle qui regroupe des choses qui sont semblables qui n'ont pas dancêtre commun et qui partage le caractère qui les a associé les uns avec les autres
    * ex: les animaux qui volent : les insectes / les oiseaux --> pas d'ancêtre commun / les algues sont polyphylétiques --> groupes qui ont des origines distincte
    * Ce sont des groupes où l'on a ramené ensemble sous un vocable des espèces qui partagent un caractère mais où les caractères partagés n'est pas le fruit d'une évolution commune.
        *  --> convergence évolutive : reselectionner les même solutions mais de façon totalement indépendante = **homoplasie**
        *  homologie : ils sont analogues et ont la même origine

### b)

 * Idée : replacer des évolutions parentés de groupes pour pouvoir bien comprendre leur position dans l'arbre du vivant. Si on veut regarder qui est avant et qui est après il nous faut un référentiel ; il faut être certain d'avoir quelque chose de bien avant et qui va servir de référentiel par rapport auquel on peut comparer le reste : c'est qu'on appelle un extra-groupe
 * 0 : si le caractère est à l'état ancêstrale --> plésiomorphe 
 * 1 : si le caractère est apomorphe
 * Caractère où l'ensemble des individus que l'on veut trier sont dans l'extra groupe n'est pas intéressant
 * Membre : on veut trier les tétrapodes --> ne nous apporte rien
 * état plésiomorphe : fait d'avoir des dents --> 1 c'est le seul qui n'en a pas (absence : caractère nouveau)
 * Ccl : caractère informatif s'il n'est pas chez tout le monde ; s'il n'est pas absent ; s'il n'est pas présent que chez un seul individu

--> faire une matrice de caractères
--> dessiner les arbres possibles et poser sur cet arbre les événements

* Ce qui repose sur moins d'événement est plus probable que ce qui nécéssite plus d'événement --> on retient l'hypothèse la plus probable est celle que l'on valide temporairement (principe de parcimonie)
* Aspect essentiel : dans le cas où on regarde 4 individus avec 6 caractères la solution la plus parcimonieuse est celle du milieu ;mais si on regarde d'autres caractères?? on doit rajouter des caractères tant qu'on peut en rajouter.

Une classification naturelle existe à partir du moment où l'on observe et que l'on décrive suffisamment la nature pour que l'analyse nous livre une histoire qui s'impose à nous à la différence d'une histoire que j'ai choisis.

## L'organisme animal : un système physico chimique en intéraction avec son environnement

* Anatomie fonctionnelle : anatomie qui a u ne fonction --> qui sert à quelque chose
* La vie est un combat pour l'énergie 

### _a) L'alimentation_

* prise alimentaire 
* Il faut rendre l'aliment assimilable
 
### _b) La digestion_
* Fragmentation mécanique
* Fragmentation acide
* Fragmentation enzymatique
    * glucides --> oses
    * Protéines --> AA
    * lipides --> Acides gras
* Vont pouvoir passer l'épithélium du système digestif
* Que signifie être "dans l'organisme"? à partir du moment où on est dans la cellule
* Le milieu intérier : l'ensemble des liquides extra cellulaires
* Besoin de distribuer spatiallement

### _c) La distribution_

* Distribution spatialle (distribution ) (ex : appareil circulatoire)
* Distribution temporelle (ex: glycogène qui va permettre de relarguer le glucose ; les tissus adipeux)
* On peut utiliser les métabolites comme précurseurs pour la synthèse (AcA-->P ; AcG -->membrane)
* On peut utiliser les métabolites comme source d'énergie : ex --> glucose

* Energie sous forme ATP
    * Transport de molécules
    * Pompes
    * Transports de vésicules sur le cytosquelette
    * Déplacement
    * Déformation
    * Mouvement de l'organisme par la contraction musculaire
    * 30,5 kJ/mol--> énergie libérée par l'hydrolyse de l'ATP
    * On module l'activité d'une protéine : possibilité de réguler son activité en la phophorylant
    * Pour des questions de rendement énergétique la dégradation aérobie des aliments a été séléctionné par les animaux

### _d) La respiration_
        
* C'est la fonction qui permet à un organisme de prélever de l'oxygène et rejeter du CO2 dans son milieu extérieur
* ATP : pour avoir de l'énergie partout et tout le temps "monnaie énergétique"
* 1100 kj/mol comparé à 2990 ? --> rendement du métabolisme aérobie par rapport au rendement de la combusion? les 1800 kj restant correspondent à la chaleur libérée

### _e) L'excrétion ou la régulation du milieu intérieur_
        
* Réguler : quest ce qui entre / quest ce qui sort ?
* CO2 ? Excrétion? respiration?

L'ensemble ne peut pas fonctionner sans coordination

### _f) Les systèmes de coordination_

* Les molécules chimiques vont être relachées à des distances très courtes de la cible dans le système nerveux.
* Les messages nerveux et le message endocrine sont de même nature, ils vont permettre une communication d'une cellule à l'autre.
* Système nerveux : message éléctrique? oui mais la communication commence après la libération de neurotransmetteur.
* La concentration au voisinage de la cible? 
* Temps de circulation ; temps de la dilution pour que l'on puisse faire monter la circulation partout. Le syst endocrine envoit son information partour et va être lu par ceux qu'ils peuvent la lire parce qu'ils expriment les récépteurs.
* Ceux qui vont recevoir l'information parce qu'elle est envoyé qu'à son voisinage

--> ce n'est pas la nature du signal qui change c'est l'espace dans lequel on le libère.
SPécificité du message endocrine : qui est capable de lire
SPécificité du message nerveux : qui reçoit le message

* système cardiaque : prise directe du courant des cellules les une aux autres.

Différence entre un motoneurone et une sécrétion d'insuline : ces deux systèmes sont semblables : beaucoup d'intéraction. Ocytocyne : fabriqué dans les neurones de l'hypothalamus (cerveau), migrent dans les axones des neurones hypothalamiques, au niveau de l'hypohyse elle est libérée par la circulation générale (hormone) --> hormone fabriquée par un neurone. Ocytocyne existe également comme un neurotransmetteur. Ces deux systèmes sont imbriqués l'un dans l'autre.

Il ne faut pas chercher à catégoriser : on ne peut pas faire de frontières ; c'est un tout intégré.
Système où l'idée initiale est lutte pour l'énergie --> investissement : on met de l'énergie pour en récupérer.

Il faut protéfer l'organisme

### _g) Les fonctions tégumentaires_

* Antéimmunité
* protection mécanique
* protection chimique (échanges en tout genre de molécule)
* protection thermique (chez les espèces qui sont * homéotermes, qui ont une température différente du milieu extérieur parce que l'animal la régule). Il y a un termostat central de l'organisme.

Poumon chez l'humain : 140 m2
Compromis à réaliser : avoir des fonction de protection

* L'animal a besoin d'énergie : l'énergie est dans le milieu
    * Autotrophie
    * Hétérotrophie
* Il faut une réponse active de l'organisme pour raccourcir la distance entre la matière organique et lui même


### _h) La fonction de locomotion_

* Rapprocher la bouche de la matière organique
* La locomotion présentée comme une fonction de nutrition améliorée
* Aspect divergent : locomotion pour pouvoir acquérir de la matière organique et ne pas être la M.O de qqn d'autre
* C'est aussi ce qui va permettre de rapprocher les congénères : de faire rapprocher un état 

Pour l'individu est optionel : ce n'est pas son problème ; c'est la seule qui n'a de sens par rapport à l'espèce ; on ne parle pas de survie de l'espèce ; on parle de la transmission de la vie d'une génération à l'autre.

On va regarder le monde animal, son évolution, la complexification du monde animal, on va regarder du côté énergie, adaptation, évolution mais l'évolution passe par l'adaptation.
En quoi l'animal est-il adapté? Comment mange t-il? quel métabolisme? Quel fonction tégumentaire? Quel reproduction? --> 9 questions ; il faut y mettre de la logique : ce n'est pas une recette de cuisine --> façon de faire qui a du sens.
permettre avec quelques exemples de pouvoir décrire les 1,2 mio restant

Voir cours papier

## III Les Protostomiens

Coelome : Classification phylogénétique qui ne sont pas faux en terme de description mais en terme d'évolution : faux! 

Les Coelomates ; les pseudocoelomates et les acoelomates.

Spiraliens/ecdysozoaires

### A) Les Spiraliens

* Repose sur un développement de l'oeuf qui va se faire en spiral. On va avoir des subdivisions et les plans de divisions à partir de 8 cellules (3ème division) vont se déplacer en oblique par rapport à un axe qui va aller du pôle végétatif vers le pôle animal. --> caractère dérivé propre des spiraliens.
* On va obtenir des cellules différentes : 
    * 1a --> 4 micromères animmaux
    * 1dA--> 1D : macromères duu pole végétatif
    * Développement spiraliens: concerne tous les organismes présents dans le taxon 

____ COURS PAPIER !!!

### B) Des Gnatifères aux Syndermates

* Gnati : mâchoire ; le pharynx est armé de mâchoire avec des pièces masticatrices. On va regarder les syndermates --> cuticule de kératine produite par les cellules de l'épiderme mais cette cuticule est intracellulaire (caractère dérivé propre des syndermates). Dispartition de la membrane entre deux cellules adjacetntes et formation d'un syncitium épidermique. CCL : cuticule de kératine intracellulaire mais continue grâce au caractère syncitial de l'épiderme.


#### i. Les Rotifères

* Animaux de petite taille qui vivent en eau douce (quelques centaines de micron max), la région antérieure de ces animaux porte une couronne ciliée qui donne son nom au groupe. Cette  courone : les cils battent mais battent en faisant une onde --> organe rotateur (d'où le nom de rotifères).

* Muscle de deux types : des muscles dans un plan : et des muscles perpendiculaires à des muscles circulaires dans une multitude de plans (muscles rétracteurs = muscles longitudinaux). En se contractant les muscles ramènent l'appareil rotateur vers le bas. 
    * Ces animaux ont la capacité de faire du mouvement (par les cils/les muscles)

* Anatomie interne du rotifère
    * Tube digestif placé sur un axe antéro postérieur avec à l'avant la bouche et à l'arrière l'anus.
    * Couronne ciliée autour de la bouche
    * Pharynx après la bouche 
    * Oesophage : relie la cavité bucal à l'estomac
    * Milieu extérieur controlé : on secrète des enzymes grâce aux glandes salivaires et gastriques.
    * Chez les bilatériens : processus de céphalisation (concentration de centre nerveux au niveau de la tête) parce qu'on est au voisinage de la bouche. --> Cerveau : ne définit rien de particulier. Le cerveau n'est qu'un centre de concentration de neurone. Quand il est unique et à l'avant on le nomme cerveau. 
    * Pseudocoelome : cavité creusée entre l'endoderme et le mésoderme --> il s'agit de liquide extracellulaire (liquide interstitielle).
        * Du point de vu physique : incompressibles mais déformables
        * Quest ce qu'un liquide d'un point de vue biologique?
            * Permet de diffuser par rapport à une matrice qui est plus figée
            * Le liquide est brassable ; on peut le prasser beaucoup plus facilement qu'une structure qui contient des matrices de fibres.
            * Déformables mais incompressibles
            * Dans un liquide des substances diffusent mieux que dans un liquide qui contiendraient une matrice.
        * Ce pseudocoelum est une cavité de distribution liquide dans lequel les métabolites vont diffuser facilement.
        * En fonction des compartiments que l'on considère on ne s'intéressera pas aux même propriétés (mais c'est à tort).
    * Les protonéphridies : ce sont les organes excréteurs de ces protifères. C'est la première fois que l'on voit un organe excréteur qui porte un nom particulier. 
    * Cloaque : désigne un carrefour entre les voies urinaires ; génitales ; et digestives. Orifice génital ; orifice digestif ; orifice urinaire = anus

#### Acanthocéphales

* Caractère dérivé du groupe : le fait d'avoir un probosicis en fait un syndermate --> homoplasie car c'est également une convergence évolutive 

* Anatomie :
    * Pas d'appareil digestif
    * Qu'un appareil reproducteur 
    * CCL : ce sont des parasites 
    * Probscyste : permet de s'accrocher à l'organe intestinal de l'hôte.
    * (Gonades sur le schéma)

* Ces Acanthocéphales sont considérés comme des Rotifères devenurs parasites. 

Com : Perte de fonctin = Perte de gènes??? similitude au niveau du génome (pseudogènes ou transposons semblables?)

### C) Des Rouphozoaires aux Plathelmynthes

* L'ingestion des proies se fait par aspiration
* Le tube digestif n'est qu'un simple sac (contradiction avec les bilitariens! axe antéro postérieur avec un anus à l'arriére et une bouche à l'avant)
    * Il s'agit d'une perte lié à ce mode d'alimentation particulier
    * Anatomie reste une anatomie antéro postérieur
    * Les proies vont être aspirée et on va avoir la digestion
* Ce tube digestif présente des subdivisions

#### Les Turbellariés

* Animaux libres, le plus souvent marins
* Ce corps est recouvert par un épithelium cilié 
* On va trouver 3 autres types cellulaires 
    * Des cellules à Rhabdites : cellules qui rejètent vers l'extérieur de baguettes de protéines sulfurées et de phosphate de calcium. Certains en font un organe de défense ; pour d'autres déchets.
    * Des cellules tactiles : cellules qui apportent des informations sur l'environnement
    * Les cellules glandulaires : cellules qui vont secreter du mucus (un liquide qui va contenir des protéines (glycoprotéine)) Ces mucus vont avoir deux conséquences différentes : lubrifiant et mucus collant (adhésif).

##### 1. Le tube digestif 

* Pas d'anus ; que la bouche 
* Quand le tube digestif va augementer de volume : pas de surface pour l'assimiler --> on va trouver des plis ; des replis ---> augemente la surface d'échange
* Derrière ce tube digestif : pas de cavité --> mais ne veut pas dire qu'il ne va pas y avoir de diffusion.
* Ces animaux sont des carnivores
* L'animal va s'entourer autour de la proie 

##### 2. Locomotion et système nerveux

* Si présence d'une musculature : il y a forcément un contrôle nerveux 
* Système qui va se développer tout au long de l'animal. Ces cordons vont former des ganglions (ganglions cérébroides ou cerveau) et des organes sensoriels : oeil 
Traiter les informations : info --> il y a de la lumière et il n'y en a pas
et l'information qui nous entoure
Parfois les deux systèmes peuvent être séparés. Ici on parle d'oeil parce qu'on a la formation d'une image (information extérieure). On a les deux aspects : photosensibilité et information visuelle.

* Complexité des muscles, qui permet à l'animal de moduler sa forme dans tous les sens. Il n'y a pas de coelome --> mais on a une matrice. Les métabolites ne diffusent pas aussi bien que dans un liquide mais une gelée est déformable et incompressible. Les muscles vont agir de la même façon que si on avait du liquide. Ceci va donc permettre à l'animal d'avoir des déplacements dirigés par les muscles

##### 3. Les organes excréteurs

--> Protonéphridie (comme chez les rotifères) tout le long de l'animal. Ces protonéphridies correspondent à des tubes qui se ramifient. Au bout du tube on a une cellule que l'on appelle une celle flamme.
Cellule flamme : cellule qui repose sur le tube par des prolongements cytoplamiques qui sont espacés les uns des autres. Au centre de la lumière du tube : la cellule a des flagelles. Nous sommes chez les opistochontes donc flagelle propulseur. Du liquide va être prelevé de l'extérieur, et placé dans ce système excréteur. Pour se débarrasser des déchets, on a deux grandes stratégies : soit on cherche les déchets un par un, et on les récupère, soit on ne cherche rien, on met tout dehors. Ccl : deux étapes dans l'excrétion:
* Une étape de filtration : les prolongements cytoplasmiques sont espacés par des fentes de filtration --> les liquides et les solutés vont passer. Ce qu'on va mettre dans le tube: composition interne de la cellule --> une urine primaire. Cette urine primaire a les même caractéristiques que tous les liquides dans lesquels baignent les tissus. Derrière cette première étape de filtration on a une deuxième étape.
* Le traitement de l'urine va permettre de passer d'une urine primaire à une urine élaborée. (Urine qui a été traitée) --> On récupère ce qui n'a pas à être jeter (A.aminé, A.Gras, Glucose). Dépend du milieu de vie de l'animal. Si l'animal est plus concentré que le milieu ou moins concentré que le milieu en sel. Un animal d'eau douce est plus concentré en ion que son  milieu. Un animal qui vit en milieu marin.

En Résumé : 
* On filtre --> urine primaire
* On traite cette urine primaire
* Régulation ionique et hydrique qui dépendent du milieu de vie

Protonéphridie = organe excréteur
Ici on met tout dehors, tout déchet est mis dehors et on récupère ce que l'on veut.

##### 4. La Reproduction

###### a) Reproduction sexuée
Les turbellariés ont une grande capacité de regénération : 1/300 de l'organe suffit pour en former 1 entier.
Reproduction clonale : l'organisme se divise par sisciparité ou par bourgeonnement 

###### b) Reproduction asexuée

Hermaphrodite : il y a des organes mâles et des organes femelles.  Les turbellariés sont hermahrodites. Ce n'est pas parce que l'animal est hermaphrodite qu'il va y avoir une autofécondation.
Le système mâle est mature en premier. --> fécondation croisée (les deux systèmes ne sont pas matures en même temps!)

#### Les cestodes et les trématodes 

Ce sont des parasites qui présentent des morphologies orientées sur cette vie parasitaire
Fasciola Hepatica --> grosse ventouse buccale qui permet à l'animal de se fixer. Chez les cestodes on va beaucoup plus loin : le corps est rubané, constitué de segments qui vont se former à l'arrière de la partie antérieure (à l'avant de l'animal). Cette partie est appelée le scoles --> l'animal va se fixer grâce à ce scolex. Il s'agit ici d'un tube digestif à l'envers. Les microvillosités se trouvent à l'extérieur. Distribution centrifuge et non centripète.
Autofécondation : segments que l'on appelle des proglottis vont se détacher à l'arrière (5 à 10 segments par jour).

### D. Les Lophotrochozoaires

Trochozoaire : vient de larve trocophore qui est une larve caractéristique de ce groupe et qui possède une forme de toupie sur laquelle on va retrouver le tube digestif. On va trouver des couronnes ciliées (ciliature apicale) en dessous --> une autre couronne de ciliature Prototroche  puis Métatroche et une télotroche.
On a également un massif mésodermique et une future chaîne nerveuse ventrale.

Les mollusques sont ce qu'on devrait appeler des Trochozoaires ; toutefois cette larve trocophore possède des modifications particulières qui vont faire qui vont en faire une larve velligère.

### E. Les Némertes

Ce sont des vers longs, pas de segmentation --> on les a mis en relation avec les plathelminthes. Ces animaux sont considérés comme des acoelomates. Leur caractère particulier --> **Proboscis** (marque de fabrique des Némerte (également des Arcantocéphales)) --> convergence évolutive. Proboscis entouré de muscle

**Rhyncocoele**: muscles autour du proboscis --> rétracteurs et des muscles circulaires. On a une cavité de liquide qiu est entre les muscles. C'est donc une cavité creusée dans du mésoderme. C'est la définition des coelomes.
Etre coelomate c'est avoir des cavités creusées dans le mésoderme. Ici on ne peut pas les considérer comme des coelomates parce que ce n'est pas un caractère générale. Cette cavité n'a qu'un intérêt --> avoir du liquide sur lequel les muscles peuvent agir. Permet de transmettre une force, apposée par les muscles sur un compartiment dont on va pouvoir modifier la forme. C'est ce que l'on appelle un **hydrosquelette** ou un squelette hydrostatique. 

Les Némertes possédent un **tube digestif orienté**. Linéus Longissimus (jusqu'à 30m). On trouve dans ce groupe ce que l'on considére être les plus longs animaux du monde vivant.
Associé à l'alimentation il y a une distribution. On va voir pour la première fois un sstème circulatoire qui n'est rien d'autre qu'une distribution orientée. 

Cellules myoépithéliales --> vaisseaux simples (muscle longitudinaux et muscles circulaires) --> les muscles longitudinaux ici sont à l'extérieur. On a une cavité creusée dans un environnement mésodermique remplie de liquide coelomique. 
Ce système circulatoire est un système clos (il est bouclé sur lui même). Type du système circulatoire (clos, ouvert, pseudo-ouvert ne donne aucune information sur l'évolution.)
Le liquide avance dans ce système grâce à une onde de contraction. Péristaltisme : pas un endroit particulier responsable de fiare avancer ce liquide --> c'est une propriété du système. 

La locomotion se fait par leur musculatuer et par leur épiderme cilié pui par la mise en place d'un **péristaltisme**

## IV. Les annelides

(une annelide)

### A. La métamérie

Figure 40 : on voit comment on passe de la larve au vers. Prostomium et Peristomium --> ne sont pas des métamères.
Un métamère est un segment mais tous les segments ne sont pas des métamères
Les deux mécanismes n'ont rien à voir l'un avec l'autre. 

Dans une organisation métamérisé on va avoir une succession de segments identiques les uns aux autres.

#### 1. Organisation des métamères autour des sacs coelomiques

Cette segementation affecte l'ectoderme et le mésoderme. Dans chaque segment il va y avoir une paire de vésicules coelomiques (une paire de sacs coelomiques 1 à droite et 1 à gauche). Ces sacs coelomiques. Deux caissons (deux demies cylindres) sont remplis de liquide coelomique. Le métamère s'organise autour d'une paire de vésicule coelomiques. Ces caissons de coelome possèdent des contacts entre eux. Ces caissons s'affrontent. Contact du même segement au dessus et en dessous du tube digestif : correspond au **mésentère** (deux caissons coelomiques du même segment!). Caissons coelomiques qui heurtent un autre segment : c'est ce que l'on appelle le **dissépiment** (acolement de deux parois de caissons coelomiques de deux segments qui se suivent).
Ces caissons coelomiques peuvent être au contact d'autre chose que des contacts coelomiques (soit vers l'intérieur ; soit vert l'extérieur) La splachnopleure ou le feuille viscérale.
La somatompleure ou feuillet pariétal ; splanchnopleure ou feuillet vicéral.

De nombreux systèmes vont être affectés par la métamérie ; d'autres qui ne sont pas affectés par la métamérie.

Le tube digestif n'est pas métamérisé. Le système circulatoire non plus.

Segmentation au niveau des organes excréteurs

Pas de protonéphridie chez les annelides --> ce sont les organes excréteurs de la larve et ne se retrouvent pas chez l'adulte. 
Ces métanéphridies apparaissent avec ces caissons coelomiques.Elles n'ont pas la même origine. Elles ont une origine mésodermique. Ce système s'ouvre dans une cavité --> néphrostome où on a un pavillon avec des cils autour qui vont aspirer du liquide --> on obtient une urine primaire puis on excréte l'urine élaborée par le néphridiopore..

Organes intersegmentaires

Le système nerveux est à la fois affecté ; à la fois non. Développement des ganglions nerveux plus important dans chaque segment mais système nerveux continu.



