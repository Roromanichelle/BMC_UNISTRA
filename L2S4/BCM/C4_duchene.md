# Chaînes d'oxydo réduction

## 1. Entrée de NADH2 

NADH : arrive au niveau du complexe 1
FADH2 : arrive au niveau du complexe 2
Transporteurs d'électrons : toujours le même type de protéines (voir dia)
Flavoprotéine : coenzyme --> soit du FAD ou du FMN (accepte 1 à 2 e-) ; très fortement lié à la protéine ; association très forte entre la coenzyme et l'enzyme ; et on ne va jamais  la trouver libre. 
Transporteurs : Ubiquinone --> existe sous plusieurs stades oxydée, sous forme semie oxydée, et sous forme réduite.
Les structures ne sont pas à connaître ; ubiquinone = réduite
Cytochromes : Protéine + hème (qui contient un ion au centre de la structure qui va être en coordination avec cycles type pyrroles).

* Protéine Fer Soufre de Rieske :  maintient du Fer au sein de la protéine --> ce fer va intervenir dans le mouvement d'éléctrons ; on peut aussi avoir 2 ou 4 ions fers. 

* Protéines à centre Cu

Ces protéines peuvent s'associer pour les différents complexes

1 C et D
2 B

NADH de la glycolyse : cytosolique --> donne ses électrons à une flavio protéine ; puis elle donne ses éléctrons à l'ubiquinone (ils ne sont pas passés par le complexe 1)