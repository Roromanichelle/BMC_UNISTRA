# Bilanz des vorherrigen Jahres

Letztes Jahr war ich in der "Classe Préparatoire aux Grandes Écoles". Englisch war eine Verpflichtung aber wir könnten eine zweite Sprache als Option wählen. Ich habe also am deutschen Unterricht teilgenommen und hatte 2 Stunde pro Woche. 
>
Außerhalb dieses Unterrichts habe ich nicht viel Deutsch geübt weil wir viele Arbeit in die anderen Fächern hatten. 
Der deutsche Unterricht war organisiert um uns an dem Wettbewerb Vorzubereiten. Wir haben drei Kompetenzen dafür erarbeitet : verstehen ; zusammenfassen und übersetzen (Version). Sprechen und Hörverstehen haben wir nicht viel geübt.  Außerdem jede zwei Woche hatten wir ein Blatt mit Gramatikregeln, die wir lernen mussten und dann hatten wir einen Test darüber. Aber davon habe ich nicht viel behalten weil wir es nie wieder benutzt haben. 
>
Zurzeit bin ich mit meinem Deutsch nicht zufrieden, ich würde gerne meine Vokabular verbessern und meine Kompetenzen beim Sprechen und Hörverstehen verbessern. Ich wunsche auch manche Grammatikregeln wieder sehen.
