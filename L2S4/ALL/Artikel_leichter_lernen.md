Leichter lernen - bild der wissenschaft 2-2016

Dieses Artikel zusammenfasst die besten Strategien um effizient zu lernen. Die Autorin ist Physikerin und lehrerin, sie hat die Einstellung für den Lernenerfolg studiert und nutzt verschiedenen Forschungsarbeiten um sich rechtfertigen. Insgesamt gibt sie Sieben Schritten zum sicheren Lernererfolg. 

Erstens beratet sie selbst fragen zu stellen oder von Freuden abfragen lassen. Es erlaubt die neuralen Netzwerken zu stärken und den Stoff zu konsolidieren. Die Kenntnisse überarbeiten und wiederholen ist auch wichtig für den Lernprozess und es wird effizienter, wenn die Lernzeiten über mehreren Tagen verteilt sind : alle drei Tagen am Anfang und dann alle 6 -12- 24 Tagen. 

Danach stellt sie aus mehrere Experiment die die Bedeutung des Schlafs nachweisen. Zum Beispiel die Gruppe um Kathy Rastle von der Holloway University of London hat gezeigt, dass direkt nach dem Lernen schlafen besonders effizient war für den Lernprozess. Jedem Tag genug schlafen ist gleichfalls eine entscheidende Voraussetzung für den Lernerfog.

Außerdem manche Personnen werden am Morgen besser lernen während anderen effizienter am Abend sind. Wenn man davon bewusst sind, könnte Jede Person versuchen, seine eigene "Chronotyp" zu finden um in den besten Bedingungen lernen zu können.

Es ist ebenfalls empfehlen langweiligen Lernstoff mit Interessantem zu kombinieren. Eine Forschungsgruppe der Universät Califonia in Davis hat nähmlich gezeigt, dass wir besser erhalten, wenn wir mit etwas interaktiv oder visuel arbeiten können (wie Bilder oder Mindmap). 

Kritisch mit dem Lernstoff zu sein erlaubt auch ein besseres Einspeichern. Wenn wir die Lernstoff überprüfen und kritisieren 

Eine Mittel um seine Lernmöglichkeit zu optimisieren ist an unsere eigene Potenzial zu glauben.

**Chronotyp** : Am Morgen : Sorgfähig
Hängt von der Charakter --> Sorgfähig = Morgenorientiert ; auch mit der Diät verbunden. Mehr