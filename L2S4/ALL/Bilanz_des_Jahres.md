# Bilanz des Jahres

Im Vergleich mit was ich letzte Jahr im Deutsch gemacht habe, bin ich für dieses Semster ziemlich zu frieden mit meinem Arbeit.  

Die Sprachkurse haben mir sehr gefallen weil es viel interaktiver war als die Kurse an denen ich letzte Jahr teilgenommen habe. Es war auch sehr frei : wir hatten die Möglichkeit, unseres Arbeitsthema zu wählen. Unseres Arbeitsthema war sehr interessant, ich habe viel gelernt und auch für mein Alltag (wie Organisation nach dem Chronotyp, oder Essgewohnheiten).

Anderereseits könnten wir zusätzliches Arbeit geben wenn wir wirklich unsere Deutsch verbessern wollen. Leider habe ich dafür keine Zeit gefunden...

Zuhause habe ich das Hörverstehen viel geübt (und ziemlich viel vor dem CLES weil ich das Hörverstehen für die Englisch Prüfung nicht geschafft habe). Ich habe bermerkt, dass ich schon viel besser verstehen kann nur durch eine Konzentrationsarbeit.

Dieses Semester war kein einfaches Semester. Ich habe an zusätzlichen Modulen teilgenommen, die viele Zeit erfordet : eine Lehrveranstaltung in Informatik an der Universität von Strasbourg und dann ein Profilmodul in Bioinformatik mit EUCOR, an der Universität von Freiburg. Programmieren macht mir Spaß, es braucht Zeit aber es war keine Zwang. Ich möchte nach meinem Bachelor ein Master in Bioinformatik in Deutschland machen. Die Schüler sind in Deutschland früher spezialisiert, und es gibt kein Master in Bioinformatik für Anfänger in Programmierung wie in Frankreich...

Dazu bin ich mit meiner EUCOR Erfährung sehr zufrieden. Die Unterrichten sind hoch interessant und auch sehr interaktiv. Es ist viel besser als die Lehrveranstaltungen in Informatik an denen ich in Frankreich teilgenommen habe. In Freiburg sind wir im Computerraum, der Lehrer erklärt was wir machen soll und dann können wir uns zusammen helfen um die Lösung zu finden. 

Zum Schluss fühle ich mich sehr glücklich, in diesem binationale Studiengang zu sein. Ich freue mich schon über mein Jahr in Saarbrücken und bereue wirklich nicht, dass ich der "Classe Preparatoire verlassen" habe.
 
