# Emploi du temps

|     | Lundi               | Mardi         | Mercredi      | Jeudi                   | Vendredi          |
| --- | ------------------- | ------------- | ------------- | ----------------------- | ----------------- |
| 8h  |                     |               | TP COE        | CC                      | TD IPR (4)        |
| 9h  | TP PAM salle tp zoo |               | TP COE        | CC                      | TD IPR (4)        |
| 10h | TP PAM              |               | TP COE        | CC                      | CM IGE            |
| 11h | TP PAM              |               | TP COE (1)    | CC                      | CM IGE            |
|     |                     |               |               |                         |                   |
| 13h | TD IGE (5)          | CM MIV ILB A5 | CM PAM ILB A3 |                         | CM PAM (ourisson) |
| 14h | TD IGE              | CM MIV ILB A5 | CM PAM ILB A3 | ALL CRL ILB             | CM PAM (ourisson) |
| 15h | TD GEF (6)          | CM BCM ILB A5 | CM PHC ILB A3 | ALL CRL ILB             | CM GEF (ourisson) |
| 16h | TD GEF              | CM BCM ILB A5 | CM PHC ILB A3 | Cours Raphaël           | CM GEF (ourisson) |
| 17h |                     |               | CM IPR (2)    | Cours Raphaël ou TD PHC |                   |
| 18h |                     |               | CM IPR (2)    | TD PHC (3)              |                   |

(1) fac chimie bat bas 1er étage sud
(2) amphi bataillon
(3) 3 TD, début en mars
(4) Salle GSC bota; TP/TD à partir du 02/02
(5) Algeco 1 bota; TD à partir du 29/01
(6) R12 inst de zoo; plannings des TD communiqués par les enseignants