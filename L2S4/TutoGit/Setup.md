# Setup Git env

### Global setup
Configurer le logiciel
````
git config --global user.name "VincentCream"
git config --global user.email "vincent.frangi@gmail.com"
````
### Create a new repository
Cloner le repository en ligne
````
git clone https://framagit.org/VincentCream/Tutorium-Emb1-IngInf.git
cd Tutorium-Emb1-IngInf
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
````

### Existing folder
Pusher un projet existant
````
cd existing_folder
git init
git remote add origin https://framagit.org/VincentCream/Tutorium-Emb1-IngInf.git
git add .
git commit -m "Initial commit"
git push -u origin master
````
### Existing Git repository
Ajouter un repository
````
cd existing_repo
git remote add origin https://framagit.org/VincentCream/Tutorium-Emb1-IngInf.git
git push -u origin --all
git push -u origin --tags
````