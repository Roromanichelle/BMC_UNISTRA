# GIT c coua ?

Wikipédia :
````
git est un logiciel de gestion de versions décentralisé.
C'est un logiciel libre créé par Linus Torvalds [...]
En 2016, il s’agit du logiciel de gestion de versions le plus populaire
qui est utilisé par plus de douze millions de personnes.
````
* **[Cours sur Openclassroom](https://openclassrooms.com/courses/gerez-vos-codes-source-avec-git)**
* [Télécharger git](https://git-for-windows.github.io/)
* [Framgit](https://framagit.org) l'hébergeur que j'utilise
* [Le Projet](https://framagit.org/VincentCream/Tutorium-Emb1-IngInf)
* [Tuto](https://try.github.io/levels/1/challenges/1)

## Command Line instruction
### Workflow
    
[Init](Setup.md)

1. Setup
2. Create / clone repo

[Travail ](Work.md)

3. Mod du code source
4. Push modifications

## Documentation en markdown
* [Cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [wikipédia](https://fr.wikipedia.org/wiki/Markdown)