# Coder avec git

0. Récupérer les modification sur le repository 
1. Modifier son code
2. Vérifier les modification
3. Ajouter les modifications
4. Commiter les modification
5. Pusher les modifications

## 0. Récupérer
Avant de commencer à travailler il est important de récupérer les dernier mises à jour
du code source (s'il y en a bien sûr)
````
git pull
````

## 2. Vérifier
Une fois que l'on a modifier le code on peut facilement controller les changement avec:
````
git status
````

## 3. Ajouter les modifications
Wikipédia : "ajoute de nouveaux objets blobs dans la base des objets pour chaque fichier modifié depuis le dernier commit. Les objets précédents restent inchangés"
````
git add .
````
**Remarque** : on peut spécifier les fichier/dossier que l'on désire ajouter
Le "point" signifie tout le dossier actuel (voir bash)


## 4. Commiter les modification
Wikipédia : "intègre la somme de contrôle SHA-1 d'un objet tree et les sommes de contrôle des objets commits parents pour créer un nouvel objet commit"

````
git commit -m "Message de commit"
````

## 5. Envoyer les modification
Envoyer les modifications sur le server
````
git push
````
**Remarque**: La première fois il est nécessaire de spécifier le repository et la branche voulue
````
git push -u origin dev1
````