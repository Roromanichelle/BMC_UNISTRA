- [Qu'est ce que le génome?](#quest-ce-que-le-g%C3%A9nome)
    - [Généralités](#g%C3%A9n%C3%A9ralit%C3%A9s)
        - [_a) Définitions_](#a-d%C3%A9finitions)
        - [_b) Différents génomes_](#b-diff%C3%A9rents-g%C3%A9nomes)
            - [i) Génome des eucaryotes :](#i-g%C3%A9nome-des-eucaryotes)
            - [ii) Génome des Eubactéries et Archébactéries](#ii-g%C3%A9nome-des-eubact%C3%A9ries-et-arch%C3%A9bact%C3%A9ries)
        - [_c) Autres définitions_](#c-autres-d%C3%A9finitions)
            - [i) Définition de Bernard Dujon](#i-d%C3%A9finition-de-bernard-dujon)
            - [ii) Définition basée sur la structure](#ii-d%C3%A9finition-bas%C3%A9e-sur-la-structure)
    - [Architecture](#architecture)
        - [_a) Taille du génome_](#a-taille-du-g%C3%A9nome)
        - [_b) Eléments qui ne sont pas des gènes_](#b-el%C3%A9ments-qui-ne-sont-pas-des-g%C3%A8nes)
    - [Un support d'information](#un-support-dinformation)
    - [Une structure dynamique](#une-structure-dynamique)
- [Quest ce que la génomique?](#quest-ce-que-la-g%C3%A9nomique)
    - [Un génome au niveau nucléotidique](#un-g%C3%A9nome-au-niveau-nucl%C3%A9otidique)
        - [_a) Analyse d'un génome à l'échelle nucléotidique_](#a-analyse-dun-g%C3%A9nome-%C3%A0-l%C3%A9chelle-nucl%C3%A9otidique)
        - [_b) Séquençage de génomes complets_](#b-s%C3%A9quen%C3%A7age-de-g%C3%A9nomes-complets)
            - [1) Généralités](#1-g%C3%A9n%C3%A9ralit%C3%A9s)
            - [2) Grands principes de séquençage](#2-grands-principes-de-s%C3%A9quen%C3%A7age)
            - [3) Stratégies de séquençage](#3-strat%C3%A9gies-de-s%C3%A9quen%C3%A7age)
                - [i. Séquençage aléatoire global](#i-s%C3%A9quen%C3%A7age-al%C3%A9atoire-global)
                - [ii. Difficultés](#ii-difficult%C3%A9s)

# Qu'est ce que le génome?
## Généralités
### _a) Définitions_

**Le génome**: c'est l'ensemble de l'information génétique portée par un individu (ce n'est pas l'ensemble des gènes) ; c'est ce qui permet de construire, de maintenir et de transmettre une cellule dans le temps. 

Terme utilisé la première fois en 1920, il l'utilise pour qualifier l'ensemble des chromosomes et l'ensemble de gènes portés par ces chromosomes 
Gène: caractère transmis dans la descendance (gène d'un côté et chromosome de l'autre --> génome)

### _b) Différents génomes_
#### i) Génome des eucaryotes : 
 * chromosomes
 * ADN des organites
 * Plasmides parfois (ex: levure)

#### ii) Génome des Eubactéries et Archébactéries 
 * Génome circulaire et plasmides

### _c) Autres définitions_

#### i) Définition de Bernard Dujon
_Le génome est la mémoire du passé, le déterminant du présent et le moteur de l'évolution à venir_ - Bernard Dujon
 >* Mémoire du passé --> un génome a une histoire, cette histoire est inscrite dans l'évolution, et comparer ces génomes nous permettra d'entrer dans cette histoire (construction d'un arbre phylogénétique) --> notion d'évolution, un génome n'est pas une structure figée, il peut subir des variations (ex mutations)
 >* Plasticité, mutations --> nouveaux caractères implique spéciation : création et mise en place de nouvelles espèces

#### ii) Définition basée sur la structure
 * Une architecture (qui n'est pas figée, architecture dyamique qui peut se modifier à l'échelle d'une cellule, d'un organisme) 
 * Un support d'information

Procaryote
 * ADN dans le cytoplasme
 * Un seul chromosome "circulaire"
 * Plusieurs milions de nucléotides
 * Présence de plasmides

Eucaryotes:
 * ADN dans le noyau
 * PLusieurs chromosomes avec ADN fortement compacté et linéaire
 * Taille variable

Génome des mitochondries et des chloroplastes
 * ADN très souvent circulaire
 * PLusieurs copides
 * Code génétique propre
 * Transmission souvent uniparentale, maternelle chez les mammifères

Rappels ADN:
Extrémité 5' : phosphate
Extrémité 3' : OH du sucre

Aspect qui entrent dans l'architecture et de la structure:

## Architecture

### _a) Taille du génome_
 * Appelée valeur C
 *  Défini avant tout en terme haploïde,en nombre de nucléotides
 * 3 mia de nucléotides chez l'humain (23 000 gènes)
 * Grande diversité de tailles chez les espèces
 * Paradoxe de la valeur C:
   * La taille des génomes n'est pas en relation directe avec la complexité des organismes et le nombre de gènes
   * Les génomes d'espèces proches (phylogénétiquement proches) peuvent différer considérablement en taille

### _b) Eléments qui ne sont pas des gènes_
 * Dans cet architecture, il y a un nombre d'éléments qui font partis intégrante de l'architecture, régions ADN essentielles pour la transmission
    * Les centromères lors de la division cellulaire sont indispensables pour la répartition du génome
    * Origine de réplication (régions chromosomique essentielles pour que les chromosomes puissent être répliqués)
    * Les télomères protégent les gènes --> ils sont endommagés à chaque division, réplication: synthèse discontinue, démarre à partir d'amorce, pose des problèmes au niveau des extremités, les télomères vont être mis en place pour que l'extremité chromosomique soit protégée au cours des divisions et pour que l'information génétique ne soient pas endomagés lors de la réplication. 


## Un support d'information

Gène: segment d'ADN qui code une fonction
Longtemps on a considéré: géne = une protéine
 * 23 000 gènes : nombre de gènes qui codent une protéine (plus facile à identifié) attention d'autres gènes codent d'autres informaitons
 * Valeur G = nombre de gènes qui codent une protéine (en accord avec les anciennes définitions)
 * Lorsqu'on considère ce support de l'information, on considère uniquement les gènes qui codent une protéine? on oublie les informations qui codent pour les ARN
 * Les gènes qui codent les ARN (ARN ribosomique, ARN de transfert, micro ARN font partis de ce support de l'information
 * Introns: important car épissage alternatif --> pour un gène on a plusieurs protéines ; un gène/une protéine n'est plus valable pour les gènes qui sont concernés par l'épissage alternatif
 * Chez le maïs plus de 90% du génome est non codant, 80% de la séquence est constituée de transposons.
 * 40% de la séquence du génome humain est constitué d'éléments génétiques mobiles de type transposons
 * Les transposons contribuent à la diversité de taille et au paradoxe de la valeur C (pas de corrélation entre la taille du génome et le nombre de gène)

## Une structure dynamique
 * Structure qui est soumise à une variabilité (ex: mutations) 
 * Mutations ponctuelles qui peuvent être géniques (rentrent dans la séquence d'un géne et qui définissent un nouvel allèle) --> confère un polymorphisme entre individus (varieté dans les régions non codantes)
    * substitution: on remplace un nucléotide par un autre
    * micro-délétion : délétion d'un ou deux nucléotides
    * micro-insertion : une ou deux bases ajoutés
--> erreurs liées à la réplication et non corrigée par les systèmes de réparation qui existent; exemple: éléments chimiques qui modifient une base ; recombinaisons homologues : crossing over: brassage intrachromosomque
recombinaison non homologue : remaniement chromosomique

 * Mutation chromosomique (modification de structure (modification de la taille) --> preuve de la dynamique. Remodelage de chromosome entre espèces proches
    * Inversion
    * Translocation
    * Duplications
    * Délétions
    * Insertions

 * Mutation génomique
    * Chromosome surnuméraire, aneuploïdie (modification du nombre de chromosomes)
    * Polyploïdie

Transposition, déplacement d'éléments génétiques mobiles (transposons) (modifie taille)

Modifications provoquées:
 * in vitro
 * Remplacement in vivo
 * trangenése
 * thérapie génique

# Quest ce que la génomique?

Discipline dans laquelle on étudie:
* La structure (taille et nombre de chromosome, taille de chromosomes)
* Le contenu (organisation de ces différents chromosomes, centromères, télomères et autres séquences particulières)
* L'évolution des génomes
* Le fonctionnement des génomes


La Génomique a pu voir le jour grâce à la génétique reverse 
* Génétique réverse
    * On sait séquencer l'ADN
    * On sait manipuler l'ADN (cloner, PCR, caractériser de l'ADN)
    * L'application de cette génétique réverse non plus à l'échelle d'un morceau de génome mais à l'échelle de la globalité du génome

On assimile généralement génomique et séquence : la structure d'ADN a été mise en évidence en 1953 par Watson et Crick.
    --> 1977 : premier ADN séquencé
    --> Inovation dans les techniques de séquençage

1951 : une protéine va être séquencée
    --> à cette époque on peut séquencer une protéine
    --> L'insuline va être la première protéine à être séquencée (Sanger)
    --> Sanger met en place une des première technique de séquençage de l'ADN (deux prix nobel)
1977 :  Deux techniques de séquençage : 
* Mawam and Gilbert en 1977
    --> 100 paires de bases seulement vont être  séquencés : tout petit segment (prouesse technologique basée sur une approche chimique. ADN purifié, marqué radioactivement puis traitement chimique) --> technique qui n'a plus été utilisée.
*  1977 : méthode de Sanger : repose sur la réplication de l'ADN ; technique biologique ; enzymatique : la réplication qui allait être utilisée étit une réplication qui statistiquement pouvait être arrêté à chaque nucléotide. Chaque segment a une taille différente.
    L'ensemble a permis de séquencer une centaine de paires de base, mais plus facilement utilisable.

* Une année après on va avoir une base de donnée --> création des premières banques qui vont contenir au départ quelques centaines de fragments jusqu'à des milions de fragments. 
* 1984 : premier génome séquencé (EBV) virus (plutot petit)
* BLAST : logiciel d'analyse de séquence permet de grandes avancées
* 1995 : La connaissance du premier génome bactérien : Haemophilus Influenzae
* 1996 : Levure ; premier eucaryote séquencé
* 1997 : E.Coli 
* 2001 : Première ébauche Homo sapiens
* 2004 : Homo sapiens, séquence complète

Centimorgan : distance génétique qui se calcul en pourcentage
Les premières cartes génétiques datent de 1929 : Morgans
 * Un ensemble de gènes liés vont former un chromosome
 * Carte génétique obtenus suite à des croisements et qui se basent sur des analyses génétique de type méiose : les gènes sont identifiés et vont être positionnés les uns par rapport aux autres et distance pourront être mesuré et ce positionnement se fait grâce à la génétique classique et l'unité utilisée est le centimorgan.
 * Cette carte circulaire va correspondre à un génome circulaire --> génome bactérien (carte génétique d'E.Coli : établit dans les années 50) ELle se base sur des cartographies par combinaison, par transduction.
 * Les cartes génétiques: opositionnement des gènes les uns par rapport aux autres utilisant la génétique.
 * Le caryotype représente une autre approche génomique avant le séquençage ; il permet de mettre en évidence des ameuploidies ; permet de mettre en évidence l'ensemble des mutations dites génomiques ou chromosomiques, si ces mutations chromosomiques vont entraîner une restructuration du chromosome visible à l'échelle du caryotype (génomique avant la génomique) ; caryotype : début des années 50
    * Chromosome métaphasique ; chromosomes bloqués en métaphase
    * Application : caractériser le nbre de chromosome d'une espèce ; puis exploité dans les anomalies du nombre de chromosome ; nombre de cellules bloqués en métaphase en jouant sur des niveaux de résolution ; fin de prophase début de métaphase
    * Caractéristique : bandes claires et bandes sombres qui représentent des régions particulières du chromosome, certaines plus denses aux éléctrons ; d'autres moins ; régions plus colorées ; régions moins colorées
    * On se rend compte que ces régions sombres sont souvent des régions repetées alors que les régions claires sont plutot des régions où les gènes vont être répétés. Ces régions bandes claires bande sombres selon le degré de compaction de l'ADN on oit plus ou moins de bandes, si de forts contraste on va pouvoir voir des réarrangements plus fins. 
    * Comparaison des génomes en utilisant des logiciels (comparaison qui date de dizaines d'années)
    * Dia 33 : analyse caryotypique qui ont montré
        * Gorilles et schimpanzés : 2n = 48
        * Inversion pericentrique : deux espèces proches : l'un a subit une inversion pericentrique --> permet de montrer un certain nombre de remaniement chromosomique
        * Conclusion : des approches génomiques pouvaient se faire avant. Le catyotype permet l'étude de certains remaniements, de mutations à une échelle autre que la séquence en nucléotide
    * On obtient un caryotype en bloquant en métaphase ; on essaie d'augmenter le volume des cellules en division de manière à ce que les chromosomes se différencient bien. On les reconnait par tailles, succession de bandes et position du centromère.

* 1980 : Localisation du gène qui code l'insuline par des expérience d'hybridation ADN-ADN
    * On réalise un caryotype (ou du moins une métaphase)
    * On hybride avec une sonde ; sonde qui correspond au gène qui code l'insuline ou au fragment de gène qui code l'insuline. Ce gène est localisé sur le bras cours à l'extrémité du chromosome 3.

* Années 90 : Technique FISH avec fluorescence

* Mise en évidence de mutations chromosomiques et génomiques :
    * Dernière génération de ces approches caryotypiques : on utilise plusieurs sondes fluorescentes (combinaison de fluorochromes différents) qui vont donner des couleurs spécifiques aux sondes utilisées
    * On fait de "la peinture chromosomique"
    * Avec ces aspects on arrive à donner l'identité d'un chromosome à une couleur donné
    * On voit des rearrangmentS chromosomiques (quels réarrangements?)
        * Génome d'une cellule tumorale qui montre que sur le plan genomique un certain nombre de remaniements.

    * A partir des années 2010 on commence à séquencer le génome en entier
        * Un gène aurait-il été délété? 

* Dia 30 : Carte génétique : on positionne les gènes les uns par rapport aux autres grâce à des outil génétiques (transducition etc)
    * Carte physique des chromosomes : Ordonner les fragments les uns par rapport aux autres ; carte nucléotidique --> à partir de séquençage

* Dia 37 : Techniques d'éléctrophorèse : chaque bande correspond à un chromosome car les chromosomes sont moins condensés et de petites tailles.


## Un génome au niveau nucléotidique

### _a) Analyse d'un génome à l'échelle nucléotidique_

* On prend une espèce donné et on voit tout ce que l'on peut en tirer de son génome.
* On cherche les séquences codantes; on cherche les gènes qui vont coder les protéines
    * Logiciel : cherche des codons stops ; ORF : phase de lecture ouverte
    * Toute séquence protéique commence par 
        * Codon d'initiation : ATG jusqu'au codon stop TAA TGA TAG
    * Considérer les cadres de lecture
        * Séquence ORF et CDS peuvent être sur un brin ou sur l'autre ; tout dépend de l'orientation. Cette ORF peut être dans un cadre de lecture ou dans un autre (lecture par triplet)

On cherche des phases de lecture ouverte : tout ce qui est codon ouvert entre deux codons stop (ORF)
CDS: on cherche la séquence où on a un ATG qui commence pour aller jusqu'au codon stop.
 **1996 : premier eucaryote séquencé**

* Dia 44

 trait noir plein : codon stop à la séquence correspondante
 trait blanc : codons ouvert
 demie barre : codons ATG
 3 lignes par brins d'ADN car 3 cadres de lecture possibles

 flèches de couleur: indiquent des zones blanches : car phase de lecture ouverte (ORF) et dans l'autre cas : CDS

 Longue case blanche : ORF mais cette phase de lecture n'est pas sur le même brin
 ; cases blanches : jamais chevauchantes
 Sur ce segement d'ADN, en utilisant cette approche a informatique on voit que l'on a plusieurs ORG et CDS que l'on peut caractériser. On voit qu'elles ne sont pas chevauchantes, on voit aussi qu'elles ne sont pas toutes dans le même cadre de lecture.
 Les différentes phases de lecture ouverte ne sont pas dans le même cadre de lecture et elles ne sont pas toutes localisées sur le même brin. CCl : le brin codant peut être le brin 3'-->5' ou 5'-->3'


ccl : c'est un génome codant car les phases de lecture sont les unses à côté des autres : peut de régions intergéniques.

Chaque ĥase de lecture va être assimilée à un gène codant des protéines.

**Pseudogène**: phase de lecture ouverte avec décalage de phase de lecture : géne inactivé qui a perdu sa fonction.

L'identification de toutes ces séquences est la première étape, il s'agit de faire une **annotation** d'un génome

Séquences surlignées : séquences particulières d'un gène, on a un ORS
* En vert : promoteur 
* En bleu : les introns
* En blanc : les exons

Dans le processus d'annotation on va essayer d'aller plus loin et d'indiquer qu'il s'agit une phase de lecture ouverte mais proposer également une fonction. 

Fonction ?
* Génétique inverse : on fait un mutant et on va regarde le phénotype (on va du génotype vers le phénotype
* Prédiction : comparer avec les séquences des protéines que l'on connait déjà dans les banques de données
* On va trouver une protéines qui va posséder des régions ; des domaines similaires
* similarité : les acides aminés que l'on voit chez la protéine dont on cherche la fonction sont identiques ou similaires. exemple : AA similaires : acide glutamique et acide aspartique ; arginine et histidine ; cystéine et méthionine.
* Gènes homologues si on a similarité ; comme on est chez deux espèces différences on a des gènes orthologues. Si on a des gènes orthologues on va faire la prédiction que la protéine a la même fonction.

* Essayer d'identifier les gènes, leur organisation et également leur fonction
* Annotation d'un gènome : recherche de tous ces éléments.

### _b) Séquençage de génomes complets_

#### 1) Généralités

* 1977 : bactériophage avec techniques de Sanger
* La mise au point de stratégie de séquençage des génomes
* Si l'on dit que le séquençage est final/finis on a un nombre de séquence qui correspond au nombre de chromosome.
permanent draft : séquences complétes ; on a toute l'information mais on a pas une répartition pas chromosome, on a peut être 100 fragments, on est pas allé ou bout de l'assemblage. 
Dans les banques de données on considère que le génome est complet mais pas assemblé.

 Les premier génomes, les premier résultats que voit-on?
    **Nombre de gènes limités** : génome humain --> la réalité montre qu'il y en a nettement moins (avec séquençage alternatif)
    **Redondance du génome** : de nombreux gènes sont dupliqués (deux exemplaires), mais ces deux exemplaires n'ont pas forcément la même fonction. La duplication est un moyen pour créer de nouveaux gènes et pour faire de l'innovation génétique.
    Des gènes sont encore à fonction inconnue
    **Conservation des grandes fonctions cellulaires** : de nombreux transposons
    Chez certaines lignées : plus de 70%

Quand on dit qu'on a séquencé un génome on a séquencé un individu appartenant à l'espèce

#### 2) Grands principes de séquençage

* 1. Extraction de l'ADN à réaliser (on isole l'ADN)+ génome de la mitochondrie (ADN fragmenté, qui n'a pas une organisation en chromosomes ; il faudra purifier cet ADN, se débarasser des histones etc pour n'avoir que des fragments d'ADN)
* 2. Préparer cet ADN pour qu'il puisse être séquencé --> on construit ce que l'on appelle une banque. 
Banque d'ADN : génerer de nombreuses copies pour avoir des fragments d'ADN. Ces fragments devront être manipulables, il faudra les incorporer dans un vecteur parce qu'une banque c'est un ensemble de fragements d'ADN qui sont clonés à partir d'un vecteur. (bactériophage ou virus; des cosmides). Ce vecteur possède une capacité de clonage. Pour le plasmides c'est jusqu'à 10 kb. A l'opposé un chromosome artificiel c'est 100 kb.
Fragments à préparer : <10kb
Une fois qu'on a choisis un vecteur on adapte la taille du vecteur à ce fragment. Il faut donc un vecteur et des fragments de taille compatible. Ces fragments devront pouvoir être insérer dans le vecteur. Il faut des extremités compatibles avec celles du vecteur.  Il faut également l'orienter.
* 3. On obtient un nombre important de séquences qu'il faudra assembler.

Les banques doivent être représentatives du génome. On veut l'ensemble donc la banque doit être riche/complète. 

#### 3) Stratégies de séquençage

##### i. Séquençage aléatoire global

* Principe : On séquence tout et on assemble après ; on crée des pièces du puzzle, on va le construire.
>
* Extraction d'ADN
* Fragmentation aléatoire (voir diapos)
Différence entre coupure enzymatique et coupure mécanique : 
    enzymatique : controlée, on connaît ce qu'on a aux extrémités. Mais on peut controler la taille dans les deux cas. (Taille pour qu'on puisse intégrer dans le vecteur).
* Sélection des fragments
>
* Pour la banque la fragmentation doit être chevauchante
* On obtient un contig (assemblage de fragment) --> logiciel
* Une banque doit être chevauchante et doit contenir un nombre de fragments largment supérieur si on met les fragments bout à bout.
* Contig : séquence consensus de séquençage et si différence de séquence la base retenue sera la plus fréquente. C'est une région du génome que l'on considére comme équivalent du génome et qui a été obtenu par le séquençage de fragments qui ont été assemblés selon le recouvrement de séquences.
>
Chevauchement?
>
_______________________ (segments d'ADN)
E   E    E   E    E   E  (Enzymes de restriction)
>
Ils ne sont pas chevauchants
Conditions expérimentales qui vont générer des coupures partielles. On peut par exemple jouer sur le temps ; on peut jouer sur la concentration ; ou sur la température qui n'est pas optimale (on fait que la réaction enzymatique ne soit pas totale).

En ayant des sites qui ne sont pas utilisés à chaque fois, donc on va avoir des chevauchements :
CCL : Fragmentation partielle ; on joue sur l'activité de l'enzyme.

##### ii. Difficultés

* Séquences répétées dans les génomes eucaryotes.
* Régions hétérozygotes --> Régions assemblés en deux contigs différents par le logiciel
* Résultat de l'assemblage automatique va donner des centaines voire des miliers de contig pour un génome, on aura jamais un assemblage parfait, le logicile peut se retrouver face à des problèmes qu'il ne peut régler. 
* Les séquences répétées sont très nombreuses --> elles vont posées problèmes : pas de point d'ancrage pour les positionner les uns par rapport aux autres. Partie rouge : partie non répétitive. 
* Il existe beaucoup de types d'assembleurs