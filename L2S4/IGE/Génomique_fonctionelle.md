# Généralités

ADN du génome fonctionnel = ADN associés à différentes protéines (histones) Protéines qui vont intéragir avec des histones modifiés chimiquement (épigénome), facteurs de transcription. 
Au niveau de ce génome : ARN non codant qui participent également à l'expression des gènes. Au niveau de l'ADN on a des régions qui codent des ARN : on a deux types : des ARN codants = ARN messagers et des ARN non codants. Un gène est sous la dépendance d'un promoteur.Un gène s'exprime en fonction du taux des facteurs de la transcription. Il peut être activé ou inhibé. Un gène est dans un contexte chromatinien qui va jouer un rôle très important dans la régulation de l'expression des gènes. Nous n'avons pas tous le même génome, on peut avoir des régions dupliquées, des régions inversées ou encore des translocations. 

**La génomique fonctionnelle a pour but de comprendre la pathogénicité de ces variation du génome.** SUite à des variations : peut être à l'origine de maladie génétique : ca rrégion qui ne touche pas le gène ; ne touche pas dans des régions importantes dans l'expression d'un gène. Quand touche un promoteur : peut être à l'origine de la maladie également. Mais lorsque des régions différentes --> comment savoir s'il peut y avoir un effet sur la pathogénicité.

Pour comprendre comment un gène s'exprime il va falloir comprendre différentes choses : identifier le gene codant pour la protéine, identifer le promoteur, identifier les enhancer, identifier les facteurs de transcription qui se fixent sur le promoteur et analyser le contexte chromatinien. 
 Différenciation cellulaire? Cette différenciation est rendue possible par la différenciation d'expression des gènes. Totes les cellules ont le même génome mais tous les gènes ne s'expriment pas.
 Si on considère tous les types cellulaires : presque tous le génome est transcris: un gène correspond à une région de l'ADN qui est transcrite ; un gène est sous la dépendance d'un promoteur.

**Un géne est dépendant d'un promoteur, un promoteur est constitué d'éléments régulateur = séquence reconnnue spécifiquement par un facteur de transcription.**

La régulation d'expression des gènes peut s'exprimer à plusieurs niveaux
* Régulation transcriptionnelle : taux de transcriptions ; le produit de la transcription du gène doit être transporté
* Du noyau vers le cytoplasme. On peut avoir une régulation au niveau du transport nucléo cytoplamsique.
* Les ARN m : maturation de l'ARN précurseur qui a lieu dans le noyau donc on peut agir également sur la méturation. 
* Au niveau traductionnelle : on peut inhiber la traduction.
* Homéostasie : durée de vie des molécules dans la cellule ; durée vie de l'ARNm peut être différente.

Certains gènes vont pouvoir s'exprimer de façon constitutive : gènes ubiquitaires --> s'expriment quelque soit les conditions. Le produit de la transcription de ce gène participe à la vie de la cellule. Pour d'autres gène la régulation va être modulée.

La régulation transcriptionnelle va être basée sur la présence du facteur de transcription. On peut définir des réseaux de régulation. Un gène code pour une protéine qui peut activer un autre gène etc. 

Pour étudier l'expression d'un gène on ne se contente plus de mettre en évidence la présence ou pas du facteur de transcription, on analyse également le contexte chromatinien et la topologie de la chromatine. 
Caractériser les gènes, caractériser les éléments régulateurs, identifier un facteur de transcription.

# Techniques d'analyse de séquençage à haut-débit


Projet NCODE : ont recherché des motifs conservés de 8 pb pouvant jouer un rôle dans la régulation de l'expression des gènes
Projet Epass: étudier la chromatine et toutes les modifications qui peuvent avoir un effet sur l'expression génétique. 
Ces modifications épigénétiques : toute modification pouvant entrainer une reprogrammation de la cellule.

Toute méthode de séquençage est basée sur la synthèse d'ADN.
Sanger ;  LUMINA ; Séquençage à Haut-débit NGS ; Séquençage à molécule unique

## Pyroséquençage

Une molécule d'ADN : fixée sur une bille 
Pyroséquençage = utilisaiton du pyrophosphate produit lors de la synthèse d'ADN pour envoyer un signal lumineux qui permettra de séquencer. POur que ce signal lumineux soit détectable il faut amplifier cette molécule d'ADN sur la bille, par PCR. On fragmente de l'ADN par ultrason, ces fragements seront hétérogènes : certains à bouts francs, certains avec extremié 3 ou 5' sortante. Si on veut faire de la PCR : on doit utiliser une amorce. Ici on ne connait pas la séquence. On va étiqueter l'ADN : on va ajouter aux extremités de ces fragments des séquences d'ADN double brin connus. Mais deux étiquettes différentes aux extremité du même fragment. Certaines ne sont pas à bouts francs: on peut utiliser des ADN polymérase à activité exonucléase dans les cas des extremités 3' qui va dégrader les simples brins. Tous les fragements d'ADN opssèdent alors une extremité franche. Il faut étiqueter avec deux étiquettes différentes. L'ADN ligase : pour cataliser la fomation phospho diester a besoin d'ATP et les fragments doivent présenter une extremité 5' phosphate et l'autre un 3' OH. Cette enzyme doit être exprimée en présence d'ATP. Au cours de cette réaction : formation d'un intermédiaire : ajout d'un monophosphate sur l'extremité 5'phosphate. 

On va avoir 3 types de molécules : fragments d'ADN etiqueté AA ; AB et BB. Il va falloir sélectionner le fragment étiqueté avec les deux extremités différentes. Astuce : biotinylé l'extremité d'une amorce. On va avoir des fragments biotinylés sur une extremité, sur les deux ou pas du tout. On veut sélectionner les fragments qui ne possèdent qu'une biotine. La biotine a une forte affinité avec ?
Réalisation d'une chromatographie d'affinité. Toutes molécules biotinylées sera retenue, les autres passeront dans l'effluants. Ensuite on dénature : on va casser les lisaisons hydrogènes --> la molécule d'intérêt va passer dans l'éluatn, toutes les molécules biotinylées vont rester fixer sur la résine. Ici c'est l'éluant que l'on souhaite récupérer.

 --> Cours 2
# Localiser un gène exprimé

Approche expérimentale : localiser le gène pour en déduire l'expression
Localiser un gène : consisite à définir les limites. =1 = **premier nucléotide incorporé dans l'ARN. --> site d'initiation de la transcription**. La connaissance de ce site d'initiation de la transcription sera utilise pour rechercher les éléments régulateurs afin de pouvoir comprendre comment ce gène va être régulé. On localise le +1 en utilisant le transcrit. On va rechercher les élements promoteurs en amont du +1, mais il peut également y en avoir en aval du +1 dans une fenêtre de 1000 pb en amont et en aval du +1.
On a une population d'ARNm représentatif de l'ensemble des gènes qui s'expriment. On va utiliser cette solution pour synthétiser du cDNA : représentatif de l'ensemble des ARNm produits dans la cellule, ils seront clonés et on aura une banque d'ADNc qui doivent être complètes (représentative!!) de l'ensemble des ARNm produits dans la cellule.
On utilise la queue poly A pour synthétiser le cDNA (**brin d'ADN qui a une séquence complémentaire et antiparallèle à l'ARNm**). Il va être synthétisé par la reverse transcriptase (= ADN polymérase qui synthétiser de l'ADN en utilisant comme matrice de l'ARN.) Elle nécéssite une amorce. 
On a un hétéroduplex: brin d'ADN hybridé à un brin d'ARN : on veut éliminer l'ARN hybridé à ADN. On utilise pour cela une ARNase H : c'est une endonucléase qui coupe sans spécificité l'ARN hybridé à l'ADN. On est alors en présente du cDNA sur lequel sont hybridés des petits morceaux d'ARN qui vont consitués des amorces pour l'ADN polymérase. On utilise l'ADN polymérase I --> seule qui a une activité 5' --> 3' exonucléase. Elle s'amorce sur les fragments d'ARN et grâce à son activité exonuléase elle va dégrader les amorces ARN et synthétiser de l'ADN. Action d'une DNA ligase pour lier ces fragements d'ADN entre eux. Après avoir inséré le cDNA dans le veceur de clonage on va séquencer l'extr&mité 5' et 3' de ce cDNA. On va comparer ces séquences à la séquence de l'ADN génomique grâce à BLAST ce qui permet de localiser la séquence sur le génome par comparaison.


# Architecture très variable pour l'expression des protéines

On va définit plusieurs zones :
* Corps promoteur : Première région qui le caractérise : qui chevauche le +1 ; ce corps promoteur mettra en place la machinerie traductionnelle ; (caractéristiques différent selon les gènes)
* Promoteur de base : 1000 pb en général en ammont du +1 
    * constitué de plusieurs motifs qui constituent la cible de facteur de transcription
    * Ce promoteur de base module l'expression des gènes ; 
* Enhancer : peuvent spécifier l'expression d'un gène ; région de l'ADN constituée de motifs spécifiques de facteur de la transcription ; ces enhancer spécifient un type cellulaire ; peuvent être situés à plusieurs miliers de paires de bases en amont mais aussi en aval du gène cible.
* Ces gènes sont localisés dans des domaines délimités par des insilateurs (= un élément de séquence pour un motif reconnu spécifiquement par une protéine).

# Il existe des corps promoteur avec et sans TATA box

* Les promoteurs dépourvus en TATA box : riches en CG ; l'initiation de la transcription débute dans une fenêtre de 100 nucléotides.

Recherche de ces motifs connus dans cette fenêtre de 1000 pb de bases en amont du gènes. Pour savoir si un gène s'dxprime on va devoir mêtrre en évidence la présence des ces protéines au niveau de ce promoteur. Si un facteur de transcription est présent --> on met en évidence l'expression de ces génes (argument fort).

# Les motifs de liaison à l'ADN :

* Doigts à Zincs
* Hélice boucle Hélice
* Leucines zipper

# Interactome

## Etude de l'interactome

Technique d'immunoprécipitation de la chromatine.
GRâce à un anticorps on peut enrichir une solution en antigène spécifique d'un anticorps donné --> on fait une chromatographie. 
L'immunoprécipitation consiste à effectuer une chromatographie d'affinité afin d'isoler le complexe atnigèe anticorps (on ve le fixé et on va l'éluer) --> la solution est enrichie.
On casse les cellules et on met en évidence les intéractions protéine-ADN ; si on casse la cellule --> on va casser les intéractions faibles ; on fait un pontage chimique. Ce ne sont presque jamais des liaisons covalentes ; on va stabiliser ces intéractions. ATTENTION EXAMEN!

## Immunoprécipitation de la chromatine : ChIP

On ajoute un pontant est ajouté aux cellules, cet agent va stabiliser l'intéraction protéines-ADN. Une fois qu'on a réalisé ce opntage chimique on solubilise la chromatine en la fragmentant ce qui permet d'obtenir la chromatine soluble que l'on peut utiliser pour faire une immonoprécipitation. On utilise des cellules en culture ; A la chromatine soluble on ajoute un anticorps spécifique d'un facteur de transcription. Il se forme des complexes antigène-anticorps. Sur le support chromatographique on a des fragments de chromatine fixé ; seul seront retenus les complexes antigènes - anticorps = fragments de chromatine qui renferme le motif spécifique du facteur de transcription. 
On élue et réversion du pontage : on est en présence d'ADN enrichie en fragment qui renferme le motif de transcription.
Une fois qu'on obtient ces fragments ; séquençage à Haut débit des fragments d'ADN ChIPSeq
On obtient des miliers de séquences que l'on aligne et compare à la séquence de l'ADN génomique ; on peut alors identifié sur le génome toutes les régions sur laquelle est fixée cette protéine.

## Résultats du programme ENCODE

* Séquences dégénérées? séquence qui varie d'un motif à l'autre ; facteurs 

Comment savoir si le promoteur est accessible? Si l'ADN est couplé à une endonucléase ??
COmment cartographier des régions condensées et décondensées. 
**Epigénome : ensemble des modifications chimiques au niveau de l'ADN et des Histones succeptibles de modifier le devenir d'une cellule, pouvant entraîner une reprogrammation.**

AA qui peuvent être modifiés chimiquement : Sérine ; Thréonine --> Acétylation et méthylation

# Oncoprotéines : origine de cancer. 
* Certaines vont acétyler la protéine, vont permettre à certains gènes de s'exprimer, et dérégulation. Dans le cas de certains cancers on a une protéine chimère qui va reconnaître la chromatine acétylée et permet l'expression du gène. ON va créer des molécules chimiques qui vont se localiser au niveau du site, ce qui empechera la protéine de s'associer au groupement acétyl (se fixe sur le bromodomaine)
* Méthylation de lysine : soit 1,2 ou 3 gpt méthyl. Des méthylations peuvent être repérés par des protéines puis propagation de l'hétérochromatine. Les modifications chimiques constituent un signal, reconnues par des protéines qui vont induire une action.

# Comment mettre en évidence ces modification?
* Immunoprécipitation de chromatine couplé au séquençage.
* Lysine 4 au niveau de l'histone 3 est triméthylée --> c'est une signature de la chromatine active = euchromatine, on dispose d'anticorps spécifique. On peut alors cartographier ces domaines.

# Comment mettre en évidence les méthylation?
* Traitement de l'ADN par le bisulfite de sodium entraine une désamination d'une cytosine --> transformation de la ytosine en uracile. Comment détecter l'apparition de ces uraciles? Réaction de PCR ensuite séquençage. SUite à une PCR : U = T donc mutation. ON peut identifier une région de l'ADN où les cytosines sont modifiés. Très faible de C dans le génome. On a une désamination régulière, si non détécté = mutation donc explique le fait qu'on a un faible taux de C dans le génome.Amplification par PCR puis séquençage SANGER auomatisé.

La structure tridimenssionnelle du génome est importante pour l'expression des gènes. 
* On dispose de plusieurs techniques permettant de mettre en évidence les structures 3D. 
* On définit des TAD = domaine topologiques ; grandes boucles au niveau du génome, on ne sait pas encore comment ils se mettent en place. On a montré que certains de ces domaines étaient facultatif ou constitutif. Les TAD constitutifs sont conservés d'un type cellulaire à l'autre, mais également d'un organisme à l'autre. Les TAD sont localisés au niveau des génes de ménage = nééssaire à la vie de la cellule.TAD peuvent s'exprimer au cours du développement. 
* TAD facultatif : au cours du développement, les zones de chromatines actives augmentent. Ces domaines topologiques se modifient donc au cours du temps, ils ne sont pas fixés. La taille du domaine topologique augmente avec le nombre de gènes aitvés.

Des gènes corégulés ont des facteurs de transcription communs au niveau du promoteur. BOucle de régulation --> voir schéma. 
CTCF = protéines qui se fixent sur les séquences inculateur?
La cohésine peut être localisée au niveau des frontières, mais également au niveau des boucles de régulation qui correspondent à l'intéraction entre les protéines de l'enhancer et les protéines qui se fixent au niveau du promoteur.

Mécanismes moléculaires sont altérés si modification génomique. Des oncométabolites peuvent être à l'origine de déréglement. La méthylation est reversible. SI alpha cétoglutarate diminue dans la cellule, les D-méthylases vont être inactives. D-méthylase inefficace empechera de se fixer sur sa séquence cible, en engendrant un destructuration du domaine topologique. Un oncogène présent dans ce domaine va pouvoir être surexprimé car il va pouvoir entrer en contact avec un enhancer voisin.

Drosophile stressé :
Dans une drosophile non stressé : ce gène est silencieux.
Le choc thermique engendrerait une phosphorylation qui se dissocierait de la chromatine et qui donnerait lieu à de l'eucromatine. Des facteurs extérieurs peuvent donc modifier l'épigénome.  Ces modifications épigénétiques induites par des facteurs tel qu'un stress: peuvent être transférer d'une génération à l'autre?  Ici cette modification a été maintenue d'une génération à l'autre, mais se trouvent diluée et peuvent être perdues au cours du temps.

# Transcriptome

On a mis en évidence des ARN intergénique, preuve que l'ensemble du génome est entièrement transcris.
Le transcriptome permet d'identifier les gènes exprimés, mettre en évidence des ARN non codants = microARN d'autres plus grands. Ce transcriptome est analysé de façon global. Au départ on utilisait des puces à ADN, maintenant : analyse à haut début?
Transcriptome est complexe? Les deux brins son transcrits? promoteur deux à deux qui permet la transcription dans un sens et dans l'autre, on peut avoir des promoteurs qui se superposent : transcription convergente. On peut avoir des promoteurs bidirectionnelles. Chez les Eucaryotes : un gène ne va pas coder pour une protéine, mais pour plusieurs protéines. Le transcrit primaire est un transcrit non fonctionnelle qui va être maturé --> Épissage. Autre mécanisme dans la diversification : la polyadénylation alternative.  CCL : augmente la capacité codante du génome : 25 000 gènes cartographiés chez l'homme, et 90 000 protéines suite à des analyses protéomique. Ces protéines sont appelées des protéines isoformes qui peuvent avoir des fonctions différentes dans différentes cellules.$

Exon = région de l'ADN conservé au cours des mécanismes de maturation appelé épissage.
Intron = région de l'ADN qui n'est pas conservé

Epissage constitutif = élimination de tous les introns et cnoservation de tous les exons
Epissage alternatif = peut entrainer la perte de la totalité d'un exon, ou d'une partie d'un exon.

La capacité codante d'un génome peut être due à une polyadénylation alternative. Au niveau de la région non codante : reconnaissance de signaux qui vont permettre de couper l'ARN et de le polyadényler --> Deux ARNm
3' UTR peut renfermer des séquences cibles impliqués dans les mécanismes de régulation de l'expression de gènes. 

Comment obtient-on les transcrits?

On doit éliminer les protéine : extraction au phénol = solvant organisque qui dénature les protéines, puis centrifugation : on aura deux phase = phase aqueuse avec acide nucléique et phase organique avec les protéines. On préléve la phase aqueuse et on fait précipiter avec de l'éthanol = chasse les molécules d'eau, on a un précipité qui renferme tous les ARN codants et les ARN messagers.On veut sélectionner les poly adényler, on fait passer l'ARN sur une colonne oligo DT et les ARN poly adénylé se fixent sur l'oligo DT, on diminue la concentrations en sel pour casser l'hybride. Dans le cas des ARNm bactériens, on retient les ARNt sur la colonne et l'ARNm passe dans l'éluant.

RNAseq : on va sésquencer le cDNA simple brin. Pour cela, on utilise des amorces. 
ADN polymérase ARN dépendante, donc amorce d'oligonucléotide Petit nucléotide de 9 nucléotides de séquence dégénéré = on aura toujours une amorce pour un ARNm cible. On ajoute la reverse transcriptase. On aura de l'ADN simple brin suite à une déshybridation. Avant de séquencer cet ADNc sera amplifier par PCR puis séquençage par Illumina.
