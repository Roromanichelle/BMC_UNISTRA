

# Métabolisme central et voies de biosynthèse

* Réaction de polymérisation  : tout le métabolisme est orienté par le fait que ces réactions de polymérisation sont irréversibles. Les briques élémentaires permettent de faire une polymérisation
* Part d'un métabolite précurseur (au nombre de 12) (=brique élémentaire)
* Toutes les voies de biosynthèse partent de métabolites précurseurs ; d'énergies etc (voir dia)
* Tous les intermédiaires des voies métaboliques sont chargées négativement
* La double couche de phospholipide est imperméable aux molécules chargées
* Le métabolisme existait avant les cellules (c'est ce qu'on appelle le métabolisme de surface) ; réactions qui se faisaient sans enzyme, dès qu'une réaction donné un produit non chargé le métabolisme était perduré alors que les intermédiaires chargés négativements restaient sur la surface.
* Le nom des enzymes décrivent la réaction que l'on peut doser facilement in vitro.

## Biosynthèse et nutriment

* Les capacités biosynthétiques et les exigences nutritionnelles sont complémentaires
* Plus nombreuses sont les voies de biosynhèse présentes dans un organisme moins nombreux sont les besoins nutritionneles

## Exemple de deux milieux de culture

* Escherichia coli : se contente d'un milieu minéral + un composé organique = glucose --> (milieu minimum)
* Streptococcus : incapable de synthétiser

## Oxydo-réduction
### Composés réduits ou oxydés?

* Un composé organique est dit "réduit": lorsqu'il est pauvre en oxygène
* ou riche en hydrogène
* ou riche en éléctron

Un composé orgainque est dit oxydé lorsqu'il est riche en oxygéne 
* ou riche en oxygène
* ou pauvre en hydrogène
* ou pauvre en éléctron

### Principales réactions rédox
* Oxydations dans les voies d'approvisionnement
SH2 + NAD+ --> S+ NADH + H+
* Réduction dans les voies de biosynthèse
M+NAH + H+ext --> NADPH + NAD+ + H+int
* Réaction de transhydrogénation
Nécéssite l'énergie de la pompe à proton
* La transhydrogénase est une enzyme membranaire utilisatrice du potentiel de protons.

### Caractéristiques des voies de biosynthèse
* Commencent toujours par un des 12 métabolite précursuer
* Finissent toujours par une brique élémentaire
* La brique élémentaire est généralement plus réduite que le métabolite précurseur (c'est le NAPH qui donne les éléctrons
* L'énergie consommée par les voies métaboliques correspondent à l'ATP
* La brique élémentaire est un effecteur allostérique de l'enzyme qui catalyse la première étape de chaque voie (phénomène de rétro-inhibition)

Toutes les réactions se font à la même vitesse et cette vitesse correspond au flux de la voie de biosynhtèse. La concentration de chaque métabolite est constante : phénomène d'homéostasie (l'ensemble des concentrations est constante)

Rétro inhibition : le produit final (brique élémentaire) inhibe la première enzyme de la voie de biosynhtèse
Le flux d'une voie de biosynthèse ne dépend donc pas de la disponibilité du métabolite précurseur mais de la vitesse d'utilisation de la brique élémentaire par les réactions de polymérisation.

La vitesse de croissance ne dépend donc pas de la vitesse de production des briques élémentaires

Ce sont les flux des voies de biosynthèse qui s'ajustent à la vitesse de croissance.

# Coordination des voies de biosynthèse

Les miliers de réactions chimiques du métabolisme sont coordonées :
* La vitesse des synthèse de chaque brique élémentaire égale la vitesse de sa consommation par les réactions de polymérisation
* Le flux de chaque voie de biosynthèse est coordoné avec les flux des autres voies
* La présence de nimporte quelle brique élémentaire dans le milieu arrete la synthèse de cette brique
* La coordination d'une voie de biosynthèse avec les autres n'est pas fixe mais modifiable.

# Deux niveaux de régulation
* Une réaction métabolique peut être régulée à trois niveaux :
    * Modifier l'activité de l'enzyme (quasi instantané)
    * Modifier la quantité de l'enzyme (plus long) ;
    * Modifier la concentration de l'un des substrats (ce qui impliquerait de réguler une autre réaction)

Ces modifications peuvent se refleter sur la rétro-inhibition des voies de biosynthèse 

# Pourquoi réguler la quantité d'une protéine?

* Ne pas synthétiser de protéine inutile
* Augmentation très rapide de la concentratino (ou diminution assez rapide (par dilution))
* Les ARN m procaryotes sont très instables d'où la possibilité de changer rapidement de jeu de protéines
* La nature polycistronique des ARNm permet d'ajuster d'un coup tous les enzymes d'une voie de biosynthèse. 

# Exemple de régulation de la quantité d'une protéine

* Les voies de biosynthèse à partir des douze métabolites précurseurs à partir des douze métabolites précurseurs, avec du pouvoir réducteur et de l'énergie, conduisent aux briques élémentaires.
* Les voies d'approvisionnement répndent à partir des nutriments aux besoins des voies de biosynthèse sous forme de douze métabolites précurseurs, pouvroir réducteur, énergie, azote, souffre.

# Les réactions d'approvisionnement

* Elles produisent :
    * Les 12 métabolites précurseurs
    * Le pouvoir réducteur (NADH,NADPH)
    L'énergie nécéssaire pour l'ensemble du métabolisme (principalement sous forme de potentiel de protons et d'ATP)

    (Elles incluent les voies cataboloiques et amphiboliques en biochimie!)
* Coeur des réactions d'approvisionnement = voies centrales du métabolisme    
    * Glycolyse
    * Voie des pentoses phosphates
    * "Cycle de Krebs" (qui n'est jamais cyclique chez les MO)
* Chez les procaryotes :
    * Les réactions d'approvisionnement sont très diverses : d'où l'utilisation possible de la quasi totalité des substrats organiques et des sources d'éléctrons disponibles dans à peu près toutes les conditions.

# Le métabolisme centrale

Deux façons de voir : Les voies centrales donctionnent de façon cycique pour dégrader complétement les substrats en CO2 et H2O
Les voies centrales fonctionnent de façon linéaire (physiologie microbienne..) pour donner les douze métabolites précurseurs.

En fait les voies centrales font tout en même temps
Les voies de biosynthèses prélèvent les métabolites précurseurs dans les voies ????

# Flexibilité du métabolisme centrale

Fonctionnement dépend de la nature des substrats disponibles pour la croissance, de la nature des donneurs et accepteurs d'éléctrons présents, de la disponibilité de briques élémentaires dans le milieu.

Si aucun accepteur d'éléctrons n'est présent : les réactions du cycle de Krebs servent uniquement pour produire des métabolites précurseurs.
Si un accepteur d'élétrons est présent (par exemple l'oxygène), le cycle de Krebs oxyde l'acétyl-CoA en CO2 ce qui engendre la majeure partie du potentiel de protons dont de l'ATP nécéssaire à la croissance
Mais le cycle de Krebs fonctionne presque exclusibement pour fournir ses trois métabolites précurseurs et non comme unc cyle enendrant de l'énergie. 
(L'énergie est un sous-produit de la synthèse)


On part du glucose ; glycolyse/ voie des pentoses phosphates ; mais pas de cycle de Krebs chez les micro-organisme. Chez ces microorganismes --> synthèse de la biomasse ; dans ce cadre : pas besoin de faire tourner un cycle de Krebs. Pour un MO réactions krebs mais linéaire : il suffit de  3 métabolites précurseurs : acétyl Co A ; Oxaloacétates ; Oxaloglutarate. 
Si on utilise du malate : réactions reverse --> on va pouvoir synthétiser les 12 métabolites précurseurs. 

Donneur d'éléctrons : deux rôles --> fournit de l'énergie ; se retrouve à l'extérieur ; deuxième rôle incorporer les éléctrons dans la biomasse.
chimiotrophie : réactions rédox

# Respiration fermentation, photosynthèse, autotrophie

Respiration : le métabolisme énéergétique (chimiotrophe) implique l'utilisation d'un accepteur d'éléctrons externe
Fermentation : le métabolisme énergétique (chimiotrophe) n'utilise pas d'accepteur d'éléctrons externe
Photosynthèse : phototrophie
Autotrophie : La source de carbone est inorganique (autotrophie = fixation du CO2)

## Respiration

Le métabolisme énergétique (chimiotrophe) implique l'utilisation d'un accepteur d'éléctrons externe (souvent appelé "accepteur terminal d'éléctrons")
* Il y a donc une oxydation nette du substrat source d'énergie
* Le rendement de production d'énergie est meilleur qu'en fermentation par rappor au substrat consommé
* Pour 1 g de biomasse 18 000 ATP que ce soit en fermentation ou en respiration (pas de différence sur la quantité d'énergie fournie). Les cellules consomment plus de nutriments pour fabriquer la même quantité d'énergie dans le cas de la fermentation. 
* Métabolisme aérobie ou anaérobie(peut être != O2)
* Accépteur d'éléctrons organiques ou inorganiques
* Implique une chaîne de transport d'électrons souvent appelée chaine respiratoire.

## Fermentation

Le métabolisme énergétique (chimiotrophe) n'utulise pas d'accepteur d'élécton externe :
* Il n'y a pas d'oxydation nette du substrat source d'énergie
* Le rendement de production d'énergie est moins bon qu'en respiration par rapport au substrat consommé
* Le métabolisme aérobie ou anaérobie.
* Synthèse d'ATP principalement par phosphorylation au niveau du substrat
* Le potentiel de protons est toujours généré, avec ou sans chaîne de transport d'éléctrons. 

Stoechiométrie : pas forcément

## Prototrophie et photosynthèse

* Phototrophie : utilisation de la lumière comme source d'énergie
* Le photosynthèse : synthèse de biomasse par la réduction de CO2 en utilisant la lumière comme source d'énergie
* Phototrophie = phototrophie !
* Photosynthèse = photoautotrophie
* Tous les phototrophes ne sont pas photosynthétiques
* Chimiosynhtèse : est le pendant de photosynthèse ; organismes capables d'utiliser le CO2 comme source de carbone à l'obscurité (chimioautotrophe)