# Quest ce qu'une classification ?

## Quel est l'intéret d'une bonne classification?

* En biologie fondamentale : 
    * Etudes comparatives
    * Etudes sur l'évolution
* En biologie appliqué
    * Agriculture
    * Santé publique
    * Environnement
* Exemple classique de Pneumocystis carinii 
    * Typanosome ?
    * Champignon (asque, ascospores...) ?
    * En fait il s'agissait d'un champignon
    * On a réussi à le guérir. 
    * On a encore un grand nombre de pathogènes qui ne sont pas classifiés, hors nécéssité pour trouver des réd

## Buts priatiques

* Identification : quel est ce MO?
* Distinction : quels sont les différences entre org
* Délimitaiotn : Ces organismes appartiennent-ils aux même groupes

En biologie de l'évolution : on ne parle plus d'individu mais de groupes de populations.

_"Les espèces sont des groupes de populations naturelles qui se croisent et sont isolées sur le plan de la reproduction, d'autres groupes similaires."_


## Intuitivement

Appartiennent à la me espèce : les individus
* Qui se ressemblent (aspect de l'organisme, squelette, caryotype, séquence) mais quel degrès?
* Qui descendent les uns des autres (ensemble d'individus ayant une orgigine commune) comment le savoir?
* Qui se fécondent (ce qui semble précis et objectif : fécondation ou non) nombreses exceptions en captivité... ; quid des espèces asexués?

## Quest ce qu'une espèce

* Défintion biologique de l'espèce
    * "Les espèces sont des gourpes de population naturelles qui se croisent et sont isolées sur le plan de la reproduction d'aures groupes ismilaires.
* CHez les organismes dépourvus de sexualité? "défintion" aussi nombreuses que semblables :
    * "Une espèce procaryote est un groupe de souches qui montrent un grand degré de similarité globale et différe beaucoup de groupes de souches apparentés pour plusieurs caractéristiques indépendantes."
--> c'est la même chose

## Quest ce qu'un arbre?

* Un arbre est une façon de représenter : 
    * Une hiérarchie
    * Un organigramme
    * Une clef d'identification
    * Des relations de parenté
* Un arbre phylogénique si les méthodes utilisées pour le construire reposent sur des hypothèses de l'évolution.
* Il est normal que les classifications changent constamment



Darwin : pas d'évolution mais l'origine des espèces
Les espèces scindées divergent graduellement et inféfiniment, au cours de l'évolution on s'éloigne de l'espèce en commun
La diversification biologique ne se produit qu'au niveau élémentaire, les taxons supra spécifiques n'en sont que des réusltats.
Les rangs taxonomiques (espèce, genre, famille...) sont donc arbitraires.
Les lignages pérennes sont ceux qui divergent le plus, ne vont se maintenir dans le temps que ceux qui s'éloignent de l'autre.
Les organismes actuels descendent d'un petit nombre de groupes ; c'est vrai à tous les niveaux taxonomiques. Peu de chance de trouver des ancêtres dans les fossiles
Si l'on veut construire une classification qui suit l'évolution il faut considirer ce qu'il y a de plus stable 

* Système des cinq règnes (Whittaker 1969)
    * Champignons 
    * 3 règnes de pluricellulaires
    * Microorganismes : protistes = unicellulaires eucaryotes ; monères : procaryotes

* Carl Woese : 1990
    * Utilise la structure du ribosome pour classifier
    * 3 types de ribosomes
    * 3 domaines
    * Ces archées en 90 rassemblaient des méthanogènes ; acidophiles = organismes ancien ; primitifs (il y a 3,5 Mia mais pas d'organismes primitifs! les archées sont en vérité plus jeunes.
    * Chez les bactéries : pas de super groupe ; les bactéries sont très anciennes --> divergées ; mais tellement ancin --> on ne voit plus ces divergences

Si on tient compte d'un grand nombre de paramètres : voir arbre

## Quelques repères dans l'histoire des classifications biologiques

* Thomas cavalier smith 1942
chromiste : résultat de la symbiogénes de deux cellules eucaryotes dont les plastes vont se loger dans la lumière du réticululm

## Deux classification des procatyotes

### Classification de Bergey

Mycoplasme : bactéries sans paroi
Bergey : renouvelle constamment ses classifications
