- [Composition et synthèse d'une cellule](#composition-et-synth%C3%A8se-dune-cellule)
- [Croissance des microbes](#croissance-des-microbes)
    - [Qu'est ce que la croissance en microbiologie?](#quest-ce-que-la-croissance-en-microbiologie)
    - [Quel est l'intéret évlutif d'une croissance efficace?](#quel-est-lint%C3%A9ret-%C3%A9vlutif-dune-croissance-efficace)
    - [Quels milieux de culture?](#quels-milieux-de-culture)
        - [Contraintes](#contraintes)
        - [Description des milieux](#description-des-milieux)
    - [Croissance équilibrée](#croissance-%C3%A9quilibr%C3%A9e)
    - [Courbe de croissance](#courbe-de-croissance)
    - [Cultures pures en laboratoire?](#cultures-pures-en-laboratoire)
    - [Biofilms](#biofilms)
        - [Modèle de formation d'un biofilm](#mod%C3%A8le-de-formation-dun-biofilm)
        - [Hétérogénéité chimique dans un biofilm](#h%C3%A9t%C3%A9rog%C3%A9n%C3%A9it%C3%A9-chimique-dans-un-biofilm)
        - [La matrice de substances polymériques extracellulaires](#la-matrice-de-substances-polym%C3%A9riques-extracellulaires)
            - [Quelques rôles de biofilms](#quelques-r%C3%B4les-de-biofilms)
            - [Intéractions entre espèces dans un biofilm](#int%C3%A9ractions-entre-esp%C3%A8ces-dans-un-biofilm)
- [Métabolisme central et voies de biosynthèse](#m%C3%A9tabolisme-central-et-voies-de-biosynth%C3%A8se)
    - [Biosynthèse et nutriment](#biosynth%C3%A8se-et-nutriment)
    - [Exemple de deux milieux de culture](#exemple-de-deux-milieux-de-culture)
    - [Oxydo-réduction](#oxydo-r%C3%A9duction)
        - [Composés réduits ou oxydés?](#compos%C3%A9s-r%C3%A9duits-ou-oxyd%C3%A9s)
        - [Principales réactions rédox](#principales-r%C3%A9actions-r%C3%A9dox)
        - [Caractéristiques des voies de biosynthèse](#caract%C3%A9ristiques-des-voies-de-biosynth%C3%A8se)

# Composition et synthèse d'une cellule

* Le métabolisme des organismes unicellulaires est orienté vers la synthèse de biomasse
* Les réactions sont quasiment les mêmes ; la logique de fonctionnement est très différente
* Composition d'une cellule E.Coli
    * 70% d'eau 30% de matière sèce
    * 55 % de protéine
    * 2 mio de molécules par cellules
    * Lipoprotéine : 700 000 exemplaires par cellule --> maintient la membrane externe et la paroi de peptidoglycane ; 50 AA ; 3 AG fixés de façon covalente dans la mb externe
    * 20 % d'ARN (ARN ribosomaux) --> le nbre de ribosome par cell dépend de la vitesse de croissance
    s* Cytoplasme de bactérie : très encombré --> les molécules se touchent l'une l'autre
    * Composition macromoléculaire : proportion entre prot/ARN et ADN en fonction du milieu de culture et de la vitesse de croissance
        * Une souche qui pousse rapidement/ l'autre qui pousse lentement ; cellules de grand diamètre plus sombres (car plus épaisses)
            * Les cellules qu'on a fait poussé lentement sont les plus petite.

Plus une cellule pousse rapidement plus son volume est grand ; rapport surface/volument --> comment concilier les deux?

Si on s'intéresse à un seul organisme : pas de pb de surface/volume : taille très proche --> facteur qui n'a pas d'effet. Pousser vite : synthétiser des protéines rapidement --> pour augmenter la vitesse de synthèse on doit augementer le nombre de ribosomes par cellules. On augmente la taille de la cellule pour augmenter la quantité de ribosome. Pour pousser vite il faut plus de ribosome ; plus de place ; plus de volume.

ARNm : très instable chez les procaryotes --> Ils sont renouvelés très rapidement mais représentent seulement 1% de la masse
ADN : 3% de la biomasse 

Chromosome d'E.Coli : circulaire ; on a une origine de réplication --> elle va démarrer dans les deux sens : processus qui prend 40 min : Dans tous les cas il faut 40 min.

On commence une réplication toute les 20 minutes --->on peut faire durer une réplication 
Une cellule qui pousse vite présente plus d'ADN
Fréquence d'initiation de la réplication.

Transcription et traduction : les mécanismes sont couplés --> l'ADN va être transcris, l'ARN va être immédiatement traduit.

Toutes les protéines vont être produit au bon endroit ; pas de transport des protéines dans le cytoplasme.

Couplage dans le temps mais pas au même endroit --> en surface du nucléoide.
Synthétiser une cellule c'est principalement sythétiser des protéines. Synthèse des macromolécules vont tirer

Organisme phototrophe =/Chimiotrophe

# Croissance des microbes

Peu de cycles de vie complexes
Quelques cas de "développement":spore
Croissance continuelle (en conditions favorables...)
La cellule croit et se divise en deux fille identique (quand les cellules se divisent elles se multiplient

## Qu'est ce que la croissance en microbiologie?

En microbiologie on parle de croissance pour l'augmentation de la taille de la population c'est à dire augmentation du nombre d'organismes.

## Quel est l'intéret évlutif d'une croissance efficace?

Le taux de survie indiviuelle est très faible

* Le taux de survie individuelle très faible est compensé par l'obtention rapide de populations nombreuses
* Compétition pour le nutriments (leur urtilisation rapide donne un avantage par rapport aux voisins)

## Quels milieux de culture?

### Contraintes

* Contraintes pratiques :
    * VItesse d'apparition des colonies sur milieu solide
    * VItesse de croissance en milieu liquide
    * Faciilté de fabrication
    * coût

* Contraintes expérimentales
    * Composition chimique précise
    * Reproductibilité de cette Composition

### Description des milieux

* Milieu synthétique : composition chimique précise, reproductible et connue ; (milieu minimum (très peu de MO, fabrication à partir de Matire minérale) ou milieu complet (MO)
* Milieu complexe : quelques ingrédientes de composition chimique indéterminée (extrait de viandes, extrait de levure)
* Milieu sélectif : favorise la croissance du micro-organisme que l'on recherche ; inhibe la croissance des autres.
* Milieu différentiel : facilite la distinction entre les colonies du MO recherché et les autres qui se développent sur la même boîte de pétri
* Milieu d'enrichissement : milieu sélectif (liquide) favorable à la croissance d'un microbes donné.

## Croissance équilibrée

La croissance équilibrée
Durant une croissance équilibrée tous les constituants de la cellule s'accroissent dans les mm proportion
Au sein des cellules toutes les concentrations, toutes les vitesses sont constantes. Ccl : toutes les cellules sont identiques entre elles, et toutes les cellules restent identiques au cours du temps. (on a pas vu le contraire?)

Croissance exponentielle? Seule cas de croissance équilibré.
Lorque le nombre

## Courbe de croissance

Défintion de courbe de croissance :
 représentation en coordonnées semi logarithmiques de la biomasse en fonction du temps.

* Pendant un certain temps on a pas d'évolution de la biomasse --> phase de latence --> adaptation de Micor-organisme à son nouveau milieu. Croissance représentée par une droite (croissance exponentielle) : pendant cette phase de croissance se font toutes les expérience se font. La croissance s'arrête une fois que l'un des substrat est épuisé --> Phase stationnaire

* Trophophase : phase de nutrition (croissance exponentielle)
* Phase stationnaire :idiophase : phase particulière : certains vont fabriqués des antibiotiques ; des capsules (idio=particulier)

* Les facteurs essentiels de la croissance
    * La tempéraure
    * Le pH
    * La pression osmotique
    * La lumière

* Les facteurs chimiques
    * Sources des éléments
    * Donneurs d'électrons
    * Accepeurs d'éléctrons


* Différence entre le minimum et le maximum de croissance : aux alentours de 40°C 
* Différence entre la température macimal de croissance et optimal est très faible. 

* Si on augmente la température : permet de ralentir la vitesse de croissance des micro organismes.
    * Thermophile : ne survivent pas à des températures au delà de 72°C --> thermophile au de là de 72°C
    * Mesophiles : température optimum de croissance : 37°C

* Concentration en sel
    * Halotolérant (staphylocoque) survivent à une pression osmotique elevé
    * Halophile : nécéssite une bonne concentration osmotique ; n'aiment pas l'augmentation de la pression osmotique
    * Halobacterium salinarum (halophiles : préférent une concentration forte)

## Cultures pures en laboratoire?

Des débuts de la microbiologie à la fin du 20 ème siècle on travaillait avec des cultures pures.
Les micro-organismes étaient vus commes des cellules libres, indépendantes, nageuses ou flottantes. Ce que l'on appelle aujourd'hui l'état planctonique par opposition à l'état sessile (assis)
La plupart de nos connaissances en microbiologie concernent des micro-organismes planctoniques qui croissent de façon équilibrée, en milieu liquide

EN pratique on a un milieu de culture liquide, on mesure une moyenne dans la population. Une mesure de la moyenne ne reflète pas forcément la réalité.

On a longtemps crus que les individus d'une population clonale était tous identiques. On constate qu'il y a une très forte hétérogénéité des indivus chez une population clonale. Tous les individus présents ont le même génotype. On constate que pour un même génotype --> vont se développer des phénotypes différents. Probème de l'expression stochastique des génes (génes exprimés à très basse fréquence de façon aléatoire, qui peut provoquer des basculements ce qui peut créer des phénotypes différents à partir d'un génotype différents). Au fur et à mesure du temps : augmente la diversité des individus.

Ces cultures ne reflètent pas un écosystème microbien, il ne peut pas être étudié par extrapolation de cultures planctoniques ou de cultures monospécifiques. 

Dans un écosystème aquatique : 99,9 % des micro-organismes sont dans un biofilm.

**Biofilms**: population de micro-oranismes inclus dans une matrice adhrent les uns aux autres ou sur une surface, ou sur une interface.


## Biofilms

### Modèle de formation d'un biofilm

* Cellules au centre de la colonie : se multiplie pas ; voire meurent

### Hétérogénéité chimique dans un biofilm

* Voir diapo : Du coeur du biofilm aux extremités
* Chaque points des biofilms : condition particulière

### La matrice de substances polymériques extracellulaires 

* Matrice 
    *  constitue une substance polymérique extracellulaire (polysaccharide, protéines, ADN)
    *  Intéractions entre ces polyméres (toutes les intéractions entrent en jeu)

#### Quelques rôles de biofilms

* Infection dans le domaine médical
* VOir Dia
    * Schéma b représente un biofilm --> le macrophage ne va pas pouvoir phagocyter le biofilm 
* Forme de colonisation et de résistance très efficace
* Infection chronique du poumon par Pseudomonas aeruginosa biofilm de 20 um 
* Biofilms qui entourent les racines et recouvrent les feuilles. 
* Plusieurs modes de vie différent permet de diversifier 

#### Intéractions entre espèces dans un biofilm

* Echanges de métaboites
* Transfert génétique horizontal
* Agrégation
* Communications intraspécifiques 


# Métabolisme central et voies de biosynthèse

* Réaction de polymérisation  : tout le métabolisme est orienté par le fait que ces réactions de polymérisation sont irréversibles. Les briques élémentaires permettent de faire une polymérisation
* Part d'un métabolite précurseur (au nombre de 12) (=brique élémentaire)
* Toutes les voies de biosynthèse partent de métabolites précurseurs ; d'énergies etc (voir dia)
* Tous les intermédiaires des voies métaboliques sont chargées négativement
* La double couche de phospholipide est imperméable aux molécules chargées
* Le métabolisme existait avant les cellules (c'est ce qu'on appelle le métabolisme de surface) ; réactions qui se faisaient sans enzyme, dès qu'une réaction donné un produit non chargé le métabolisme était perduré alors que les intermédiaires chargés négativements restaient sur la surface.
* Le nom des enzymes décrivent la réaction que l'on peut doser facilement in vitro.

## Biosynthèse et nutriment

* Les capacités biosynthétiques et les exigences nutritionnelles sont complémentaires
* Plus nombreuses sont les voies de biosynhèse présentes dans un organisme moins nombreux sont les besoins nutritionneles

## Exemple de deux milieux de culture

* Escherichia coli : se contente d'un milieu minéral + un composé organique = glucose --> (milieu minimum)
* Streptococcus : incapable de synthétiser

## Oxydo-réduction
### Composés réduits ou oxydés?

* Un composé organique est dit "réduit": lorsqu'il est pauvre en oxygène
* ou riche en hydrogène
* ou riche en éléctron

Un composé orgainque est dit oxydé lorsqu'il est riche en oxygéne 
* ou riche en oxygène
* ou pauvre en hydrogène
* ou pauvre en éléctron

### Principales réactions rédox
* Oxydations dans les voies d'approvisionnement
SH2 + NAD+ --> S+ NADH + H+
* Réduction dans les voies de biosynthèse
M+NAH + H+ext --> NADPH + NAD+ + H+int
* Réaction de transhydrogénation
Nécéssite l'énergie de la pompe à proton
* La transhydrogénase est une enzyme membranaire utilisatrice du potentiel de protons.

### Caractéristiques des voies de biosynthèse
* Commencent toujours par un des 12 métabolite précursuer
* Finissent toujours par une brique élémentaire
* La brique élémentaire est généralement plus réduite que le métabolite précurseur (c'est le NAPH qui donne les éléctrons
* L'énergie consommée par les voies métaboliques correspondent à l'ATP
* La brique élémentaire est un effecteur allostérique de l'enzyme qui catalyse la première étape de chaque voie (phénomène de rétro-inhibition)