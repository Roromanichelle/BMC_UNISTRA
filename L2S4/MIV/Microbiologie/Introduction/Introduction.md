- [Introduction](#introduction)
    - [Quest-ce qu'un micro-organisme?](#quest-ce-quun-micro-organisme)
    - [Un micro-organisme est un organisme unicellulaire](#un-micro-organisme-est-un-organisme-unicellulaire)
    - [Classification de l'ensemble des micro-organismes](#classification-de-lensemble-des-micro-organismes)
    - [Pourquoi s'intéresser aux micro-organismes ?](#pourquoi-sint%C3%A9resser-aux-micro-organismes)
    - [Utilisation des micro-organismes](#utilisation-des-micro-organismes)
    - [Origine](#origine)
    - [Colonisation de toutes les niches](#colonisation-de-toutes-les-niches)
    - [Certains micro-organismes](#certains-micro-organismes)
    - [Procaryotes petits ? Ecaryotes grands?](#procaryotes-petits-ecaryotes-grands)
    - [Les archéobactéries ne sont jamais géantes](#les-arch%C3%A9obact%C3%A9ries-ne-sont-jamais-g%C3%A9antes)
    - [Certains eucaryotes sont très petis](#certains-eucaryotes-sont-tr%C3%A8s-petis)
    - [La cryotomographie éléctronique](#la-cryotomographie-%C3%A9l%C3%A9ctronique)
    - [Plus un organisme est petit plus son rapport surface/ volume est grand](#plus-un-organisme-est-petit-plus-son-rapport-surface-volume-est-grand)
    - [Taille des micro-organsimes](#taille-des-micro-organsimes)
    - [Les micro-organismes sont très nombreux](#les-micro-organismes-sont-tr%C3%A8s-nombreux)
    - [Il y a dans la biosphère ?](#il-y-a-dans-la-biosph%C3%A8re)
    - [Croissance très rapide](#croissance-tr%C3%A8s-rapide)
    - [Comment croître rapidement?](#comment-cro%C3%AEtre-rapidement)
    - [Croissance très lente](#croissance-tr%C3%A8s-lente)
    - [Les micro-organismes (résumé)](#les-micro-organismes-r%C3%A9sum%C3%A9)

# Introduction

## Quest-ce qu'un micro-organisme?

 * Organisme de petite taille : que l'on ne voit pas
 * Unicellulaire


## Un micro-organisme est un organisme unicellulaire

 * Une seule cellule? pas toujours   
 * Pas toujours une petite taille
 * Une cellule? donc pas un virus
        --> Un virus n'est pas un organisme unicellulaire (car pas autonome)


## Classification de l'ensemble des micro-organismes

 * Classement avec ARNr: on a 3 groupes, eucaryote, archées et batéries
 * Haeckel --> Protiste: tous les organismes unicellulaires qui ne sont ni des plantes ni des animaux
 * 1925 Chaton --> procaryotes et eucaryotes
 * whittaker EN 1969 --> Protiste: ni animaux, ni champignons, ni plantes
 * Woese: 3 Grands types de ribosome --> Bactéries, archées et eucaryotes (absurde du point de vue biologique, il faut s'intéresser aux organismes entiers)
 * Les Chromistes: organismes qui ont leurs plastes dans la lumière du réticulum --> 6 règnes; cavalier Smith (pas seulement la séquence)
    
Ed. Chatton
Déjà avant 1925 on avait une classification qui distinguait les procaryotes et les eucaryotes 

## Pourquoi s'intéresser aux micro-organismes ?

 * À l'origine de tous les organismes vivants
 * Beaucoup plus divers que les animaux
 * Extrêmement abondants
 * Ils colonisent tous les milieux, il suffit qu'il y ait un peu d'eau liquide
 * Ils font tourner tous les cycles biogéochimiques
 * Construction et destruction des roches
 * Grande influence sur le climat
 * Symbiose aec les plantes, les animaux et entre eux
 * Joue avec le comportement des plantes et des animaux (écosystème intestinal)
 * Quelques uns sont pathogènes

## Utilisation des micro-organismes 

 * Amélioration de l'alimentation (production, transformation)
    --> conserver des aliments à long terme: ex yaourt, fromages etc.., améliore le goût
 * synthèse industrielle de nombreux composés (début du 20ème siècle), on a commencé à modifier de microorganismes
        -lactate, éthanol
        -antibiotiques
        -acides aminés, vitamines
        -protéines
 * fonctionnement des stations d'épuration
 * "tubes à essai" dans les laboratoires de recherche
 * "bio-remédiation" des zones polluées
 * armes biologiques, bio-terrorisme

## Origine 

 * 3,7 mia d'années

## Colonisation de toutes les niches 

 * déserts froids
 * déserts chauds
 * Sources d'eau à plus de 100°c
 * milieux très acides ou très alcalins
 * milieux très salés

## Certains micro-organismes 

 * peuvent vivre dans un milieu purement minéral, c'est à dire indépendamment de toute autre forme de vie (producteurs primaires primaires)
 * Vie dans quelques décénies..?

## Procaryotes petits ? Ecaryotes grands?

 * Certaines bactéries sont très grandes: ex thiomargarita; epulopiscium
 * Thiomargarita: nitrate, couche d'acide nitrite va recouvrir la coquille de provanna laevis (gastéropode--> mollusque
 * L'escargot va se retourner dans la vase --> comportement de l'animal pilloté par la symbiose avec un microorganisme
 * Epulopiscium, bactérie vivipare en division, à côté une paramécie (très grosse cellule eucaryote)

## Les archéobactéries ne sont jamais géantes 

 * Archéobactéries filamenteuses avec ectosymbiontes bactériens, recouvrement par une bactérie (_Giganthauma karukerense_)

## Certains eucaryotes sont très petis 

 * eostrococcus dory <= 1um de diamètre
 * pico-eucaryotes <= 2,3um 

## La cryotomographie éléctronique 

 * Congeler sans que des cristaux se forment, donc pas de modification de structure
 * Reconstitution du modèle numérique de l'objet en fonction de la trajectoire des éléctrons

## Plus un organisme est petit plus son rapport surface/ volume est grand 

 * nombre de um2 de membrane pour nourir 1 um3 de cytoplasme
 * Si la cellule est petite elle dispose de plus de membrane pour faire entrer des nutriments dans le cytoplasme
 * Dès que les conditions sont favorables les microorganismes vont se multiplier le plus possible, plus la population est grande, plus grande est la probabilité d'avoir des survivants dans des conditions défavorables.

--> Diversité de tailles et de formes chez les procaryotes
Amoeba proteus: gros protiste

## Taille des micro-organsimes 

 * Procaryotes: entre 1 et 10 um
 * Eucaryotes: entre 10 et 100 um

## Les micro-organismes sont très nombreux 

 * 10⁶ micro-organismes par ml dans les écosystèmes aquatiques
 * 10⁹ micro-organisme par gramme de sol
 

## Il y a dans la biosphère ?

 * La biomasse microbienne est supérieure à la biomasse végétale

## Croissance très rapide 

 * Masse d'une cellule d'escherichia coli = 10⁻¹² gramme ; temps de génération = 20 minutes ; en partant d'une seule cellule on obtiendrait en 48 heures:
 * 2,23.10⁴³ cellules
 * 2,23.10²⁵ tonnes de biomasse
(la planète Terre pèse 6.10²¹)
 * Pourquoi cela ne se produit pas?
    * Manque de nutriments 
    * Accumulation de déchets

## Comment croître rapidement?

 * Il faut un métabolisme très rapide 
    * grâce à une utilisation très rapide des nutriments
    * que permet un rapport surface/volume très grand

Un gramme de bactéries en croissance consomme les nutriments 100 à 1000x plus rapidement que les cellules animales.

## Croissance très lente 

 * De nombreux microorganismes croissent très lentements et ont de grandes capacités à survivre très longtemps
 * Dans tous les déserts
 * Dans les milieux très froids
 * Au sein des roches (temps de génération de plusieus mois ou années...)
 * Mycobacterium tuberculosis (vie dans le cytoplasme des cellules eucaryotes, stratégie pour résister aux antibiotiques, se cache et croissance très lente lui confère un grand avantage)


## Les micro-organismes (résumé) 

 * Les micro-organismes sont très anciens
 * Ils sont petits (sauf rares exceptions)
 * Ils sont partout (où il y a un peu d'eau)
 * Ils sont nombreux (en individu et espèces)
 * Certains croissent très rapidement.
 * Certains vivent dans des conditions “extrêmes”.
 * Ils s’adaptent aux fluctuations de leur environnement.
 * Ils sont indispensables aux autres organismes.
 * Ils jouent un rôle fondamental dans le fonctionnement de la planète.
