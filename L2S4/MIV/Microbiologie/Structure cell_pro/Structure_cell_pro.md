
- [Structure des cellules procaryotes](#structure-des-cellules-procaryotes)
    - [L'enveloppe](#lenveloppe)
        - [Généralités](#g%C3%A9n%C3%A9ralit%C3%A9s)
        - [La membrane plasmique](#la-membrane-plasmique)
        - [La capsule](#la-capsule)
        - [L'enveloppe des bactéries Gram-négatives](#lenveloppe-des-bact%C3%A9ries-gram-n%C3%A9gatives)
        - [L'enveloppe des bactéries Gram-positives](#lenveloppe-des-bact%C3%A9ries-gram-positives)
        - [L'enveloppe des archéobactéries](#lenveloppe-des-arch%C3%A9obact%C3%A9ries)
        - [La couche S](#la-couche-s)
    - [Les appendices de surface](#les-appendices-de-surface)
        - [Les flagelles](#les-flagelles)
        - [Les pili ou fimbriae](#les-pili-ou-fimbriae)

# Structure des cellules procaryotes

## L'enveloppe

### Généralités 

 * L'enveloppe: ensemble des couches qui séparent le cytoplasme de l'environnement
    --> Voir portrait escherichia coli (nucléoide/cytoplasme/enveloppe)
 * L'enveloppe est consituée de la membrane plasmique, la paroi et la capsule (enveloppe externe à la paroi)
 * La membrane plasmique des procaryotes est très riche en protéines (70% --> maximum de protéines possible dans la membrane).

### La membrane plasmique 

 * C'est la véritable frontière entre l'intérieur et l'extérieur de l'organisme
 * Fonctions diverses :
     * barrière osmotique
     * Transport des nuriments et des déchets
     * siège potentiel de protons
     * synthèse des protéines membranaires et externes
     * sécrétion des protéines membranaires et externes
     * synthèse des lipides
     * synthèse de la paroi
     * phototrophie (pour les bactéries photosynthétiques)
     * réplication et ségrégation du chromosome
 * Lipides membranaires constitués de bareaux hydrophobes qui ne se dissocient pas à très hautes températures (chaînes isopréniques)
 * Liaisons éther plus stables à haute température que liaisons ester (premières archéobactéries dans des milieux très chauds et très acides)

### La capsule 

 * Polysacharides de grand poids moléculaire, parfois polymères d'acides aminés (couche la plus externe)
 * Protection contre la dessiccation
 * Adhésion aux surfaces
 * Défense contre la phagocytose
 * Les pathogènes qui possèdent une capsule sont beaucoup plus virulents
 * Maintient l'eau


### L'enveloppe des bactéries Gram-négatives 

 * Le périplasme: compartiment entre la membrane externe et la membrane interne, dans ce périplasme on trouve la paroi; paroi constituée de Muréine=peptidoglycane --> voir structure d'une couche de peptidoglycane (muréine)
 * Feuillet externe de la membrane externe: Lipopolysaccharide
 * Rien ne traverse la membrane des bactéries Gram-négatives
 * Sacculus : peptidoglycane vidé
 * Réplication du sacculus (ou du peptidoglycane) 
 * On remplace un brin de glycane sur deux, chacun de ces brins de glycane détruit va être remplacé par 3 brins nouveaux
 * On a lors de la division cellulaire des bactéries qui possèdent des parties plus ou moins agées, toutes les cellules ne sont pas identiques!
 * D'autres  types de divisions cellulaires en fonction des types dr procaryotes

### L'enveloppe des bactéries Gram-positives 

 * Les bactéries Gram-positives sont sous forte pression, (13/14 voire 15 atmosphères alors que gram neg: 3/4 atmosphères)
 * Composition et structure du sacculus de peptidoglycane
 * Membrane très épaisse qui permet d'avoir une pression importante
 * Pas de membrane externe (donc plus sensible aux antibiotiques)

* Coloration de gramm
    * Première coloration qui colore tous les organismes
    * Décoloration
    * Puis recolaration des cellules qui ont été décolorés avev un colorant moins puissant (pour pouvoir les voir)
    * GRamm positive : violoet / Gramm négative rose

### L'enveloppe des archéobactéries

* Très grande diversité d'enveloppe
    * Dans la plupart des cas membrance avec couche S et pseudomuréine

### La couche S
* Couche critallise de surface
    * Réseau cristallin bidimensiinnel
    * cristal de la protéine S
    * Membrane sans lipide (purement protéique)
    * Mode de cristallisation autour de la cellule très différente selon les espèces
    * A des endroits variés de l'enveloppe???? Fonction??? Plus courant chez les archées?

## Les appendices de surface

### Les flagelles

* Les flagelles sont de structure très longues, de forme hélicoidale, rotatifs, rôle dans les déplacements
    * Constitué de trois parties : 
        * filament constitué de polymère de flagelline (tout flagelle est creux d'une extrémité à l'autre (20 nm de diamètre ; 10 um de longuer)
        * Le crochet ; jonction filament : permet de transmettre le mouvement
        * Le corps basale: deux fonction --> au moment de la synthèse ce corps basale sert de système d'excrétion ; extrémité distale ) ; syst de sécrétion pendant la formation du flagelle ; le corpas basale devient un moteur 
            * Stator -> partie fixe 
            * Partie mobile
            * Passage des ions fournis l'energie nécéssaire pour faire tourner l'hélice
        * Énergie du flagelle : force proton motrice
>
 * Formation du flagelle
    * première synthèse protéique dans la membrane interne puis se poursuit éléments du cytoplasme qui vont former une structure de forme hélocoidale de 1 à 2 microns
    * 10 à 20 flagelles chez E.Coli avec un corps indépendant mais mouvement coordonné
    * Flagelle d'eucaryote ne peut pas tourner, peut seulement changer de forme (microtubules permet les ondulitions)
    * Deux types de flagelles procaryotes :
        * Flagelle de Halobacterium (archéobactérie) --> apparues dans un milieu très chauds/acide ; n'ont pas pu construire de flagelle puis ont reconstitués un flagelles à partir de pi
    * Pilus s'allonge au niveau de IM (voir dia) ; peut changer de longueur avec polymérisation et dépolarisation ; hydrolyse de l'ATP
    * Flagelle d'archeobactérie : très différent --> dérive d'un pili (on suppose).


### Les pili ou fimbriae 

* Plus fin, plus courts et rôles très divers zvec une fonction commune --> il y a des ions ; une adésine qui va reconnaître une structure qui va permettre au pilus de s'associer à telle ou telle structure
    * Grande diversité de pili ; pas de classification 
    * Rôle très divers des pili
        * Adhésion
            * à des surfaces 
            * Des bactéries entre elles
            * Aux cellules de l'hôte
        * Mobilité (certaines bactéries marchent sur leur pilis)
        * Entrée de l'ADN :NON! pas de canal de conjugaison
        * TRansfert de protéines, d'électrons
        

    * Quel est le rôle des pili F?
        *  Raprochement : polymérisation pour rapprocher une cellule et dépolymérisation pour rappocher 
        * Conjugaison : échange d'ADN entre deux bactéries par contact très étroits
            * Organe sesoriel pour l'explration d'un grand bolume
            * IL est capable de faire tous les mvt possibles (elongation/retraction/rotation)
            * Reconnaître et transmettre l'ADN lors de la conjugaison
        * Exemple de contact
            * B : cellules proches
            * C : cellules en conjugaison (on parle de canal de conjugaison --> canal de conjugaison n'existe pas ; pas d'échange de cytoplasme ; jonctions ) --> fusion des membranes externes 
            * Canal de conjugaison ? flagelles cassés

Pili des Gram-positive : pas de rôle bien précis, paroi plus épaisse 
La conjugaison se fait par rapprochement très proche, syst de transfert : similaire : moteur qui fait passer l'ADN par hydrlyse de l'ATP
