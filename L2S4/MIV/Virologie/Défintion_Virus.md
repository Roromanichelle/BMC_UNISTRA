# II Qu'est ce qu'un virus?
- [II Qu'est ce qu'un virus?](#ii-quest-ce-quun-virus)
    - [Taille variable](#taille-variable)
    - [Distribution](#distribution)
    - [Cycle viral : généralités](#cycle-viral-g%C3%A9n%C3%A9ralit%C3%A9s)
    - [Définition de 1953 par LWOFF](#d%C3%A9finition-de-1953-par-lwoff)
    - [Vers une définition simplifiée](#vers-une-d%C3%A9finition-simplifi%C3%A9e)
    - [Spécificités](#sp%C3%A9cificit%C3%A9s)
    - [Critères taxonomique](#crit%C3%A8res-taxonomique)
    - [Agents subviraux](#agents-subviraux)
        - [Prions](#prions)
        - [Viroides](#viroides)
    
## Taille variable
 
 * Pandovirus, se fait prendre pour une bactérie pour infecter l'amibe qui se nourrit de bactérie

## Distribution
 * 10 x plus de virus que de bactéries ou d'archées

## Cycle viral : généralités 

 * Décapsulation mis à nu de l'Acide nucléique
 * 1. Phase d'éclipse : on initie l'infection, lorqu'on commence à voir quelque chose dqns le cytoplqsme ou dans le noyau --> correspond à la fin de la phase d'éclipse
 * 2. Néoformation: fin lorsqu'on a libération de particules virales
 * 3. Latence

## Définition de 1953 par LWOFF

 * Définition contestable car certains points ne sont plus vrais
 * **Entité nucléoprotéique** (donc ARN ou ADN) renfermant un seul type d'acide nucléique (ADN ou ARN) qui constitue le génome
 * Le virus se reproduit uniquement à partir de son acide nucléique (mais parfois il est aidé par autre chose)
 * Incapable de croitre en taille (point ambigu par rapport à certains virus qui ont été découverts et de **subir des divisions binaires** pour se multiplier. 
 * Il se multiplie dans la cellule uniquement par synthèse et assemblage des constituants.
 * Le virus ne renferme aucune information génétique spécifique des enzymes impliquées dans la production d'énergie. Il ne peut pas développer par lui-même l'énergie nécessaire pour la synthèse de ses constituants.(Certains virus augmentent la production en ATP de certaines cellules photosynthétiques)
 * Le virus ne se multiplie qu'à l'intérieur des cellules vivantes. IL est incapable de se développer sur milieu inerte même si on lui fournit des éléments nutritifs

## Vers une définition simplifiée

 * Le virus est une entité nucléoprotéique renfermant un seul type d'acide nucléique
 * Le virus est incapable de subir des divisions binaires. Il se multiplie dans la cellule uniquement par auto-assemblage de ses constituants
 * UN virus est dépendant de l'énergie et de l'appareil de traduction de la cellule hôte.

## Spécificités

 * Un virus eucaryote n'infectera que des eucaryotes, un bactériophage n'infectera que des bactéries, un virus d'archée n'infectera que les archées.
 * 1) Spécificité lors de l'entrée
 * 2) Spécificité pour la machinerie de transcription/traduction

## Critères taxonomique

 * 1) La nature du génome

    * ADN / ARN ?
    * Double brin ; simple brin?
    * ARN capable de diriger la synthèse protéique avec système de traduction --> (ssRNA+) ex :virus mosaique du tabac
    * ARN incapable de diriger la synthèse protéique avec syst§me de traduction --> (ssRNA-) ex : ebola
        * Lorsqu'il initie l'infection, une enzyme virale va diriger la synthèse d'ARN messager pour permettre l'expression du protéome viral.
    * ARN double brin : l'enzyme amène avec lui les enzymes nécéssaires
>

 * 2) Stratégie de multiplicaiton du virus
    * 6 et 7 utilisent la reverse transcription, les deux utilisent l'autre A.nucléiique comme intermédiare de la réplication
    * eucaryotes: 3' poly A permet d'initier la traduction et dirige la traduction 
    * procatyotes : 5' triphosphate et en 3' un hydroxile
    * Exemples : (voir diapo) : 
        * Virus de classe 4 directement codant, il faut pouvoir les copier, il faut une matrice. ARN répliqué en ARN "double brin" ; le brin viral complémentaire a servir de matrices pour produire de nouveau brin 
        * Virus de classe 5 (ebola ; oreillon ; rougeole) : ne sont pas codant, ne peuvent pas diriger la synthèse de protéine ; ARN messager 
>
 * 3) Morphologie
    * Nucléocapside iosaédrique (nu ou enveloppé) ; enveloppé --> glycoprotéine (garde toujours la structure)
        * sphérique 
    * Hélicoidale (peuvent perdre leur structure) 
        * Rigide : 
        * Flexueux : font des vagues ; symétrie hélicoidale enveloppée ; il existe des formes pléomorphes (en fonction de la préaparation son apparence en microscopie va changer).

Pas encore trouvé de virus à ARN double brin à polarité négative 

>
ssRNA+ = ARNm
>

## Agents subviraux

### Prions

Longuement considéré comme un agent viral car ultrafiltrant et responsable de pathologie (Creutzfeld-Jakob)
On retrouve ces prions chez les mammifères et chez les bactéries.
Les agents transmissibles n'étaient pas des virus car ils résistaient à de très forte radiations.
Unité génomique virale : casse car Acide nucléique --> ccl : pas d'acide nucléique
C'est uniquement une protéine qui est reponsable de la pathogénicité du prion
Il dénome le prion (protease resistant protein (PRP, on exprime tous PRP mais forme PRP cellulaire mais également forme pathologique ), cette protéine va avoir un effet de la modification de la protéine sauvage. --> devient hautement résistant et forme des structures fibrileuses resistantes à tout (notamment aux protéases). Maladie à Prions : incurables

Hormone de croissance : dans l'hypophyse ; on récupère l'hypophyse sur les cadavres --> maladie à Prions

Virino : acide nucléique suffisament petit pour que les doses de radiation soient sans effet --> hypothèse mise de côté

### Viroides

Effets: dépend du viroïde (plus de 1700 connus)

Deux types de viroides: nucléaires et chloroplastiques : leur transmission est en général mécanique
    --> Peut également être transporté par des virus
