# III Structures virales et génomes

## Composition et structure

### Capside

* Rôle de la capsule
pH alcalin sur un génome ARN : découpe
sUR ADN --> effet hyperchrome : rupture des liaisons hydrogène 
pH acide : Dépurination acide : on enlève des bases suite aux fortes acidifications
--> Protection contre le milieu extérieur nécéssaire

* Reconnaissance spécifique du génome par les protéines de capsides ; reconnaître l'AN pour le protéger, pour former de nouvel particules virales.
But : s'attacher à une nouvelle cellule cible

**Auto assemblage** : elles cherchent à trouver qqc de stable. Se fait par intermédiaire de protéines d'échaffaudage.
Protéines d'échaffaudage ou génome chaperon. Le génome va servir d'échaffaudage pour produire la particule virale. 
Liaisons de type ioniques ; hydrogènes ; hydrophobes --> van der walls

Acide nucléique recouvert de capside --> Aucun contact avec le milieu extérieur.

Protection totale de l'AN par les sous unités (pas en contact avec le milieu extérieur)

Benyvirus 12 à 13 kb : ADN fragmenté
closterovirus : helicoidal mais pas rigide, les intéractions entre les sous unités sont plus faibles
Orthomyxoviridae : envelopé, structure helicoidal (pléiomorphe)
Particularité des capsides helicoidales : Rhabdovirus --> le virion a une forme rigide (protéines de matrice confère la structure au virus helicoïdal)
Si on utilise un détergent pour casser les membranes --> voir dia

Structure jelly roll est commune à de nombreux virus à capside icodaédrique
3 sous unités pour avoir un centre de symétrie (les protéines ne sont jamais symétriques) --> 60 sous unités par face.

### Géométrie 

Capsomères : hexamères + pentamères

* T4 : Tête icosahédrique allongée : une rangée de capsomères supplémentaire. Tête et queue non contractile (symétrie : 7). La queue est de type hélicoïdal (pas organisé autour de l'A.nucléique). Queue contractile --> conduit à l'intéraction entre les sturctures des fibres du phage. Les fibres codales (représenté sous forme de patees). Phage ; les fibres sont sur la queue et repliées sur la tête --> sutructure capable de rouler. Plaque codale : sous l(action mécanique des fibres--> on va avoir un redressement de la pate codale. 12 SU par tour, peut faire pénétrer le manchon dans le cytoplasme bactérien et envoit de l'ADN.

* Dans le cas de Caudovirales : T7
ADN pas complétement envoyé : il plonge un morceau d'ADN : ce qui mort --> ADN polymérase ADN dépendant et tsre l'ADN en le lysant.

### Virus Pléomorphe : hélicoïdal envolopé pour la plupart

* Dans la morphologie (voir taxonomie) : Peplos --> membrane qui possède un rôle de protection.
On isole la nucléocapside (3 fonctions de la nucléocapsides) ; si on l(enveloppe, on ne peut plus assurer ces trois fonctions. Protéines transmembranaires ; le virus va l'usurper à la cellule hôte. Ces protéines qui vont réassurer la reconnaissance des structures de la cellule hôte sont d'origine virale. Ces protines intéragissent les unes avec les autres. Intéractions entre même protéines ou protéines différentes. Spicule : petites antennes qui assurent l'attachement. Le peplos résulte de la membrane plasmique ou de celle des organites. Fonction : Si on forme un virion enveloppé, cela sous entend qu'on a une reconnaissance des nucléocapsides par les glucoprotéines soit directiment, soit indiretement pour avoir un enveloppement spécifique viral ou de rien d'autre.

* Critère taxonomiques ? Quel membrane a été intégré? On regarde la composition en lipide.
Site d'acquisition multiple et font partis des critéres de classification.

* Un virus icosaédrique va conserver sa forme en présence ou en absence d'enveloppe

* Les virus hélicoidaux enveloppés ont tous une structure hélicoidale flexueuse. Ces virus vont présenter une structure pléomorphe : on va avoir une grand nombre de structure différente. Sauf exeption : le virus de la rage -->> présente une structure en forme d'obus : inérant aux protéines de matrice qui stabilisent la structure de façon extrême. 

### Glycoprotéine

* Enchainement de sucres liés à une protéine. 
* O glycosylation et N glycosylation
* N glycosylation : la protéine va être adressée --> elle doit se retrouver dans le reticulum de l'appareil de golgi.
    * Premiers A.nucléiques qui vont codés un peptides signal qui va se retrouver clivé dans le réticulum
    * N terminal de la protéine et le C terminal reste traîné dans le cytoplasme. Le N terminal va se retrouvé à l'extérieur et le C terminal est interne (La lumière du réticulum endoplasmique correspond à l'extérieur de la cellule). 
* Reconnaissances spécifiques entre les protéines de nucléocapsides et les protéines du C terminal.
* Regroupement des segements génomiques : certains génomes ont leur A.Nucléiques répartis sur plusiseurs segments. Pour que le virion soit virus il doit posséder tous les segments. L'enveloppe va permettre de regrouper les segments.
* La reconnaissance se fait via des intéractions watson crickss

### Bourgeonnement et libération

* Après bourgeonnement il faut une maturation --> pas directement fonctionnelle : elle n'est pas directement capable de reconnaître les récepteurs (ou moins de récepteurs présentés par la cellule hôte (verouillage)) ; permet la dissémination des particules virales.
* Molécule d'échaffaudage, car parfois l'acide nucléique dirige la formation de la capside.

## Fonction
### Fonction de la nucleopside

* Reconnaissance de la cellule cible
* Confère la forme du virion
* Protège e génome viral
* Fixation au récepteur cellulaire
* Rétention par le vecteur

### Fonction de l'Enveloppe

* Les glycoprotéines de surface vont assurer la reconnaissance de la cellule infectée et éventuellement du vecteur
* L'enveloppe peut assurer une protection suplémentaire
* Les glycoprotéines de l'enveloppe vont permettre l'entrée soit de la nucléocapside, soit du virus dans la cellule.
