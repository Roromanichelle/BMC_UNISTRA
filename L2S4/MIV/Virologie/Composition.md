* Pas de virus à ARN circulaire
* Différence entre ADN et ARN --> ribose / désoxyribose : L'ADN peut contenir de l'uracile!

* Les virus modifient la structure de l'ARN
* Système de défense bactérien : 
* Enzyme de restriction : reconnaîte des sites et induit un clivage. Elle est couplée à une enzyme de modification GAATTC : 6 paires de bases ; sur un génome de 5 Mb ; sur le génome d'E.Coli --> on a des sites ecoR1. L'enzyme reconnait le site et coupe. Si la bactérie produit l'enzyme pour dégrader son ADN : 
* site de restriction : modifié (méthylation) qui empêche l'enzyme de couper. Si on a une modification --> l'enzyme ne fonctionne plus (si on met de l'uracile à la place d'une thymine)

Le virus est incapable de synthétiser son propre génome de façon autonome :
Le virus utilise ce qui est disponible dans la cellule pour récupérer les briques et snthétiser son propre génome. Propre nucléase pour dégrader celui de l'hôte. Il protège son propre génome. Le virus utilise la machinerie de la cellule. EN même temps qu'il injecte son ADN il injecte une protéine qui va modifier l'ADN polymérase ADN dépendant qui permettra d'exprimerl le génome virale --> synthèse exclusive du génome virale.
 
Le virus est incapable de synthétiser des protéines
--> Pas de machinerie traductionnelle
* Protéines soit reprogrammées soit impliquées dans la réplication. Point commun : **absence de machinerie traductionnelle**

# Virus dont le génome est constitué d'un ARNm directement codant 

## Chez les procaryotes 

* CHez les procaryotes : les messagers sont polycistroniques : 1 gène --> 1 ARNm --> plusieurs ORF : recrutement des ribosomes s ( par la séquence chain d'algarno ou RBS (ribosome binding site))
    *  Cette séquence permet le recrutement la petite sous unité ribosomique en amont du codon d'initiation AUG pour positionner cette sous unité au voisinage du codon d'initation qui va permettre de recruter la grande sous unité pour former le ribosome 70S --> transcription jusqu'au codon stop. 
    *  Messager : possède un 3' hydroxyle et 5' : triphosphate
* Chez les eucaryotes : 
    * ARNm monocistroniques : produits une seule protéine  
    * Possède une coiffe en 5' (étape nucléaire) = 5 méthyl triphosphate ; on a le cistron ; et une queue poly A (étape nucléaire également)

* Classe IV de baltimore : (chez pro et euc.) 
    * Virus de type icosahédrique non enveloppé
    * Une protéine structurale
    * Une protéine impliquée dans la réplication

    * Probeta: ARNm polarité positif : directement positif (apprendre l'ordre des gènes)
        * Un cistron code pour une protéine A : protéine structurale incorporé dans la particule virale --> permet de se fixer au f pili (uniquement pili mâle d'E.coli) et injection de l'ARN à travers le conduit du pili jusque dans le cytoplasme via un pili. Beaucoup de structure secondaire vont se retrouver dans le cytoplasme (fer à cheval rouge --> Séquence RBS) DU fait de la structure de l'ARN --> Appariement de séquences RBS avec une autre partie de l'ARN : donc la seule séquence accessible et celle de l'ORF codant pour la protéine de capside (en amont de l'ORF cp) --> va recruter et forme 70S --> se positionne au niveau du codon d'initiation et traduction.  Les intéractions à longue distance qui bloque la séquence vont être ouvert --> le passage du ribosome va rendre accessible aux séquences bloquées. ON commence à traduire.
        * Reconnaissance de l'ARN génomique (ARN polymérase ARN dépendant : synthèse d'un ARN viral complémentaire : ce brin complémentaire va permettre de faire de nouveaux brins d'ARN génomique qui vont pêtre reconnus par le ribosome pour produire des nouvelles particules virales) Si suffisament : si la protéine de capside s'accumule suffisament va bloquer la séquence RBS donc bloquage de la traduction. 
        * Protéine A : synthétisée que lors de la néosynthèse d'un nouveau brin
            * Séquence Chain d'argarno interagit avec l'ARN ; la séquence de la réplicase n'est pas accessible. La seule accessible est celle codant pour la 
        * Codon stop : incorporation d'ARNt suppresseur ARNt amino acylé : recruté lors de la rencontre du ribosome avec une structure particulière. fréquence : Suppression de codon stop : 1 à 10 % dépend de la quantité d'ARNt. 
        * Deux types synthétisés : 1 à 10 % de protéines de translecture
        
    * La réplication : la réplicase lorsqu'elle est produite va s'associer à l'extrémité 3'; recopie l'ARN génomique en anti génomique sans synthétiser le A au départ, il est ajouté à la fin. Ce A va protéger le protéine virale. (La séquence pour la protéine A est masquée dans un premier temps).  Le brin déplacé en among de la protéine A. La traduction est couplée à la transcription. Cette protéine A : reconnaissance du pili ; asembly --> car clé de voute de l'encapsidation (finalise la morphogénése de la noculéocapside) ; bloque la synthése de peptidoglycane --> cette protéine va conduite à la lise de la bactérie.  ccl : 10⁴ vont être produits. L'ARN antigénomique sert de matrice pour produire de nouveaux ARN génomiques. 

## Chez les Eucaryotes 

* Chez les eucaryotes (plantes) PVX
    * 3 gènes : 3 cistrons qui consistent à faire passer le génome à travers les plasmodesmes
    * Chez les procaryotes en général : polycistroniques
    * Chez les eucaryotes : monocystroniques (que poly A en 3' et coiffe en 5'). C'est un ARN messager ; arrive dans une cellule : libère un ARN viral et recrute la machinerie traductionnelle. Dirige la production de sa propre polymérase ; ensuite la polymérase s'associe avec des facteurs cellulaires et va reconnaitre l'extremité 3' ce qui va initier la synthèse d'ARN viral complémentaire. Seul le cistron 5' proximal est traduit. Stratégie : consiste à reconnaître des séquences internes à l'ARN polymérase pour synthétiser un ARN viral plus court en démarrant sur le brin complémentaire : donc synthèse d'ARN colinéaire à l'extremité 3' du génome. L'initiation interne de la transcription va permettre de synthétiser des ARN qui vont posséder l'ORF 2 et "/4
    * L'ARN viral possède une coiffe : pour répliquer il faudrait de la méthyl transférase (donc dans le noyau!) ; la polymérase possède une activité polymérase ARN dépendante --> induit l'ajout de la coiffe sur l'ARN sub génomique.

* Cas du VMT OU TMV

Pas de poly A : mime un ARN de transfert --> amino acylé par la machinerie cellulaire (signal de reconnaissance pour synthétiser le brin complémentaire). 
Cette structure va être reconnue pour initier la synthèse du brin complémenaire. Un ARN peut être traduit plusieurs fois. Certains ribosomes vont récupérer un ARNt suppresseur. --> mécanisme de translecture
Quantité d'ARNt dans la cellule : dicte la suppression  de
MT - Helicase : polymérase ADN dépendante (RpRd) ; Methyl transférase ; helicase
ARN subgénomique : ne sont pas répliqués dans la plupart des cas

* Cas du poliovirus

Pas de coiffe en 5' mais poly ; puis protéine d'origine virale fixé de façon covalente à l'extrimité du génome. V
Monocistronique : une seule ORF ; le ribosome va initier jusqu'au codon stop (7000 nucléotides)/3 --> polypetides d'environ 2000 AA. IRES : internal ribosome entry site : positionne à l'endroit pour initier la transcription. polyprotéine : à 800 nucléotides de l'extremité.
Lors de la synthèse de la polyprotéine : 2A et 3C sont des protéases (des endopeptidases)--> coupe. prot 2A : active au sein même de la polyprotéine ; cette prot 2A va se struturer et donner une prot fonctionnelle dans son sain (va pouvoir couper la chaîne naissante). Début de la maturation protéique. 3D : ARN polymérase ARN dépendante va s'associer avec des facteurs cellulaires pour reconnaître l'extrémité 3'. Vpg : est impliqué dans la réplication : elle sert d'amorce à la synthèse. 
180 évenements de traduction pour synthétiser une nucléocapside : stratégie virale --> protéine 2A : est une protéase. Le virus va réorienter la machinerie traductionnelle uniquement pour lui même.

* Autres exemples :

* Génome ARN(+) segmenté (Beet necrotic yellox vein virus)
    * = Mécanisme de translecture : suppression de codon stop
    * ARN Subgénomique : synthèse des brins complémentaires 

* Génome ARN (+) subgénomique et polyprotéines : Alphavirus
    * POly protéine forme réplicase puis suynthèse du brin viral complémentaire pour former de nouveaux ARN subgénomiques directement codant pour une polyprotéine. Ces protéases ne sont pas virales mais cellulaire.

    # Bilan ARN(+)

Pas d'ARN polymérase ARN dépendant dans les cellules. Les virus induisent la synthèse de leur propre enzyme virale.
CHez PVX : la protéine responsable de la synthèse d'ARN est localisée en 5' ; ORF 1 
Chez polio : une protéine légére et une protéine lourde ; ces protéines portent les fonctions methyl transférase hélicase, ARN dépendant. CHez polio le domaine responsable de la synthèse est le domaine 3d ; La protéine 3D dès que la polymérisation commencera elle sera synthétisée, elle doit être traduite en 1. Il faut commencer par produire l'enzyme!
Utiliser le brin viral comme matrice ; brin raccourci --> possèden en 5' proximél l'ORF à traduire

# Réguler

Comment? En limitant certains niveau traductionnels. Le VMT : première protéine à être synthétisée ? 

Initiation : ?
Le mécanisme de traduction : besoin d'un mécanisme. Ce sont des mécanismes qui sont décrits par les ARN cellulaires

TRaduction du génome viral en compétition avec les ARN cellulaires : stratégie? Un des domiane protéas va cliver un facteur de l'initiation de la traduciton coiffe dépendnat pour l'incactiver. --> machinerie traductionnelle : uniquement pour son génome.

Strucure virale : hélicoidal rigide  : peut casser ; stratégie : répartir les différents gènes sur les segements
Si on a un seul génome : une mutation sur le génome peut avoir des conséquences drammatiques sur le déroulé de l'infection --> on risque de toucher à la reconnaissance du génome viral ou à l'ARN polymérase ARN dépendant.
* Réplication plus rapide 
* Peu d'introduction de mutation de manière statistique
Le partitionisme du génome s'explique par des critéres d'évolution.

- : brin complémentaire au brin matrice

Le brin - va servir de matrice pour produire de nouveux ARN génomiques
Facteur cellulaire :

# Virus ARN -

VIrus à ARN double brin : en présence d'un ribosome ne saura pas traduire
--> ARN polymérase ARN dépendant qui a été produit au cycle précédant nécéssaire pour la réplication de son génome. (RdRp) En début du cycle, l'ARN polymérase va être orientée transciptase. Elle va reconnaître les segments, et va synthétiser des ARN complémentaires coiffés et poly adénnylés --> des ARN messagers : ils vont codés pour les 3 ARN polymérases. Ces protéines dont l'ARN polymérase ARN dépendant vont s'accumuler, si suffisamment l'ARN polymérase ne sera plus transcriptase mais réplicase --> elle va lire pour synthétiser un ARN virale complémentaire recouvert de nucléocapside.

# Virus ARN - mais à polarité ambisens

Ici chaque brin peut servir de matrice pour produire des ARN messagers
La polymérase va reconnaitre la région à transcrire et va synthétiser un ARN messager ; L'ARN de polarité négative sera copié en ARN viral complémentaire ; l'ARN viral complémentaire peut servir de matrice pour produire un ARN messager. Ambisens : fonctionne dans les deux sens (mais beaucoup de confusion entre ARN géénomique et anti génomique et ARN messager) ARN génomique et ANti génomique ne sont pas codants, même s'il possède


# Infection par un virus eucaryote à ADNdb

VIrus doit amplifier le génome --> nécéssité d'avoir. Certains virus doivent utiliser l'ADN polymérase cellulaire mais il faut induire la division de la cellule --> effet mitogènes : induit la réplication. Le virus utilise l'ADN polymérase cellualire ou code pour une ADN polymérase. La cellule entre en phase S (alors qu'elle ne doit pas). La protéine P 53 et la protéine PRB permet l'apoptiose. Ces deux protéines vont ressentir l'entrée en phase S normale ou anormale. La protéine p53 --> elle va être inhibée par des particules virales. Action apoptotique : (le virus va pouvoir répliquer). Certains virus sont responsables de cancer. 

# Infection par un virus eucaryote à ADNsb

Ces virus ne sont généralement pas mitogène, pour se multiplier ils vont infecter des cellules en division. 

Pour le CC : Notion d'intégrité du génome : important 

# Pararetrovirus

Utilisent la réverse transcriptase dans leur cycle viral = critère de classification. Ils l'utilisent à des moments différents de leur multiplication. Classe 6 = génome à ARN ; polarité positive pour le géome n'a pas de fait en début de cycle. Les retrovirus = génome à ARN et pararetrovirus à ADN. L'ARN n'est pas pris en charge par la machinerie traductionnelle en début de cycle. Ces virus : leur génome est à ARN mais passent par un intermédiaire d'ADN. ARN = matrice pour la synthèse d'un brin d'ADN. Il faut apporter une reverse transcriptasen par le cycle 0 et l'incorpore pour cycle +1. L'ARN n'est donc pas directement infectieux. Les praretrovirus ont un génome à ADN qui utilisent l'ARN polymérase II qui va permettre l'expression d'un ARN prégénomique qui va servir de matrice pour produire, propre polymérase? A REVOIR!
La reverse transcription initie le cylce virale d'un retrovirus. Intégrase : intégration dans le génome celulaire va permettre le recrutement d'ADN pol II.
Pararetrovirus : l'ADN arrive se retrouve dans la cellule, et est transcriptionnellement actif --> ARNm : pas maturé = sert à la retrotranscription. La reverse transcription finalise le cycle. 
Dans les deux cas il faut des séquences répétées aux extrémités.

--> à retenir : les 3 caractéristiques de la reverse transcriptase :

* Une ADN polymérase : produire de l'ADN en utilisant un matrice ARN. ADN polymérase ARN dépendant. IL faut synthétiser du double brin : donc ADN pol ARN ET ADN dépendant. 
* Nécéssite une amorce : l'amorce peut être une protéine ou un ARNt.
* Quand on synthèstise on a un hybride ARN/ADN : beaucoup plus stable que ADN/ADN ; hors on a pas d'hélicase. Lorsqu'on veut lire : RNAase --> n'est active que sur un hybride ARN/ADN. Dégradation de l'ARN pour lire l'ADN.


--> Un retrovirus? 

* Nucléocapside enveloppé
* 2 copies : qui intéragissent les unes avec l'autres. 
* Séquence LTRlong terminal repeat, elles sont produites grâce aux séquences répétés sur l'ARN génomique.

pol = protéase, reverse transcriptase et intégrase
Certains retrovirus utilisent un syst de translecture : 90 % de prot GAL et 10% de GAP pol, d'autres utilisent le french shift = décalage du cadre de lecture : nécéssite une structure --> un pseudonoeud ; le ribosome arrive se retrouve face au pseudo noeud, beaucoup continueront à lire, d'autres entreront en collisions avec le pseudo noeud, et décalage du cadre de lecture. Au niveau de friendschip on a 7 nucléotides (complexe heptanucléotidique) qui permettent de conserver l'appariement Watson et Crick. Ces deux protéines ont le même domaine terminal. Tout cela va s'associer avec l'ARN pour former. 

Polyprotéine : active qu'après le bourgeonnement.

LTR : U3 R U5 : long terminal repeat sont les même ; recrutement de l'ADN pol

# COURS 4

Mécanisme de défense? 
Exclusion de superinfection : le virus empêche une surinfection --> masquage de récepteur. Pour la bactérie : être capable de résister aux systèmes de défense des organismes sur lesquelles elle va se retrouver et empêche une surinfection. COnversion lysogénique? Au fil de l'évolution?
