- [I Un peu d'histoire](#i-un-peu-dhistoire)
    - [_La variole_](#la-variole)
    - [_La rage_](#la-rage)
    - [_Vers la terminologie Virus_](#vers-la-terminologie-virus)
    - [_La Fièvre jaune_](#la-fi%C3%A8vre-jaune)
    - [_D'autres virus_](#dautres-virus)
    - [_Essor de la biologie_](#essor-de-la-biologie)
    - [_Grippe espagnole_](#grippe-espagnole)
    - [_La virologie née_](#la-virologie-n%C3%A9e)
    - [_Grandes découvertes_](#grandes-d%C3%A9couvertes)
    - [_Vers une définition_](#vers-une-d%C3%A9finition)
    - [_Découverte de la transcriptase inverse (1970)_](#d%C3%A9couverte-de-la-transcriptase-inverse-1970)

# I Un peu d'histoire

## _La variole_

 * La Variole (déjà des cas avant JC)
 * Mortalité: 20 à 30 %
 * On estime qu'il est responsable de la chute de l'empire Romain
 * Amérique du nord: on passe d'une population de 72000 à 9000 (Les Amerindiens sont morts de la variole)
 * Maladie purement humaine (transmission interhumaine)
 * La vaccination était pratiquée avant la découverte de l'existence des virus
    * Chine/Turquie: inhalation de croûtes séchées de variole pour se prémunir (variolisation) : charleur --> diminue la virulence du virus
    * Le système immunitaire développe des anticorps contre la variole
    * Benjamin Jetsy (1774) Il récupère des lésions de picote des vaches et scarifie sa famille
    * Les vachères (qui réalisent la traite des vaches) ne sont jamais atteintes par la variole, il relie cela aux pustules présents sur les pis de vache. Il utilise les pustules pour scarifier l'enfant. Suite à cela il scarifie ce même enfant avec la variole humaine(américains en 1795), l'enfant survit (2 enfants sur 5 en tout)

## _La rage_

 * Maladoe 100% mortelle
 * Maladie portée par les animaux, progression lente en fonction du lieu de l'infection
 * Extrait de moelle épinière de lapin après n passages
 * Paster a fait un appelle aux dons internationaux pour soigner les gens de la rage
 * L'institut paster vient du succès de la vaccination d'un enfant mordu par un chien
 * Seule vaccination que l'on peut mettre en place après l'infection virale car le virus est lent, le temps que le virus arrive au cerveau permet de faire une réponse immunitaire
 * 1885 Louis Paster réusssit à empêcher la mort du jeune Alsacien Joseph Meister mordu par un chien enragé en lui injectant l'agent infectieux atténué. L'enfant survit. Pour rendre hommage à Jenner, Pasteur nommera le procédé vaccination

## _Vers la terminologie Virus_

 * Virus définie poison invisible
 * A Mayer prend une feuille de Tabac malade, le filtre, récupére le filtra et frotte cela à un tabac sain, le tabac sain devient malade
 * Chamberland crée un filtre pour stériliser l'eau
 * Ivanoski reproduit l'expérience de Meyer --> quelque chose traverse le filtre de Chamberland, il le nomme "virus", mais rien ne prouve qu'il s'agit d'une entité biologique
 * **Beijerinck** conduit la même expérience, il réalise plusieurs dilution --> même avec des dilutions extrêmes on a la même pathologie sur toutes les plantes, on a donc quelque chose de vivant, qui se reproduit
 * Ne se multiplie pas dans un milieu de culture, il faut un hôte
 * Il parle de "COntafiom vivum fluidum"
 * Cet agent est le **Virus de la mosaïque du tabac (VMT)**

## _La Fièvre jaune_

 * 1793 : Rush est convaincu que la transmission n'est pas interhumaine
 * 1881 : Carlos FInlay émet l'hypoth!se d'une transmission de la fièvre jaune par moustique _aedes aegypti_
 * 1900 Walter Reed et collègues. Découverte du virus de la fièvre jaune et décrit son cycle de transmission par le mustique. --> premier virus humain décrit
 * Expérimentation humaine sur volontaires. transmission d'homme à homme (injection de sérum dilué et filtré) ou par moustique
 * La mort de Clara Maass mit fin à l'expérimentation humaine

## _D'autres virus_

 * Grippe aviaire
 * Rage
 * Leucémie aviaire
 * Poliovirus
 * Sarcome de Rous

## _Essor de la biologie_
 * 1921 Des virus peuvent causer des tumeurs
 * 1916-1919 : Découverte des bactériophages: Tous les organismes vivants sont atteints par des virus. A cet époque on sait marqué grâce à la radioactivité, on sait faire de la culture cellulaire ; Si on veut amplifier le virus il faut trouver un modèle animale? à partir de la culture cellulaire, on peut aborder la purification de virus et avoir des cultures pures.

## _Grippe espagnole_

 * Pandémie de grippe : 40 à 100 mio de morts dans le monde
 * 1918-1919 : Découverte du virus de la grippe (humain)
 * Grippe espagnole : vient d'Amérique

## _La virologie née_

 * 1921 : Description de la malade de Creutsfeldt-Jakob
 * 1923 : Développement de l'ultracentrifugation (obtention de culture pure)
 * 1933 : invention du microscope éléctronique (première visuation d'un virus --> bactériophage
 * 1935 purification et cristallisation du VMT
 * 1944 : Premières indications que l'ADN est support de l'information génétique
 * 1951 : Diffraction des rayons X
 * **1952 : Démonstration que l'ADN est support de l'hérédité**
    * Expérience d'Hershey et Chase
    * L'ADN du bactériophage porte toute l'information nécessaire pour la synthèse de nouveaux virions
    * Blend: décroche les particules virales qui sont fixées sur la bactérie
    * Le bactériophage reste dans le surgagent ; radioactivité qui n'est pas dans le surnagent, le bactériophage injecte son ADN dans la bactérie.

## _Grandes découvertes_
 * 1955 : Reconstitution du VMT à partir des ses protéines et de sons Acide nucléique
 * 1956 : l'acide ribonucléique du VMT est infectieux
 * 1962 Première classification des virus
 * 1966 : Création d'une institut de virologie
 * L'ARN est le suppoert de l'information génétique

## _Vers une définition_

Les virus ultrafiltrants ne se multiplient pas en milieu nutritifs, ils nécessitent des cellules vivantes.
Tous les organismes sont des hotes potentiels de virus mais il y a maintien de barrières d’espèces (Bactéries, 
Archées, Eucaryotes - Animaux, Champignons, plantes, insectes) 
Les virus sont constitués de protéines et d’acide nucléique : ADN ou ARN. 
L’acide nucléique est parfois infectieux. 

## _Découverte de la transcriptase inverse (1970)_

La découverte de la reverse transcriptase de retrovirus a 
permis d’initier les premières expériences de génétique inverses 
sur les virus à ARN (bactériophage)