#!/usr/bin/python3
# -*- coding:utf8 -*-


class Convertisseur :

    def convert(self, n, temperature):
        """ call the right converting function depending on n
        """ 
        temperature = float(temperature)
        if n==1 :           # call the Faranheit converting function if n==1
            return self.convert_to_faranheit(temperature)
        elif n==2 :         # call the Celcius converting function if n==2
            return self.convert_to_celcius(temperature)
        else :              # if n is not an integer between 1 or 2
            raise ValueError    
    
    def convert_to_faranheit(self, temperature) : # Convert from Faranheit to Celcius
        t = temperature * 1.8 + 32
        return t

    
    def convert_to_celcius(self, temperature) : # Convert from Celsius to Faranheit
        t = (temperature - 32) / 1.8
        return t
