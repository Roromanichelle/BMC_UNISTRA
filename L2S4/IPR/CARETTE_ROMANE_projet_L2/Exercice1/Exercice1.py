#!/usr/bin/python3
# -*- coding:utf8 -*-
 
from Convertisseur import *

def main() :                                                        # Fonction qui appelle la classe
    v = Convertisseur()
    while True :
        temperature = input("Enter the temperature : ")             # Entre la température       
        n = int(input("Enter 1 (celsius) or 2 (Faranheit) : "))     # Entre n
        try : 
            print(v.convert(n, temperature))                        # Si float(tamperature) dans la classe ne fonctionne pas : ValueError, de même si n est différent de 1 ou 2 (voir fichier Convertisseur.py)
        except ValueError :                                         # Si lors de l'appelle de la classe on obtient une ValueError on relance la fonction
            print("wrong input : n or temperature")                 
    

if __name__ == '__main__':
    main()
