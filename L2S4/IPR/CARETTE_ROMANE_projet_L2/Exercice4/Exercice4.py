
nucleotides_valables = ['A', 'T', 'G', 'C']
print()

def sequenceADN() :                         # fonction qui vérifie si la séquence entrée est valable
    a=True 
    while a :
        sequence = input('Entrez une séquence d ADN : ')       # Permet d'entrée une séquence d'ADN      
        a=False                             # Si la séquence est correcte, la boucle while ne tourne plus.
        for i in sequence.upper() :         # On vérifie les nucléotides un à un
            if i not in nucleotides_valables :
                print('It is not a valable DNA sequence')
                a=True                      #Permet de reprendre dans la boucle while, la séquence sera demandée à nouveau
                break                       #si un nucléotide n'est pas valable : la boucle for s'arrête et la boucle while reprend pour demander une nouvelle séquence complémentaire

    return sequence

sequence = sequenceADN().upper()            # Si la séquence est entrée en minuscules : renvoie la séquence en majuscules

def composition() :
    n_a = 0                                 # Initialisation
    n_t = 0 
    n_c = 0
    n_g = 0
    for i in sequence :                     # On regarde chaque nucléotide un à un
        if i=='A' :
            n_a = n_a + 1                   # On ajoute 1 on compteur à chaque fois que l'on rencontre le nucléotide
        elif i=='T' :
            n_t = n_t + 1
        elif i=='C' :
            n_c = n_c + 1
        elif i == 'G' :
            n_g = n_g + 1
    
    return {'A': n_a, 'T' : n_t, 'C' : n_c, 'G' : n_g}  # La clé correspond au nucléotide, la valeur au nombre de nucléotide dans la séquence

print()
print('Composition de la sequence d ADN : ', composition())
print()


def pourcentGC() : 
    return ((composition()['C']+composition()['G'])/len(sequence))*100 # renvoie le pourcentage en GC qui correspond au nombre de GC / nombre de nucléotide

print ('pourcentage en GC : ', pourcentGC(), '%')
print()

def tempFusionHowley() :
    if len(sequence) > 10 :
        return 67.5+(0.34*pourcentGC())-(395/len(sequence))
    else :
        return "the DNA sequence is too short"

print ('Temperature de fusion : ', tempFusionHowley(), '°C')
print()

def estComplémentaire() :                   # On entre la séquence complémentaire                 
    a=True 
    while a :
        sequence_complementaire = input('Entrez la sequence complementaire : ')       # Permet d'entrée une séquence d'ADN      
        sequence_complementaire = sequence_complementaire.upper()  # Si la séquence est entrée en minuscules : renvoie la séquence en majuscules 
        a=False                             # Si la séquence est correcte, la boucle while ne tourne plus.
        for i in range(len(sequence)) :     # On vérifie les nucléotides un à un
            if sequence[i] == 'A' and sequence_complementaire[i] == 'T' :
                continue
            elif sequence[i] == 'T' and sequence_complementaire[i] == 'A' :
                continue
            elif sequence[i] == 'G' and sequence_complementaire[i] == 'C' :
                continue
            elif sequence[i] == 'C' and sequence_complementaire[i] == 'G' :
                continue
            else : 
                return False
                a=True                      # Permet de reprendre dans la boucle while, la séquence sera demandée à nouveau
                break                       # Si un nucléotide n'est pas complémentaire : la boucle for s'arrête et la boucle while reprend pour demander une nouvelle séquence complémentaire
        if a==False:                        # Si a==False : cela signifie que tous les nucléotides étaient complémentaires
            return True

print ('Les deux brins sont complémentaires : ', estComplémentaire())

def ADN2ARN() :
    ADN = list(sequence)
    l = []
    for i in range(len(sequence)) :          # On vérifie les nucléotides un à un
        if ADN[i] == 'T' :              
            l = l + ['U']                    # On ajoute U si T correspond à ADN[i]
        elif ADN[i] == 'A' or ADN[i] == 'G' or ADN[i] == 'C' :
            l = l + [ADN[i]]                 # Sinon on complète avec les nucléotides de départ
    return ''.join(l)

print()
print ('Séquence d ARN : ', ADN2ARN())

sequence_ARN = ADN2ARN().lower()

code_genetique = {'uuu': 'F', 'ucu': 'S', 'uau': 'Y', 'ugu': 'C',
'uuc': 'F', 'ucc': 'S', 'uac': 'Y', 'ugc': 'C',
'uua': 'L', 'uca': 'S', 'uaa': '*', 'uga': '*',
'uug': 'L', 'ucg': 'S', 'uag': '*', 'ugg': 'W',
'cuu': 'L', 'ccu': 'P', 'cau': 'H', 'cgu': 'R',
'cuc': 'L', 'ccc': 'P', 'cac': 'H', 'cgc': 'R',
'cua': 'L', 'cca': 'P', 'caa': 'Q', 'cga': 'R',
'cug': 'L', 'ccg': 'P', 'cag': 'Q', 'cgg': 'R',
'auu': 'I', 'acu': 'T', 'aau': 'N', 'agu': 'S',
'auc': 'I', 'acc': 'T', 'aac': 'N', 'agc': 'S',
'aua': 'I', 'aca': 'T', 'aaa': 'K', 'aga': 'R',
'aug': 'M', 'acg': 'T', 'aag': 'K', 'agg': 'R',
'guu': 'V', 'gcu': 'A', 'gau': 'D', 'ggu': 'G',
'guc': 'V', 'gcc': 'A', 'gac': 'D', 'ggc': 'G',
'gua': 'V', 'gca': 'A', 'gaa': 'E', 'gga': 'G',
'gug': 'V', 'gcg': 'A', 'gag': 'E', 'ggg': 'G'}


def traduction() :    
    s = ''                                                  # Initialisation
    a=True
    while a :
        n = int(input('Entrez un cadre de lecture 0,1 ou 2 : '))                                                           
        if n == 0 or n == 1 or n == 2 :
            for i in range (n , len(sequence), 3) :         # On regarde chaque codon 1 à 1
                for k,v in code_genetique.items() :         # On regarde chaque clé et valeur
                    if k == sequence_ARN[i:i+3] :           # on compare la clé au codon en question
                        s = s + v                           # On ajoute l'acide aminé correspondant à la séquence
            a=False
            return s
        else :                                              # On redemande d'entrer n si l'entrée a échoué
            return 'Erreur : entrez un cadre de lecture correspondant à 0,1 ou 2'
                                                         
        
print()
print ('Sequence d acides amines correspondant : ', traduction())

def localiser_motif_simple() :
    if 'AATTGC' in sequence :                               # On regarde dans un premier temps si le motif est présent dans la séquence
        for i in range(0,6) :                               # On regarde dans chaque cadre de lecture
            for k in range(i, len(sequence),6) :            
                if sequence[k:k+6] == 'AATTGC' :            # k correspond à la position où se trouve le motif
                    return k
    else :
        return 'Le motif AATTGC n est pas dans la séquence' # Dans le cas où le motif n'est pas dans la séquence
            

print()
print ('position AATTGC : ',localiser_motif_simple())

def signature():
    while True:
        try:
            taille_motif = int(input("Entrez la taille du motif: "))
        except ValueError:
            print("L'entrée ne correspond pas à un nombre valide")
            continue

        if taille_motif >= 10:
            print("La taille doit être inférieur à 10")
            continue

        # Stock le nombre d'occurence des motifs
        dic_motifs = {}

        # Test tout les blocs de longueur taille_motif jsuqu'à la fin, sans déborder
        for i in range(len(sequence) - taille_motif):
            # Prend le motif courant
            motif = sequence[i : i+taille_motif]

            # Vérifie que l'on a pas déjà vérifié ce motif
            if motif in dic_motifs:
                continue

            # Compte le nombre d'occurence du motif
            occurences = sequence[i:].count(motif)

            # Ne stock pas les motifs n'apparaissant qu'une fois
            if occurences == 1:
                continue
            
            # Stock le nombre d'occurences dans le dictionnaire
            dic_motifs[motif] = occurences

        # Retourne le dictionnaire
        return dic_motifs

print(signature())