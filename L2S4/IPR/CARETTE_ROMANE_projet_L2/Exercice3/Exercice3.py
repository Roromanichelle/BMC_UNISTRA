#!/usr/bin/python3
# -*- coding:utf8 -*-


def MinMaxMoy(l) :

    if type(l) == list :                              # Vérification qu'il s'agit bien d'une liste
        a=0                                           # Initialisation
        min = l[0]
        max = l[0] 
        for i in l :                                  # On regarde les composants de la liste 1 à 1                                  
            e=True       
            try :
                i=int(i)                              # On vérifie qu'il s'agit bien d'un entier
            except ValueError :             
                print("Enter a list object composed of integer please")
                e=False
                break                                 # On arrête la boucle s'il ne s'agit pas d'un entier
            if i < min :
                min = i
            if i > max :
                max = i
            a=a+i   
            moy = a/len(l)

        if e==True :                                  # On retourne si tous les composants de la liste correspondent à un entier
            return tuple((min,max,moy))
    
    else :
        return "Enter a list object please"           # On demande d'entrer une liste si ce n'était pas une liste qui a été entré en argument


if __name__ == '__main__':
    print(MinMaxMoy([10, 18, 14, 20, 12, 16]))
    

