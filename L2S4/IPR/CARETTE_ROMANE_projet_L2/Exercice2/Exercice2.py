from turtle import *

# centrer
up()
forward(25)
left(90)
forward(75)
right(90)
backward(75)


def triangle_equilateral(l,c,a) : # triangle équilatérale en longeur l, la couleur c  et angle de 60°

    down()
    fillcolor(c)
    begin_fill()
    forward(l)
    for i in range(2):
        left(180-a)
        forward(l)
    up()
    end_fill()

i=0
while i<6 :
    if i%2==0:
        triangle_equilateral(100,'blue',60)
        left(120)
        forward(100)
        right(60)
        i=i+1
    else:
        triangle_equilateral(100,'yellow',60)
        left(120)
        forward(100)
        right(60)
        i=i+1

left(120)
forward(100)

right(180)
down()
forward(400)

up()
right(120)
forward(200)
right(120)

down()
forward(400)
up()

right(120)
forward(200)
right(120)

down()
forward(400)



