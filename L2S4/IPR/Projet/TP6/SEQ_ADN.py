def dico_number() :
    a=0
    t=0
    c=0
    g=0
    B=True
   
    while B :
        sequence = input('Entrez une séquence ADN :')
        sequence = sequence.upper()
        B=False
        for i in sequence :
            if i == 'A' :
                a=a+1
            elif i == 'T' :
                t=t+1
            elif i == 'C' :
                c=c+1
            elif i == 'G' :
                g=g+1
            else:
                B=True
                print('Entrez une séquence valable')

    return {'A' : a, 'T' : t, 'C' : c, 'G' : g}

SEQ = dico_number()
print(SEQ)