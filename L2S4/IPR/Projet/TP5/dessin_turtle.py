from turtle import * 

def turtle_carres(nombre, taille, couleur) :
    a=0
    for i in range(nombre) :
        a=0
        while a<4 :
            forward(taille)
            right(90)
            a=a+1
        up()
        forward(30)
        down()
        
turtle_carres(8,25,'red')
input()