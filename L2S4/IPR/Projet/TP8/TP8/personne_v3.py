class Personne:
    compte=0
    @staticmethod
    def compteur():
        val=Personne.compte
        Personne.compte +=1
        return val

    def __init__(self, nom, prenom, age):
        self.nom=nom
        self.prenom=prenom
        self.age=age
        self.id=Personne.compteur()

    def quiSuisJe(self):
        print(self.id, self.nom, self.prenom, self.age)

    def __str__(self):
        frm="<{:3d},{},{},{:2d}>"
        return frm.format(self.id, self.nom, self.prenom, self.age)

listePersonne=[ 
    Personne("Dupont", "Albert", 20),
    Personne("Colin", "Joseph", 28),
    Personne("Dupont", "Marie", 21),
    Personne("Colin", "Louise", 30)
    ]


for individu in listePersonne:
    individu.quiSuisJe()

for individu in listePersonne:
    print(individu)
                

