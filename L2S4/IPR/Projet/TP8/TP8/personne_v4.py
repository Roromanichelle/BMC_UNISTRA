import math
class Personne:
    compte=0
    @staticmethod
    def compteur():
        val=Personne.compte
        Personne.compte +=1
        return val

    def __init__(self, nom, prenom, age):
        self.nom=nom
        self.prenom=prenom
        self.age=age
        self.id=Personne.compteur()

    def quiSuisJe(self):
        print(self.id, self.nom, self.prenom, self.age)

    def __str__(self):
        frm="<{:3d},{},{},{:2d}>"
        return frm.format(self.id, self.nom, self.prenom, self.age)

    def __gt__(self, other):
        return self.age > other.age

    def __lt__(self, other):
        return self.age < other.age

    def __eq__(self, other):
        return self.age == other.age

    def __ne__(self, other):
        return self.age != other.age

    def __ge__(self, other):
        return self > other or  self == other 

    def __le__(self, other):
        return self < other or self == other 

listePersonne=[ 
    Personne("Dupont", "Albert", 20),
    Personne("Colin", "Joseph", 28),
    Personne("Dupont", "Marie", 21),
    Personne("Colin", "Louise", 30)
    ]


for individu in listePersonne:
    individu.quiSuisJe()

print()
for individu in listePersonne:
    print(individu)

print()
ind=listePersonne[2]
for individu in listePersonne:
    print(ind, "Plus grand que ", individu, " ?", ind > individu)
    print(ind, "Plus petit que ", individu, " ?", ind < individu)
    print(ind, "egale à ", individu, " ?", ind == individu)
    print()

                

