import math
class Point:
    def distance(self, b):
        dx = self.x - b.x
        dy = self.y - b.y
        return math.sqrt(dx*dx + dy*dy)

def distance(a, b):
    dx = a.x - b.x
    dy = a.y - b.y
    return math.sqrt(dx*dx + dy*dy)

p1=Point()
p1.x=10
p1.y=20

p2=Point()
p2.x=1
p2.y=2
print("distance fonction: ", distance(p1,p2))
print("distance méthode de classe: ",p1.distance(p2))

