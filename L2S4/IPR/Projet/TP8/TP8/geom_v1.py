class Forme:
    "Forme est la super classe dont hérite toutes les formes géométriques"
    def __init__(self):
        self.nom="anonyme"

    def surf(self):
        raise NotImplementedError
    def perim(self):
        raise NotImplementedError
    
    def __str__(self):
       return self.nom

    def affiche(self):
        print("Je suis la figure :", self)


class Carré(Forme):
    "La classe Carré hérite de Forme"
    def __init__(self, nom, coté):
        super(Forme,self).__init__()
        self.nom=nom
        self.coté=coté

    def surf(self):
        return self.coté*self.coté

    def perim(self):
        return 4*self.coté
    
    def __str__(self):
        return super(Carré,self).__str__()+ ", de coté :{:8.3f}".format(self.coté)


class Rectangle(Forme):
    "La classe Rectangle hérite de Forme"
    def __init__(self, nom, long, larg):
        super(Rectangle,self).__init__()
        self.nom=nom
        self.long=long
        self.larg=larg

    def surf(self):
        return self.long*self.larg

    def perim(self):
        return 2*self.long*self.larg
    
    def __str__(self):
        return super(Rectangle,self).__str__()+ ", de longueur :{:8.3f} et largeur {:8.3f}".format(self.long, self.larg)

class Cercle(Forme):
    "La classe Cercle hérite de Forme"
    def __init__(self, nom, rayon):
        super(Forme,self).__init__()
        self.nom=nom
        self.rayon=rayon

    def surf(self):
        return self.rayon*self.rayon*math.pi

    def perim(self):
        return 2*self.rayon*math.pi
    
    def __str__(self):
        return super(Cercle,self).__str__()+ ", de rayon {:8.3f} ".format(self.rayon)

lstForme=[ Cercle("c1", 10), Carré("k1", 55.5), Rectangle("r1", 3, 4), Forme(), Cercle("c2", 6)]

for fig in lstForme:
    fig.affiche()
