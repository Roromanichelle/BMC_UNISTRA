def fonctionAvecArgs(arg1, arg2, arg3, 
    *argsListe, 
    argn1='val1', argn2=1000, argn3=[10, 20, 30], 
    **argsDico
    ) :
    print('arg1',arg1, 'arg2', arg2, 'arg3', arg3)
    print('argn1', argn1,'argn2', argn2,'argn3', argn3)
    i=0
    for elt in  argsListe :
	    print("argLst",i,":", elt, end=",", sep="")
	    i+=1
    i=0
    print()
    for cle, val in  argsDico.items() :
        print(cle,":", val, end=",", sep="")
        i+=1


fonctionAvecArgs(10, '100', 1000, 
    'aaa', 'bbb', 20, 40, 60, 
    argn2=200, 
    a='a', b='bb', c='ccc')

print("\n=============================================")
fonctionAvecArgs(10, '100', 1000, 
    'aaa', 'bbb', 20, 40, 60, 
    argn2=200, 
    a='a', b='bb', argn1='ccc')


