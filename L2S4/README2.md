# Classes

## Attributs

* Correspondent aux caractèristiques de la classe (ex : taille, couleur, etc...)

```python
self.attribut # Dans une classe
objet.attribut
```

### Méthodes

* Action de l'objet

#### Déclaration

```python
def nom_de_methode(self, paramettre):
    # Action
```

#### Appelle

```python
objet.nom_de_methode(paramettre)
```

### Constructeur

#### Déclaration**

```python
def __init__(self):
    # initiqlisation des attributss
```

#### Appelle a la creation de l objet

```python
objet = Classe(paramettre)
```

> Le constructeur est une méthode d'initialisation d'une classe appelée lors de la création de l'objet.
> Lorsqu'aucun constructeur n'est définit,  le constructeur
appelé lors de la création de l'objet est le constructeur de la superclasse objet.

## Lancer un scripte

### Bonne methode

```bash
python3 convertisseur.py
```

### Moins bonnes methode

```bash
python3
>>> import Convertisseur
>>> Convertisseur.main()
```

## Modules

> ```__init__.py``` files are required to make Python treat the directories as containing packages.s

```python
import Convertisseur
Convertisseur.mqin()
```

```python
from Convertisseur import *
main()
```