
- [Physiologie des cellules excitables](#physiologie-des-cellules-excitables)
    - [I. Proprietés et structures des cellules excitables](#i-propriet%C3%A9s-et-structures-des-cellules-excitables)
        - [1) Définitions d'une cellule excitable:](#1-d%C3%A9finitions-dune-cellule-excitable)
        - [2) Conditions nécéssaires](#2-conditions-n%C3%A9c%C3%A9ssaires)
        - [3) Neurone: exemple de cellule excitable](#3-neurone-exemple-de-cellule-excitable)
            - [3.1) Le corps cellulaire](#31-le-corps-cellulaire)
            - [3.2) Les dendrites](#32-les-dendrites)
            - [3.4) L'axone](#34-laxone)
        - [4) Caractéristiques ultrastructurales](#4-caract%C3%A9ristiques-ultrastructurales)
            - [4.1) Les éléments du cytosquelettes](#41-les-%C3%A9l%C3%A9ments-du-cytosquelettes)
                - [i. Les microtubules: (24 nm)](#i-les-microtubules-24-nm)
                - [ii. Les microfilaments (7nm)](#ii-les-microfilaments-7nm)
                - [iii. Filaments intermediaires (7-11nm)](#iii-filaments-intermediaires-7-11nm)
            - [4.2) Répartition différentielle dans le soma](#42-r%C3%A9partition-diff%C3%A9rentielle-dans-le-soma)
            - [4.3) Transports axonaux](#43-transports-axonaux)
        - [5 Synapses et réseaux de neurones](#5-synapses-et-r%C3%A9seaux-de-neurones)
            - [5.1) Généralités sur la synapse](#51-g%C3%A9n%C3%A9ralit%C3%A9s-sur-la-synapse)
            - [5.2) Exemple d'un circuit réflexe spinal](#52-exemple-dun-circuit-r%C3%A9flexe-spinal)
    - [II. Membrane plasmique et canaux ioniques](#ii-membrane-plasmique-et-canaux-ioniques)
    - [III. Schéma éléctrique équivalent de la membrane](#iii-sch%C3%A9ma-%C3%A9l%C3%A9ctrique-%C3%A9quivalent-de-la-membrane)
    - [IV. Lois de diffusion](#iv-lois-de-diffusion)
    - [V. Potentiel de repos](#v-potentiel-de-repos)
    - [VI. La pompe Na+/K+](#vi-la-pompe-nak)
    - [VII. Le potentiel d'action](#vii-le-potentiel-daction)


# Physiologie des cellules excitables

## I. Proprietés et structures des cellules excitables

### 1) Définitions d'une cellule excitable:

Une cellule excitable est une cellule qui est capable de générer un potentiel d'action. Toute cellule capable de faire un potentiel d'action est une cellule excitable.
ex: Le neurone ; cellules musculaires striées, squeletiques, lisses et cardiaques ; cellules endocrines. Toutes les cellules endocrines ne sont pas des cellules excitables. Une partie des cellules endocrines sont des cellules excitables ; d'autres non.
Cellules épithéliales: non excitables

### 2) Conditions nécéssaires

Deux milieux qui sont de composition différente : il faut une barrière physique qui les sépare --> double couche lipidique dans le cas d'une membrane plasmique.
Molécules qui peuvent traverser la double couche hydrophobe:
-O2
-N2
-Benzène
-Glycerol
-urée
-CO2
Molécules qui ne peuvent pas traverser:
-Saccharose
-Glucose
-Na+;K+;H+ (Cations)
-Ca2+; Mg2+ (Cations divalents)
-Cl- ; HCO3- --> Ces ions ne peuvent pas passer spontanément il faut des structures :
 * Les caneaux ioniques: toutes des protéines intrinsèques de la membrane : diffusion --> gradient éléctrochimique (ne dépend pas directement de la production d'ATP)
 * Transporteurs : symports / antiports / uniports
 * Transports membranaires : Transports passifs / actifs: primaire ou secondaire
 * Un type de transport ne correspond pas forcément à un type de transporteur
 * Passif : suit un gradient (mettent en jeu une diffusion simple selon un gradient)
 * Actif : dépendant de l'hydrolyse de l'ATP ; primaire: ATPase ; secondaire : pas une ATPase, le transporteur lui même n'hydrolyse pas l'ATP

--( )--

Avantage d'avoir un transporteur ou un canal? Quand on veut une signalisaiton rapide on a intérêt à avoir recours à des mécanismes de diffusion
Plus on augmente la concentration plus la vitesse du transport est rapide ; saturabilité en récépteur --> on atteint une vitesse maximale : cinétique d'un transporteur est une cinétique de type enzyme. 


### 3) Neurone: exemple de cellule excitable

Message nerveux: est éléctrique --> il va être organisé sous forme de fréquence de potentiel d'action. Les neurones peuvent communiquer les uns avec les autres. 
Tous les neurones sont différents mais ont certains points communs.
Neurone type: 3 régions anatomiques
 * Le Corps cellulaire
 * Les dendrites
 * L'axone
Synapse au nivaau des corps cellulaires et des dendirtes (reception des informations)
Axone: pôle émetteur du neurone

#### 3.1) Le corps cellulaire

 * Il contient le noyau + lieu de synthèse des macromolécules ; transport axonaux
 * Cytosquelette et forme du neurone + molécules qui servent à la fontion du neurone

#### 3.2) Les dendrites

 * La dendrite peut se ramifier, on appelle ces dendrites selon l'endroit où on la prend : primaire ; secondaire etc..
 * Lorsque la dendrite se ramifie son diamètre se diminue
 * Dans certains dendrites on trouve des excroissances membranaires que l'on nomme des épine dendritiques : ce sont les sites où se font les synapses avec d'autres neurones.

#### 3.4) L'axone

 * Prolongement qui émane du corps cellulaire
 * Cône d'émergence : avant le début de la partie allongée de l'axone on a une zone : le segement initial (où né le potentiel d'action). Il né au segment initial et se propage dans l'axone. Il s'agit d'un prolongement fin et possède un diamètre constant sur toute sa longueur.
 * Deux grands types d'axone: axone myelinisé (une gaîne --> constituée de myélinne) ; axone non myelinisé.
 * La myéline est produite par des cellules gliales. On distingue en général 4 principaux types de cellules gliales :
    * les astrocytes 
    * les oligodendrocytes 
    * les cellules de Schwann 
    * la microglie

 

>
 * **_Système nerveux centrale_** :

    * **Cellules microgliales** : cellules souches qui entrent dans le syst nerveux centrale, qui vont rester et se multiplier (même fonction que les macrophages) --> non myelinnisante
    * **Astrocytes** : Elles ont généralement une forme étoilée, d'où provient leur étymologie : Astro - étoile et cyte - cellule. Elles assurent une diversité de fonctions importantes, centrée sur le support et la protection des neurones. 

    * **Oligodendrocytes** : Sa principale fonction est la formation de la gaine de myéline entourant les fibres nerveuses (axones) du système nerveux central 
>
 * **_Système nerveux périphérique_** (cellules de schwann non myelinisantes et celles de schwann myelinisantes)

    * **Cellules de Schwann** : Contrairement aux oligodendrocytes qui sont situées uniquement dans le système nerveux central, les cellules de Schwann n'existent qu'au niveau du système nerveux périphérique. D'autre part, elles forment la gaine de myéline autour d'un seul axone, alors que les oligodendrocytes peuvent en myéliniser plusieurs.

    * **Cellules de Schwann non myelinisantes?** :  Les cellules de Schwann non-myélinisantes entourent plusieurs axones de faible diamètre.
>
* Glaine de myeline ne colle pas contre l'axone
--> pour avoir un potentiel de repos

### 4) Caractéristiques ultrastructurales

#### 4.1) Les éléments du cytosquelettes

Rôle structurale du cytosquelette ; tansport antérograde et rétrodgrade (des extrémités vers le corps cellulaires)

##### i. Les microtubules: (24 nm)

tubuline alpha et tubuline beta
Associés à ces microtubules on trouve des protéines qu'on appelle les microtubules associated proteins (MAP)
MAP 1 ; MAP 2 ; MAP 3
MAP 2 : on les trouve dans les dendrites et dans les corps cellulaires alors que MAP 1 ne se trouve que dans les axones

##### ii. Les microfilaments (7nm)

Actine : G

##### iii. Filaments intermediaires (7-11nm) 

cytokératines: (Les neurofilaments sont spécifiques des neurones)

Le cytosquelette établit des structures entre eux, avec les mitochondries, avec le reticulum et des vésicules.

#### 4.2) Répartition différentielle dans le soma

Chromoatine relativement uniforme ; un gros noyaux ; un ou deux nucléoles ; c'est une cellule en interphase.
Synthétiser des protéines? ADN dans le noyau transcrit en ARNm ; puis après synthèse de protéines dans le reticulum endoplasmiqe rugueux (ou granulaire). Ces protéines peuvent être modifiées (glycosylation)
REG
app de golgi
mitochondries
polysomes

On trouve dans les dendrites du reticulum lisse mais pas d'app de golgi ; on trouve des mitochondries, des polysomes et ribosomes. On a pu montrer qu'il y avait un transport d'ARN vers les dendrites --> synthèse locale de protéine
Dans l'axone : on trouve des mitochondries (il faut fournir de l'énergie)

#### 4.3) Transports axonaux 

Expérience : Weise et Hiroe
Transports antérogrades : -rapide (100 à 400) et lent (0,1 à 0,8)
Transports rétrogrades 

### 5 Synapses et réseaux de neurones

#### 5.1) Généralités sur la synapse

Différentes synapses: 
    -Synapse neuro neuronale
    -Synapse neuro- musculaire

Dans une synapse l'élément présynaptique est toujours un neurone
* Les synapses chimiques : neurotransmetteur permet la transmission de l'information
* Les synapes éléctriques : cap jonctions --> permet le passage des ions

Les cellules gliales délimitent la fente synaptique 
L'élévation de calcium implique la fusion de la vésicule avec la membrane de la synapse

On a des synapses
-excitatrices : Dépolarisation
-inhibitrices : neurotransmetteur va inhiber et va hyperpolariser la cellule et va empêcher le message nerveux de se propager.

#### 5.2) Exemple d'un circuit réflexe spinal

* Muscle extenseur
* Muscle fléchisseur
* Neurotransmetteur : glutamate --> effet excitateur sur le motoneurone --> décharge de potentiel d'action (acétyl choline --> va augmenter le potentiel d'action (dépolarisation))
On étire le muscle : excitation et ça se contracte
La même fibre va faire une collatérale dans la moelle et va fiare synapse avec un interneurone qui va lui même faire synapse avec le motoneurone fléchisseur (motoneurone du muscle antagoniste)
On va exciter l'interneurone inhibiteur il va exciter plus 
D'un côté contraction du muscle extenseur puis inhibition de l'autre côté du muscle fléchisseur.

## II. Membrane plasmique et canaux ioniques

## III. Schéma éléctrique équivalent de la membrane

## IV. Lois de diffusion

## V. Potentiel de repos

## VI. La pompe Na+/K+

## VII. Le potentiel d'action
