# III Schéma éléctrique équivalent de la membrane

## 1) But

Condensateur : double couche lipidique
Courant : courant ionique qui passe à travers le canal
Résistance : canal

U=RI
I=U/R
I=((1/R)*U
G: conductance : 1/R

## 2) Rappels sur les circuits éléctriques

Dans le cas des canaux ioniques : résitance en parallèle --> le courant peut passer par deux canaux à la fois.
Gtot=G1+G2+...+Gn

