- [II Membrane plasmiques et canaux ioniques](#ii-membrane-plasmiques-et-canaux-ioniques)
    - [1) Double couche lipidique](#1-double-couche-lipidique)
    - [2) Caractéristiques structurales et fonctinnelles des canaux ioniques](#2-caract%C3%A9ristiques-structurales-et-fonctinnelles-des-canaux-ioniques)
        - [2.1) Introduction/généralités](#21-introductiong%C3%A9n%C3%A9ralit%C3%A9s)
        - [2.2) Structure](#22-structure)
            - [2.2.1) Canaux Na+ et Ca 2+ voltage dépendant](#221-canaux-na-et-ca-2-voltage-d%C3%A9pendant)
            - [2.2.2) Canaux K+ Voltage dépendants](#222-canaux-k-voltage-d%C3%A9pendants)
            - [2.2.3) Les récepteurs canaux](#223-les-r%C3%A9cepteurs-canaux)
        - [2.3) Caractéristiques fonctionnelles](#23-caract%C3%A9ristiques-fonctionnelles)
    - [3) Distribution des différents types de canaux](#3-distribution-des-diff%C3%A9rents-types-de-canaux)
    - [4) Techniques d'enregistrement éléctrophysiologique](#4-techniques-denregistrement-%C3%A9l%C3%A9ctrophysiologique)
    
# II Membrane plasmiques et canaux ioniques

## 1) Double couche lipidique

* Composition de la membrane
    * Phospholipides : glycérophospholipides --> Acide gras : acide palmitique et acide oléique les plus courant
    * Cholestérol : conditionne la fluidité de la membrane (10%)
    * Glycolipides
* Double couche lipidique qui ne laisse pas passer l'eau, les ions
* Molécules qui peuvent traverser la double couche hydrophobe:
-O2
-N2
-Benzène
-Glycerol
-urée
-CO2
* Molécules qui ne peuvent pas traverser:
-Saccharose
-Glucose
-Na+;K+;H+ (Cations)
-Ca2+; Mg2+ (Cations divalents)
-Cl- ; HCO3- --> Ces ions ne peuvent pas passer spontanément il faut des structures 


## 2) Caractéristiques structurales et fonctinnelles des canaux ioniques

### 2.1) Introduction/généralités

* Canaux du potentiel de repos : canaux constament ouvert qui laisse passer des ions selon le gradient.
Différent de degré de perméabilité : séléctivité stricte pour une espèce ionique ; on peut avoir une sélectivité plus large, récepteur à l'acétyl choline --> laisse passer que des anions et pas des cations.
Séléctivité très stricte ou séléctivité plus large
>
* Canaux voltage dépendant : canaux dépendant du potentiel de membrane
* Les récepteurs canaux : les canaux chimio dépendants ; neurotransmetteur se fixe et permettent l'ouverture du canaux
* Canaux jonctionnels (ou encore cap jonction) --> canal qui permet de faire passer des ions et entre les cellules (couplage éléctrique)

### 2.2) Structure

#### 2.2.1) Canaux Na+ et Ca 2+ voltage dépendant

Na+ et Ca 2+
On a plusieurs sous unités.
Canaux sodium : premier à avoir été cloné et décrit.
On un grand nombre de type de canaux sodium
Glycoprotéines : jusqu'à un tiers de leur poids moléculaire.
Alpha (270 kDA) --> canux sodium mais qui n'ont pas les même propriétés que les canaux que l'on trouve dans les canaux natifs ; les sous unités Beta1 et Beta2 sont des sous unités auxilliaires --> permettent la fonctionnalité.
On a 4 motifs qui se resemblent --> la structure de base est la même ; chaque motif est constitué de 6 segments transmembranaires avec une boucle intramembranaire (région P : boucle intramembranaire = pore forming region)

Acides aminés vont prendre une certaine orientation si le potentiel de membrane change ; le mouvement produit par le champ éléctrique sur ces acides aminés vont permettre la sensibilté au potentiel de membrane ; si mutation --> perte de sensibilité au potentiel de membrane

P va se placer au centre du canal : la protéine va se réorganiser dans la membrane de façon à ce que toutes les régions P soient face à face
>
#### 2.2.2) Canaux K+ Voltage dépendants

On a qu'un seul motif, il faut 4 sous unités synthétisés indépendamment

#### 2.2.3) Les récepteurs canaux

Pentamères (5 Sous unités) --> architecture toujours la même mais grande diversité de sous unités, à l'intérieur d'un récepteur on va retrouve 5 sous unités (2 sous unités alpha et 3 autres sous unités)
La sous unité alpha porte le site de liaison de l'acétyl choline : ccl pour un recepteur : deux moléucles d'A. choline qui se fixent et vont laisser passer des ions.

Avant d'être cloner on a du purifier ces recepteurs.
Blanc : protéine 
NOir : canal
Une fois purifier on peut cristalliser afin de déterminer la structure tridimenssionnelle de la protéine.

Ce qui délimite la région de la paroi est le segment M2 (segment transmembranaire 2) s'associent face à face pour donner le canal. 
Deux molécule d'A.choline pour ouvrir le canal

### 2.3) Caractéristiques fonctionnelles
 
Canaux : 10⁸ ions/s
Transporteur : 10⁴ions/s

On a besoin de signalisation rapide --> canaux ioniques

Les ions ne se promènent pas dans le milieu environnant, ils sont en solution et ils intéragissent avec l'eau qui l'entoure. 

* Filtre selectif pour les canaux :
    * Charge
    * Taille
        * Taille d'un ion K+ : 0,133 nm
        * ion Na+ : 0,095 nm
        * Tenir compte de l'ion hydraté : de sa taille et des intéractions de l'ion avec les régions du canal
        * Les ions potassium sont plus gros que les ions sodium, mais l'hydratation n'est pas la même. L'ion passe sous forme deshydraté dans le canal, il doit se débarrasser de ses molécules d'eau, mais besoin d'énergie. Dans le canal il y a des sites de liaison pour l'eau, les intéractions avec une certaine molécule vont être thermodynamiquement plus favorable. Pourquoi l'ion ne reste pas fixé? Ion suivant va prendre la place de l'ion prédant qui va se fixer sur le site suivant. 

## 3) Distribution des différents types de canaux

Canaux potentiel de repos : partout
Canaux Na+ voltage dépendant : au niveau du segment initial, au niveau des noeuds de ranvier et terminaison
Canaux K+ voltage dépendant : Dendrites et corps cellulaire Entre les noeuds de ranvier
Canaux Ca 2+ : Corps cellulaire ; dendrites ; pas le long de l'axone mais au niveau des boutons synaptiques
Recepteur canaux : en face du bouton synaptique et face aux dendrites ; pas le long de l'axone

## 4) Techniques d'enregistrement éléctrophysiologique

* Enregistrement intracellulaire
    * Patch clamp
        * On introduit une electrode pour pouvoir mesurer le potentiel d'action, amplificateur ; électrode intra et extra cellulaire, on veut meusurer la différence entre les deux signaux.
        * Coussins d'air pour ne pas qu'il y ai de vibration (voir shéma patch clamp)
        * -60 mV --> potentiel de repos
        * Dépolarisation (augment) et hyperpolarisation (diminue
* Neher et Salemann --> pensait qu'ils y avaient des particules qui passaient, ils ont isoler un canal et enregistré ce qui se passe au niveau d'une seule molécule ; ils ont tenté de coincer un canal sous la pipettes et d'enregistrer le signal éléctrique quand il s'ouvre
    * Fuites : brouilli éléctrique : il faut diminuer ces fuites : aspiration --> résistance éléctrique de l'ordre de 10⁹ ohm (gigaohm) plus de fuite. Quand le verre se colle à la membrane, grande résistance mécanique on peut arracher le canal. On peut résoudre les événements liés à l'ouverture d'un seul canal 
