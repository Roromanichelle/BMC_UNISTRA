# Messagers intracellulaires

## Notion de second messagers

Les molécules informatives lipophiles sont capables de franchir la membrane plasmique de la cellule peuvent agir sur des récepteurs intracellulaires ou des récepteurs nucléaires pour induire leurs effets.
Cependant les molécules hydrophiles qui ne peuvent pas franchir la membrane plasmique ne peuvent avoir un effet sur la cellule qu'en activant des récepteurs membranaires puis une cascade de réaction intracellulaires qui conduira à l'expression de l'effet biologique. Ainsi ces molécules ont besoin de divers facteurs capables de relayer ou d'amplifier leurs effets dans le milieu intracellulaire. On regroupe l'ensemble de ces facteurs sous le terme de **second messager**.
Ce qui démontre l'importance des seconds messagers dans la médiations des effets des molécules informatives à partir des récepteurs membranaires peut être prouvé par des expériences. 
La 1ère : en absence du ligand -> si on procède expérimentalement à des modifications intracellulaires du 2nd messager impliqué on retrouvera l'effet cellulaire du ligand.

### Les familles

Il esxiste diverses catégories de 2nd messagers qui peuvent intéragir par grande famille :

La famille des nucléotides cycliques : L'Adénosine monophosphate cyclique, la guanosine monophosphate cyclique, l'adénosine diphosphate ribose cyclique

La famille des dérivés du métabolisme des lipides :
Dans cette famille les 2nd messagers peuvent être l'inositol phosphate ; les diglycérides ;

La famille des ions : ex le calcium, les ions hydrogènes, la famille des gazs (notamment le NO (monoxide d'azote) qui peut agir comme second messager. 

Il existe des milions de molécules informatives : mais que très peu de second messagers aussi bien naturels que exogènes. On a certes une diversité de second messagers mais pas suffisant pour induire autant d'effet sur la cellule

# Récepteurs couplés aux protéines G

## Structure du récepteur 

Messagerie pas l'intermédiaire des récepteurs couplés aux protéines G. 
Ces récepteurs sont des récepteurs protéiques qui sont constitués par 7 domaines transmembranaires. 
Extremité N terminale : extracellulaire et une extremité C terminale : intracellulaire. 
Les récepteurs couplés aux protéines G ont 3 boucles intracelluaires ; certains disposent d'une 4 ème pseudo boucle qui se forme suite à des modifications post traductionnelles.Ces récepteurs possèdent sur la partie C term intracellulaire des sites de phosphorylations. 
Le premier récepteur qui a été identifié  correspond à la **rhodopsine** (pigment visuel) = macromolécule protéique qui contient les 7 domaines transmembranaires --> ce récepteur est particulier (c'est un cas à part : il est lié en permanence à son ligand qui est le 11-Cis Rétinal et dans cette configuration le récepteur n'est pas activé, même fixé à son ligand. Les stimulus d'activation correspond aux photons de la lumière qui permet d'isomériser le 11 cis Rétinal en Tout Trans-Rétinal.


Lorsque le ligand active le récepteur : Le complexe ligand récepteur activera une protéine G constituée de 3 sous unités (protéine trimérique : alpha, beta et gamma). Ces protéines G activées à son tour agira sur des effecteurs cellulaires qui peuvent être des enzymes des canaux ou des transporteurs. Cette protéine agit à son tour sur des effecteurs : (canaux, enzyme etc), à partir de ces effecteurs sont modulés les concentrations intracellulaires des seconds messagers. 

## Fonctionnement de la protéine G

Au repos: la protéine G sont constitués de alpha beta gamma qui sont reliés
Les 3 sont associés et portent une GDP
Lorsque la protéine G est en présence du complexe ligand récepteur : on assiste à une phosphorylation du GDP : la molécule est remplacée par une molécule de GTP. Dans ces conditions on observe une séparation des sous unités : alpha associé au GTP et beta gamma qui restent associés. 
Dans la plupart des cas c'est alpha GTP qui agit sur les messagers.
Intervention d'une GTPase intrinsèque qui dephosphoryle GTP en GDP pour que la réaction s'arrête. Puis reconstitution du trimère.

La SU alpha agit en général sur le récepteur ; il y a des sous unités alpha stimulatrices de l'Adénylate cyclase. La nature de la sous unité alpha est variable,selon la nature. Donc de multiples protéines G qui vont avoir un grand nombre d'effets.

Adénylate cyclase = enzyme qui permet la production d'AMPc à partir d'ATP, ainsi lorsqu'un récepteur couplé aux protéines G est activé par son ligand agoniste et que l'ensemble stimule une protéine G, activatrice de l'adénylate cyclase : augmentation de la production intracellulaire d'AMPc. L'AMPc a pour fonction d'activer des protéines kinases dites protéines kinase AMPc dépendantes ou pKa, La PKA dans on état inactif, au repos est constitué de 4 sous unités à savoir : deux sous unités régulatrices et deux sous unités catalytiques qui sont associées. En présence d'AMPc: l'AMPc se fixent aux sous unités régulatrices et dissocie donc des sous unités catalytiques et peuvent donc phosphoryler des protéines cibles qui possèdent une séquence de reconnaissance constituée par deux AA basiques ; suivis d'un AA neutre puis d'une sérine, ou d'une thréonine. Extinction de la réaction : phosphodiesterase AMPc dépendante, qui dégrade l'AMPc pour donner une molécule de 5' AMP : pour mieux exploiter cette signalisation cellulaire : dans quelle situation peut on bien comprendre le rôle et l'implication physiologique. Le Glucagon est une hormone hyperglycémiante. SUr les cellules où il y a les reéserves glucidiques ; le glucogène est stocké au niveau du foie ou au niveau des cellules musculaires

Guanilate cyclase : ou guanilyte cyclase est l'enzyme qui permet la production de GMPc à partir de GTP. Lorsque un récepteur est lié à son ligand agoniste et qu'il stimule une protéine G activatrice de la guanilate cyclase : on observera une augmentation de la produciton intracellulaire de GMPc qui a pour fonction d'activer des protéins kinase GMPc dépendantes ou PKG. La PKG au repos est consititué de deux SU qui sont chacune à la fois régulatrice et catalytique.  (ne pas confondre R c avec le R ru récepteur (Reg=regulatrice)). Le GMPc se fixe sur ces SU et c'est l'ensemble du complexe GMPc ; CKG qui phosphorylera des protéines cibles qui ont également des séquences de reconnaissance constituée de deux acides aminés basiques ; des composés neutres suivis d'une thréonine. Le GMPc est dégradé par une phosphodiesterase GMPc dépendante pour donner une 5'GMP. 

Examen : Tableaux: voies comparatives ; mettre les éléments de similitude et de différences. Quels sont les voies activés? Quels sont les récepteurs? suivre la signalisation dans chaque cas

Phosphlipase : enzymes qui hydrolysent les liaisons ester : 4 liaisons ester dans un phospholipide. Une liaison entre chacun des AG et le glycérol ; une liaison etre le phosphate et le glycérol, et une liaison entre le phosphate et l'alcool. Les phospholipases A1 hydrolysent les l'AG de la fonction alcool primaire en C1 pour libérer un AG et un lisophospholipide. Les phospholipase B ; hydrolysent les deux liaisons ester en C1 et en C2. Ce qui libère 2 AG et un glycéro phosphoryl alcool. Les phospholipases C hydrolysent la liaison entre le phosphate et la fonction alccol primaire en C3 pour libérer un diglycéride et un phosphoalcool.  Les phospholipases D hydrolysent la liaison entre l'alcool et la fonction acide du phosphate ce qui libère un acide phosphatidique et un alcool. 

Il existe divers types de phospholipases C : mais ce sont des phospholipases C Beta ; C gamma: sont activés par des récepteurs enzymes ; à activité thyrosine kinase. Lorsque sur la cellule on a positionné une phospholypase C. Si c'est un récepteur enzyme : et qu'ils ont pour cible une phospholipase C, c'est une phospholipase C gamma qui est impliquée. Phosphlipase C dégradée :  donne le diglycéride et l'inositol triphosphate.Le phosphatidyl inositol diphosphate fait partie des phospholipides de la membrane ; lorsque que le phospahte est activé converti le en diglycéride et en inositol triphosphate. 
Lorsque le récepteur couplé à une protéine G est stimulé par un ligand agoniste et que l'ensemble stimule à son tour une protéine G (G alpha Q); que l'ensemble stimule une protéine G (galphaQ)--> activera à son tour la phospholipase C --> va hydrolyser le PIP 2 pour donner un diglycéride et l'inositol triphosphate (IP3). L'inositol triphosphate est capable d'nteragir avec les récepteurs canaux situés sur les citernes du réticulum endoplasmique dans lesquels sont enfermés les réserves calciques.  IP3 va faciliter la sortie du calcium des citernes vers le cytosol. On observera donc une augmentation de la concentration cytosolique de calcium. différence entre concentration cytosolique et concentration intracellulaire (intérieur de la cellule ). Permet d'activer des protéines cibles calcium dépendantes = calmoduline (active la contraction des fibres musculaires lisses). Permettra également la translocation d'une protéine kinase C du milieu intracellulaire vers la membrane plasmique.  Cette protéine kinase C qui est inactive avant sa translocation peut être activée au niveau de la membrane plasmique par les diglycérides. Les diglycérides peuvent aussi petre hydorlysé par la diglycérides lipases pour donnér l'Acide Arachidonique qui est un des précurseur des prostaglandines. Enfin l'inositol tri phosphate peut subir une phosphorylation supplémentaire pour donner l'IP4 ou inositol tétraphosphate ; l'IP4 stimule l'ouverture des canaux calciques de la membrane plasmique pour faciliter un influx d'ions calcium à partir du milieu extracellulaire. Il existe ds types cellulaires tel que les cellules du pancréas exocrine et certaines cellules musculaires dans lesquelles les récepteurs canaux des citernes du Reticulum endoplasmique ne sont pas sensibles à l'IP3. Dans ces cellules c'est donc un autre second messager qui permettra d'ouvrir ces canaux pour faire sortir le calcium. Ce second messager correspond à l'ADP ribose cyclique = IP3 où les récepteurs canaux ne sont pas sensbles à l'IP3 ; on les appelle les récépteurs RYanodine parce qu'ils ont été caractérisé grâce à une alcaloide végétale (la ryanodine) qui inhibe ces canaux insensibles à l'IP3. L'ADP ribose cyclique joue ce rôle pour ouvrir ces canaux. Récepteurs canaux activables pas les ligands (=RCAL) ; LGIC : Ligand Gated Ion Channels. On prend le canal comme étant un récepteur ; le récepteur réagit avec une molécule informative ; au lieu que le réepteur soit une protéine ; il s'agit d'un canal.

# Superfamille des recepteurs canaux, divisés en 3 grandes familles

Un canal est une protéine transmembranaire qui permet de le passage à travers une membrane de corps chimiques chargés dans le sens du gradient. Autrement dit, les canaux membranaires constituent des chaînes constitutives, délimitent un espace et laissent passer des ions hydrophiles. Les parois constitutives sont des parois constitutives du canal. Le plus souvent ce sont des ions mais d'autres substances peuvent traverse ce canal = glucose. Les corps chimiques qui transitent par un canal se déplacent selon le sens du gradient. Certains canaux sont spécifiques à un seul ions ; certains peuvent laisser passer des ions dans un sens ; d'autres peuvent faire passer deux ions mais dans des sens opposés (= symports; antiports). CAnal doit avoir des sites de fixation ; Les récepteurs canaux activables par les ligands constituent une super famille de canaux que l'on peut subdiviser en plusieurs familles.

* Les nicotinoïdes 
    * Nicotinique
    * GABAa
    * GABAc
    * Rglycine
    * R-SHT3
    * R Anionique du Glutamate

Canaux pentamèriques et chaque sous unités sont différentes. On va décrire l'organistation de la chaîne : une extremité N terminal ; un domaine extracellulaire (lieu de la fixation) suivi de 3 segments transmembranaires ; vient un deuxième domaine intracellulaires.
Un 4ème segment transmembranaires et enfin l'extremité C terminal qui est extracellulaire

**Récepteurs nicotiniques**
On va commenter le récepteur Nicotinique : 5 sous unités : tous les membres sont des pentamères --> ce récepteur est trouvé dans de nombreux organes en particulier dans le muscle et dans le système nerveux. Dans le muscles, les 5 SU  sont deux SU alpha, une SU beta, une SU delta et une Gamma. Ce récepteur nicotinique : perméable aux cations --> sodium et calcium, mais le récepteur musculaire est particulièrement perméable aux ions sodium. Le même récepteur nicotinique dans le système nerveux : es 5 SU sont constitués par 2 SU alpha et 3 SU beta : ce sont des réepteurs perméables aux cations mais avec une grande perméabilité aux calciums. Ce récepteur nicotinique est retrouvé dans la médiation des effets dans le syst nerveux central ou dans le syst musculaire. Au niveau de la jonction neuro musculaire : on a des récepteurs nicotiniques dont le ligand naturel est l'acétylcholine. Si on utilise des antagoniste : incapacité de contracter les membres --> relaxation. Au niveau du cerveau : nicotine active les récepteurs pour libérer la dopamine qui donne l'envie. 
Acétylcholine agit par deux types de récepteurs : un récepteur canal et un récepteur métanotrope = récepteur couplé aux protéines G.

**Les récepteurs GABA A et GABA C**
**GABA A**
Le GABA est le principal neurotransmetteur inhibiteur du syst nerveux et agit par l'intermédiaire de trois types différents de récepteur : GABA A ; GABA B ; GABA C
Le récepteur GABA B est un récepteur métanotrope = récepteur couplé aux protéines G. 
Les récepteurs GABA A et GABA C sont des récepteurs canaux. Ces deux récepteurs ont 5 SU --> POur GABA A: les 5 SU sont constituées par des SU alpha, beta, gamma, delta et epsilon et pi. Le plus souvent dans le système nerveux pour GABA on a deux fois alpha, deux fois beta et une fois gamma ou delta. Il existe d'autres isotypes : 6 isotypes de alpha, 4 isotypes de beta et gamma qui ont été clonés. Il y a un grand nombre de possibilitésde SU, on peut avoir un grand nombre de configuration. 
A quoi peut servir le fair qu'il y ait autant de possibilités pour un récepteur qui répond à une substance? Régulation très fine par rapport à la concentration de la substance. Cela signifie que l'on a pas la même affinité pour le GABA selon la configuration. On peut avoir une distribution cellule spécifique ou tissus spécifique. Tous les organes ne doivent pas réagir de la même façon au GABA.
**GABA C**
Le récepteur GABA C n'est constitué que de la SU ro : mais quelques possibilités parce qu'il 3 SU différents pour la SU ro.
**GABA A**
Ces deux types de récepteurs sont perméables aux ions chlorures mais présentent des différences par rapport à leur agoniste et antagonistes. D'abord le récepteur GABA A: ce canal permet un influx d'ions chlorure (entrée) --> crée une hyperpolarisation. Au cours de l'ondogénese (dvpt) on peut observer un efflux d'ions chlorures suite à l'activation du récépteur GABA A. Sur le récepteur GABA A on observe le site principal de fixation du GABA et une molécule : la bicu(pi)lline est un antagoniste compétitif du GABA sur le site GABA du récepteur. Il existe des sites allostériques sur le récepteur GABA A. Ces sites peuvent être des modulateurs allostériques positifs ou des modulateurs allostériques négatifs : les site des Bensobiadépines et le site des Barbituriques = site de modulation allostérique positif et le site de certains neurostéroides allobetanolone. Ces composés potentialisent l'action du GABA sur le récepteur GABA A. Il existe également des sites de modulation négatif : le site de la picrotoxine = antagoniste non compétitif qui va diminuer l'action du GABA sur le récepteur GABA A. Dans le domaine intracellulaire du récepteur GABA A on trouve un site de phosphotylation qui peuvent moduler l'activité du récépteur. 
**GABA C**
Le site principal de fixation est antaganosié de façon compétive par le BNTA : pas par la bicupiline. Il n'y a pas de site de modulation allostérique pour les bensobiadépines, les barbituriques et les neurostéroides sur les récepteurs gaba C. En revanche ce recepteur posséde également un site de modulation allostérique positive pour la picrotoxine de même que les sites de phosphorylation pour la protéine kinase. 

* Les récepteurs cationiques du glutamate
    * AMPA, NMDA, Kaïnate

Le glutamate est le principal neurotransmetteur excitateur du syst. nerveux chez les mammifères. Peut aussi agir par l'intermédiaire de différents types de récepteurs dont les récepteurs canaux, les récepteurs métanotropes ou récepteurs couplés aux protéines G.

Tous ces récepteurs sont perméables à des canaux perméables aux cations, sodium potassium calcium.

Canaux tétramèriques et chacune des 3 unités sont constitués de la façon suivante : N terminel extracellulaire ; premier doamaine extracellualaire ; 3 segments transmembranaires un domaine extracellualire puis vient une boucle intramembranaire ; un deuxième segment transmembranaire, un deuxième domaine extracellualire avec un C terminal intramembranaire.

On va illustrer le récepteur NMDA : récepteur canal acitvié par des ligands mais également voltage dépendant. Il est constitué par 4 SU et lorsque ce récepteur est au repos, le canal est constamment bloqué par un ion magnésium. On trouve sur le récepteur : un site principal de fixation pour le glutamate  ; ou le N-méthyl B aspartate, site allostérique de fixation sur ce récepteur NMDA.s

* Les récepteurs ionotropiques de l'ATP
    * Récepteurs p2x

Ce sont des trimères ; N terminal intracellulaire ; un premier segmenet transmembranaire ; un domaine de fixation des ligands qui est extracellulaire ; vient le deuxième segment transmmembranaire et vient l'extremité C terminal qui est intracelluaire.

