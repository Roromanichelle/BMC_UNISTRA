# Généralités

## Introduction

* propranolol? laisser dans une mémoire à court terme ; stress post traumatique (dvpt d'une pathologie très invalidante)
--> molécule informative, déjà connu dans le syst cardiovasculaire
* Intéraction ligand récepteur

Démarche expérimentale : 
* inhibiteur de synthèse protéique? -> perturber le rôle de certaines protéines qui ont un rôle dans la mémorisation
* récépteur beta adénergique, on a besoin de ces récepteurs pour consolider les évenements traumatisant, si on l'empêche de se consolider, on atténue la force de ce souvenir sur les patients concernés.

Les êtres pluricellulaires ont besoin pour assurer leur survie de réaliser des fonctions vitales qui leur permettent de pouvoir survivre, de pouvoir communiquer avec l'environnement dans lequel ils vivent, et surtout que les différentes parties qui les constituent puissent communiquer entre elles. Ces fonctions vitales sont assurées grâce à des molécules informatives qui agissent par l'intermédiaire de récepteurs cellulaires qui en controlant les mécanismes cellulaires permettent aux individus pluricellulaires de réaliser les fonctions vitales.

Une molécule informative ou encore molécule informationnelle est un corps chimique qui est produit par une cellule vivante pour transmettre un signal à une autre cellule qui reçoit ce signal par l'intermédiaire d'un récepteur chimique. C'est grâce aux molécules informatives que les deux grands systèmes de communication qui existent chez les mammifères peuvent fonctionner (système nerveux et système endocrinien).

Les molécules informatives endogènes (synthétisé par l'organisme, ex: hormones, neurotransmetteurs etc) agissent sur des récepteurs cellulaires et sont considérés comme des ligands naturels ou des ligands endogènes de ces récepteurs. 
Les molécules informatives exogènes peuvent agir sur les organismes vivants lorsqu'elles ont des structures chimiques qui leur permettent d'interagir avec les récepteurs cellulaires (cf cours ligand).

On distingue plusieurs familles de molécules informatives: famille des neurotransmetteurs, famille des neurohormones, famille des hormones, famille des facteurs de croissance, famille des immunoglobulines.

## La famille des neurotransmetteurs

### _Définition_
 
 Un neurotransmetteur est une substance secretée par la terminaison de l'axon d'un neurone appelé neurone pré-synaptique.
 Cette sécretion se fait selon un processus d'exocytose rapide au niveau d'une fente synaptique, qui est consécutif à la stimulation du neurone pré synaptique. Le neurotrasmetteur agit sur la cellule post-synaptique par l'intermédiaire de récepteur membranaire.

### _Exemples_

Glutamate, neuradrénaline, acétylcholine, GABA, sérotonine

Pour qu'une molécule soit considérée comme un neurotransmetteur elle doit répondre à certain critère d'identification:


### _Critères_

* Critères d'identifications

    * La molécule doit être localisée dans les neurones du système nerveux centrale avec une distribution régionale caractéristique --> dans l'encéphale et dans la moelle épinière. Chaque neurotransmetteur aura sa propre cartographie
    * Le neurotransmetteur doit être situé dans la terminaison nerveuse
    * Un mécanisme de biosynthèse doit exister dans le neurone pour permettre la fabrication intracellulaire du neurotransmetteur
    * La stimulation du neurone doit entraîner la libération du neurotransmetteur en quantité physiologiquement significative (ordre de umol)
    * Des récépteurs spécifiques reconnaissant le neurotransmetteur doivent être présents au voisinage de la structure présynaptique 
    * L'intéraction du neurotransmetteur avec son récepteur doit induire des modifications du potentiel éléctrique et de la perméabilité de la membrane post-synaptique
    * Des mécanismes d'inactivation doivent permettre le blocage de l'intéraction entre le neurotransmetteur et son récepteur dans une échelle de temps physiologique

Lorsque certaines molécules ne vont répondre que partiellement aux critères d'identification des neurotransmetteurs on les appelle les neuromodulateurs.
Synonyme de neurotransmetteur = neuromédiateur
Ces neuromodulateurs : (ex : neurostéroides--> cellules fabriquées par les cellules nerveuses (cellules gliales) --> progestérone ; oestrogènes) les neurostéroides : neuromudulateurs car ne sont pas fabriqués dans le système nerveux centrales --> ils ne peuvent pas être concentrés à la terminaison de l'axon enfermé dans une vésicule
Les cellules ne peuvent pas les concentrer à l'intérieur des cellules car ne peuvent pas être vésiculées

Sérotonine et histamine ? Composés connus comme impliqués dans des réactions d'allergiques, avant on considérait que ces molécules étaient des molécules périphériques, or on a découvert qu'elles étaient présentes dans le cerveau et produites uniquement dans certaines cellules, elles peuvent intervenir dans la modulation de l'activité cérébrale : on les fait passer au rang de neuromédulateurs. On a démontré que ce sont des neurotransmetteurs car correspondent à tous les critères. Elles sont désormais définies comme neurotransmetteurs.
Quelle application?
    Le neurone post synaptique possède des récepteurs. On va avoir des systèmes de transporteur : si inhibiteur présent on ne va pas avoir de dégradation de la sérotonine ; si pas d'inhibiteur --> présnece de sérotonine. Application : implique une augmentation de la présence sérotonine au niveau de la fente synaptique

Adrénaline : hormone secrétée par la glande medulo surrénale : mais peut être considéré comme un neurotransmetteur car aussi fabriqué dans le cerveau. --> mais ne leur fait pas perdre la fonction qu'ils avaient au départ

## La famille des Hormones

* Hormone : Substance secrétée en faible quantité par un tissus glandullaire qui après transport par le milieu intérieur agit sur d'autres tissus ou organes éloignés appelé orgnaes cibles. Sur ces organes cibles l'hormone va déclencher des activités spécifiques. Le caractère endocrine est donc essentiel pour l'identification d'une molécule comme étant une hormone. Caractère endocrine : fait que la mol soit secrétée dans le milieu à l'intérieur pour être véhiculés et agir sur des ceibles interne. Il se peut que l'hormone agisse sur les cellules qui l'ont secrétés on parle d'hormones autocrines ou agir sur des cellules voisines de celles qui l'on secrétés : on parle d'hormone paracrine.

_Comment peut-on avoir une hormone autocrine s'il faut qu'elle soit transportée?_ Ce n'est pas parce qu'elle est transportée qu'elle n'agit pas sur les cellules voisines.

Différence entre Hormones et neurotransmetteurs : hormones possèdent le mode endocrine et non les neurotransmetteurs.

Autocrine : permet l'autorégulation

* Lorsqu'on considère la structure chimique des hormones on peut les classer en 4 types : 
    * les hormone peptidique (une seule séquence d'AA ex : le glucagon ; l'insuline)
    * Les hormones dérivés d'un seul AA : monoamines (adrénaline)
    * Les hormones stéroides : dérivés de cholestérol
    * Les homones thyroidiennes : produites par la glande thyroidienne (ex : tyroxine)

## La famille des Neurohormones

* Une neurohormone est une molécule informative qui est secretée par des cellules appelées cellules neurosécrétrices ; il s'agit en général de molécules de nature peptidiques et lorsque cette molécule est sécrétée dans une fente synaptique dans le système nerveux centrale la molécule se comporte comme une neurotransmetteur mais lorsqu'elle est secrétée au contact d'un vaisseau sanguin elle peut rejoindre la circulaton sanguine et agir sur des cibles lointaines.
    * ex : ocytocine --> contrôle dans le comportement des animaux ; dans les relations sociales ; dans l'espèce humaine ; douleur? et vasopressine (chez l'Homme)


Le terme facteur de croissance désigne une large catégorie de polypeptide sérique (dans le sérum) ayant des propriétés régulatrices de nombreux paramètres de la vie cellulaire.

Les facteurs de croissance régulent la prolifération, la différenciation et la survie cellulaire. Les effets d'un facteur de croissance sont determinés par la nature de la cellule cible, par la concentration du facteur de croissance et par la présence simultanée éventuelle d'autres facteurs.

Les cellules cibles des facteurs de corissance possèdent des récepteurs membranaiares qui sont en général des récépteurs enzymes, localisés sur les cellules cibles. La dénomination des facteurs de croissance est le plus souvent (mais pas toujours) tirée du matériel biologique dans lequel ils ont été mises en évidence pour la première fois. 

Exemples de facteurs de croissance : facteur de croissance nerveux :
* NGF : nerve
* EGF : epidermal
* FGF : fibroblast
* HGF : hepatocyte
* VEGF : Vascular

Quels sont les domaines d'application?
Cas du FGF dans le domaine des neurosciences :
VEGF : facteur de corissance qui stimule la fromation des vaisseaux sanguins que l'on appelle le phénomène d'angiogenèse. Quelle application en cancerologie ?
Les cell cancéreuses sécrétent du VEGF ; ce VEGF va faciliter la production de nouveaux vaisseaux sanguins (méioangiogénèse)
Antagonistes du VEGF : on adresse des composés qui vont bloquer la formation de ces vaisseaux sanguins --> soit de fixe sur le VEGF soit sur les recepteurs. Cellules cancéreuses : n'ont pas les éléments nutritifs cancéreuses.
--> stratégie endioangiogénèse ou diagnostique

## Notion de récepteurs

Les récepteurs : terme qui désigne des macromolécules qui sont capables de reconnaître d'autres molécules qui sont produites dans des cellules et qui sont porteuses d'une information.
Les récepteurs peuvent être localisés sur la membrane plasmique ou dans le milieu intracellulaire (on a des récépteurs membranaires ou des récépteurs intracellulaires) Parmi les récépteurs membranaires on distingue 
 * les récépteurs couplés aux protéines G
 * les récépteurs enzymes
 * les récépteurs canaux activables par des ligands

Pour les récépteurs intracellulaires on distingue les recepteurs cytoplasmiques ou récépteurs cytosoliques ou les récepteurs nucléaires qui sont dans le noyau. Ces récepteurs cellulaires sont soit des récepteurs de la membrane plasmiques ou des récepteurs nucléaires.

