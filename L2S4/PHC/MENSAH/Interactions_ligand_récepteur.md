# Intéractions ligand-récépteur

## Caractéristiques

* Un récepteur cellulaire peut comporter un seul site d'action ou plusieurs sites d'action. Le processus de reconnaissance d'un récépteur par une molécule informative implique un degré élevé de stéréocomplémentarité. La stéréocomplémentarité est la cause de spécificité de l'effet biologique du ligand ou de la molécule informative.
Seul une molécule stéréocomplémentaire d'un récepteur ou de son site peut interagir avec ce récépteur.
* La spécificité des sites de reconnaissance des récépteurs est déterminée génétiquement car la structure des molécules informatives endogènes peuvent varier d'une espèce à une autre
* La liaison d'un ligand avec son récépteur provoque des changements de conformation du récépteur ou de son environnement immédiat --> on dit alors que le récépteur est activé et cette activation représente la première étape de la cascade d'évenements intracellulaires qui conduiront à l'effet biologique du ligand. La conformation du ligand peut aussi être modifiée lors de son intéraction avec le récepteur.
* L'affinité du ligand pour le récepteur est également importante pour le déclenchement de la signalisation à partir du récepteur. Un ligand affin (qui possède une grande affinité) aura besoin d'une faible concentration pour activer le récepteur, alors qu'un ligand moins affin aura besoin d'une plus grande concentration.
* Intéraction ligand-récepteur est un processus réversible qui ne met pas en jeu de liaisons covalentes.
* Les récepteurs existent en nombre limité dans une cellule cible. Cette caractéristique a pour conséquence que la réponse cellulaire à l'exposition du ligand peut atteindre un maximum à partir d'une certaine concentration du ligand.

## Agoniste 

* Un agoniste de récépteur est un ligand capable de se fixer spécifiquement à ce recepteur pour le stimuler ou l'activer. L'adrénaline qui est une molécule informative a deux types de récépteur (alpha et beta adenergique). Ces récepteurs peuvent contenir des sous types
* L'adrénaline est un agonistee ; c'est à dire une molécule capable de stimuler l'ensemble de ces récepteurs
* Lieu de localisation de ces recepteurs et le mode de signalisation couplé à ces récepteurs va déterminer l'action de l'agoniste.

## Antagoniste

* Un antagoniste est une molécule informative capable de se fixer spécifiquement à un recepteur mais n'induit pas d'effets physiologiques propres. L'antagoniste bloque l'action de l'agoniste sur le récepteur.

## Les Agonistes inverses

* Effet opposé à celui de l'agoniste

## Exemples 

* Recepteurs alpha 1 et alpha 2 adenergique
    * Les receptuers alpha 1 adenergique ont pour agoniste sélectif par exemple l'étilefrine --> contraction utérine ; agrégations plaquettaires (vasospasme ; augmentation de la pression arterielle) ; une mydriase (action du muscle dilatateur des pupilles)
    *CCl : nbrx effets 
    * Pour bloquer ces effets on a des antagonistes sélectifs --> tels que l'afflusosine
    * Les recepteurs alpha 2 adenergiques : dans le syst nerveux central, ils sont exprimés sur les terminaison des neurones présynaptiques adenergique. Ils sont exprimés sur les terminaisons des neurones pré synaptique adnergique. Ils peuvent donc être considérés comme des **autorecepteurs** sur des neurones adenergiques et neuradernergiques sur le système nerveux central.

* Ces autorecepteurs au niveau de la neurone pré synaptique permettent l'amplification du signal ou l'inhibition -->> phénoméne de rétrocontrôle qui peut être positif ou négatif. 
Stimulation d'alpha 2 sur le neurone présynaptique qui libére ADR (adrénaline)) contribue à diminuer la sécrétion d'adrénaline dans la fente synaptique

* Adrénaline : molécule anxiogène
La Clonidine : moins d'adrénaline dans la fente syaptique donc effet anxyolitique
La Johimbine est un antagoniste : plus d'adrénaline dans la fente synaptique donc effet anxiogène.

* Récepteur beta 1 et beta 2 : 
    * Recepteur beta 1 adenergique : récepteurs situés au niveau du coueur et des reins : l'activations de ces recepteurs entrainent une stimulation de l'activité cardiaque et également une contraction du coeur. On donne un agoniste sélectif pour les insuffisances cardiaques --> L'isoprostérénol
    * Augmentation du rythme cardiaque : antagoniste de beta 1 --> propanolol
    * Recepteurs beta 2 adernergique : musculature lisse des poumons ; des bronches --> lorsqu'on stimule ces récepteurs : relaxation des ces fibres musculaires alors que lorsqu'on bloque ce recepteur 
        * Sulbutamol --> agoniste du recepteur B2
        * Butoxamine et alprenolol

## Mécanismes des intéractions ligand-recepteur

* Mécanismes expliqués par deux grande hypothèse: 
 
### Hypothèse du modèle simple :

* Elle stipule que dans une cellule cible le ligand va interagir avec des recepteurs identiques qui n'exercent aucune influence entre eux
* Les molécules du ligand doivent parvenir suffisament près des récepteurs pour que les forces d'attraction qui s'établissent entre cette molécule et les récepteurs deviennent supérieures aux forces qui lient cette molécule avec les autres molécules du milieu extracellulaire.
* Les forces mises en jeu dans les intéractions ligand-recepteur sont des liaisons ioniques - hydrogènes - hydrophobes et de type Van der Walls mais jamais covalentes
>
* A+R <--> AR
* K1 : constante cinétique d'association
* K-1 : constante cinétique de dissociation (Reversibilité)
>
* Intéraction ligand-récepteur : réaction réversible
* Cette réaction obéit à la loi d'action de masse ; plus on aura de ligand et plus on pourra former le complexe ligand-récepteurs. On peut déterminer la vitesse d'association : Va = k1[A][R] alors que dissociation : Vd = k-1[AR]
* à l'équilibre : K1[A][R]=k-1[AR] (association=dissociation)
* Kd : constante globale de dissociation
* Kd et Ka : Kd = 1/Ka
>
* Kd = 1/Ka = K-1/K1 = [AR]/[A][R] = [A] (à l'équilibre) --> Kd: concentration de ligand capable d'interagir avec la moitié des récepteurs
* Plus basse est la valeur du Kd plus elevée sera l'affinité de la molécule informative ou du ligand pour son recepteur. Toute la démonstration mathématique consiste à expliquer que dans une situation d'équilibre où on a la moitié des recepteurs Kd= [A]
* L'affinité du ligand se traduit en utilisant la valeur de la constante de dissociation et en particulier on considère cette valeur lorsque la moitié des recepteurs est occupée.
>
* Les effets biologiques d'une molécule informative ne dépendent pas uniquement du nombre de recepteurs occupés : ces effets dépendent ausis d'un facteur d'efficacité propre qu'on appelle l'acivité intrinsèque alpha. C'est une valeur comprise entre 0 et 1. Elle traduit la puissance de l'action de la molécule informative après sa fixation sur son récepteur. 
* L'activité intrinsèque d'une molécule correspondante à une valeur de 1 : la molécule sera considérée comme un agoniste pur ou un agoniste total du récepteur.
* Lorsque l'activité intrinsèque est égale à 0 : c'est un antagoniste du récepteur. 
* Lorsque alpha est comprise entre 0 et 1 : on parle alors de molécule agoniste partielle.
>
* Antagonistes
Courbes doses réponses : on étudie la réponse cellulaire en fonction de la concentration qu'on teste. Em : Effet maximal que l'on peut obtenir sur le récepteur. 
Ea : réponse obtenue en testant la concentration du ligand.
Substance S2 et S3 : on peut atteindre l'effet maximale alors que l'on ne peut pas avec S1
>
* Antagonistes compétitifs :
    * on met dans le milieu d'essai une seconde molécule donc 2 paramètres. Quand A est seul : ffet maximum à la plus faible concentration. Un antagoniste compétitif (B) est une molécule qui est capable de provoquer un déplacement vers la droite de la courbe dose réponse de l'agoniste. En présence des concentrations croissantes de l'antagoniste spécifique on peut continuer à atteindre l'effet maximum de A à condition d'augmenter les concentrations de A. L'effet antagoniste est surmontable.
>
* Antagonistes non compétitifs :
    * Provoque un abaissement de la courbe dose-réponse de l'agoniste
    * En présence de concentration croissante de l'antagoniste non compétitif il devient impossible d'atteindre l'effet maximal de l'agoniste même si on augmente sa concentration. On dit alors que l'antagonisme non compétitif est insurmontable. 
    * Les deux se fixent sur un recepteur : mais on ne peut pas déloger B de son réceptueur --> B ne pourra pas diminuer.

### Hypothèse du modèle complexe

* Forme activée du récepteur R et forme inactivée : R' ; le changement conformationnelle serait responsable de ces deux formes. Un ligand Agoniste à activité intrinsèque maximale aurait une activité nulle pour la conformation R' et déplacerait l'équilibre vers la conformation R.  
>
* Cependant si on prend un antagoniste : l'antagoniste aurait une activité R' qui est supérieur à son activité pour R et déplacerait l'équilibre vers R'.
>
* Cette hypothèse du modèle complexe permet de comprendre les intéractions entre les récepteurs hétérogènes dans une cellule cible et notamment la notion de coopérativité entre récepteur. La coopérativité ; les changements de conformation secondaires à l'interaction ligand-récepteur peut provoquer des variations conformationnelles des récepteurs voisins et modifier leur activité pour le caractère de leur intéraction avec le ligand. La coopérativité positive serait lorsque l'intéraction ligand-récepteur provoquerait une variation conformationnelle des récepteurs voisins pour augmenter leur affinité au ligand et la coopérativité négative.

CCL : Notion de coopérativité positive et négative importante pour la cellule. 

* L'hypothèse du modèle complexe permet également de comprendre la notion de désensibilisation des récepteurs. Une désensibilisation est un phénomène dans lequel on constate qu'un récepteur exposé à des concentration normalement actives du ligand ne répond plus. En particulier lorsque ce récepteur a été préalablement exposé à des concentrations élevées ou de façon chronique au ligand.

* Ici on a un récepteur canal activé par la nicotine (ligand naturel = acétylcholine), constitué de 7 SU. Ici la notion de conformation dans le cas du modèle complexe --> permet de laisser passer des molécules. 5 SU délimitent l'epace dans lequel des cations vont pouvoir traverser. Si on prend la définition du modèle complexe ; on peut avoir plpusieurs conformations (au moins deux); ici on a 3 conformations 

CCL : l'hypothèse du modèle complexe permet d'identifier une conformation du récepteur qui correspond à l'état de désensibilisation qui n'est ni la configuration du récepteur activié, ni celle du récepteur désactivé.

* Il existe d'autres mécanismes qui peuvent déterminer ce qui est impliqué dans la désensibilisation des récepteurs : 
    * Variation de la conformation
    *  phénomène de l'internalisation du récepteur. Des récepteurs peuvent être internalisés grâce à des processus d'endocytose ce qui fait que les récepteurs ne répondent plus. Le deuxième phénomène qui peut être impliqué : **"Downrégulation"???** 
    *  Exposition chronique au ligand va provoquer une diminution de la transcription qui code pour le récepteur.
    * Phénomène de Phosphorylation / déphosphorylation qui interviennent sur les récepteurs (notament dans les domaines intracellulaires du récepteur).
--> Désensibilisation du récepteur
Cependant ; quelles sont les mécanismes impliqués dans la désensibilisation? Ce qui fait qu'en présence d'une molécule donnée à une concentration donéne l'effet n'est pas observé?
Dégradation du ligand lui même ! Or dans le milieu extracellulaire : le ligand peut être dégradé.
Autre processus : si