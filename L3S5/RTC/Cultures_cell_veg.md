# Cultures cellulaires végétales

Etudes sur cell végétale : 
* Concept de la totipotence des cellules. On a pu le démontrer chez les plantes, la totipotence chez toutes les cellules de l'organisme. Comment a t-on pu progresser pour démontrer cette totipotence.

## I. Cultures cellulaires végétales
### 1. Totipotences des cellules végétales
* 1. AU début du XXème siècle, on veut savoir si à partir d'une entité (explan) regénerer une plante entière.
  * Toute cellule végétale est capable de regénérer un nouvel organisme identique à celui dont elle est issue. Ce concept a été prouvé par l'expérimentation 60 ans plus tard.
* 2. Historique de la culture in vitro
  * Il a permis à une racine de se développer avec ses caractéristiques particulière de racine.
  * CUlture de carottes : cellules différenciés --> ON a toujours aujourd'hui des fragments de cal.
  * Le milieu de culture optimal ne dépend pas de la variété concernée? Saccharose (inerte et non réactif).pH 5.
* 3. Activation de processus cellulaires programmés
  * Changement des signaux perçus par les cellules. Cellules qui ont perdu leur polarité, la position, la capacité de récupérer des éléments dans une racine. Les nutriments arrivent de tous les côtés  => nouvelles informations => nouveau comportement. 
  * Mucltipliation cellulaire
  * Certaines familles de plantes, certains génotypes qui sont plus capables de se multiplier que d'autres. Embryon zygotique immature. Ils ont deisséqué des petits embyons. Ils ont aussi pris des tissus appelés des hypocotyle (croissance d'une petite partie sous le cotylédon)
* 4. développement d'explants pluricellulaires
  * 1ère voie : l'oganogenèse, on arrive à faire selon la balance hormonale oxines cytokinine, on favorise soit les feuilles soit les racines, on a dans tous les à faire ) des cellules différenciés
  * 2 ème voie : embyogenèse somatique =/ zygotique
  * Graine : environnement confiné car la graine a une taille limitée, embryon va etre confiné et toduf ans la graine, le suspenseur : a une durée de vie restreinte. 
* 5. Conditions de culture
  * Aseptie du milieu et de l'explant (sous les hottes)
  * Solide ou liquide?
  * Dans le milieu solide, très peu de diffusion de produits toxiques dans le milieu.
  * Repiquage : risque de contamonation, par des bactéries/levures/ champignons filamenteux).
  * Plus il y a des citokibibes, mons on va eller vers la sénescnce (ex: chute des feuilles).
  * Dosage oxines, cytokinine on peut faire soit de l'organigènese de racine ou de tige.


4. Haplodiploïdisation : un partir d'un lot haploide on double le matériel génétique 
5. Plus avantageux d'augmenter la ploidie d'un organisme pour éviter l'altération de l'information génétique.
6. Obtention de protoplaste : permet de séparer les C végétales les unes des autres, protoplastes se font par digestion de la paroi avec des  peptidase qui vont attaquer la paroi. POur pouvoir garder ces protoplastes en vie on ajoute du sucre et des minéraux aussi. 
   
   1. Hybridation somatique
      * protoplastes de cellules diffentes, la fusion peut générer des protoplastes qui possèdent soit deux noyaux, soit un seul noyau résultant mais avec un noombre de proportion variable de chromosome) 
      * Souvent ces hétérocaryons ne génèrent pas de plantes viables. 
      * Les hybrides (exclusivement A ou B ont été très utilisé.)
      * Les hybrides somatiques avec création de nouvelles espèces
    2. Pourquoi?
       * Hybridation sexuée : rendue compte qu'on était pas capable par hybridation zygotique par croisement sexué d'aboutir à une résistance à certains pathogènes, il a fallu passer par l'hybridation soomatique, pour transmettre d'induire la capacité de résister à un pathogène. hybridation zygotique n'a pas pu fonctionner car la solution se trouvait dans les mitochondries, information portée par le cytoplasme (mitochondries.). On a besoin 
    3. Ul faut éviter l'autofécondation. SI croisement de la meme espèce, ce polyallélisme est ce qui favorise la robustesse. quand on regarde la fleur de colza : fleur standard, ces plantes ont une une facilité pour l'autofécondations, graines de mons en moins performante, l'agriculteur veut favoriser l'héréofécondation, il faut des colza femelles mais des mâles stériles. croisement de 2 lignées )> graines hybrides hétérozygote, il faudra forcément la stérélité mal. 

## Cours 2

* Maïs a été importé du mexique, on peut potentiellement d'utiliser des transgènes, puisque ce mais n'a aucun pouvoir pour aller polliniser d'autres plants. 

## Agroinfection

* Utilisation d'une machinerie d'une bactérie, agrobacterium, bactéirie qui infecte les plantes, elle existe naturellement, elle va pouvoir s'associer aux cellules végétales, une fois qu'il y a contact, la bactérie va mettre en oeuvre une machinerie qu'elle possède dans son généome, et mettre un plasmide particulier qui a la propriété d'induire des tumeurs chez les plantes, plasmide qui va induire la formation de tumeur parce qu'il trnasfère un petit fragment (ADNt, ADN de transfert, qui contient peut des gènes mais des gènes qui favorisent la multiplication cellulaire
* Problème majeur avec cet OGM (Maïs BT)? à la prototoxine, pendant longtemps, très utilisé mais à postériori, on s'est rendu compte que d'autres organismes étaient sensibles à cette toxine, quantité de papillon qui ont failli s'éteindre, principalement sur les côtes des états unis de l'ouest. Dans quel plante? prototoxine? uniquement dans la feuille ??
* Anticorps de plantes : 