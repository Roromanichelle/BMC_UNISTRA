word = str(input("What's the secret word ? "))
guesses = ' '
turns = 10
a=0
print()

while turns != 0 and a==0 :
    for char in word :
        if char in guesses:
            print(char, sep=',', end=' ')
        else :
            print("_", sep=",", end=" ")
    guessed_character = str(input("\n \n What's the guessed character ? "))
    if guessed_character in word :
        guesses += guessed_character
    if len(guesses) == len(word) :
        print("you won")
        a=1
    if turns == 1 :
        print("you lose")    
    turns = turns - 1

