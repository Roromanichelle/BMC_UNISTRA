# La réplication
* Structure de l'ADN connue depuis 1953.
* Chaque brin d'ADn parental sert de matrice pour la production d'ADN, UN brin d'ADN parentale et un brin d'ADN néosynthétisé. (Revoir l'expérience de Meselson et Stahl). 
* Ceci implique l'ouverture de la double hélice.

## Procaryote (mécanisme chez E.coli)
* Sur un ADN génomique circulaire? où commence la réplication? nécéssite l'ouverture complète? Chaque brin est synthétisé à la même vitesse?
* On utilise la radioactivité (permet d'avoir l'image de la molécule) Est-ce que la réplication est bi directionnelle? --> expérience de John Cairns. E.coli en présence de trimidine tritiée, elle va être incorporé dans l'ADN.  La qauntité de TT ajouté dans un premier temps est faible --> marquage faible de l'ADN, dans un second temps, quantité plus importante ajoutée, implique un marquage fort, ensuite l'ADN est ectrait et déposé sur une plaque pour avoir l'image de cet ADN (voir image dia) Cela a permis de montrer que la synthèse de l'ADN se faisait dans deux sens oposés.
* ADN extrait à des temps variables, oeil de réplication, la réplication commence en un point unique. On remarque que l'oeil est symétrique, chaque brin synthétisé est synthétisé simultanément. L'a double hélisce s'ouvre progressivement et chacun des deux brins est synthétisé simultanément.
* Motifs répétés Dna A = site de liaison d'une protéine appelée DnaA


  ### Rôles de ces motifs dans l'initiation de la réplication.
  * Dans un premier temps la protéine va se fixer sur l'ADN sur chacun des quatres sites, permet la fixation coopératives de DnaA, puis complexe protéiques. Suite à l'enroulement de l'ADN autour du complexe multiprotéique, il y a aura fusion de l'ADN au niveau de la région riche en AT. Puis formation de l'oeil de réplication.**fusion** de l'ADN???
  * L'hélicase l'hélicase se déplacera vers l'extrémité 3' de l'ADN. On a des petites protéines qui viennent se fixer sur l'ADN simple brin évitent le réappariment des deux brins, il se forme alors deux fourches de réplication et chaque fourche renferme une hélicase. L'hélicase se fixe donc à l'ADN et dénature, rompt les liasions hydorgènes de l'ADn. Au niveau de la fourche de réplication, l'hélicase est associée à une enzyme appelée primase, qui est une ARN polymérase, cette ARN polymérase synthétise l'amorce ARN, on retrouve égalemet les protéine SSB. Les topoisomérases interviennent également en amont de la fourche de réplication, enzyme qui coupe l'ADN et limite la formation de supertours positifs en amont de la fourche de réplication. Supers tours positifs (dans le sens des aiguilles d'une montre).
 
* Plusieurs réplicons chez les métazoaires (mécanisme plus complexe)
* 
 
## Réaction de polymérisation
*  Les ADN pol synthétisent de l'ADN
*  Une ADN pol a besoin d'une amorce pour synthétiser l'ADN (enrouge) amore hybridée sur l'ADN parental, la synthèse d'ADN repose sur la complémentarité des bases. 
*  Attaque du phosphate en position alpha du dntp entrant par un hydoxile des extremités 3' OH soit de l'amorce soit de l'ADN en cours de synthèse (attaque nucléophile). libération de pyrophosphate. Pour que cette réaction puisse se faire, nécéssite des ions magnésium (deux notamment), et forme des lisaisions éléctrostratiques avec chacun des trois phosphates du dNTP entrant, ce qui permet de positionner correctement les groupements phosphates en position alpha par rapport au 3' OH. MG2+ forme des liaison avec des acides aspartiques de l'ADN polymérase (permet de stabiliser le mg2+)
*  Ces ADN polymérases synthétisent de 5' --> 3'
  
### Fidélité de copie des ADN polymérases
* Principe de la séléction géométrique. dNTP entre dans le site qui est ouvert. Changement de conformation, suite à un mésappariement, le site catalytique ne peut plus se former car plus de changement de conformation, l'ADN ne peut plus être synthétisée. Suite à un mésappariement l'ADN se dénature et l'extremité 3' va se positionner au niveau du site catalytique de l'exonucléase, les AN sont dégradés à partir d'une extremité, 3'5' exonucléase.

### ADN pol bactériennes
* ADN pol processive : capable de renfermer ?? sans se dissocier.
* ADN polymérase III est l'enzyme impliquée dans la synthèse des deux brins d'ADN.
* Fragment d'OKASAKI = amorce + ADN
* Une ADN polymérase III qui synthétise les deux brins au niveau de la fourche, élimination des amorces ARN au niveau des extremité 5' des fragements d'OKASAKI va être réalisée par l'ADN polymérase I. ADN pol I s'amorce au niveau de l'xtremité 3'OH d'un fragment d'OKASAKI et grace à son activité exonucléase type 5'3' dégrade l'amorce ARN du fragment suivant. Elle remplace l'ARN qu'elle dégrade par de l'ADN grâce à son activé 5'3' polymérase.


GROS VIDE


## Terminaison
* Pourquoi y a t'il toujours plusieurs motifs ter? renforce l'efficacité du mécanisme de terminaison

## Les ADN polymérases eucaryote
* ADN pol alpha synthétise l'amorce de l'ADN (donc ARN), nécéssaire à la synthèse d'ADN et aussi un fragment d'ADN, c'est une enzyme très peu processive et fait beuacoup d'erreurs. 
* ADN pol delta: impliqué dans la réplication du brin tardif, (brin à synthèse discontinue)
* ADN pol epsilon : synthèse continue du brin.
* On regarde delta et epsilon, aucune d'entre elles n'a une activité 5' 3' exonucléase. 
* ADN subit des dommages en permanence, l'ADN doit être réparé et beaucoup d'enzymes sont impliqués dans ce mécanisme.
* L'ADN simple brin, mécanismes identiques, différence entre réplication chez les procaryotes et les eucaryotes : présence de deux enzymes, une pour chacun des deux brins néosynthétisés. 

### Élimination des amorces
* Aucune des ADN pol ne posséde d'activité 5'3 exonécluase, enzyme FEN1 possède une activité exonucléase 5'3'
* 5' phosphate innhibe l'action des exonucléases. comment FEN1 impliqué dans l'élimination de l'amorce va pouvoir la réaliser ?
* Modèle 1 ; L'ADNi est déplacé, il se dissocie du brin matrice, FEN1 va couper au niveau du branchement et grâce à son activité exonucléase. Intervention d'une DNA ligase. 
* Modèle 2 : RNAse H, va cliver l'amorce ARN et FEN 1 pourra éliminer le DNAi
* Puis DNA ligase ligature les fragments d'ADN.
* RNAase H impliqué car présence de cette enzyme.
  
### Terminaison
* Pas de terminateur, génome linéaire, réplication des extremités des réplicons linéaires.
* À chaque cycle de réplication  
* Racourcissement de l'ADN : uniquement au niveau du brin à synthèse discontinue, diminution à chaque cycle cellualire.
* Amorce ARN du ernier fragment d'okasaki ne peut pas être éliminé et l'ARN ne peut pas servir d'amorce
* Cellules germinales et cancéreuses : pas de racourcissement de l'extremité, on parle alors d'horloge moléculaire. 
* On isole des cellules somatiques d'un sujet jeune, elles se divisent rapidement, 
* Comment expliquer que ce racourcissement ne se produise pas dans les cellules germinales?
* A l'extremité des chromosomes : on trouve des télomères, ces télomères jouent un rôle important dans la stabilité du génome. Un ADN linéaire peut être à l'origine de recombinaison qui altère la structure du génome, donc structure aux extremités qui permettent d'éviter ces phénomènes de recombinaison. 
* Les télomères sont une structure particulière constitués de protéines et d'ADN = complexe nucléo protéique. L'ADN télomérique est constitué d'hexamères ou hexanucléotides répétés plusieurs fois. On distingue un brin riche en G et un riche en C, et l'extremité 3' des télomères est simple brin. Ces séquences sont très conservées au cours de l'évolution. Comment ces télomères vont être synthétisés? Télomérase = particule au niveau nucléo protéique, on a un ARN et une protéine. Ici on a une enzyme, la protéine présente dans cet télomérae = reverse transcriptase, ADN pol qui utilise de l'ARN comme matrice pour synthétiser de l'ADN. Cette télomérase renferme un ARN. Cet ARN sert de matrice à la reverse transcriptase pour pouvoir synthétiser les télomères. Cet ARN a une séquence complémentaire à 1,5 unité. **Revoir mode d'action de la télomérase!!**

## Les dommages de l'ADN
* désamination 
* Sous l'effet de rayonnement UV : formation de liaisons covalentes : ex dimères de thymine entrainant une déformation de la double hélice et qui peut provoquer un arret.  
* Quel est l'effet d'une méthylation d'un nucléotide
* Site AP = site dépourvu de bases azotés.
* Mismatch repair : réparer les erreurs (mésappariements) suite des erreurs de la réplication chez les bactéries.
* BER NER chez bactéries + eucaryote.

### Réversion du dommage
* Photoréactivation : la photolyase est une enzyme impliquée dans l'éliminatino des dimères de thymine. 
* Deux chromophores, cofacteur de la photolyase, est activé par une lumière bleue, ce chromophore va passer par une lumière bleue dans un état activé, il va céder son énergie au FADH-, ce FADH- va céder à son tour un éléctron au niveau du dimère de thymine et réarrangement éléctronique. Ce qui entraîne un réarrangement éléctronique au niveau des couches entrainant la rupture des deux liaisons covalentes entre ces deux thymines, il y aura relargage d'un élétron, suite au réarrangement éléctronique un éléctron sera relargué afin de reformer le FADH-. Chez les mammifères les dimères de thymine ne sont pas réparés comme cela
* Méthyltransférase, déméthylation et transfert du groupement méthyl de l'ADN sur une cystéine, l'enzyme est ensuite inactivée par l'ajout d'un groupement SH.
* Suite à la coupure de la caine de désoxyribose phosphate par une endonucléase