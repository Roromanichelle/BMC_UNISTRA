# La transcription des gènes
## I. Introduction

Transcriptome : 2 grandes familles?
* ARN codant (4 à 5%) et ARN non codant
* ARN précurseurs souvent
  
ARN polymérase est capable d'ouvrir la double hélice, elle catalyse la même réaction, mais contrairement à l'ADN polymérase, les ARN pol ne nécéssitent pas d'amorces, elles sont dépourvues d'activité 3-5' exonucléase. ARN toujours 5'-3'. Au cours de sa synthèse, l'ARN est hybridé en partie au brin matrice. 

# Initiation : 
Une ARN pol doit reconnaitre spécifiquement un promoteur au sein d'un généome de 4.10⁶ paires de bases (2000 promoteurs chez les bactéries). Def promoteur : Séquences cibles.  
CHez les bactéries tous les promoteurs ont la même architecture, chez les eucaryotes et notamment chez pol II pas d'architecture type du promoteur.

* Mise en évidence entre une protéine et l'ADN
  * Étude in vitro mettre en évidence que cette protéine se fixe sur le  motif que l'on a identifié, (fixation de la protéine sur le motif). Expérience de retard sur gel ou bande shift. On utilise des fragments d'ADN qui renferment des éléments de séquence spécifiques d'une protéine donnée. Les AN on les marque avec du P32, permet de visualiser les molécules. Le fragment d'ADN qui renferme le motif spécifique est marqué au P32. Témoin : ADN seul. L'esaie : ajouter une protéine pure ou un extrait nucléaire si eucaryote, si la protéine trouve sa cible sur le fragment d'ADN elle va se fixer, on travaille toujours en excès de protéines. On dépose le milieu réactionnel sur un gel, (se fait en conditions natives, le complexe ADN - protéine doit être maintenu en conditions natives, (ne doit pas se dissocier) ) La protéine retarde le déplacement de l'ADN. 
  * Puit 1 : seul l'ADN a été déposé, 2-3- 4 -5 en ajoutant des quantités croissantes de protéines
  * Puit 2 : Une bande migre beaucoup moins vite
  * Puit 3 : deuxième bande avec migration ralentie
  * Puit 5 : protéine en excès par rapport à l'ADN
  * Il faut trouver les conditions dans lesquelles l'intéraction est spécifique.
* Empreinte à la DNase I
  * Digestion DNase I : coupe sans spécificité
  * La fixationd e la protéine sur la molécule d'ADN protége la région sur laquelle est fixée la protéine est une protection qui lui permet de tenir une empreinte
  * Mise en place d'un témoin : permet d'identifier la région sur laquelle se fixe la protéine? **témoin**
  * Electrophorèse en condition dénaturante
* Intérférence
  * Diméthyl sulfate : méthyl ajouté sur la position 7 d'une guanine, si un élément de séquence permet la fixation d'une protéine et si cette séquence renferme un G : elle ne pourra plus se fixer. Modification du G par le diméthyl sulfate. La protéine va se fixer sur son motif uniquement si le G n'a pas été modifié donc l'ADN n'a pas été coupé.
  * Fragments d'ADN qui peuvent avoir des 
  * Témoin : ADN que l'on coupe et que l'on hydrolyse
  * ADN libre : Correspond à la molécule d'ADn sur lesquels la protéine n'a pas pu se fixer car ADN méthylé.
  **Si ce sont d'autres nucléotides qui sont essentiels à la fixation?** 
* immuoprécipitation de la chromatine ChIP
  * Objectifs : localiser au nieau du génome les sites de fixation d'une protéine
* Analyse des fragments d'ADN purifiés par Chlp